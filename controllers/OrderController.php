<?php
namespace app\controllers;

use Yii;
use app\models\Order;
use app\models\User;
use app\models\OrderProduct;
use app\components\Controller;
use yz\shoppingcart\ShoppingCart;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


class OrderController extends Controller {

    private $_model;

    public function actionIndex() {
        $model = $this->loadModel();
        $basket = new ShoppingCart();
        if(empty($basket->positions)){
            return $this->redirect(Yii::$app->request->referrer);
        }
        $user = User::findOne(Yii::$app->user->id);
        if($user){
            $model->user_id = $user->id;
            $model->address = $user->address;
            $model->phone = $user->phone;
            $model->email = $user->email;
            $model->country_id = $user->country_id;
            $model->city_id = $user->city_id;
            $model->region_id = $user->region_id;
            $model->_lastname = $user->surname;
            $model->_name = $user->name;
        }
        
        $model->price = floatval($basket->getSummPrise()['summ']);
        $model->createtime = time();
        $model->status = 0;

        if(!isset($_POST['Order'])){
            $model->country_id = 1;
            $model->region_id = 2;
            $model->accepted = 1;
        }
        
        
        
        $form = (isset($_POST['Order'])) ? $_POST['Order'] : null ;
        
        if(isset($form)){

            $model->attributes = $form;
//            $model->region_id = (isset($form['region_id']) && $form['region_id']) ? $form['region_id'] : null;
            $model->city_id = (isset($form['city_id']) && $form['city_id']) ? $form['city_id'] : null;

            if ($model->validate()) {
                
                $products = $basket->getPositions();
                //перещет общей цены с доставкой
                $supPrace = 0;
                
                foreach ($products as $value) {
                   $supPrace += $value->tradePrice * $value->quantity;
                }
                
                $model->price = $supPrace;
                
                $user = User::find()->where(['email'=>$_POST['Order']['email']])->one();
                if(!$user){
                    //зберігаєм користувача по email
                    $user = \app\models\UserCreate::create($model, $form);
                }
                $model->user_id = $user->id;
//                $model->address = $user->address;
//                $model->phone = $user->phone;
//                $model->email = $user->email;
//                $model->country_id = $user->country_id;
//                $model->city_id = $user->city_id;
//                $model->region_id = $user->region_id;
//                $model->_lastname = $user->surname;
//                $model->_name = $user->name;
                
                if ($model->save()){
                    foreach ($products as $value){
                        $order_product = new OrderProduct;
                        $order_product->product_id = $value->id;
                        $order_product->order_id = $model->id;
                        $order_product->amount = $value->quantity;
                        $order_product->save();
                    }
                    
//                    $model = Order::find()
//                        ->where(['order.id'=>$model->id])
//                        ->select('order.*, (order.price + delivery.price) AS sumPrice')
//                        ->join('INNER JOIN', 'delivery', 'delivery_id = delivery.id')
//                        ->one();
                    
                    $model = \app\models\Order::find()
                        ->where(['order.id'=>$model->id])
                        ->select("order.*, CASE   
                                    WHEN `delivery`.`id` IN (11,10) THEN `order`.`price`
                                    ELSE `order`.`price` + `delivery`.`price` END  AS sumPrice
                                ")
                        ->join('INNER JOIN', 'delivery', 'delivery_id = delivery.id')
                        ->one();
                    
                    
//                    $model->phone = $_POST['Order']['phone'];
//                    $model->email = $_POST['Order']['email'];
//                    $model->_name  = $_POST['Order']['_name'];
//                    $adress  = $_POST['Order']['address'];
//                    $region = \app\models\Region::Name($_POST['Order']['region_id']);
//                    $city = \app\models\City::Name($_POST['Order']['city_id']);
//                    $country = \app\models\Country::Name($form['country_id']);
                    
//                    sales@partsnb.ru - м. Бухарестская, Курьером до адреса (СПБ), Почтой России, Почтой России первым классом, EMS по России
//                    salesk@partsnb.ru - м. Комендантский пр, Доставка службой IML по России 
//                    saless@partsnb.ru -, м. Садовая 
                    
                    $mailMeneger = Order::getMailMeneger($model->delivery_id);
                    
                    
//                    return $this->render('_mail', ['model'=>$model, 'region'=>$region, 'basket'=>$basket]);
//                    $this->sendMail($model->email, $title, $this->renderPartial('_mail', ['model'=>$model, 'region'=>$region, 'basket'=>$basket]), $mailMeneger);
//                    $this->sendMail($mailMeneger,  Yii::$app->name, $this->renderPartial('_mail', ['model'=>$model, 'region'=>$region, 'basket'=>$basket]), $mailMeneger);
                    $title = "№ заказа {$model->id}, {$model->fullName}, {$model->sumPrice} руб";
                    
                    $this->sentMailYandex($mailMeneger, $title, $this->renderPartial('_mail', ['model'=>$model, 'city'=>$model->city_name, 'basket'=>$basket]), $mailMeneger);
                    $this->sentMailYandex($model->email, $title, $this->renderPartial('_mail', ['model'=>$model, 'city'=>$model->city_name, 'basket'=>$basket]), $mailMeneger);
                    
                    $basket->removeAll();//очищаем корзину
                    Yii::$app->getSession()->setFlash('info','Ваш заказ оформлен');
                    $this->redirect(Url::to(['/user/order','id'=>$model->id]));
                }
            }
        }
        return $this->render('index', [
                'model' => $model, 
                'basket'=>$basket,
                'payment'=> \app\models\Payment::getListForOrder(),
                'deliveryPrice'=>\app\models\Delivery::getDeliveryPrice(),
//                'city'=> \app\models\Region::getRegion(1),
                'city'=> \app\models\City::getCitysForOrder(1),
                'radioDescription'=> ArrayHelper::map(\app\models\Delivery::find()->where(['is_delete'=>0, 'status'=>1])->all(),'id', 'description')
            ]);
    }
    
    // KASSA

    public function actionKassaok() {

        $model = $this->loadModel();
        $basket = new ShoppingCart();
        if(empty($basket->positions)){
            return $this->redirect(Yii::$app->request->referrer);
        }
        $user = User::findOne(Yii::$app->user->id);
        if($user){
            $model->user_id = $user->id;
            $model->address = $user->address;
            $model->phone = $user->phone;
            $model->email = $user->email;
            $model->country_id = $user->country_id;
            $model->city_id = $user->city_id;
            $model->region_id = $user->region_id;
            $model->_lastname = $user->surname;
            $model->_name = $user->name;
        }
        
        $model->price = floatval($basket->getSummPrise()['summ']);
        $model->createtime = time();
        $model->status = 1;

        if(!isset($_GET['orderdata'])){
            $model->country_id = 1;
            $model->region_id = 2;
            $model->accepted = 1;
        }

        $orderData = urldecode($_GET['orderdata']);

        parse_str($orderData,$orderDataParsed);

        $form = (isset($_GET['orderdata'])) ? $orderDataParsed['Order'] : null ;
        
        if(isset($form)){

            $model->attributes = $form;
            $model->city_id = (isset($form['city_id']) && $form['city_id']) ? $form['city_id'] : null;

            if ($model->validate()) {
                
                $products = $basket->getPositions();
                //перещет общей цены с доставкой
                $supPrace = 0;
                
                foreach ($products as $value) {
                   $supPrace += $value->tradePrice * $value->quantity;
                }
                
                $model->price = $supPrace;
                
                // return(print_r($form,true));

                $user = User::find()->where(['email'=>$form['email']])->one();
                
                if(!$user){
                    $user = \app\models\UserCreate::create($model, $form);
                }
                $model->user_id = $user->id;

                if ($model->save()){

                    foreach ($products as $value){
                        $order_product = new OrderProduct;
                        $order_product->product_id = $value->id;
                        $order_product->order_id = $model->id;
                        $order_product->amount = $value->quantity;
                        $order_product->save();
                    }
                    
//                    $model = Order::find()
//                        ->where(['order.id'=>$model->id])
//                        ->select('order.*, (order.price + delivery.price) AS sumPrice')
//                        ->join('INNER JOIN', 'delivery', 'delivery_id = delivery.id')
//                        ->one();
                    
                    $model = \app\models\Order::find()
                        ->where(['order.id'=>$model->id])
                        ->select("order.*, CASE   
                                    WHEN `delivery`.`id` IN (11,10) THEN `order`.`price`
                                    ELSE `order`.`price` + `delivery`.`price` END  AS sumPrice
                                ")
                        ->join('INNER JOIN', 'delivery', 'delivery_id = delivery.id')
                        ->one();
                    
                    
//                    $model->phone = $_POST['Order']['phone'];
//                    $model->email = $_POST['Order']['email'];
//                    $model->_name  = $_POST['Order']['_name'];
//                    $adress  = $_POST['Order']['address'];
//                    $region = \app\models\Region::Name($_POST['Order']['region_id']);
//                    $city = \app\models\City::Name($_POST['Order']['city_id']);
//                    $country = \app\models\Country::Name($form['country_id']);
                    
//                    sales@partsnb.ru - м. Бухарестская, Курьером до адреса (СПБ), Почтой России, Почтой России первым классом, EMS по России
//                    salesk@partsnb.ru - м. Комендантский пр, Доставка службой IML по России 
//                    saless@partsnb.ru -, м. Садовая 
                    
                    $mailMeneger = Order::getMailMeneger($model->delivery_id);
                    
                    
//                    return $this->render('_mail', ['model'=>$model, 'region'=>$region, 'basket'=>$basket]);
//                    $this->sendMail($model->email, $title, $this->renderPartial('_mail', ['model'=>$model, 'region'=>$region, 'basket'=>$basket]), $mailMeneger);
//                    $this->sendMail($mailMeneger,  Yii::$app->name, $this->renderPartial('_mail', ['model'=>$model, 'region'=>$region, 'basket'=>$basket]), $mailMeneger);
                    $title = "№ заказа {$model->id}, {$model->fullName}, {$model->sumPrice} руб";
                    
                    $this->sentMailYandex($mailMeneger, $title, $this->renderPartial('_mail', ['model'=>$model, 'city'=>$model->city_name, 'basket'=>$basket]), $mailMeneger);
                    $this->sentMailYandex($model->email, $title, $this->renderPartial('_mail', ['model'=>$model, 'city'=>$model->city_name, 'basket'=>$basket]), $mailMeneger);
                    
                    $basket->removeAll();//очищаем корзину
                    Yii::$app->getSession()->setFlash('info','Ваш заказ оформлен');
                    $this->redirect(Url::to(['/user/order','id'=>$model->id]));
                }
            }
        }
        return $this->render('index', [
                'model' => $model, 
                'basket'=>$basket,
                'payment'=> \app\models\Payment::getListForOrder(),
                'deliveryPrice'=>\app\models\Delivery::getDeliveryPrice(),
//                'city'=> \app\models\Region::getRegion(1),
                'city'=> \app\models\City::getCitysForOrder(1),
                'radioDescription'=> ArrayHelper::map(\app\models\Delivery::find()->where(['is_delete'=>0, 'status'=>1])->all(),'id', 'description')
            ]);
    }

    // END KASSA

    public function loadModel() {
        if ($this->_model === null) {
            if (isset($_GET['id'])){
                    $this->_model = Order::find()
                        ->where(['order.id'=>$_GET['id']])
                        ->select('order.*, (order.price + delivery.price) AS sumPrice')
                        ->join('INNER JOIN', 'delivery', 'delivery_id = delivery.id')
                        ->one();
                if ($this->_model === null)
                    throw new NotFoundHttpException('Товар не определен', 404);
            }else
                $this->_model = new Order();
        }
        return $this->_model;
    }

}
