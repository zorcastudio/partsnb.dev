<?php

namespace app\controllers;
use app\components\Controller;
use app\components\GoogleMarket;
use Yii;
use yii\helpers\Url;


class GooglemarketController extends Controller {
    
    public function actionFile(){
        ini_set("memory_limit","2000M");
        ignore_user_abort(true);
        set_time_limit(0);
        $public_path = Yii::$app->params['public_path'];
        $log = new \app\modules\admin\models\LogsSearch();
        $log->createtime = time();
        
        $XML = GoogleMarket::getXML($log);
        
        $XML->save(Yii::$app->basePath . "/$public_path/products_GM.xml");
        
        //header("Content-Type: application/force-download");
        //header("Content-Disposition: attachment; filename=products_GM.xml");
        //echo $XML->saveXML();
        
        $XML->save(Yii::$app->basePath . "/$public_path/history/products_GM_".date("Y_m_d_H_i_s", $log->createtime).".xml");

        $log->type = 1;
        $log->finishtime = time();
        $log->save();
    }
    
    public function actionGet(){
        ini_set("memory_limit","20000M");
        ignore_user_abort(true);
        set_time_limit(0);
        
        $log = new \app\modules\admin\models\LogsSearch();
        $log->createtime = time();

        $XML = GoogleMarket::getXML($log);
        header("Content-type: text/xml; charset=utf-8");
        echo $XML->saveXML();
//        $XML->asXML(Yii::$app->basePath . "/$domen/products_GM_".date("Y.m.d H:i:s", $log->createtime).".xml");

        $log->type = 1;
        $log->finishtime = time();
        $log->save();
        Yii::$app->end();
    }
    
    protected function disconnect_continue_processing($time_limit = null) {
        ignore_user_abort(true);
        session_write_close();
        set_time_limit((int) $time_limit); //defaults to no limit
        while (ob_get_level() > 1) {//only keep the last buffer if nested
            ob_end_flush();
        }
        $last_buffer = ob_get_level();
        $length = $last_buffer ? ob_get_length() : 0;
        header("Content-Length: $length");
        header('Connection: close');
        if ($last_buffer) {
            ob_end_flush();
        }
        flush();
//        fastcgi_finish_request();
    }
}
