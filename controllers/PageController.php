<?php

namespace app\controllers;

use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;


use app\models\LoginForm;
use app\models\Product;
use app\components\Controller;
use app\models\Page;

class PageController extends Controller {

    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionIndex() {
        return $this->render('index', ['popular' => Product::random()]);
    }
    
    public function actionView() {
        return $this->render('view',['model'=>$this->loadModel()]);
    }
    
    
    public function loadModel() {
        if($url = Yii::$app->request->pathInfo){
            if(preg_match("/\/(.*)/i", $url, $matches)){
                $url = $matches[1];
            }
            $model = Page::find()
                    ->where(['name_lat'=> $url])
                    ->andWhere(['is_delete'=>0])
                    ->one();
            if($model)
                return  $model;
            else
                throw new NotFoundHttpException('Товар не определен', 404);
        }
    }
}
