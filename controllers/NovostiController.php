<?php

namespace app\controllers;

use yii\web\NotFoundHttpException;
use Yii;

use app\models\Novosti;
use app\components\Controller;


class NovostiController extends Controller {
 
    public function actionIndex() {
        $model = Novosti::find()->where(['is_published'=>1])->all();
        return $this->render('index', ['model' => $model]);
    }
    
    public function actionView() {
        return $this->render('view',['model'=>$this->loadModel()]);
    }
    
    
    public function loadModel() {
        if($url = Yii::$app->request->get('url')){
            $model = Novosti::find()
                    ->where(['name_lat'=> $url])
                    ->andWhere(['is_published'=>1])
                    ->one();
            if($model)
                return  $model;
            else
                throw new NotFoundHttpException('Новости не найдены', 404);
        }
    }
}
