<?php

namespace app\controllers;

use app\components\Dumphper;
use yii\base\Exception;
use yii\data\Pagination;
use Yii;
use yii\helpers\VarDumper;
use yii\data\ArrayDataProvider;
use app\models\Product;
use app\components\Controller;
use app\models\User;
use app\models\Category;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use app\models\ProductPnSearch;
use app\models\SearchModel;
use app\models\Producer;
use yii\data\ActiveDataProvider;
use app\models\Serie;
use app\models\Model;
use app\models\FilterValue;
use app\models\Page;

class ProductController extends Controller {
    public $_model;
    public $_category;
    public $_filter;

    public function actionIndex() {
        $this->_category = Category::findOne(['name_lat'=>$_GET['category']]);
        if ($this->_category === null)
                throw new NotFoundHttpException('Страница не найдена', 404);
 
        $model = new Product();
        
        $data = [   'category' => $this->_category,
                    'producer' => Producer::getProducerInCategory($this->_category),
                    'dataProvider' => $model->search($this->_category),
                ];
        
        if (Yii::$app->request->isAjax){
            $this->renderPartial('produser', $data);
            Yii::$app->end();
        } else
            return $this->render('produser', $data);
    }

    public function actionProducer(){
        $this->_category = Category::findOne(['name_lat'=>$_GET['category']]);
        $request = Yii::$app->request->get();
        
        $producer = Producer::findOne(['name_lat'=>$_GET['producer']]);
        if ($producer === null)
            throw new NotFoundHttpException('Товар не определен', 404);
        
        $serie = Serie::getSerie($producer->id, $this->_category);
                
        $query = Product::find();
        $query->select(['product.*','page.text AS discount', '(`in_stock1` + `in_stock2` + `in_stock3`) > 0 AS in_stock'])
                ->innerJoin('category c', 'c.id = product.category_id')
                ->innerJoin ('producer pr', 'product.producer_id = pr.id')
                ->leftJoin('page', 'page.id = 16')
                ->where([
                    'product.is_delete' => 0,
                    'product.status' => 1,
                    'product.is_published' => 1,
                    'product._is_problem' => 0,
                    'c.name_lat' => $request['category'],
                    'pr.name_lat' => $request['producer'],
                    'c.is_published' => 1,
                    'c.is_delete' => 0,
                ])
                ->groupBy('product.id')
                ->orderBy('in_stock DESC, sort, article ASC');
        
        $article = FilterValue::getArticle();
        if(!empty($article) && $article !== TRUE){
            $query->andWhere("product.article IN ({$article})");
        }elseif($article == '0'){
            $query->andWhere("product.article IN (000000)");
        }
        
        $filter = Product::getFilterArray();
        $getH1 = Product::getH1($filter, $this->_category, $producer);
        $title = Product::getTitle($filter, $this->_category, $producer);
        $descripription = Product::getDescripription($filter, $this->_category, $producer);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);
        
        $data = [
                'category' => $this->_category ? $this->_category : new \app\models\Category,
                'producer' =>$producer,
                'serie' =>$serie,
                'h1' =>$getH1,
                'title' =>$title,
                'descripription' =>$descripription,
                'dataProvider' => $dataProvider
        ];
        if (Yii::$app->request->isAjax){
            $this->renderPartial('produser_product', $data);
            Yii::$app->end();
        } else {
            return $this->render('produser_product', $data);
        }
        
    }
    
    public function actionSerie() {
        $this->_category = Category::findOne(['name_lat'=>$_GET['category']]);
        $category = $this->_category ? $this->_category : new \app\models\Category();
        $producer = Producer::findOne(['name_lat'=>$_GET['producer']]);
        $serie = $_GET['serie'];
        
        $query = Product::find();
        $query->select(['product.*','page.text AS discount', '(`in_stock1` + `in_stock2` + `in_stock3`) > 0 AS in_stock'])
                ->innerJoin('category c', 'c.id = product.category_id')
                ->innerJoin ('producer pr', 'product.producer_id = pr.id')
                ->innerJoin('serie s', 's.id = product.serie_id')
                ->leftJoin('page', 'page.id = 16')
                ->where([
                    'product.is_delete' => 0,
                    'product.status' => 1,
                    'product.is_published' => 1,
                    'product._is_problem' => 0,
                    'c.name_lat' => $_GET['category'],
                    'pr.name_lat' => $_GET['producer'],
                    's.name_lat' => $serie,
                    'c.is_published' => 1,
                    'c.is_delete' => 0,
                ])
                ->groupBy('product.id')
                ->orderBy('in_stock DESC, sort, article ASC');
        
        $serie = null;
        if($producer){
            $serie = Serie::findOne([
                    'name_lat'=>$_GET['serie'],
                    'serie.producer_id'=>$producer->id
                ]);
        };
        
        $article = FilterValue::getArticle();
        $filter = Product::getFilterArray();
        $getH1 = Product::getH1($filter, $this->_category, $producer, $serie);
        $title = Product::getTitle($filter, $this->_category, $producer, $serie);
        $descripription = Product::getDescripription($filter, $this->_category, $producer, $serie);
        
        if(!empty($article) && $article !== TRUE){
            $query->andWhere("product.article IN ({$article})");
        }elseif($article == '0'){
            $query->andWhere("product.article IN (000000)");
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        
        $data = [   
                'category' => $category,
                'producer' =>$producer,
                'serie' =>$serie,
                'h1' =>$getH1,
                'title' =>$title,
                'descripription' =>$descripription,
                'dataProvider' => $dataProvider
            ];
        
        if (Yii::$app->request->isAjax){
            $this->renderPartial('serie', $data);
            Yii::$app->end();
        } else{
            return $this->render('serie', $data);
        }
        
    }
    
    public function actionModel(){
        $this->_category = Category::findOne($_GET['category']);
        
        $articles = \app\models\ModelCompatible::getModelCompatible($_GET['model']);
        
        $rawData = ($this->_category)  
                ? Product::find()
                    ->where(['producer_id' => $_GET['prod'], 'category_id'=>$_GET['cat'], "is_delete"=>0, 'is_published'=>1, "status"=>1])
                    ->andWhere('article IN (' . $articles. ')')
                    ->orderBy($this->_category->getSort())
                    ->all()
                : Product::find()->where(['producer_id' => $_GET['prod'], "is_delete"=>0, 'is_published'=>1, "status"=>1])->all();
        
    
        $active_f = Product::getActiveFilter();
        $result = [];
        foreach ($rawData as $value) {
            if ($item = $value->isActiveFilterProduct($active_f))
                $result[] = $item;
        }
        
        $count_all = count($rawData);
        
        $dataProvider = new ArrayDataProvider([
            'allModels' => $result,
            'pagination' => array(
                'pageSize' => $this->_category ? ($this->_category->_list == 1 ? 12 : 10) : null,
//                'pageVar'=>'page',
            ),
        ]);
        $data =[
            'category' => $this->_category ? $this->_category : new \app\models\Category,
            'dataProvider' => $dataProvider,
            "view" => $this->_category ? ($this->_category->_list == 1 ? '_view' : '_block') : '_view',
            "active_f" => Product::getActiveFilter(),
            'count_all' => $count_all,
        ];
        
        if (Yii::$app->request->isAjax){
            $this->renderPartial('index', $data);
            Yii::$app->end();
        } else{
            return $this->render('index', $data);
        }
        
        
    }
    
    public function actionView() {
        $model = $this->loadModel();

        $model->h2_compatible_model = $model->getH2forModelSearch();
        $backet = new \yz\shoppingcart\ShoppingCart();
        $count = isset($backet->positions[$model->id]) ? $backet->positions[$model->id]['quantity'] : 1;
        $properties = $model->propertiesList;
        $properties +=['Гарантия'=>$model->warranty." мес."];
       
        $image = [];
        $image[]=[
            'meta'=>$model->name,
            'url'=> preg_replace("/_150x150/u", '', $model->logo)
        ];
        foreach ($model->image as $value) {
            $image[]=[
                'meta'=>$value->meta,
                'url'=>preg_replace("/_150x150/u", '', $value->url),
            ];
        }
        if($model->producer && $model->model){
//            $serch = preg_match('/^(.*?) ('.$model->producer->name.' .*)?$/i', $model->name, $sought);
//            $title = $serch ? $sought[1]." ".$sought[2]." в интернет-магазине г. Санкт-Петербург" : false;
            $title = $model->name. " в интернет-магазине PartsNB"; //| Запчасти для ноутбуков: цены, характеристики, совместимость
        }else{
            $title = false;
        }
        
        if($model->producer && $model->model){
            if(in_array($model->category->name_lat, ['petli', 'shlejfy-dlya-matrits']))
                $nameKeywords = $model->name;
            else
                $nameKeywords = $model->category->name . " для ноутбука ". $model->model->name. ", " .$model->name;
            
            $keywords =  mb_strtolower($nameKeywords, 'UTF-8'); //, $nameKeywords санкт-петербург, ". $model->name
            $description = "Для покупки товара ".mb_strtolower($model->name, 'UTF-8')." позвоните нам +7 (812) 927-06-76 или оформите заказ в интернет-магазине";
        }else{
            $keywords = $description = false; 
        }
        
//        vd($model->modelChildren, false);
//        vd(SearchModel::getModelSortArray($model), false);
        $delivery_stock = Product::getAvailableStock([$model]);
        return $this->render('view', [
            'model' => $model,
            'image' => $image,
            'listModel' => Model::getListModelArticles($model->article),
            'keywords' => $keywords,
            'title' => $title,
            'description' => $description,
            'properties' => $properties,
//            'otherProducer' => $model->otherProducer,
            'count' => $count,
            'modelForSearch'=> Product::getModelSortArray($model->article),// ,$model->modelChildren
//            'pnForSearch'=> ProductPnSearch::getPNArray($model->article),
            'pnModel'=> Model::getPnMOdel($model->article),
            'delivery_stock' => $delivery_stock,
            'delivery_rules' => Product::getAvailableRules()
        ]);
    }

    public function actionSearch() {
        
        $request = trim(Yii::$app->request->get('search'));
       
        $full = false; //флаг который обозначает чтобы открыть значения поиска в категории
        if ($request) {
            $result = Product::getSqlForSearch($request);
            if(isset($result['category']) && !is_array($result['category']['key'])){
                $dataProvider = new  ActiveDataProvider([
                    'query' => $result[0]['sql'],
                    'pagination' => array(
                        'pageSize' => 12,
                    ),
                ]);
                
                if(isset($_GET['t'])){
                    $result[0]['sql']->orderBy('error');
                }
                $full = TRUE;
            }else{
                $category = $result[0]['sql'];
                $category->groupBy('category_id')
                        ->select("category.*, COUNT( * ) AS _count")
                        ->joinWith('category');
            }
            $_category = (isset($category)) ? $category->all() : false;
                   
            $data = [
                "search"        => trim($result['request']), 
                "category"      => $_category, 
                "producer_id"   => (isset($result['producer']['id']) && $result['producer']['id'] != '' ) ? "p={$result['producer']['id']}" : NULL, 
                "dataProvider"  => isset($dataProvider) ? $dataProvider     : false, 
                'full'          =>$full
            ];
              
            
            if($data['dataProvider'] == FALSE && $data['category'] == false){
                return $this->render('model_empty_search', array(
                    'category' => NULL,
                    'producer' =>NULL,
                    'serie' => NULL,
                    'model' => $_GET['search']
                ));
            }
            
            if($data['dataProvider'] && $data['category'] == false && !$data['dataProvider']->query->exists()){
                    return $this->render('model_empty_search', array(
                        'category' => NULL,
                        'producer' =>NULL,
                        'serie' => NULL,
                        'model' => $_GET['search']
                    ));
            }
            
                
            if (Yii::$app->request->isAjax){
                $this->renderPartial('search', $data);
                Yii::$app->end();
            } else{
                return $this->render('search', $data);
            }
        } 
    }


    //brand path
    public function actionParams_search($producer=null, $serie=null, $model=null, $category=null) {

        $items_name = ['category' => 'Категории', 'serie' => 'Серии', 'model' => 'Модели', 'producer' => 'Производители'];
        $fields = ['producer'];

        if($producer) {
            array_unshift($fields, 'serie');
        }
        if($serie) {
            array_unshift($fields, 'model');
        }
        if($model) {
            array_unshift($fields, 'category');
        }
        $arr = [];
        foreach($fields AS $field) {
            if($$field) {
                $key = 'name_lat';
                /*if($field == 'model') {
                    $key = 'name';
                }*/
                $arr[$field] = ['key' => $key, 'value' =>$$field];
            }
        }

        $params = (new Product)->getSelectParams(
            $fields,
            $arr
        );

        if($params['count'] == 0) {
            throw new NotFoundHttpException('Товар не определен', 404);
        }

        $data = [];
        krsort($fields);
        $items_key = array_pop($fields);
        $data['items'] = $params[$items_key];
        foreach($fields AS $field) {
            $data[$field] = $params[$field][0];
        }
        $data['fields'] = $fields;
        if(isset($fields[1])) {
            $data['current_item'] = $data[$fields[1]];
        }
        else {
            $current_item = new \stdClass();
            $current_item->description = '';
            try {
                $current_item->description = Page::find()->where('name_lat=:name_lat', [':name_lat'=>Yii::$app->request->getPathInfo()])->one()->text;
            } catch(yii\base\ErrorException $e) {

            }
            $data['current_item'] = $current_item;
        }
        $data['breadcrumbs'] = $this->getBreadcrumbsBrandPath($fields, $data);
        $data['title'] = $this->getTitleBrandPath($fields, $data);
        $data['description'] = $this->getDescriptionBrandPath($fields, $data);
        $data['items_name'] = $items_name[$items_key];

        return $this->render('brand_path', $data);
    }

    private function getTitleBrandPath($fields, $data) {
        $count = count($fields);
        $title_array = [
            'producer' => ['title' => 'Запасные части для ноутбуков и нетбуков ', 'keys' => ['producer']],
            'serie' => ['title' => 'Запасные части для ноутбуков и нетбуков ', 'keys' => ['producer', 'serie']],
            'model' => ['title' => 'Запасные части для ноутбука ', 'keys' => ['producer', 'model']],
        ];

        $title = 'Запасные части для ноутбуков и нетбуков ';

        if($count > 0 ) {
            $title = $title_array[$fields[1]]['title'];
            foreach($title_array[$fields[1]]['keys'] AS $item) {
                $title .= $data[$item]['name'].' ';
            }
        }

        return $title;
    }

    private function getDescriptionBrandPath($fields, $data) {
        $count = count($fields);
        $description = 'В нашем интернет магазине partsnb.ru вы можете купить запчасти для ноутбука %sпо самой лучше цене в России. Самовывоз, доставка курьером по СПб, Почтой россии, EMS и IML по России';
        $description_array = [
            'producer' => ['title' => $description, 'keys' => ['producer']],
            'serie' => ['title' => $description, 'keys' => ['producer', 'serie']],
            'model' => ['title' => $description, 'keys' => ['producer', 'model']],
        ];
        $description_info = '';
        if($count > 0 ) {
            foreach($description_array[$fields[1]]['keys'] AS $item) {
                $description_info .= $data[$item]['name'].' ';
            }
        }

        return sprintf($description, $description_info);
    }

    private function getBreadcrumbsBrandPath($fields, $data) {
        $i = 1;
        $path = '/brands';
        $count = count($fields);
        if($count == 0) {
            $breadcrumbs = ['Производители'];
        }
        else {
            $breadcrumbs = [['label' => 'Производители', 'url' => $path]];
        }
        foreach($fields AS $key => $item) {
            if($count == $i) {
                $breadcrumbs[] = $data[$item]['name'];
            }
            else {
                $path = $path.'/'.$data[$item]['name_lat'];
                $breadcrumbs[] = ['label' => $data[$item]['name'], 'url' => $path];
            }
            $i++;
        }

        return $breadcrumbs;
    }

    public function actionModel_search($brand, $series, $model) {
        $_brand = Producer::find()->where('name_lat=:name_lat', [':name_lat'=>$brand])->one();
        $_series = (new Serie)->getSeriesByParams($series, $_brand['id']);
        $_model = (new Model)->findModels($_brand, $_series, $model);

        $query = Product::find()
            ->where([
                'is_delete'=>0,
                'status'=>1,
                'is_published'=>1,
                '_is_problem'=>0,
                'model_id'=>($_model ? $_model['id'] : ''),
            ])
            ->orderBy('category_id');

        /*if (!$query->exists()){
            return $this->render('model_empty_search', array(
                'producer' =>$producer,
                'serie' =>$_series,
                'model' => $model
            ));
        }*/

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);

        return $this->render('produser_product_brands', array(
            'producer' =>$_brand,
            'series' =>$_series,
            'model' =>$_model,
            'dataProvider' => $dataProvider,
        ));

    }

    public function actionModel_serch(){
        $this->_category = Category::findOne(['name_lat'=>$_GET['category']]);
        $producer = Producer::findOne(['name_lat'=>$_GET['producer']]);
        
        $query = Model::find();
        $query->where(['not', ['serie_id'=>Null]]);
        $query->andWhere(['like', 'name', "%".$_GET['model'], false]);
        $query->orWhere(['name_lat' => $_GET['model']]);
        $query->select('id');
        $query->asArray();
        
        if($producer){
            $serie = Serie::findOne([
                    'name_lat'=>$_GET['serie'],
                    'producer_id'=>$producer->id
                ]);
            $query->andWhere(['producer_id'=>$producer->id]);
            if($serie){
                $query->andWhere(['serie_id'=>$serie->id]);
            }
        };
            
        if (!$query->exists()){
            return $this->render('model_empty_search', array(
                    'category' => $this->_category,
                    'producer' =>$producer,
                    'serie' =>$serie,
                    'model' => $_GET['model']
                ));
        }
        
        
        
        $model = $query->all();
        $result = [];
        foreach ($model as $value) {
            $result[] = $value['id'];
        }
        
        $query = Product::find();
        $query  ->where([
                    'is_delete'=>0,
                    'status'=>1,
                    'is_published'=>1,
                    '_is_problem'=>0,
                    'category_id'=>$this->_category->id, 
                    'model_id'=>$result,
                ]);
//                ->select('id, price, name, logo, category_id, producer_id, name_lat, serie_id');

        if(isset($_GET['t'])){
            vd($query->orderBy('error')->all(), false);
        } 
        
        if (!$query->exists())
            throw new NotFoundHttpException('Товар не определен', 404);
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);

        return $this->render('produser_product', array(
                    'category' => $this->_category ? $this->_category : new \app\models\Category,
                    'producer' =>$producer,
                    'model' => Model::findOne($model[0]['id']),
                    'serie' =>[],
                    'dataProvider' => $dataProvider,
        ));
    }

    public function actionProblem(){
        
        $query = Product::find();
        $query  ->where([
                    'is_delete'=>0,
                    'status'=>1,
                    'is_published'=>1,
            
//                    '_is_problem'=>0,
//                    'category_id'=>$this->_category->id, 
//                    'producer_id'=>$producer->id, 
//                    'article'=>$article
                ]);
        if(isset($_GET['_is_problem'])){
            $query->andWhere(['not',['_is_problem'=>0]]);
        }else{
            $query->andWhere(['_is_problem'=>0]);
        }
        
        if(isset($_GET['producer_id'])){
            $query->andWhere(['producer_id'=>null]);
        }else{
            $query->andWhere(['not',['producer_id'=>0]]);
        }
        
        if(isset($_GET['model_id'])){
            $query->andWhere(['model_id'=>null]);
        }else{
            $query->andWhere(['not',['model_id'=>0]]);
        }
        
        if(isset($_GET['serie_id'])){
            $query->andWhere(['serie_id'=>null]);
        }else{
            $query->andWhere(['not',['serie_id'=>0]]);
        }
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 16,
            ],
        ]);
        
        return $this->render('_problem', array(
                    'dataProvider' => $dataProvider,
        ));
    }
    
    public function actionTest(){
//        $this->layout = 'empty';
        $query = \app\models\ProductCreate::find()
                ->select('id, logo, name')
                ->where(['between', 'id', 0, 100000]);
        
        foreach ($query->all() as $value) {
            $url = "/uploadfile/images/product/resized/$value[logo]";
            if(!file_exists(Yii::$app->basePath.'/web'.$url)){
                vd($value['name'], false);
                echo "<br/>";
//               if(preg_match("/_(.*)/i", $value['logo'],  $matches)){
//                   $replace = preg_replace("/_.*/", '', $value['logo']);
//                   $value->logo = $replace."_150x150.jpg";
//                   vd($value->logo, false);
//                   echo "<br/>";
//                   $value->save();
//               };
            }
        }
        die("<br/>end");
        return $this->render('test', array(
            'product' => $query->all(),
        ));
        }
    
    public function loadModel() {
        if ($this->_model === null) {
            if (isset($_GET['name'])) {
                $search = preg_match('/-detail/i', $_GET['name'], $subject);
                $url = preg_replace("/-detail/i", '', $_GET['name']);
                $this->_model = Product::find()
                        ->select(["product.*, REPLACE (c.description_product, '{name}', CONCAT(pr.name, ' ', m.name)) AS description_product, page.text AS _spetc_price"])
                        ->where(['product.name_lat' => $url, 'product.is_delete'=>0, 'product.is_published'=>1])
                        ->leftJoin('category c', "product.category_id=c.id")
                        ->leftJoin('producer pr', "product.producer_id = pr.id")
                        ->leftJoin('model m', "product.model_id = m.id")
                        ->leftJoin('page', "page.id = 27")
                        ->one();
            }
            if ($this->_model === null){
                // Ищем товар если он был удален но его ссылка сохранена в другом такомже
                $this->_model = Product::find()
                        ->where(["product.original_name_lat" => $_GET['name'] , 'product.is_delete'=>0, 'product.is_published'=>1])
                        ->one();
                $this->redirect($this->_model->getUrl(), 301)->send();
            }
            if ($this->_model === null){
                throw new NotFoundHttpException('Товар не найден', 404);
            }
        }
        return $this->_model;
    }

}
