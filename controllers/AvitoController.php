<?php

namespace app\controllers;
use app\components\Controller;
use yii\helpers\ArrayHelper;
use app\models\Category;
use app\models\Product;
use yii\helpers\Url;
use app\components\AvitoMarket;
use Yii;


class AvitoController extends Controller {
    
    public function actionFile(){
        ini_set("memory_limit","2000M");
        ignore_user_abort(true);
        set_time_limit(0);
        
        $log = new \app\modules\admin\models\LogsSearch();
        $log->createtime = time();
        $public_path = Yii::$app->params['public_path'];
        
//        $XML = AvitoMarket::getXML($log);
        $XML = \app\components\AvitoMarket::getXML($log);

        
//        header("Content-Type: application/force-download");
//        header("Content-Disposition: attachment; filename=avito.yml");
//        echo $XML->saveXML();
        $XML->asXML(Yii::$app->basePath . "/{$public_path}/avito.yml");
        $XML->asXML(Yii::$app->basePath . "/{$public_path}/history/avito_".date("Y_m_d_H_i_s", $log->createtime).".yml");

        $log->type = 4;
        $log->finishtime = time();
        $log->save();
    }
    
    public function actionGet(){
        ini_set("memory_limit","20000M");
        ignore_user_abort(true);
        set_time_limit(0);
        
        $log = new \app\modules\admin\models\LogsSearch();
        $log->createtime = time();

//        $XML = AvitoMarket::getXML($log);
        $XML = \app\components\AvitoMarket::getXML($log);

        header("Content-Type: text/plain");    
//        header("Content-Type: application/octet-stream");
//        header("Content-Type: application/force-download");    
//        header("Content-Disposition: attachment; filename=avito.yml");
        $public_path = Yii::$app->params['public_path'];
        print_r($XML->asXML());
        $XML->asXML(Yii::$app->basePath . "/{$public_path}/history/avito_".date("Y_m_d_H_i_s", $log->createtime).".yml");

        
        $log->type = 4;
        $log->finishtime = time();
        $log->save();
    }
}
