<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
//use yii\web\Controller;
use app\components\ControllerSite;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class ModelController extends \app\components\Controller
{
    public function actionIndex()
    {
       
        $model = \app\models\Producer::find()->all();
        return $this->render('index', ['model'=>$model]);
    }
    
    public function actionProduer()
    {
        $model = \app\models\ModelCut::find()
                ->where(['producer_id'=>$_GET['id']])
                ->all();
        return $this->render('produer', ['model'=>$model]);
    }
}
