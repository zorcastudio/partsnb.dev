<?php
namespace app\controllers;

use yii\helpers\VarDumper;
use app\components\Controller;
use Yii;
use app\models\Registration;
use app\models\LoginForm;
use app\models\User;
use app\models\OrderUser;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\UserChangePassword;
use yii\web\NotFoundHttpException;



//use yii\data\Pagination;

class UserController extends Controller {
    private $_model;

    public function actionIndex() {
        $model = $this->loadModel();
        return $this->render('index', array('model'=>$model));
    }

    public function actionOrder() {

        if(Yii::$app->user->id && !is_numeric($_GET['id'])){

            $searchModel = new OrderUser();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->render('order', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);

        } elseif(substr(Yii::$app->request->referrer, -7) == '/basket' || is_numeric($_GET['id'])) {

            $model = \app\models\Order::find()
                        ->where(['order.id'=>$_GET['id']])
                        ->select('order.*, (order.price + delivery.price) AS sumPrice')
                        ->join('INNER JOIN', 'delivery', 'delivery_id = delivery.id')
                        ->one();
            
            return $this->render('../order/order_finish', ['model'=>$model]);

        }  else

            throw new NotFoundHttpException('У вас нет доступа к этой странице', 404);

        }

    public function actionOrder_view() {
        if(isset($_GET['id'])){
            $model = OrderUser::find()
                    ->select("order.*, (order.price + delivery.price) AS sumPrice")
                    ->where([   
                                'order.id'=>$_GET['id'], 
                                'user_id'=>Yii::$app->user->id
                            ])
                    ->join('INNER JOIN','delivery', 'order.delivery_id = delivery.id')
                    ->one();
            if($model)
                return $this->render('view', array('model'=>$model));
            else
                $this->redirect('/user');
        }else{
            $this->redirect('/user');
        }
    }
    
    public function actionProfile(){
        $model = $this->loadModel();
        $form = (isset($_POST['User'])) ? $_POST['User'] : null ;
        if ($form) {
            $password = $model->password;
            $model->attributes = $form;
            $model->region_id = (isset($form['region_id']) && $form['region_id']) ? $form['region_id'] : null;
            $model->city_id = (isset($form['city_id']) && $form['city_id']) ? $form['city_id'] : null;
            
            if ($model->validate()) {
                if($model->password != $password)
                    $model->password =  md5($model->password);
                
                $model->createtime = time();
                if ($model->save()){
                    Yii::$app->getSession()->setFlash('info','Ваши данные обновленны');
                    $this->redirect(array('/user'));
                }
            }
        }
        
        return $this->render('profile', array('model'=>$model));
    }

    public function actionRegistration() {
        $model = new Registration();
        $form = Yii::$app->request->post('Registration');
        if ($form) {
            $model->attributes = $form;
            $model->createtime = time();
            $model->status = 1;
            $model->region_id = (isset($form['region_id']) && $form['region_id']) ? $form['region_id'] : null;
            $model->city_id = (isset($form['city_id']) && $form['city_id']) ? $form['city_id'] : null;
            
            if ($model->validate()) {
                
//                $this->sendMail($model->email, Yii::$app->name, "Для подтверждения регистрации пройдите по ссылке");
//                $temp = $this->sentMailYandex($model->login_or_email, $subject, $message);
                
                $model->password = ($model->password) ? md5($model->password) : '';
                $model->verifyPassword = ($model->password) ? md5($model->verifyPassword) : '';
                $model->save();
                Yii::$app->getSession()->setFlash('success','Вам на почту было отправлено письмо с подтверждением e-mail');
                $this->redirect('/');
            }
        }else{
            $model->country_id = Registration::$_country_id;
        }
       return $this->render('registration', ['model' => $model]);
    }

    public function actionLogin() {
        $model = new LoginForm();
        
        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            
            if ($model->validate() && $model->login()) {
                User::setLastVisit($model);
                return $this->goBack();
            }
        }
        $this->render('login', ['model' => $model]);
        
        
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
        
        
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }
    
    
//    public $defaultAction = 'recovery';
    
    public function actionRecovery() {
        $model = new \app\models\UserRecoveryForm();
        if (Yii::$app->user->id) {
            $this->redirect('/user');
        } else {
            $email = ((isset($_GET['email'])) ? $_GET['email'] : '');
            $activkey = ((isset($_GET['activkey'])) ? $_GET['activkey'] : '');
            if ($email && $activkey) {
                $form2 = new UserChangePassword();
                $find = User::find()->where(['email' => $email])->one();
                if ($find && $find->activkey == $activkey) {
                    if (isset($_POST['UserChangePassword'])) {
                        $form2->attributes = $_POST['UserChangePassword'];
                        if ($form2->validate()) {
                            $find->password = md5($form2->password);
                            $find->activkey = microtime() . $form2->password;
                            if ($find->status == 0) {
                                $find->status = 1;
                            }
                            $find->save();
                            Yii::$app->getSession()->setFlash('info','Новый пароль изменен.');
                            return $this->redirect('/login');
                        }
                    }
                } else {
//                    Yii::$app->getSession()->setFlash('info', 'Неправильная ссылка востановления пароля.');
                    return $this->goBack();
                }
                return $this->render('recovery/changepassword', array('form2' => $form2));
            } else {
                if (isset($_POST['UserRecoveryForm'])) {
                    $model->attributes = $_POST['UserRecoveryForm'];
                    if ($model->validate()) {
                        $user = User::findOne($model->user_id);
                        $activation_url = Html::a('Востановить пароль', Url::to(['/user/recovery', "activkey" => $user->activkey, "email" => $user->email],$scheme));
                        $subject = 'Вы запросили восстановление пароля на сайте ' . Yii::$app->name;
                        $message = "Вы запросили восстановление пароля на сайте " . Yii::$app->name . ". Чтобы получить новый пароль, перейдите на " . $activation_url . ".";

                        $temp = $this->sentMailYandex($model->login_or_email, $subject, $message);
                        Yii::$app->getSession()->setFlash('info', "На Ваш адрес электронной почты было отправлено письмо с инструкциями.");
                        return $this->render('recovery/recovery', array('model' => $model));
                    }
                }

                return $this->render('recovery/recovery', array('model' => $model));
            }
        }
    }

    public function loadModel() {
        if ($this->_model === null) {
            $this->_model = User::findOne(Yii::$app->user->id);
            if ($this->_model === null)
                throw new NotFoundHttpException('Заказ не найден', 404);
        }
        return $this->_model;
    }

}
