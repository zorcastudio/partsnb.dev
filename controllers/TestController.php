<?php

namespace app\controllers;

use yii\helpers\VarDumper;
use app\models\Product;
use app\models\Category;
use app\models\Page;
use app\models\CategoryFilter;
use app\components\HelloWidget;
use app\components\FilterWorkingCount;
use Yii;
use app\models\Model;
use app\models\ModelCompatible;
use app\components\WaterMark\WaterMark;
use app\modules\admin\components\TranslitFilter;
use app\models\ProductFilterValue;
use yii\helpers\Url;

//require_once (__DIR__.'/vendor/autoload.php');
use Omnipay\YandexMoney;
use Omnipay\Omnipay;
use app\components\SendMailSmtpClass;

class TestController extends \app\components\Controller {

    public function actionIndex() {
        ini_set("memory_limit","2000M");
        ignore_user_abort(true);
        set_time_limit(0);
//        die("<br/>end");
//         \app\components\GoogleMarket::saveFile();
//        \app\components\YandexMarket::saveFile();
//        \app\components\Sitemap::saveFile();
//        die("<br/>end");

        $public_path = Yii::$app->params['public_path'];
        foreach (file(Yii::$app->basePath . "/{$public_path}/PRODUCT_2.csv") as $key => $value) {
            $result = explode(';', trim($value));
//            sort($result);
//            unset($result[0]);
//            vd($result, false);
//            echo "<br/>";
            $array = [
                'id'=>"'". trim(preg_replace("/[,\"\']/i",'', $result[0]))."'",
                'article'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[1]))."'",
                'in_stock1'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[2]))."'",
                'in_stock2'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[3]))."'",
                'in_stock3'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[4]))."'",
                'in_res1'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[5]))."'",
                'in_res2'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[6]))."'",
                'in_res3'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[7]))."'",
                'trade_price'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[8]))."'",
                'is_timed'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[9]))."'",
                'delivery_date'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[10]))."'",
                'warranty_months'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[11]))."'",
                'category_id'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[12]))."'",
                'producer_id'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[13]))."'",
                'model_id'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[14]))."'",
                'serie_id'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[15]))."'",
                'price'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[16]))."'",
                'trade_price1'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[17]))."'",
                'trade_price2'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[18]))."'",
                'trade_price3'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[19]))."'",
                'name'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[20]))."'",
                'name_lat'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[21]))."'",
                'original_name_lat'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[22]))."'",
                'old_name_lat'=>"''",//$result[23]
                'logo'=>"''", //$result[24]
                'description_short'=>"''", //$result[25]
                'description'=>"''",//"'$result[26]'",
                'available'=>"''",//$result[27]
                'is_delete'=>"''",//$result[28]
                'status'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[29]))."'",
                'pay_count'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[30]))."'",
                'createtime'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[31]))."'",
                'updatetime'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[32]))."'",
                'update'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[33]))."'",
                'warranty'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[34]))."'",
                'is_published'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[35]))."'",
                'ya_market'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[36]))."'",
                'sort'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[37]))."'",
                '_original_id'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[38]))."'",
                '_list_model'=>"''",//$result[39]'",
                '_list_pn'=>"''",//$result[40]'",
                '_is_new'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[41]))."'",
                '_is_problem'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[42]))."'",
                '_m_name'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[43]))."'",
                '_'=>"'".trim(preg_replace("/[,\"\']/i",'',  $result[44]))."'",
            ];
            if($array['id'] >= 126067)
                $list = Yii::$app->db->createCommand("
                    INSERT INTO product_3 (`id`, `article`, `in_stock1`, `in_stock2`, `in_stock3`, `in_res1`, `in_res2`, `in_res3`, `trade_price`, `is_timed`, `delivery_date`, `warranty_months`, `category_id`, `producer_id`, `model_id`, `serie_id`, `price`, `trade_price1`, `trade_price2`, `trade_price3`, `name`, `name_lat`, `original_name_lat`, `old_name_lat`, `logo`, `description_short`, `description`, `available`, `is_delete`, `status`, `pay_count`, `createtime`, `updatetime`, `update`, `warranty`, `is_published`, `ya_market`, `sort`, `_original_id`, `_list_model`, `_list_pn`, `_is_new`, `_is_problem`, `_m_name`, `_`)
                    VALUES (".  implode(', ', $array) .");
                ")->execute();
            
//                VALUES ({$value});
//            echo "<pre>";
//            print_r($array);
//            echo "</pre>";
//            echo "<br/>";
//            vd($result, false);
//            echo "<br/>";
//            if($array['id'] > 1)
//                die_my();
            
//            product_3
            
            
//            die_my();
//            id, article, in_stock1, in_stock2, in_stock3, in_res1, in_res2, in_res3,trade_price, is_timed, 
//            delivery_date, warranty_months, category_id, producer_id, model_id,  serie_id
//            $model = \app\models\ProductCreate::findOne($array['id']);
////            vd($model->attributes, false);
//            if(!empty($model) && ($model->model_id != $array['model_id'] || $model->producer_id != $array['producer_id'])){
//                $model->model_id = $array['model_id'];
//                $model->producer_id = $array['producer_id'];
//                
//                vd($model->save(), false);
////                die_my();
//            }
////            foreach ($result as $item) {
//                echo "$item,";
//            }
//            die("<br/>end");
//            if(preg_match('/\d{6}/', $result[0]'", $search1)){
//                $articl = $result[0];
//            }
//                $model = trim($result[1]) ? trim($result[1]) : null;
//                $pn    = trim($result[2]) ? trim($result[2]) : NULL;
//                
//                if($model){
//                    $list = Yii::$app->db->createCommand("
//                        INSERT INTO `model_stacenko2` (article, model) 
//                        VALUES ({$articl},'{$model}');
//                    ")->execute();
//                }else{
//                    $list = Yii::$app->db->createCommand("
//                        INSERT INTO `model_stacenko` (article, pn) 
//                        VALUES ({$articl}, '{$pn}');
//                    ")->execute();
//                    
//                };
        };
        die("<br/>end");
        
        
        
       
        
        
        
        
        
//        vd($result, false);
        
        
        die("<br/>end");

        $public_path = Yii::$app->params['public_path'];
        $path = Yii::$app->basePath ."/{$public_path}";
        $src = "{$path}/uploadfile/images/product/resized/0227927_150x150.jpg";
        $dest = "{$path}/uploadfile/0227927_150x150.jpg";
        \app\models\Image::imgResize($src, $dest, 150);
        
        die("<br/>end");
        
        
        vd($src, false);
        vd(file_exists($src), false);
        die("<br/>end");
         if (!file_exists($src))
            return false;
        
        $size = getimagesize($src);

        if ($size === false)
            return false;

        $w = $size[0];
        $h = $size[1];

        $format = strtolower(substr($size['mime'], strpos($size['mime'], '/') + 1));
        $icfunc = "imagecreatefrom" . $format;

        if (!function_exists($icfunc))
            return false;

        $isrc = $icfunc($src);
        $idest = imagecreatetruecolor(142, 57);
        
        imagefill($idest, 0, 0, $rgb);
        imagecopyresampled($idest, $src, 0, 0, 0, 0, 142, 57, $w, $h);
        imagepng($idest, $dest, $quality);

        imagedestroy($isrc);
        imagedestroy($idest);
        
        die("<br/>end");
        
        
        
        
        
        
        
        
        
        
        
        
//    {$path}/history/{$name}
    die("<br/>end");
        
    $backgroundImagick = new \Imagick;
    $imagick = new \Imagick();
    $imagick->setCompressionQuality($quality);
    $imagick->newPseudoImage(
        $backgroundImagick->getImageWidth(),
        $backgroundImagick->getImageHeight(),
        'canvas:white'
    );

    $imagick->compositeImage(
        $backgroundImagick,
        \Imagick::COMPOSITE_ATOP,
        0,
        0
    );
    
    $imagick->setFormat("jpg");    
    header("Content-Type: image/jpg");
    echo $imagick->getImageBlob();
        
        
        die("<br/>end");
        
        
        
        
        $properties = Yii::$app->db->createCommand("
                SELECT f.name AS filter, fv.name AS value 
                FROM product_filter_value pfv
                INNER JOIN filter f ON filter_id = f.id 
                INNER JOIN filter_value fv ON pfv.filter_value_id = fv.filter_value_id
                WHERE article ='040070' AND serie_id=489
            ")->queryAll();
        $result = [];
        foreach($properties as $value){
            $result[$value['filter']] = $value['value'];
        }
        vd($result, false);
        
        die("<br/>end");
        
        
        $model = \app\models\MarketProduct::find()
                ->where(['not', ['description' => null]])
                ->asArray()
                ->all();
        foreach ($model as $value) {
            $product = \app\models\ProductCreate::find()
                    ->select('product.id, product.article, product.name')
                    ->where(['_original_id' => $value['id_product']])
                    ->joinWith('productFilterValue')
                    ->asArray()
                    ->one();

            $result = [];
            foreach (explode(",", $value['description']) as $item) {
                $filter = explode(":", $item);
                $result[ltrim($filter[0])] = ltrim($filter[1]);
            }

            vd($product, false);
            die("<br>end");
//            $result [] = $value['description'];
        }
        vd($result, false);

        die("<br>end");
//        $xml = simplexml_load_string($xml_string);
//        $json = json_encode($xml);
//        $array = json_decode($json,TRUE);
//        $model = Product::find()
//                ->select('product.id')
//                ->joinWith('image')
////                ->asArray()
//                ->limit(2000)
//                ->andWhere('product.id > 0')
////                ->andWhere('between', 'product.id', 1105, 50000)
//                ->all();
//          SELECT * FROM `product` WHERE product.article NOT IN ( SELECT DISTINCT `article` FROM `product_filter_value`)
//        $model = \app\components\sinc\Import::syncXML();
//        $model = ModelCompatible::setModelCompatibleISProduct();
//        $model = Product::findOne(69055);
//        $model = \app\models\ProductCreate::createImageEqualImageProduct();
//        $model = \app\models\ProductCreate::getProductNotModel();
//        $model = \app\models\ProductUpdate::deleteProductNotOriginalId();
//        $model = \app\models\ModelUpdate::createProductInCompatible();
//        $model = \app\models\Producer::getProducerMOdel();
//        $model = \app\models\ProductUpdate::cresteProductisCompotible();
//        $model = \app\models\ModelUpdate::deleteModel(0, 10);
//        $model = \app\models\FilterCreate::createFilterIsProperties(0, 10);
//        $model = \app\models\FilterCreate::createProductFilterIsProperties(0, 10);
//        $model = \app\models\FilterCreate::createFilterIsProp();
//        $model = \app\models\ProductUpdate::cresteProductCompotible('040004');
//        $model = \app\models\ModelUpdate::updateModelInDeskription(0,100);
//        $model = \app\models\ProductFilterValue::setArticle();
//        $model = FilterWorkingCount::setCountProductFilter();
//        $model = Model::setSerie();
//        $model = Model::getModelSerieIsNull();
//        $model = \app\models\ModelUpdate::deleteModelIsPN(198, 199);
//        $model = \app\models\ModelUpdate::get_Listmodel();
//        $model = \app\models\ProductUpdate::deleteProductNotIsArticle();
//        $model = \app\models\ProductUpdate::setSerieForModel();
//        $model = \app\models\ModelUpdate::deleteModelIsNotCompatible();
//        $model = \app\models\ProductProperties::setArticle();
//        $model = \app\modules\admin\models\ProductSet::setFilter(0, 100);
//        $model = FilterWorkingCount::setCountProductFilter();
//        $model = \app\models\SerchModel::setArticle(0, 200);
//        $model = \app\models\ProductCreate::updateUrl();
//        $model = \app\models\ProductFilterValue::updateProductFilterValue(658, 660);
//        $model = \app\models\ProductProperties::createProperties(8610, 8615);
//        $model = \app\models\ModelUpdate::deleteModel();
//        $model = \app\models\ProductUpdate::updateModelProduct();
//        $model = \app\modules\admin\models\ProductSet::updateProduct();
//        $model = \app\modules\admin\models\ProductSet::updateProductNull();
//        $model = \app\modules\admin\models\ProductSet::updateProductNull();
//        $model = \app\modules\admin\models\ProductSet::setProductProblem();
//        $model = \app\modules\admin\models\ProductSet::setProperties();
//        $model = \app\models\Serie::setSerie();
//        $model = \app\models\Serie::setProductSerie(0, 100000, $_GET['series']);
//        $model = \app\modules\admin\models\ProductTest::updateProduct();
//        $model = \app\models\Producer::getProducerInCategory(3);
//        vd(date('d.m.Y', '1437115009'), false);
//        die("<br/>end");
//       $model = \app\models\Product::createProductCompatible();
//       $model = \app\models\Product::updateProductName();
//        isset($_GET['id']) ? $model = \app\models\Product::getOneParsModel($_GET['id']) : null;
//        !isset($_GET['id']) ? $model = \app\models\Product::setProblemProductt() : null;
//       $model = \app\models\Image::updateImage();
//        $product = Product::find()
//                ->select('producer_id, id, _list_model, _original_id')
//                ->where(['not', ['producer_id'=>0]]) //'category_id' =>[4,12]'",
//                ->andWhere(['not', ['_list_model'=>null]])
////                ->andWhere(['not', ['category_id' =>[4,12]]])
//                ->andWhere(['like','article','07%', FALSE]) // 07 не проверять!!!
////                ->andWhere(['between', 'id', 30000, 40000])
//                ->all();
//        $model = \app\modules\admin\components\TranslitFilter::translitUrl('Блок питания ACER 19V 3.42A (65W)');
//        $model = Category::getCategoryArticle('010258');
//       $model = \app\models\Model::getModel(1, 1);
//       $model = \app\models\Product::findOne(1572);
//       $model = \app\models\settingValues::setParsModel($model->description, $model->id);
//       $model = \app\models\settingValues::setParsPN($model->description, $model->id);
//       $model = \app\models\settingValues::setListModel();
//       $model = \app\models\settingValues::dellDubleProduct();
//       $model = \app\models\settingValues::createToArticleModelinProduct('09');
//       $model = \app\models\settingValues::setProblemProduct();
//       $model = \app\models\settingValues::createProductModel();
//       $model = \app\models\settingValues::createNewPartNamber();
//       $model = \app\models\settingValues::dublePartInProduct();
//       $model = \app\models\settingValues::addModelInPars();
//       $model = \app\models\settingValues::cresteModelCut();
//       $model = \app\models\settingValues::setModelCompatible();
//       $model = \app\models\settingValues::delProductDuble();
//       $model = \app\models\settingValues::createNewModel();
//        $model = Product::dellProduct(1);
//       $model = \app\models\settingValues::setUpdateProduct();
//       $model = \app\models\Product::setProductCategory();
//       $model = \app\models\Product::setDableProduct();
//       $model = \app\models\Model::createModelCompatibleIsProduct();
//       $model = \app\models\settingValues::reviewCountModel();
//       $model = \app\models\settingValues::getNameProductNameModel('010102');
//       $model = \app\models\settingValues::setModelCompatibleDescriptionEmpty();
//       $model = \app\models\Model::setDuplicateModel();
//       $model = \app\models\ModelCompatible::createCompatible();
//        $model = \app\modules\admin\models\ProductSet::updateUrlProduct();
//        $model = \app\modules\admin\models\ProductSet::createProductCompotible('05', 8600, 8800);
        vd($model, false);
        die("</br>end");
    }

    
    public function actionReturn() {
        
     
        $result['POST'] =  $_POST;
        $result['GET'] =  $_GET;
        
        
        
        $model = new \app\models\Test();
        $model->request = json_encode($result);
        $model->createtime = time();
        $model->save();
        
        return TRUE;
    }
}
