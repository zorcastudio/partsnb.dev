<?php

namespace app\controllers;

use Yii;
use app\components\Controller;
use app\models\Product;
use app\models\Region;
use app\models\OrderProduct;
use app\models\Order;
use app\models\City;
use app\models\FilterValue;
use yz\shoppingcart\ShoppingCart;
use yii\web\NotFoundHttpException;
use app\models\User;
use yii\helpers\Url;

class AjaxController extends Controller {

    public function actionCountry_select() {
        $region = Region::getRegion(Yii::$app->request->post('country_id'));
        echo json_encode($region);
    }

    public function actionRegion_select() {
        $citys = City::getCitys(Yii::$app->request->post('region_id'));
        echo json_encode($citys);
    }

    /*
     * Добавление товара в корзину
     */

    public function actionPut_basket() {
        $id = Yii::$app->request->get('id');
        $scenario = Yii::$app->request->get('scenario');
        $amount = Yii::$app->request->get('amount');

        if ($id) {

            $cart = new ShoppingCart();
            $model = Product::findOne($id);

            switch ($scenario) {
                case 'add' :$cart->put($model, 1);
                    break;
                case 'remuve':
                    $amount = isset($cart->positions[$model->id]) ? $cart->positions[$model->id]->_quantity : 0;
                    $cart->update($model, $amount - 1);
                    break;
                case 'update':
                    $cart->update($model, $amount);
                    break;
            }

            $result['amount_summ'] = $cart->getSummPrise()['summ'];
            $result['count_one'] = count($cart->positions);
            $result['count'] = $cart->getCount();

            $result += $cart->getProductCart();
            echo json_encode($result);
        }
    }

    /*
     * Удаление товара с корзины
     */

    public function actionDelete_product() {
        if (isset($_POST['id'])) {
            $cart = new ShoppingCart();
            $cart->removeById($_POST['id']);
            $price = Product::getSummPrise();
            if ($price['count'] == 0)
                $this->redirect('/');
            echo json_encode($price);
        } else
            echo -1;
    }

    public function actionDelete_product_admin() {
        if (isset($_POST['id'])) {
            $item = explode("_", $_POST['id']);
            $model = OrderProduct::find()
                    ->where([['order_id' => $item[0], 'product_id' => $item[1]]])
                    ->one();

            $order = Order::findOne($item[0]);
            $summ = $model->product->getPrice($model->amount);
            $order->price -= $summ;

            $order->update();
            $model->delete();
            echo json_encode(array('summ' => $order->price));
        } else
            echo -1;
    }

    /**
     * Редактирование количества товара в корзине
     */
    public function actionUpdate_product() {
        if (isset($_POST['count']) && $_POST['count'] != 0) {
            $basket = new ShoppingCart();
            $model = Product::findOne($_POST['id']);
            $basket->update($model, $_POST['count']);

            $price = $basket->positions[$_POST['id']]->getSummPrise();
            $p_price = $basket->positions[$_POST['id']]->getPrice();
            $price += array('product' => array('price' => $p_price));
            echo json_encode($price);
        } else
            echo -1;
    }

    public function actionUpdate_productadmin() {
        if (isset($_POST['count']) && $_POST['count'] != 0) {
            $item = explode("_", $_POST['id']);
            $model = OrderProduct::find()->where(['order_id' => $item[0], 'product_id' => $item[1]])->one();
            $order = \app\models\OrderSearch::findOne($item[0]);

            $model->amount = $_POST['count'];
            $model->update();

            $price_p = $model->product->getPrice($model->amount);

            $summ = 0;
            foreach ($order->order_p as $value) {
                $summ += $value->product->getPrice($value->amount);
            }
            $order->price = $summ;
            $order->save();

            $price = array('summ' => $order->price);
            $price += array('product' => array('price' => $price_p));

            echo json_encode($price);
        } else
            echo -1;
    }

    public function actionDelete_filter_value() {
        if (isset($_POST['id'])) {
            $model = FilterValue::findOne($_POST['id']);
            echo $model->delete() ? 1 : -1;
        } else
            echo -1;
    }

    public function actionDelete_model() {
        if (isset($_POST['id'])) {
            $model = \app\models\Model::findOne($_POST['id']);
            //ищем связи если нет связей то удаляем модель
            //если есть удаление запрещено
            $modelСompatible = \app\models\ModelCompatible::findOne(['model_id' => $model->id]);
            if ($modelСompatible) {
                echo -2;
            } else {
                echo $model->delete() ? 1 : -1;
            }
        } else
            echo -1;
    }

    public function actionSearch() {
        $request = trim(Yii::$app->request->get('term'));

        if ($request != '') {
            $request = ucfirst($request);
            $query = Product::find()
                ->select('name, name_lat, id, article, category_id, producer_id')
                ->where([
                    '_is_problem' => 0,
                    'status' => 1,
                    'is_delete' => 0,
                    'is_published' => 1,
                ])
                ->andWhere(['not', ['producer_id' => NULL]])
                ->andWhere(['not', ['category_id' => NULL]])
                ->andWhere(['not', ['model_id' => NULL]])
                ->andWhere(['not', ['serie_id' => NULL]])
                ->orderBy('name ASC')
                ->limit('10');
            
            $producer = Yii::$app->db->createCommand("
                SELECT GROUP_CONCAT(`id` SEPARATOR ', ') AS id,
                GROUP_CONCAT(`name` SEPARATOR '|') AS name
                FROM `producer`
                WHERE '{$request}' LIKE CONCAT('%',name,'%')     
            ")->queryOne();
                
            if($producer['id'] != NULL){
                $request = trim(preg_replace("({$producer['name']})", '', $request));
                $query->andWhere(['producer_id'=>$producer['id']]);
                $query->groupBy('producer_id, category_id');
            }else{
                 $query->groupBy('category_id');
                $producer = NULL;
            }
                    
            //проверяем если введен артикль
            $serch = preg_match("/^\d{6}$/u", $request, $matches);
            $column = $serch ? 'article' : 'name';

            $model = $query->andWhere(['like', $column, "%" . $request . "%", false])->all();

            $data = array();
            foreach ($model as $key => $get) {
                if ($url = $get->getUrl()) {
                    $data[$key]['label'] = $get->name;
                    $data[$key]['value'] = $url;
                }
            }
            $this->layout = 'empty';
            echo json_encode($data);
        }
    }

    /**
     * возвращает список модулей при выбраном производителе
     */
    public function actionSearch_model() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if (!is_null($_GET['term'])) {
            $list = Yii::$app->db->createCommand("
                    SELECT  m.id, m.name AS label, m.name_lat AS 'value'
                    FROM product p
                    LEFT JOIN producer pr ON pr.id = p.producer_id
                    LEFT JOIN serie s ON s.id = p.serie_id
                    LEFT JOIN category c ON c.id = p.category_id
                    LEFT JOIN model m ON m.id = p.model_id
                    WHERE pr.name_lat = '{$_GET['producer_id']}'
                    AND s.name_lat = '{$_GET['serie_id']}'
                    AND c.name_lat = '{$_GET['category_id']}'
                    AND m.`name` LIKE '{$_GET['term']}%'
                    LIMIT 10
            ")->queryAll();
            //return \yii\helpers\ArrayHelper::map($list, 'id', 'text');
            return $list;
        }
        return FALSE;
    }
    
    public function actionSearch_model_all($q = null, $id = null) {
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $list = Yii::$app->db->createCommand("
                        SELECT  m.id, m.name AS text, pr.name AS producer
                        FROM  `model` m
                        LEFT JOIN producer pr ON pr.id = m.producer_id
                        WHERE m.`name` LIKE '{$q}%'
                        ORDER BY m.producer_id
                        LIMIT 20
            ")->queryAll();
            $out['results'] = array_values($list);
        }
        elseif ($id > 0) {
//            $out['results'] = ['id' => $id, 'text' => User::find($id)->name];
        }
        return $out;
    }

    public function actionDelete_productadmin() {
        if (isset($_POST['id'])) {
            $item = explode("_", $_POST['id']);
            $model = OrderProduct::find()->where(['order_id' => $item[0], 'product_id' => $item[1]])->one();
            $order = \app\modules\admin\models\OrderSearch::findOne($item[0]);

            $summ = $model->product->getPrice($model->amount);
            $order->price -= $summ;

            $order->update();
            $model->delete();
            echo json_encode(array('summ' => $order->price));
        } else
            echo -1;
    }

    /**
     * Возвращает список моделей с производителя 
     */
    public function actionModel() {
        if (isset($_POST['producer_id'])) {
            $model = \app\models\Model::find()
                    ->where(['producer_id' => $_POST['producer_id']])
                    ->andWhere(['not', ['serie_id' => null]])
                    ->orderBy('name ASC')
                    ->all();
            $result = [];
            foreach ($model as $value) {
                $result[$value->id] = $value->name;
            }
            echo json_encode($result);
        } else
            echo -1;
    }

    public function actionIndex() {
        echo "Test";
    }

    public function actionCreateproduct() {
        if (User::isAdmin()) {
            $article = $_POST['article'];
            $s = $_POST['first'];
            $f = $_POST['last'];
            echo \app\modules\admin\models\ProductSet::createProductCompotible($article, $s, $f);
        }
    }

    /**
     * Перещитывает количество товаров в фильтрах
     */
    public function actionRecalculate() {
        if (User::isAdmin()) {
//            $model = \app\components\FilterWorkingCount::setCountProductFilter();
            echo $model ? 1 : -1;
        } else
            echo -1;
    }

    /**
     * Перещитывает количество товаров в фильтрах
     */
    public function actionSyncing() {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
//        if (User::isAdmin()) {
        $log = new \app\modules\admin\models\LogsSearch();
        $log->createtime = time();
        
        
        $model = \app\components\sinc\Import::syncXML($log);
        echo $model ? json_encode($model) : -1;
        
        $log->type = 3;
        $log->finishtime = time();
        $log->save();
        
        if($model){
            \app\components\GoogleMarket::saveFile();
            \app\components\YandexMarket::saveFile();
            \app\components\AvitoMarket::saveFile();
            \app\components\OthersMarket::saveFile();
            \app\components\Sitemap::saveFile();
        }
//        } else
//            echo -1;
        $public_path = Yii::$app->params['public_path'];
        SyncController::dellHistory('/sync/history');
        SyncController::dellHistory('/'.$public_path.'/history');
    }
    
    
    
    /**
     * Синхронызацыя через 1с
     */
    public function actionSyncing2() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_HTML;
        
        $log = new \app\modules\admin\models\LogsSearch();
        $log->createtime = time();

        $model = \app\components\sinc\Import::syncXML($log);
        if(isset($model['import'])){
            $count = count($model['import']);
            foreach ($model['import'] as $key => $value) {
                if(is_array($value)){
                    if($key != $count-1)
                        echo $value[0]."\n";
                    else
                        echo $value[0];
                }else{
                    if($key != $count-1)
                        echo $value."\n";
                    else
                        echo $value;
                    
                }
            }
        }
        
        $log->type = 3;
        $log->finishtime = time();
        $log->save();

        if(!isset($model['import_no_file'])){
            \app\components\GoogleMarket::saveFile();
            \app\components\YandexMarket::saveFile();
            \app\components\AvitoMarket::saveFile();
            \app\components\OthersMarket::saveFile();
            \app\components\Sitemap::saveFile();
        }
        $public_path = Yii::$app->params['public_path'];
        SyncController::dellHistory('/sync/history');
        SyncController::dellHistory('/'.$public_path.'/history');
    }

    /**
     * Отправляет письма с обратного звонка
     */
    public function actionCall_back() {
        if (empty($_GET)) {
            throw new NotFoundHttpException('Страница не найдена', 404);
        }
        $host = preg_replace("/^.*\//i", '', Yii::$app->request->hostInfo);
        $data = $_GET;
        $data += ['host' => $host];
        $geo = \app\models\Country::getGeo();
        if($geo) {
            $data += $geo;
        }
//salesk@partsnb.ru
//      bikov_sc@mail.ru
//        sales@partsnb.ru
        if ($this->sentMailYandex('salesk@partsnb.ru', 'Обратный звонок '. $host, $this->renderPartial('_call_back', ['data' => $data])))
            echo $this->renderPartial('_call_back_ok');
        else
            echo -1;
    }

    public function actionSerie() {
        if (User::isAdmin()) {
            $serie = \app\models\Serie::getSerieAll($_POST['serie_id']);
            echo json_encode($serie);
        } else
            echo -1;
    }

    public function actionCreateproperties() {
        if (User::isAdmin()) {
            echo \app\modules\admin\models\ProductSet::setProperties($_POST['first'], $_POST['last']);
        }
    }

    public function actionUpdate_product_serie() {
        if (User::isAdmin()) {
            echo \app\models\Serie::setProductSerie($_POST['first'], $_POST['last'], $_POST['producer']);
        }
    }

    /**
     * определяем каких товаров несуществует но есть связь
     */
    public function actionProduct_error() {
        if (User::isAdmin()) {
            echo \app\modules\admin\models\ProductSet::getError($_POST['first'], $_POST['last']);
        }
    }

    /**
     * Создаем связи фильтров с товарами
     */
    public function actionProduct_filter() {
        if (User::isAdmin()) {
            echo \app\modules\admin\models\ProductSet::setFilter($_POST['first'], $_POST['last']);
        }
    }

    public function actionAdd_producer_search() {
        if (User::isAdmin()) {
            echo json_encode(\app\models\ProducerSearch::addProducerSearch($_POST['id'], $_POST['article']));
        }
    }
    
    public function actionAdd_producer_model() {
        if (User::isAdmin()) {
            $value =['model'=>['name'=>$_POST['name'], 'p_id'=>$_POST['id']]];
            
            echo $this->renderAjax('../../modules/admin/views/product/_compatible_model', ['children'=>$value, 'article'=>$_POST['article']]);
        }
    }
    
    public function actionAdd_model() {
        if (User::isAdmin()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $out = ['results' => ['id' => '', 'text' => '']];
            if (!is_null($_GET['q'])) {
                $query = new \yii\db\Query;
                $query->select('m.id, m.name AS text')
                        ->from('model m')
                        ->where("m.producer_id = '{$_GET['producer_id']}' AND  m.name LIKE '%{$_GET['q']}%' AND is_pn = {$_GET['is_pn']}")
//                        ->andWhere("id NOT IN (SELECT model_id FROM product WHERE article = '{$_GET['article']}' AND model_id IS NOT null)")
                        ->limit(20);
                
                $command = $query->createCommand();
                $data = $command->queryAll();
                $out['results'] = array_values($data);
            } elseif ($id > 0) {
                $out['results'] = ['id' => $id, 'text' => City::find($id)->name];
            }
            return $out;
        }
    }
    
    public function actionAdd_pn() {
        if (User::isAdmin()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $q = $_GET['q'];
            if (!is_null($q)) {
                $out = ['results' => ['id' => '', 'text' => '']];
                $query = new \yii\db\Query;
                $query->select('pn.id, pn.name AS text')
                        ->from('product_pn_search pn')
                        ->where("pn.name LIKE '%".$q."%'
                                AND pn.article IN (
                                    SELECT distinct(article) FROM product 
                                    WHERE producer_id IN (
                                        SELECT distinct (producer_id)
                                        FROM product 
                                        WHERE article = '$_GET[article]'
                                    )
                                )
                            ")
                        ->limit(20);
                $command = $query->createCommand();
                $data = $command->queryAll();
                $out['results'] = array_values($data);
            } 
            return $out;
        }
    }

    /**
     * Создаем связи фильтров с товарами
     */
    public function actionGo_function() {
        if (User::isAdmin()) {
//            $model = \app\modules\admin\models\ProductSet::setFilter($_POST['first'], $_POST['last']);
//            $model = \app\models\SerchModel::setArticle($_POST['first'], $_POST['last']);
//            $model = \app\models\ProductPnSearch::setArticle($_POST['first'], $_POST['last']);
//            $model = \app\models\ProductFilterValue::updateProductFilterValue($_POST['first'], $_POST['last']);
//            $model = \app\models\ProductProperties::createProperties($_POST['first'], $_POST['last']);
//            $model = \app\models\ModelUpdate::updateModelInDeskription($_POST['first'], $_POST['last']);
//            $model = \app\models\ProductUpdate::cresteProductCompotible($_POST['article']);
//            $model = \app\models\FilterCreate::createFilterIsProperties($_POST['first'], $_POST['last']);
//            $model = \app\models\FilterCreate::createFilterIsProperties($_POST['first'], $_POST['last']);
//            $model = \app\models\ModelUpdate::deleteModelIsPN($_POST['pn']);
//            $model = \app\models\ProductUpdate::deleteProductNotIsArticle($_POST['article']);
//            $model = \app\models\FilterCreate::createProductFilterIsProperties($_POST['first'], $_POST['last']);
//            $model = \app\models\ProductCreate::createImageEqualImageProduct();
//            $model = \app\models\Model::deleteModel();
//            $model = \app\models\ModelCompatible::creatProductOnModel($_POST['article']);
            $model = \app\models\Image::setImagesFoProduct($_GET['article']);
//            $model = \app\models\Image::genereteImagesFoArticle($_POST['article']);
//            $model = \app\models\NEWimage::setUniqeUrl();
//            $model = \app\models\NEWimage::createImageInParentProduct();
//            $model = \app\models\TempProduct::setCatProdModSerie();
            echo json_encode($model);
        }
    }

    /**
     * возращает весь масив парт номеров с товара
     */
    public static function actionGet_listmodel() {
        $pn = \app\models\ModelUpdate::get_Listmodel();
        echo json_encode($pn);
    }

    /**
     * возвращает уникальные артикулы с ModelCompatible
     */
    public static function actionArticle() {
        $article = \app\models\ModelCompatible::find()
                ->where(['not', ['article' => null]])
                ->select('article')
                ->asArray()
                ->distinct()
                ->all();
        $result = [];
        foreach ($article as $value) {
            $result[] = $value['article'];
        }
        echo json_encode($result);
    }

    /**
     * возвращает уникальные артикулы с Product
     */
    public static function actionArticle_product() {
        $article = \app\models\Product::find()
                ->where(['not', ['article' => null]])
                ->andWhere(['not', ['article' => '']])
                ->select('article')
                ->asArray()
                ->distinct()
                ->all();
        $result = [];
        foreach ($article as $value) {
            $result[] = $value['article'];
        }
        echo json_encode($result);
    }

    /**
     * возвращает уникальные артикулы с ModelCompatible
     */
    public static function actionArticle_not_product() {
        $list = Yii::$app->db->createCommand("SELECT mc.article
                                                FROM model_compatible mc
                                                LEFT JOIN model m ON m.id = mc.model_id
                                                LEFT JOIN product p ON p.model_id = m.id
                                                AND p.article = mc.article
                                                WHERE p.model_id IS NULL 
                                                GROUP BY article")
                ->queryAll();
        $result = [];
        foreach ($list as $value) {
            $result[] = $value['article'];
        }
        echo json_encode($result);
    }

    /**
     * вернет уникальные артикулы с таблицы товаров
     */
    public static function actionArticle_in_product() {
        //артикули товары в которых неопределен главный товар
//        echo \app\models\ProductCreate::getProductNotModel();
//        $article = Product::find()
//                ->select('article')
//                ->andWhere(['not', ['_original_id'=>NULL]])
//                ->andWhere(['description'=>''])
//                ->groupBy('article')
//                ->asArray()
//                ->all();
        $article = Product::find()
                ->select('article')
                ->distinct()
                ->asArray()
                ->all();
        $result = [];
        foreach ($article as $value) {
            $result[] = $value['article'];
        }
        echo json_encode($result);
    }
    
    
    public function actionEditable_sort() {
        // Check if there is an Editable ajax request
        if (isset($_POST['hasEditable'])) {
            $value = array_shift($_POST['FilterSearch']);
            $model = \app\models\Filter::findOne($_POST['editableKey']);
            
            // use Yii's response format to encode output as JSON
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $model->sort = $value['sort'] ;

            return ($model->save()) 
                ? ['output'=>$model->sort, 'message'=>''] 
                : ['output'=>'', 'message'=>$model->getErrors()];

            // return JSON encoded output in the below format
            // alternatively you can return a validation error
            // return ['output'=>'', 'message'=>'Validation error'];
            // else if nothing to do always return an empty JSON encoded output
        }else
            return ['output'=>'', 'message'=>''];

        // Else return to rendering a normal view
        return $this->render('view', ['model'=>$model]);
    }

}
