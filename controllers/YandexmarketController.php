<?php

namespace app\controllers;
use app\components\Controller;
use yii\helpers\ArrayHelper;
use app\models\Category;
use app\models\Product;
use yii\helpers\Url;
use app\components\YandexMarket;
use Yii;


class YandexmarketController extends Controller {
    
    public function actionFile(){
        ini_set("memory_limit","2000M");
        ignore_user_abort(true);
        set_time_limit(0);
        
        $log = new \app\modules\admin\models\LogsSearch();
        $log->createtime = time();
        $public_path = Yii::$app->params['public_path'];
        
        $XML = YandexMarket::getXML($log);
        
        //header("Content-Type: application/force-download");
        //header("Content-Disposition: attachment; filename=yandexmarket.yml");
        //echo $XML->saveXML();
        $XML->asXML(Yii::$app->basePath . "/{$public_path}/yandexmarket.yml");
        $XML->asXML(Yii::$app->basePath . "/{$public_path}/history/yandexmarket_".date("Y_m_d_H_i_s", $log->createtime).".yml");

        $log->type = 0;
        $log->finishtime = time();
        $log->save();
    }
    
    public function actionGet(){
        ini_set("memory_limit","20000M");
        ignore_user_abort(true);
        set_time_limit(0);
        
        $log = new \app\modules\admin\models\LogsSearch();
        $log->createtime = time();

        $XML = YandexMarket::getXML($log);
//        header("Content-Type: application/octet-stream");
        
        header("Content-Type: text/plain");    
//        header("Content-Disposition: attachment; filename=yandexmarket.yml");
        
        print_r($XML->asXML());
        $public_path = Yii::$app->params['public_path'];
        $XML->asXML(Yii::$app->basePath . "/{$public_path}/history/yandexmarket_".date("Y_m_d_H_i_s", $log->createtime).".yml");
        
        $log->type = 0;
        $log->finishtime = time();
        $log->save();
    }
    
    protected function disconnect_continue_processing($time_limit = null) {
        ignore_user_abort(true);
        session_write_close();
        set_time_limit((int) $time_limit); //defaults to no limit
        while (ob_get_level() > 1) {//only keep the last buffer if nested
            ob_end_flush();
        }
        $last_buffer = ob_get_level();
        $length = $last_buffer ? ob_get_length() : 0;
        header("Content-Length: $length");
        header('Connection: close');
        if ($last_buffer) {
            ob_end_flush();
        }
        flush();
//        fastcgi_finish_request();
    }
}
