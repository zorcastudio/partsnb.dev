<?php
namespace app\controllers;

use Yii;
use app\components\Controller;
use app\models\User;
use yz\shoppingcart\ShoppingCart;



class BasketController extends Controller {

    public function actionIndex() {
        $basket = new ShoppingCart();
        if(empty($basket->positions)){
            return $this->redirect(Yii::$app->request->referrer);
        }
//        $this->layout = 'colum1';
        return $this->render('index', [
            'model' => $basket, 
        ]);
    }

    public function actionClear(){
        $basket = new ShoppingCart();
        $basket->removeAll();
        $this->redirect('/');
    }
}
