<?php

namespace app\controllers;

use yii\data\Pagination;
use Yii;
use yii\helpers\VarDumper;
use yii\data\ArrayDataProvider;
use app\models\Product;
use app\components\Controller;
use app\models\User;
use app\models\Category;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use app\models\ProductPnSearch;
use app\models\SearchModel;



class ProblemController extends Controller {

    
    public function actionAllproducer() {
        return $this->render('allproducer', [
            'producer' => \app\models\Producer::getProducerMOdel()
        ]);
    }
    
    public function actionModelnullserie() {
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \app\models\Model::find()->where(['serie_id'=>NULL]),
            'pagination' => [
                'pageSize' => 70,
            ],
        ]);
        
        return $this->render('model_null_serie', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
