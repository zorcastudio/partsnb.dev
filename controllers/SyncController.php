<?php

namespace app\controllers;
use app\components\Controller;
use yii\helpers\Url;
use app\modules\admin\models\LogsSearch;
use Yii;
use app\models\ProductCreate;
use app\models\User;



class SyncController extends Controller {
    
    
    public function actionImport(){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $log = new \app\modules\admin\models\LogsSearch();
        $log->createtime = time();
        
        $model = \app\components\sinc\Import::syncXML($log);
        echo $model ? json_encode($model) : -1;
        
        $log->type = 3;
        $log->finishtime = time();
        $log->save();
        
        if($model){
            \app\components\GoogleMarket::saveFile();
            \app\components\YandexMarket::saveFile();
            \app\components\AvitoMarket::saveFile();
            \app\components\OthersMarket::saveFile();
            \app\components\Sitemap::saveFile();
        }
    }
    
    
    public function actionIndex() {
        $log = new LogsSearch;
        $log->createtime = time();
        
        $temp = Yii::$app->basePath."/web/sync/sinc.txt"; //Url::to(["/web/sync/sinc.txt"], true);
        
//        $fp = fopen("counter.txt", "a"); // Открываем файл в режиме записи 
//        $mytext = "Это строку необходимо нам записать\r\n"; // Исходная строка
//        $test = fwrite($fp, $mytext); // Запись в файл
//        if ($test) echo 'Данные в файл успешно занесены.';
//        else echo 'Ошибка при записи в файл.';
//        fclose($fp); //Закрытие файла
        
//        
        $fp = fopen($temp, 'w');
        
        $log->type = 3;
        $log->finishtime = time();
        $log->save();
        die();
    }
    
    /**
     * Видаляє з історії все окрім останній 2-х днів
     * @param string $link
     * @return boolean
     */
    public static function dellHistory($link) {
        $date = date('Y_m_d', time());
        $date = array_values(date_parse_from_format("Y_m_d", $date));
        list($year, $month, $day) = $date;
        if ($day - 1 != 0) {
            $day_last = $day - 1;
            $month_last = $month;
        } else {
            //текущий день в месяце 
            $n = date("j");
            $n = intval($n);
            //UNIX-таймстамп из прошлого месяца 
            $n = time() - $n * 86400 + 1;
            //количество дней прошлого месяца 
            $day_last = date("t", $n);
            $month_last = $month - 1;
        }

        $link = Yii::$app->basePath . $link;
        $list = scandir($link);
        foreach ($list as $value) {
            if (!preg_match("/^\./iu", $value) && !preg_match("/{$year}_0?($month|$month_last)_0?($day|$day_last)/iu", $value)) {
                if (!unlink($link . '/' . $value)) {
                    return false;
                }
            }
        }
        return true;
    }

}
