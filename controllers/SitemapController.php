<?php
namespace app\controllers;
use app\components\Controller;
use app\modules\admin\models\LogsSearch;
use yii\helpers\Url;
use Yii;


class SitemapController extends Controller
{
   
    public function actionIndex()
    {
        ini_set("memory_limit","2000M");
        ignore_user_abort(true);
        set_time_limit(0); 
        
        $log = new LogsSearch;
        $log->createtime = time();
        
        $XML = \app\components\Sitemap::getXML($log);
        header("Content-type: text/xml; charset=utf-8");
        print_r($XML->saveXML());
        
        $log->type = 2;
        $log->not_error = 1;
        $log->finishtime = time();
        $log->save();
        die();
    }
    
    public function actionFile(){
        ini_set("memory_limit","2000M");
        ignore_user_abort(true);
        set_time_limit(0); 
        
        $log = new LogsSearch;
        $log->createtime = time();
        
        $XML = \app\components\Sitemap::getXML($log);
        $public_path = Yii::$app->params['public_path'];
        //header("Content-Type: application/force-download");
        //header("Content-Disposition: attachment; filename=sitemap.xml");
        //echo $XML->saveXML();
        
        $XML->save(Yii::$app->basePath . "/$public_path/history/sitemap_".date("Y_m_d_H_i_s", $log->createtime).".xml");
        $XML->save(Yii::$app->basePath . "/$public_path/sitemap.xml");

        $log->type = 2;
        $log->not_error = 1;
        $log->finishtime = time();
        $log->save();
    }
}
