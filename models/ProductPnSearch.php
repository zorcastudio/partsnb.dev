<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;


class ProductPnSearch extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "product_pn_search";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['article', 'name'], 'required'],
            [['article', 'name'], 'isUnique'],
            [['article'], 'string', 'max' => 40],
            [['name'], 'string', 'max' => 100]
        ];
    }
    
    public function isUnique(){
        $serch = self::findOne(['name'=>$this->name, 'article'=>$this->article]);
        if($serch){
            $this->addError('name', 'Название модели и партномер должны быть уникальными');
            return false;
        }
        return TRUE;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'id' => "ID",
            'article' => "Артикль",
            'name' => "Название партномера",
        ];
    }
    
    /**
     * масив всех моделей которые есть у того товара
     */
    public static function getPNArray($article){
        $model = self::find()
                ->where(['article'=>$article])
                ->asArray()
                ->all();
        
        $result = [];
        foreach ($model as $value) {
            $result[$value['id']] = $value['name'];
        }
        return $model ? $result : [];
    }
    
    /**
     * масив всех моделей (id) которые есть у  товара
     */
    public static function getPNArrayId($article){
        $model = self::find()
                ->where(['article'=>$article])
                ->asArray()
                ->all();
        
        $result = [];
        foreach ($model as $value) {
            $result[] = $value['id'];
        }
        return $model ? $result : [];
    }
    
    
    public static function updatePN($model, $POST){
        if(isset($POST['GenereteProduct']['_pnSearch'])){
            $setId = self::getPNArrayId($model->article);
            $_pnSearch = !empty($POST['GenereteProduct']['_pnSearch']) ? $POST['GenereteProduct']['_pnSearch'] : [];
            
            //створюємо нові парт номера
            foreach (array_diff($_pnSearch, $setId) as $name) {
                $pnSearch = new self();
                $pnSearch->article = $model->article;
                $pnSearch->name = $name;
                if(!$pnSearch->save())
                    return FALSE;
            }

            //Видалення парт номерів
            self::deleteAll(['id'=> array_keys(array_diff($setId, $_pnSearch))]);
        }
        return TRUE;
    }

    /**
     * заполняем таблицу
     */
    public static function setArticle($f, $l){
        $product = Product::find()
                ->where(['not', ['_list_pn'=>null]])
                ->select('article, _list_pn, id')
                ->andWhere(['between', 'id', $f, $l])
                ->asArray()
                ->all();
        $result =[];
        foreach ($product as $value) {
            $list_model = json_decode($value['_list_pn']);
            if(!empty($list_model)){
                foreach ($list_model as $item) {
                    $serchModel = self::findOne(['article'=>$value['article'], 'name'=>$item]);
                    if(!$serchModel){
                        $serchModel = new self();
                        $serchModel->article = $value['article'];
                        $serchModel->name = $item;
                        $serchModel->save();
                    }
                }
            }
            $result[] = $value['article'];
        }
        return $result;
    }
    
    
    /**
     * ищем в тексте партномер
     * возвращаем его article
     * @param type $serch
     * @return type
     */
    public static function seartchPn($serch) {
//        $serch = preg_replace("/[а-яА-Я]*/u", '', $serch);
//        $serch = preg_replace("/^\s*/u", "", $serch);
        if(empty($serch)){
            return FALSE; 
        }
        $model = self::find()
                ->select('pn.id, pn.name, pn.article')
                ->from('product_pn_search pn')
                ->innerJoin('product p', 'p.article = pn.article')
                ->where(['like', 'pn.name', $serch."%", false])
                ->andWhere(['not',['p.category_id'=>2]])
                ->all();
        if(!empty($model)){
            $result = [];
            foreach ($model as $value) {
                $result['key'][] = $value->id;
                $result['article'][] = $value->article;
                $result['name'][] = $value->name;
            }
            return $result;
        }else
            return false;
    }
    
    
    public static function searchPnInProduct(){
        $list = ProductCreate::find()
                ->select('p.id,  p.article, p.name_lat,  p.category_id,  pr.name AS p_name , p.producer_id,  p.model_id,  p.serie_id, p.name')
                ->from('product p')
                ->leftJoin('producer pr', 'p.producer_id = pr.id')
                ->where(['NOT', ['p.is_delete'=>1]])
                ->andWhere('(p.model_id IS NULL OR p.serie_id IS NULL) AND p.producer_id IS NOT NULL AND p.is_published !=-3')
                ->limit(1000)
                ->all();
//        $list = ProductCreate::find()
//                ->select('p.id,  p.article, p.name_lat,  p.category_id,  pr.name AS p_name , p.producer_id,  p.model_id,  p.serie_id, p.name')
//                ->from('product p')
//                ->leftJoin('producer pr', 'p.producer_id = pr.id')
//                ->where(['p.is_published'=>-3])
//                ->andWhere(['not', ['p.is_published'=>-4]])
//                ->limit(1000)
//                ->all();
        
        foreach ($list as $product) {
//            echo $product->name;
//            vd($product->name, false);;
//            vd($product->p_name, false);
            $name = preg_replace("/([а-яё]*|\([а-яё]*\))/iu", '', $product->name);
            $name = preg_replace("/$product->p_name/", '', $name);
            
//            $name = preg_replace("/^.* /", '', $name);
            $name = trim($name);
            
            $pn = Yii::$app->db->createCommand("
                SELECT  pn.name AS pn_name, pn.article, m.id AS m_id, s.id AS s_id
                FROM  `product_pn_search` pn
                LEFT JOIN model m ON m.name = pn.name
                LEFT JOIN serie s ON (s.producer_id = $product->producer_id AND s.name = 'P/N')
                WHERE pn.name LIKE '".$name."' AND article= $product->article
            ")->queryOne();
            
//            vd($name, false);
//            echo $name;
//            echo "<br/>";
            if($pn && !$pn['m_id']){
                $model = new Model();
                $model->name = $name;
                $model->producer_id = $product->producer_id;
                $model->serie_id = $pn['s_id'];
                if($model->save()){
                    $product->model_id = $model->id;
                    $product->serie_id = $model->serie_id;
                    $product->is_published = 1;
                }
            }elseif($pn['m_id']){
                //модель існує
                $product->model_id = $pn['m_id'];
                $product->serie_id = $pn['s_id'];
                $product->is_published = 1;
            }else
                $product->is_published = -3; // неопределил парт номер
            
            $product->_is_problem = 0;
            $product->status = 1;
            vd($product->save(), false);;
        }
    }
}
