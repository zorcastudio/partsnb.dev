<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\admin\components\TranslitFilter;



class Serie extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "serie";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['name', 'producer_id'], 'required'],
            [['name'], 'isUnique'],
            [['producer_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['description'], 'string'],
        ];
    }
    
    public function isUnique(){
        $serch = self::findOne(['name'=>$this->name, 'producer_id'=>$this->producer_id]);
        if($serch && $this->id != $serch->id){
            $this->addError('name', 'Название модели и производитель должны быть уникальными');
            return false;
        }
        return TRUE;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'id' => "ID серии",
            'producer_id' => "Производитель",
            'name' => "Название серии",
            'description' => "Описание серии",
        ];
    }
    
    public function getProducer() {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }
    
    public function getModel() {
        return $this->hasMany(Model::className(), ['producer_id' => 'producer_id', 'serie_id'=>'id']);
//                ->andWhere(['like','model.name', "%serie.name%", FALSE]);
    }
    
    public function getProduct() {
        return $this->hasMany(Product::className(), ['producer_id' => 'producer_id', 'serie_id'=>'id']);
    }

    public function getUrlBrandPath() {
        $producer = Yii::$app->request->get('producer');
        return '/brands/'.$producer.'/'.$this->name_lat;
    }

    /**
     * Существует ли данная модель
     */
    public function isExists(){
        return self::find()
                ->where(['producer_id'=>$this->producer_id, 'name'=> $this->name])
                ->exists();
    }
    
      /*
     * Перед тек как сохранить в БД
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (!$this->name_lat) {
                $this->name_lat = TranslitFilter::translitUrl($this->name);
            }else{
                $model = self::findOne($this->id);
                if($model && $model->name != $this->name && $model->name_lat == $this->name_lat)
                    $this->name_lat = TranslitFilter::translitUrl($this->name);
            }
            return TRUE;
        } else
            return false;
    }
    
    /**
     * перед тем как удалить серии обнуляем таблицы где они присутствуют
     */
    public function beforeDelete(){
        Product::updateAll(['serie_id' =>null ], ['serie_id'=>$this->id]);
        Model::updateAll(['serie_id' => null], ['serie_id'=>$this->id]);
        ProductFilterValue::updateAll(['serie_id' => null], ['serie_id'=>$this->id]);
        ProductFilterCount::deleteAll(['serie_id'=>$this->id]);
        return parent::beforeDelete();
    }
    
    /**
    * Вернет все серии данного производителя для создания модели
     */
    public static function getSerieAll($producer_id=false){
        if($producer_id)
            return ArrayHelper::map(self::findAll(['producer_id'=>$producer_id]), 'id', 'name');
        else
            return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
    
    /**
     * Вернет все серии данного производителя
     */
    public static function getSerie($producer_id, $category){
        return Yii::$app->db->createCommand("
            SELECT s.id, s.name, s.name_lat
            FROM `product` p
            INNER JOIN serie s ON p.serie_id = s.id AND p.producer_id = s.producer_id 
            WHERE p.category_id = '{$category->id}' 
                AND p.producer_id = '{$producer_id}'
                AND p.model_id IS NOT  NULL
                AND p.serie_id IS NOT NULL
                AND p.is_delete=0
                AND p.status=1
                AND p.is_published=1
                AND p._is_problem=0
            GROUP BY p.serie_id
        ")->queryAll();
    }
    
    public static function getSerieArray($category, $producer){
        $list = Yii::$app->db->createCommand("
            SELECT s.id, s.name, s.name_lat
            FROM `product` p
            INNER JOIN serie s ON p.serie_id = s.id AND p.producer_id = s.producer_id 
            INNER JOIN category c ON p.category_id = c.id
            INNER JOIN producer pr ON p.producer_id = pr.id
            WHERE c.name_lat = '{$category}' 
                AND pr.name_lat = '{$producer}'
                AND p.model_id IS NOT  NULL
                AND p.serie_id IS NOT NULL
                AND p.is_delete=0
                AND p.status=1
                AND p.is_published=1
                AND p._is_problem=0
            GROUP BY p.serie_id
        ")->queryAll();
        
        $result = [];
        if($list){
            foreach ($list as $value) {
                $result[$value['name_lat']] = $value['name'];
            }
        } 
        return $result;
    }

    public static function setSerie(){
        $str = "ACER:;Aspire;Aspire One;ChromeBook ;Extensa;Ferrari;Revo;TM TimeLineX;TravelMate:APPLE:;iBook;MacBook;MacBook Air;MacBook Pro;PowerBook:ASUS:;A Series;ASmobile;PRO;B Series;C Series;EEE PC;F Series;G Series;K Series;L Series;Lamborghini;M Series;N Series;NX Series;P Series;R Series;S Series;T Series;Taichi;Transformer;U Series;UL Series;UX Series;V Series;VivoBook;VX Series;W Series;X Series;Z Series;ZenBook:Benq:;JoyBook;A Series;U Series:Clevo:;DeskNote;MobiNote;PortaNote;Ultimate:Dell:;Alienware;Inspiron;Latitude;OptiPlex;Precision;SmartPC;SmartStep;Vostro;XPS:eMachines:;D Series;E Series;E-Slate Series;G Series;M Series;250 Series;350 Series;355 Series:Fujitsu-Siemens:;Amilo;Biblo;Esprimo;LifeBook;LiteLine;Stylistic:Gateway:;200 Series;3000 Series;400 Series;4000 Series;600 Series;6000 Series;7000 Series;8000 Series;CX Series;E Series;HandBook Series;M Series;MX Series;Nomad Series;NV Series;NX Series;P Series;S Series;Solo Series;T Series;Tablet Series:HP:;Armada;Chromebook;EliteBook;Envy;Envy DV6;Envy DV7;Envy M6;Envy Sleekbook;Evo;Folio;G-Series;HDX;Compaq Series;Jornada;Mini;OmniBook;Pavilion;Pavilion DM ;Pavilion DM1;Pavilion DM3;Pavilion DM4;Pavilion DV;Pavilion DV2;Pavilion DV3;Pavilion DV4;Pavilion DV5;Pavilion DV6;Pavilion DV7;Pavilion DV8;Pavilion ET;Pavilion EW;Pavilion EZ;Pavilion G;Pavilion G4;Pavilion G50;Pavilion G6;Pavilion G60;Pavilion G7;Pavilion M;Pavilion N;Pavilion SleekBook;Pavilion T;Pavilion TouchSmart;Pavilion X;Pavilion ZD;Pavilion ZE;Pavilion ZT;Pavilion ZU;Pavilion ZV;Pavilion ZX;Portable;Presario;Presario A;Presario B;Presario C;Presario CQ;Presario F;Presario G;Presario M;Presario R;Presario V;Presario X;ProBook;Special Edition;Spectre;Split;Tablet PC;TouchSmart;Vectra;Voodoo Envy;ZBook:LENOVO:;3000 Series;B Series;E Series;F Series;G Series;IdeaPad;K Series;M Series;S Series;ThinkPad Edge;ThinkPad F-Series;ThinkPad Helix;ThinkPad L;ThinkPad R;ThinkPad S;ThinkPad SL;ThinkPad T;ThinkPad W;ThinkPad X;ThinkPad Z;U Series;V Series;W Series;Y Series;Yoga;Z Series:LG:;Xnote:MSI:;Megabook;Wind;X-Series;CR Series;CX Series;ER Series;EX Series;FT Series;FX Series;GE Series;GT Series;GX Series;GP Series;GS Series;PR Series;PX Series;U Series;VR Series;VX Series;WT Series:Packard Bell:;Chroma;Diplomat;Dot Series;EasyLite;EasyNote;EasyOne;iGo;iPower;LH Series;LS Series;LT Series;MT Series;TH Series;TE Series;TJ Series;TJ Series;TS Series;TV Series:ROVERBOOK:;Discovery;Explorer;Nautilus;Navigator;Neo;Partner;Pro;Voyager;Z Series:SAMSUNG:;A Series;Chromebook;M Series;N Series;NB Series;NC Series;ND Series;NF Series;NP Series;NS Series;P Series;Q Series;QX Series;R Series;RC Series;RF Series;RV Series;SF Series;V Series;VM Series;X Series:Sony:;VAIO CA series;VAIO CB series;VAIO Duo;VAIO Fit;VAIO PCG-Series;VAIO Pro;VAIO SVE;VAIO SVF Series;VAIO SVS-Series;VAIO SVT Series;VAIO SVZ;VAIO VGC-L Series;VAIO VGC-Series;VAIO VGN-A Series;VAIO VGN-AR Series;VAIO VGN-AW Series;VAIO VGN-AX Series;VAIO VGN-B Series;VAIO VGN-BX Series;VAIO VGN-C Series;VAIO VGN-CR Series;VAIO VGN-CS Series;VAIO VGN-CW Series;VAIO VGN-FE Series;VAIO VGN-FJ Series;VAIO VGN-FS Series;VAIO VGN-FW Series;VAIO VGN-FX Series;VAIO VGN-FZ Series;VAIO VGN-G Series;VAIO VGN-GR Series;VAIO VGN-K Series;VAIO VGN-N Series;VAIO VGN-NR Series;VAIO VGN-NS Series;VAIO VGN-NW Series;VAIO VGN-P Series;VAIO VGN-S Series;VAIO VGN-Series;VAIO VGN-SR Series;VAIO VGN-SZ Series;VAIO VGN-T Series;VAIO VGN-TT Series;VAIO VGN-TX Series;VAIO VGN-TXN Series;VAIO VGN-TZ Series;VAIO VGN-U Series;VAIO VGN-UX Series;VAIO VGN-X Series;VAIO VGN-Z Series;VAIO VGP-CKP Series;VAIO VGP-Series;VAIO VPC-A Series;VAIO VPC-B Series;VAIO VPC-CA Series;VAIO VPC-CB Series;VAIO VPC-CW Series;VAIO VPC-EA Series;VAIO VPC-EB Series;VAIO VPC-EC Series;VAIO VPC-EE Series;VAIO VPC-EF Series;VAIO VPC-EG Series;VAIO VPC-EH Series;VAIO VPC-EJ Series;VAIO VPC-EK Series;VAIO VPC-EL Series;VAIO VPC-F Series;VAIO VPC-M Series;VAIO VPC-P Series;VAIO VPC-S Series;VAIO VPC-SA Series;VAIO VPC-SB Series;VAIO VPC-SD Series;VAIO VPC-SE Series;VAIO VPC-Series;VAIO VPC-SVE Series;VAIO VPC-W Series;VAIO VPC-X Series;VAIO VPC-Y Series;VAIO VPC-YA Series;VAIO VPC-YB Series;VAIO VPC-Z Series:TOSHIBA:;AC100 Series;AZ100 Series;DynaBook;Equium;Gigabeat;Libretto;NB-Series;Portege;Qosmio;Satellite;Satellite Pro;Small Business;T Series;Tecra";

        $array =  explode(":", $str);
        $result = [];
        foreach ($array as $key => $value) {
            if($key%2 == 0){
                $series = preg_replace('/^;/i', '',$array[$key+1]);
                $series =  explode(";", $series);
                $result[$value] = $series;
            }
        }
        
        foreach ($result as $key => $value) {
            $producer = Producer::findOne(['name'=>$key]);
            foreach ($value as $item) {
                $serie = new self;
                $serie->producer_id = $producer->id;
                $serie->name = $item;
                vd($serie->save(), false);
            }
        }
    }
    
    /**
     * пересохраняем ыерии для создания name_lat
     */
    public static function setNameLat(){
        $serie = self::find()->all();
        foreach ($serie as $value) {
            vd($value->update(), false); 
        }
    }
    
    /**
     * обновляем товар у которого находим соответственно серию и производитяеля
     * устанавливаем ему serie_id
     */
    public static function setProductSerie($f, $l, $producer_id){
        $series = Serie::find()
                ->where(['producer_id'=>$producer_id])
                ->select('id, name, producer_id')
                ->asArray()
                ->all();
       
        foreach ($series as $value) {
            $product = Product::find()
                    ->where(['producer_id'=>$value['producer_id']])
                    ->andWhere(['like','name', $value['name']])
                    ->andWhere([ 
                        '_is_problem' => 0, 
                        'is_delete' => 0,
                        'is_published'=>1,
                        'status' => 1,
                    ])
                    ->select('id, serie_id, producer_id, name')
                    ->andWhere(['between', 'id', $f,$l])
                    ->all();
            
            foreach ($product as $item) {
                $item->serie_id = $value['id'];
                vd($item->update(), false);
            }
        }
        return "$f-$l : $producer_id";
    }
    
      /**
     * повертає id виробника шукаючи його в назві товару name
     * якщо в імені зустріне парт номер то серію невизначить
     * @param type $producers
     * @param type $name
     * @return type
     */
    public static function getIdSerieInName($series, $name) {
        $result = [];
        $key = NULL;
        $srt = '';
        foreach ($series as $serie) {
            if (preg_match("/^($serie[name])/i", $name, $matches)) {
                $result[$serie['id']] = $matches[1];
                $key = $serie['id'];
            }
        }
        if (empty($result)) {
            return null;
        } else {
            if (count($result) > 1) {
                foreach ($result as $k => $val) {
                    if (strlen($srt) < strlen($val)) {
                        $key = $k;
                        $srt = $val;
                    }
                }
            };
            
            return $key;
        }
    }

    public function getSeriesByParams($series, $brand) {
        return self::find()
            ->leftJoin(Model::tableName().' m', 'm.serie_id = serie.id')
            ->where('serie.name_lat=:name_lat', [':name_lat'=>$series])
            ->andWhere('serie.producer_id=:producer_id', [':producer_id'=>$brand])
            ->andWhere(['m.is_pn' => 0])
            ->one();
    }

}
