<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;


class SerchModel extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "product_model_search";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['article', 'name'], 'required'],
            [['article', 'name'], 'isUnique'],
            [['article'], 'string', 'max' => 40],
            [['name'], 'string', 'max' => 100]
        ];
    }
    
    public function isUnique(){
        $serch = self::findOne(['name'=>$this->name, 'article'=>$this->article]);
        if($serch){
            $this->addError('name', 'Название модели и артикль должны быть уникальными');
            return false;
        }
        return TRUE;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'id' => "ID",
            'article' => "Артикль",
            'name' => "Название модели",
        ];
    }
    
    /**
     * масив всех моделей которые есть у того товара
     */
    public static function getModelArray($article){
        $model = self::find()
                ->where(['article'=>$article])
                ->asArray()
                ->all();
        
        $result = [];
        foreach ($model as $value) {
            $result[$value['id']] = $value['name'];
        }
        return $model ? $result : [];
    }
    
    /**
     * масив всех моделей (id) которые есть у  товара
     */
    public static function getModelArrayId($article){
        $model = self::find()
                ->where(['article'=>$article])
                ->asArray()
                ->all();
        
        $result = [];
        foreach ($model as $value) {
            $result[] = $value['id'];
        }
        return $model ? $result : [];
    }

        /**
     * заполняем таблицу
     */
    public static function setArticle($f, $l){
        $product = Product::find()
                ->where(['not', ['_list_model'=>null]])
                ->select('article, _list_model, id')
                ->andWhere(['between', 'id', $f, $l])
                ->asArray()
                ->all();
        $result =[];
        foreach ($product as $value) {
            $list_model = json_decode($value['_list_model']);

            foreach ($list_model as $item) {
                $serchModel = self::findOne(['article'=>$value['article'], 'name'=>$item]);
                if(!$serchModel){
                    $serchModel = new self();
                    $serchModel->article = $value['article'];
                    $serchModel->name = $item;
                    $serchModel->save();
                }
            }
            $result[] = $value['article'];
        }
        return $result;
    }
}
