<?php

namespace app\models;

use Yii;

class ModelChild extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "model_child";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
//            ['name', 'unique', 'criteria' => ['condition' => 'model_id = :model_id','params' => ['model_id' => null]]],
            [['name', 'model_id'], 'unique', 'targetAttribute' => ['name', 'model_id']],
            [['model_id'], 'integer'],
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            //если была изменена серия - 
            if ($this->isNewRecord) {
                $this->createtime = time();
                return TRUE;
            }
        };
        return TRUE;
    }

    public function getModel() {
        return $this->hasOne(Model::className(), ['id' => 'model_id']);
    }

    /*     * ******************************************************************* */

    public static function setModelChildren() {
        $list = Yii::$app->db->createCommand("
                        -- SELECT pms.id, pms.name,  '#', m.producer_id, m.name AS Модель
                        -- FROM  `product_model_search` pms
                        -- LEFT JOIN model m ON m.name = pms.name
                        -- WHERE article = 010001
                        
                        SELECT m.`id` , m.`name`
                        FROM  `model` m
                        LEFT JOIN model_child mc ON m.id = mc.model_id
                        WHERE mc.model_id IS NULL
                        LIMIT 1000000;
                    ")->queryAll();
        foreach ($list as $value) {
            $model = SearchModel::find()
                    ->select('name')
                    ->where(['RLIKE', 'name', "^$value[name]"])
//                    ->offset(10)
                    ->asArray()
                    ->all();
            foreach ($model as $item) {
                $child = new ModelChild();
                $child->model_id = $value['id'];
                $child->name = $item['name'];
                vd($child->save(), false);
            }
        }
    }
    
    public static function deleteSpace() {
            $list = Yii::$app->db->createCommand("
                SELECT id, model_id, name,  TRIM(REPLACE(name, '+', '')) AS _name, 
                    GROUP_CONCAT(id   SEPARATOR ';') AS group_id,
                    COUNT(id) _count
                FROM `model_child`
                GROUP BY _name, model_id
                HAVING _count > 1       
            ")->queryAll();

        foreach ($list as $value) {
            vd($value, false);
            $result = explode(';', $value['group_id']);
            unset($result[0]);
            self::deleteAll(['id'=>$result]);
        }
    }

}
