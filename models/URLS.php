<?php
namespace app\models;

use Yii;

class Image extends  \yii\db\ActiveRecord{

    public $images_temp;
    
 

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "image";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            
            [['url', 'product_id','meta','thumb', 'title', 'description', 'is_delete'], 'safe'],
            [['url'], 'string', 'max' => 255],
//            [['product_id', 'is_delete', 'url'], 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'product_id' => "Товар",
            'is_delete' => "Удалить",
            'url' => "Изображение",
        ];
    }
    
    public function getProduct(){
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    
    public function scopes() {
        return array(
            'active' => ['condition' => 'image.is_delete = 0'],
        );
    }
    
    public function defaultScope() {
        return [ 'condition' => 'image.is_delete = 0'];
    }

    
    public function saveImage($model, $name, $i){
        $this->images_temp = \yii\web\UploadedFile::getInstanceByName($name."[$i]");
        $name2 = $name;
        if ($this->images_temp) {
            $serch = preg_match("/\.\w{3}$/u", $this->images_temp->name, $extension);
            $extens = mb_strtolower($extension[0]);
            $name = "img_" . $i . "_" . time();
            $public_path = Yii::$app->params['public_path'];
             
            $path = Yii::$app->basePath . "/$public_path/uploadfile/images/product/".$name;
            $temp = $this->images_temp->saveAs($path);
            $name = Product::setWaterMark($path, $name);
            $name = preg_replace('/(\/.*\/)?/u', '', $name);
            $this->url = $name;
            
            $this->product_id = $model->id;
            $this->save();
        }
        return  $name;
    }
    
    public static function updateImage(){
        $model = self::find()
                ->select('id, url')
//                ->where(['between', 'id', 0, 10])
                ->where(['id'=>1])
                ->all();
        foreach ($model as $value) {
            vd($value->url, false);
            echo "<br/>";
            $serch = preg_match("/^(.*)\.(\w{3})$/u", $value->url, $extension);
            $extens = mb_strtolower($extension[0]);
            
            $value->url = $extension[1].'_150x150.'.$extension[2];
            
            vd($extension, false);
            echo "<br/>";
            vd($value->url, false);
            echo "<br/>";
        }
        
    }

    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('product_id', $this->product_id, TRUE);
        $criteria->compare('url', $this->url, TRUE);
        $criteria->compare('image.is_delete', $this->is_delete, TRUE);
        $criteria->compare('id', $this->id, TRUE);
      
        return new CActiveDataProvider('Image', [
            'criteria'   => $criteria,
            'pagination' => ['pageSize' => isset($_GET['pagination']) ? $_GET['pagination'] * 1 : 10],
            'sort'       => ['defaultOrder' => 'id DESC']
        ]);
    }
    
    public static function alignImagesInProduct(){
        $model = self::find()
                ->join('LEFT JOIN', 'product p', 'p.id = image.product_id')
                ->where('p.id IS NULL')
                ->all();
        foreach ($model as $value) {
            vd($value->delete(), false);;
        }
//        vd($model, false);
        die("<br/>end");
    }
    
    /**
     * Создает изображения для товаров с старой таблицы joomla
     */
    public static function setImagesFoProduct($article){
        $list = Yii::$app->db->createCommand("
                        SELECT  p.id, p.article, `_original_id` ,file_url, file_meta, file_description, file_url_thumb
                        FROM `product` p
                        LEFT JOIN j251_virtuemart_product_medias jpm ON jpm.virtuemart_product_id = p._original_id
                        LEFT JOIN j251_virtuemart_medias  jm USING(virtuemart_media_id)
                        WHERE p.article = '$article' AND  jpm.virtuemart_media_id IS NOT NULL
                        GROUP BY _original_id, file_url
                    ")->queryAll();
        vd($list, false);
        die("<br/>end");
        
        $result = [];
        if(!empty($list)){
            foreach ($list as $value) {
                $url = preg_replace('/\w*\//i', '', $value['file_url']);
                $image = self::find()->where(['product_id'=>$value['id'], 'url'=>$url]);
                if(!$image->one()){
                    $image = new Image();
                    $image->product_id = $value['id'];
                    $image->article = $value['article'];
                    $image->title = $value['file_meta'];
                    $image->url = $url;
                    $image->thumb = $value['file_url_thumb'];
                    $image->description = $value['file_description'];
                    $result[$value['id']][] = $image->save();
                }
            }
        }else{
            $list = Yii::$app->db->createCommand("
                        SELECT  jp.virtuemart_product_id AS id, jp.product_sku AS article, 
                                TRIM(LEADING 'images/stories/virtuemart/product/' FROM file_url) AS url,   
                                TRIM(LEADING 'images/stories/virtuemart/product/resized/' FROM file_url_thumb) AS thumb,
                                file_meta AS title, 
                                file_description AS description   
                        FROM `j251_virtuemart_products` jp
                        LEFT JOIN j251_virtuemart_product_medias jpm USING(virtuemart_product_id)
                        LEFT JOIN j251_virtuemart_medias jm USING(virtuemart_media_id)
                        WHERE `product_sku` LIKE '%$article%'
                        GROUP BY article, url
                    ")->queryAll();
            if(!empty($list)){
                $product = Product::find()->where(['article'=>$list[0]['article']])->one();
                if($product){
                    foreach ($list as $value) {
                        $image = self::find()->where(['product_id'=>$product->id, 'url'=>$value['url']]);
                        if(!$image->one()){
                            $image = new Image();
                            $image->product_id = $product->id;
                            $image->article = $value['article'];
                            $image->title = $value['title'];
                            $image->thumb = $value['thumb'];
                            $image->url = $value['url'];
                            $image->description = $value['description'];
                            $result[$product->id][] = $image->save();
                        }
                    }
                    $product->_original_id = $list[0]['id'];
                    $result['id'] = $product->update();
                }
            }
        }
        return $result;
    }
    
    /**
     * Догенерирует изображения к товарам если были раннее созданы изображения по артикулу
     */
    public static function genereteImagesFoArticle($article){
        $product = Yii::$app->db->createCommand("
                        SELECT p.id, p.article, i.url
                        FROM `product` p
                        LEFT JOIN image i ON product_id = p.id
                        WHERE p.article = '$article' AND  i.url IS NULL
                    ")->queryAll();
        $result = [];
        if(!empty($product)){
            $iamgeOne = Yii::$app->db->createCommand("
                        SELECT i.*
                        FROM `image` i
                        WHERE i.article = '$article'
                    ")->queryOne();
            if($iamgeOne){
                $iamges = Yii::$app->db->createCommand("
                            SELECT i.*
                            FROM `image` i
                            WHERE i.article = '$article' AND product_id = $iamgeOne[product_id]
                        ")->queryAll();
                foreach ($product as $value) {
                    foreach ($iamges as $item) {
                        $image = self::find()->where(['product_id'=>$value['id'], 'url'=>$item['url']]);
                        if(!$image->one()){
                            $image = new Image();
                            $image->product_id = $value['id'];
                            $image->article = $value['article'];
                            $image->title = $item['title'];
                            $image->thumb = $item['thumb'];
                            $image->url = $item['url'];
                            $image->description = $item['description'];
                            $result[$value['id'].' '.$value['article']][] = $image->save();
                        }
                    }
                }
            }
            
        }
        return $result;
    }
    
    

}
