<?php
namespace app\models;
use Yii;



class MarketProduct extends \yii\db\ActiveRecord 
{
    public $_article;
    public static function model($className=__CLASS__){
            return parent::model($className);
    }

    public static function tableName() {
        return "_market_product";
    }
    /**
     * Распарыиваем яндекс маркет и заполняем таблицу
     * с данными
     */
    public static function setParams(){
        $urlSync = Yii::$app->basePath ;
//        $market  = file_get_contents($urlSync . "/web/prod240.xml");
        $market  = file_get_contents($urlSync."/web/delete/prod240_2.xml");
      
        $xml = simplexml_load_string($market);
        $json = json_encode($xml);
        $array = json_decode($json,TRUE);
//        vd($array, false);
        
        foreach ($array['shop']['offers']['offer'] as $value) {
            $model = self::findOne(['id_product'=>$value['@attributes']['id']]);
            if(!$model){
                $model = new self();
                $model->id_product = isset($value['@attributes']['id']) ? $value['@attributes']['id'] : null ;
                $model->category_Id = isset($value['categoryId']) ? $value['categoryId'] : null;
                $model->category = isset($value['typePrefix']) && !is_array($value['typePrefix'])  ? $value['typePrefix'] : null;
                $model->producer = isset($value['vendor']) ? $value['vendor'] : null;
                $model->name = isset($value['model']) ? $value['model'] : null;
                $model->description = isset($value['description']) ? $value['description'] : null;
                if(isset($value['param']) && is_array($value['param']))
                    $model->param = implode(",", $value['param']);
                else 
                    $model->param = null;
                    
                if(isset($value['param']) && !is_array($value['param']))
                    $model->param = $value['param'];
                else
                    $model->param = null;
                
                vd($model, false);
                $model->save();
            }
        }
    }
    
    /*
     * Дополняем значения фильтров с маркета
     */
    public static function setFilterInMarket(){
        $filterValueBD = self::getFilterBD();
        foreach (self::getArrayFilterInmarket() as $key => $value) {
            foreach ($value as $k=> $item) {
                if($k != 'cat'){
                    /********1 - дозаполняем фильтры************/
//                    $filter = Filter::find()
//                            ->where([
//                                'category_id'=>$value['cat']['id'],
//                                'name'=>$k,
//                            ]);
//                    if(!$filter->one())
//                        self::createFilter($k, $value['cat']['id']);
                    
                    /**2 - альтерацыя дозаполняем значения фильтров****/
//                    $filter_id = array_search($value['cat']['id']."=".$k, $filterValueBD);
//                    $filterValue = FilterValue::find()
//                            ->where([
//                                'filter_id'=>$filter_id,
//                                'name'=>self::getNormaliseName($item),
//                            ]);
//                    if(!$filterValue->one()){
//                        self::createFilterValue($item, $filter_id);
//                    }
                }
            }
        }
    }
    
    /**
     * Изменяет или добавляет " " или "-" в названии
     */
    public static function getNormaliseName($name) {
        if (preg_match("/[^\s]([VA])$/iu", $name)) {
            $name = preg_replace("/V$/iu", ' V', $name);
            $name = preg_replace("/A$/iu", ' A', $name);
        };
        if (preg_match("/(\spin)$/iu", $name)) {
            $name = preg_replace("/\spin/ui", "-pin", $name);
        }
        return  trim($name);
    }

    /**
     * Создает несуществующие фильтры
     */
    public static function createFilter($name, $category_id){
        $filter = new Filter();
        $filter->_market = 1;
        $filter->category_id = $category_id;
        $filter->name = $name;
        $filter->save();
    }
    
    /**
     * Создает несуществующие значения фильтров
     */
    public static function createFilterValue($name, $filter_id){
        $filterValue = new FilterValue();
        $filterValue->_market = 1;
        $filterValue->filter_id = $filter_id;
        $filterValue->name = $name;
        $filterValue->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($filterValue->name);
        vd($filterValue->save(), false);;
    }

    

    public static function getArrayFilterInmarket() {
        $model = self::find()
                ->where(['not', ['description' => null]])
                ->select('id, description, category')
                ->groupBy('description, category')
                ->asArray()->all();
        
        $category = self::getCategoryBD();
        $result = [];
        foreach ($model as $key => $value) {
            if (preg_match("/,\s/i", $value['description'])) {
                foreach (preg_split("/,\s/", $value['description']) as $item) {
                    $filter = explode(":", $item);
                    $result[$key][trim($filter[0])] = trim($filter[1]);
                }
            } else {
                $filter = explode(":", $value['description']);
                $result[$key][trim($filter[0])] = trim($filter[1]);
            }
            $result[$key]['cat']['name'] = self::getNameCategoryInBD($value['category']);
            $result[$key]['cat']['id'] = array_search($result[$key]['cat']['name'], $category);
        }
        return $result;
    }
    
    /**
     * устанавливает фильтры с соответственно товаров в марките
     */
    public static function setFilterInBD(){
//        vd(self::getFilterInProduct(), false);
//        die("<br/>end");
        foreach (self::getFilterInProduct() as $key => $value) {
            foreach ($value['filter'] as $k =>$item) {
                $prodFiltVal = ProductFilterValue::find()
                    ->where(
                            "article = $value[article] "
                            . "AND category_id = $value[category] "
                            . "AND producer_id = $value[producer] "
                            . "AND serie_id = $value[serie] "
                            . "AND filter_id = $k "
                    )
                    ->one();
                
//                if(isset($_GET['q']) && $productFilterValue){
//                    $query->orderBy('error');
//                    $query->one();
//                }
                if($prodFiltVal){
                    if($prodFiltVal->filter_value_id != $item){
                        
                        vd($prodFiltVal, false);
                        echo $item;
                        echo "<br/>";
                            $prodFiltVal->filter_value_id = $item;
                            vd($prodFiltVal->update(), false);;
                        vd($prodFiltVal, false);
                        echo "<br/>";
                        echo "<br/>";
                        echo "<br/>";
                    }
                    
                    
//                    vd($query->createCommand()->getRawSql(), false);
//                    $prodFiltVal = $query->one();
//                    $prodFiltVal->filter_value_id = $item;
//                    vd($prodFiltVal, false);
//                    vd($prodFiltVal->update(), false);
//                    echo "<br/>";
//                    echo "<br/>";
                }else{
                    
                }
                
//                if(!$productFilterValue){
//                    $productFilterValue->orderBy('id_');
//                    $productFilterValue->one();
//                    echo $key;
//                    vd($value, false);
//                    die("<br/>end");
//                    echo "<br/>";
//                }
            }
            
        }
        
    }

    /**
     * получим уникальные товары с фильтрами
     * в масиве нет товаров с нераспознанных серий
     */
    public static function getFilterInProduct(){
        $model = self::find()
                ->select('article, description, category, producer, name')
                ->where(['not',['article'=>'']])
                ->andWhere(['not',['description'=>'']])
//                ->groupBy('article')
                ->limit(1000)
                ->offset(5000)
                ->asArray()->all();
        

        $category = self::getCategoryBD();
        $filterArray = self::getFilterBD();
        $filterValueArray = self::getFilterValueBD();
        $producerArray = self::getProducerBD();
        $serieArray = self::getSerieBD();
        
        $result = [];
        foreach ($model as $key => $value) {
            $cat = array_search(self::getNameCategoryInBD($value['category']), $category);
            $prod_id =  array_search($value['producer'], $producerArray);
            $serie_id = self::getSerieID($serieArray, $prod_id, $value['name']);
            if($serie_id){
                $result[$key]['serie'] = $serie_id;
                $result[$key]['article'] = $value['article'];
                $result[$key]['category'] = $cat;
                $result[$key]['producer'] = $prod_id;

                if (preg_match("/,\s/i", $value['description'])) {
                    foreach (preg_split("/,\s/", $value['description']) as $item) {
                        $filter = explode(":", $item);
                        $filter_id = array_search($cat."=".trim($filter[0]), $filterArray);
                        $nameValue = self::getNormaliseName(trim($filter[1])); 
                        $filterValue_id = array_search($filter_id."=".$nameValue, $filterValueArray);
                        $result[$key]['filter'][$filter_id] = $filterValue_id;
                    }
                } else {
                    $filter = explode(":", $value['description']);
                    $filter_id = array_search($cat."=".trim($filter[0]), $filterArray);
                    $nameValue = self::getNormaliseName(trim($filter[1])); 
                    $filterValue_id = array_search($filter_id."=".$nameValue, $filterValueArray);
                    $result[$key]['filter'][$filter_id] = $filterValue_id;
                }
            }
        }
        return $result;
    }
    
    /**
     * Возвращает id серии
     */
    public static function getSerieID($serieArray, $prod_id, $name){
        $array = [];
        foreach ($serieArray as $key => $serie) {
            $dataSerie = explode("=", $serie);
            if($prod_id == $dataSerie[0] && preg_match("/$dataSerie[1]/i", $name)){
                $array[$key] = $dataSerie[1];
            }
        }
        $serie_id = 0;
        $length = 0; 
        foreach ($array as $key => $value) {
            if($length < strlen($value)){
                $serie_id = $key;
                $length = strlen($value);
            }
        }
        return $serie_id;
    }

        /**
     * Устанавливаем артиклы
     */
    public static function setArticle(){
        $product = self::find()
                ->select('_market_product.id, _market_product.article, id_product, product.article AS _article')
                ->where(['_market_product.article'=>''])
                ->join(	'INNER JOIN', 'product', '_market_product.id_product =product._original_id') 
                ->limit(1000)
                ->all();
        foreach ($product as $value) {
            $value->article = $value->_article;
            vd($value->save(), false);;
        }
    }

    /**
     * скрипт соответствий названия категорий с маркета и в БД
     */
    public static function getNameCategoryInBD($categoryName) {
        switch ($categoryName) {
            case 'Аккумулятор'      : return 'Аккумуляторы';
            case 'Блок питания'     : return 'Блоки питания';
            case 'Вентилятор'       : return 'Вентиляторы';
            case 'Клавиатура'       : return 'Клавиатуры';
            case 'Матрица'          : return 'Матрицы';
            case 'Петли для матриц' : return 'Петли для матриц';
            case 'Петля для матриц' : return 'Петли для матриц';
            case 'Разъём питания'   : return 'Разъёмы питания';
            case 'Разъем питания'   : return 'Разъёмы питания';
            case 'Шлейф для матриц' : return 'Шлейфы для матрицы';
            case 'Шлейф для матрицы': return 'Шлейфы для матрицы';
            case 'Шлейф матрицы'    : return 'Шлейфы для матрицы';
            case 'Термопаста КПТ'   : return 'Термопаста КПТ';
            default                 : return $categoryName;
        }
    }

    /**
     * Масив категорий с Базы данных
     */
    public static function getCategoryBD(){
        $category = Category::find()
                ->where(['is_delete'=>0])
                ->select('id, name')
                ->asArray()
                ->all();
        
        $result = [];
        foreach ($category as $value) {
            $result[$value['id']] = $value['name'];
        }
        return $result;
    }
    /**
     * Масив фильтров с Базы данных
     */
    public static function getFilterBD(){
        $filter = Filter::find()
                ->select('id, name, category_id')
                ->asArray();
        
        $result = [];
        foreach ($filter->all() as $value) {
            $result[$value['id']] = $value['category_id']."=".$value['name'];
        }
        return $result;
    }
    
    /**
     * Масив значений фильтров с Базы данных
     */
    public static function getFilterValueBD(){
        $filterValue = FilterValue::find()
                ->select('filter_value_id, filter_id, name')
                ->asArray();
        $result = [];
        foreach ($filterValue->all() as $value) {
            $result[trim($value['filter_value_id'])] = trim($value['filter_id']."=".$value['name']);
        }
        return $result;
    }
    
    /**
     * Масив производителей с Базы данных
     */
    public static function getProducerBD(){
        $producer = Producer::find()
                ->select('id, name')
                ->asArray();
        $result = [];
        foreach ($producer->all() as $value) {
            $result[trim($value['id'])] = trim($value['name']);
        }
        return $result;
    }
    /**
     * Масив серий с Базы данных
     */
    public static function getSerieBD(){
        $producer = Serie::find()
                ->select('id, producer_id, name')
                ->asArray();
        $result = [];
        foreach ($producer->all() as $value) {
            $result[trim($value['id'])] = $value['producer_id']."=".trim($value['name']);
        }
        return $result;
    }

}