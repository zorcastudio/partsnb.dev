<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;


class SearchModel extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "product_model_search";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['article', 'name'], 'required'],
            [['article', 'name'], 'isUnique'],
            [['article'], 'string', 'max' => 40],
            [['name'], 'string', 'max' => 100]
        ];
    }
    
    public function isUnique(){
        $serch = self::findOne(['name'=>$this->name, 'article'=>$this->article]);
        if($serch){
            $this->addError('name', 'Название модели и артикль должны быть уникальными');
            return false;
        }
        return TRUE;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'id' => "ID",
            'article' => "Артикль",
            'name' => "Название модели",
        ];
    }
    
    /**
     * масив всех моделей которые есть у того товара
     */
    public static function getModelArray($article){
        $model = self::find()
                ->where(['article'=>$article])
                ->asArray()
                ->all();
        
        $result = [];
        foreach ($model as $value) {
            $result[$value['id']] = $value['name'];
        }
        return $model ? $result : [];
    }
    
    /**
     * масив всех моделей отсортированных по данному производителю
     */
    public static function getModelSortArray($model){
//        if($model->producer){
//        $listModelProducer = Yii::$app->db->createCommand("
//                        SELECT  article, name
//                        FROM  `product_model_search`
//                        WHERE article = $model->article
//                    ")->queryAll();
//                            -- AND name LIKE '%".$model->producer->name."%'
//        }
        
        $model = self::find()
                ->where(['article'=>$model->article])
                ->asArray()
                ->all();

        $result = [];
        foreach ($model as $value) {
            $result[$value['id']] = $value['name'];
        }
        return $model ? $result : [];
    }
    
    /**
     * масив всех моделей (id) которые есть у  товара
     */
    public static function getModelArrayId($article){
        $model = self::find()
                ->where(['article'=>$article])
                ->asArray()
                ->all();
        $result = [];
        foreach ($model as $value) {
            $result[] = $value['id'];
        }
        return $model ? $result : [];
    }

    /**
     * созданиеи/удаление моделей для поиска
     */
    public static function updateModel($model, $POST) {
        if (isset($POST['GenereteProduct']['_modelSearch'])) {
            $setId = self::getModelArrayId($model->article);
            $modelSearch = !empty($POST['GenereteProduct']['_modelSearch']) ? $POST['GenereteProduct']['_modelSearch'] : [];
            
            //создаем новые парт-номера
            foreach (array_diff($modelSearch, $setId) as $name) {
                $pnSearch = new self();
                $pnSearch->article = $model->article;
                $pnSearch->name = $name;
                $pnSearch->save();
            }
            
            //удаляем парт-номера
            foreach (array_diff($setId, $modelSearch) as $id) {
                $pnSearch = self::findOne($id);
                $pnSearch->delete();
            }
        }
    }

    /**
     * заполняем таблицу
     */
    public static function setArticle($f, $l){
        $product = Product::find()
                ->where(['not', ['_list_model'=>null]])
                ->select('article, _list_model, id')
                ->andWhere(['between', 'id', $f, $l])
                ->asArray()
                ->all();
        $result =[];
        foreach ($product as $value) {
            $list_model = json_decode($value['_list_model']);

            foreach ($list_model as $item) {
                $serchModel = self::findOne(['article'=>$value['article'], 'name'=>$item]);
                if(!$serchModel){
                    $serchModel = new self();
                    $serchModel->article = $value['article'];
                    $serchModel->name = $item;
                    $serchModel->save();
                }
            }
            $result[] = $value['article'];
        }
        return $result;
    }
    
      /**
     * ищем в тексте модель
     * возвращаем его article
     * @param type $serch
     * @return type
     */
    public static function seartchModel($serch) {
//        $serch = preg_replace("/[а-яА-Я]*/u", '', $serch);
        $serch = preg_replace("/^\s*/u", "", $serch);
        $serch = preg_replace("/\s*$/u", "", $serch);
        $model = self::find()
                ->where(['like', 'name', "%".$serch."%", false])
                ->select('article')
                ->distinct()
                ->asArray()
                ->all();
        if(!empty($model)){
            $result = [];
            foreach ($model as $value) {
                $result['article'][] = $value['article'];
            }
            return $result;
        }else
            return false;
    }
    
    /**
     * Добавляєм новий запис
     */
    public static function createModelSearch($product, $name){
        $search = self::find()
                    ->where([
                        'article'=>$product->article,
//                        'producer_id'=>$product->producer_id, тимчасово закоментовано
                        'name'=>"$name",
                    ])
                    ->one();
        if(!$search){
            $search = new self();
            $search->article = $product->article;
            $search->name = "$name";
            $search->producer_id = $product->producer_id;
            $search->createtime = time();
            return $search->save() ? TRUE : FALSE;
        }
    }
}
