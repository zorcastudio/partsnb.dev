<?php

namespace app\models;

use Yii;
use yii\base\Model;

class settingValues extends Model {

    public static function Connection() {
        $connection = new \yii\db\Connection([
            'dsn' => 'mysql:host=localhost;dbname=partsnb_jumla',
            'username' => 'partsnb_jumla',
            'password' => 'vvVpteUznR8SFXmP',
            'charset' => 'utf8',
        ]);
        $connection->open();
        return $connection;
    }

    public static function getProperties() {
        $connect = self::Connection();
        return $connect->createCommand("SELECT virtuemart_custom_id, custom_title  FROM  j251_virtuemart_customs WHERE virtuemart_custom_id > 2")->queryAll();
    }

    public static function getProductCustomfields() {
        $connect = self::Connection();
        return $connect->createCommand(
                        "SELECT virtuemart_customfield_id, virtuemart_product_id, virtuemart_custom_id,custom_value, ordering   FROM  j251_virtuemart_product_customfields"
//                        . " WHERE virtuemart_product_id = 131 ORDER BY virtuemart_product_id ASC"
                )->queryAll();
    }

    public static function getCategories() {
        $connect = self::Connection();
        return $connect->createCommand(
                        "SELECT category_name, slug, published, category_watermark FROM  j251_virtuemart_categories_ru_ru c"
                        . " JOIN j251_virtuemart_categories cr ON c.virtuemart_category_id = cr.virtuemart_category_id"
                )->queryAll();
    }

    /**
     * установка значений свойст товаров
     */
    public static function setCategories() {
        $result = self::getCategories();
        foreach ($result as $value) {
//            $categories = Category::find()->where(['name' => $value['category_name']])->one();
//            $categories = new Category();
//            if (!$categories) {
            $categories = new \app\models\Category();
            $categories->name = $value['category_name'];
            $categories->name_lat = $value['slug'];
            $categories->image = $value['category_watermark'];
            $categories->is_delete = !$value['published'];
            if (!$categories->save()) {
                return false;
            };
//            }
        }
        return true;
    }

    /**
     * установка значений соответствий товара и свойств
     */
    public static function setProductProperties() {
        $result = self::getProductCustomfields();
        foreach ($result as $value) {
            $productProperties = new \app\models\ProductProperties();

            $productProperties->v_customfield_id = $value['virtuemart_customfield_id'];

            $product = Product::find()->where(['relationship' => $value['virtuemart_product_id']])->one();
            $product ? ($productProperties->product_id = $product->id) : null;


            $productProperties->value = $value['custom_value'];
            $productProperties->ordering = $value['ordering'];

            $properties = Properties::find()->where(['v_mart_id' => $value['virtuemart_custom_id']])->one();
            $properties ? ($productProperties->properties_id = $properties->id) : null;


            if (!$productProperties->save()) {
                return false;
            };
        }
        return true;
    }

    /**
     * установка значений свойст товаров
     */
    public static function setProperties() {
        $result = self::getProperties();
        foreach ($result as $value) {
            $customs = new \app\models\Properties();
            $customs->v_mart_id = $value['virtuemart_custom_id'];
            $customs->name = $value['custom_title'];
            if (!$customs->save()) {
                return false;
            };
        }
        return true;
    }
    
    public static function getImage($relationship) {
        $connect = self::Connection();
        return $connect->createCommand(
                        "SELECT * FROM j251_virtuemart_product_medias pm "
                        . " JOIN j251_virtuemart_medias m ON m.virtuemart_media_id = pm.virtuemart_media_id WHERE pm.virtuemart_product_id = $relationship"
                )->queryAll();
    }

    public static function setImage($relationship, $id) {
        $result = self::getImage($relationship);
        foreach ($result as $value) {
            $img = new \app\models\Image();
            $img->product_id = $id;
            $img->meta = $value['file_meta'];
            $img->url = preg_replace('/images\/stories\/virtuemart\/product\//', '', $value['file_url']);
            $img->thumb = preg_replace('/images\/stories\/virtuemart\/product\/resized\//', '', $value['file_url_thumb']);
            $img->title = $value['file_title'];
            $img->description = $value['file_description'];
            if (!$img->save()) {
                return false;
            };
        }
        return true;
    }
    
    
     /**
     * Возвращает все товары с диапазона
     * только virtuemart_product_id
     * для проверки на наличии такого товара в новой базе
     */
    public static function getOnliProduct() {
        $connect = self::Connection();
        return $connect->createCommand(
                        'SELECT virtuemart_product_id FROM j251_virtuemart_products_ru_ru'
                        .' WHERE virtuemart_product_id BETWEEN 31000 AND 40000'
                )->queryAll();
     }
    
    /**
     * получаем полную информацыю о товаре с таблицы джумлы
     */
    public static function getFullProduct($id){
        $connect = self::Connection();
        $result = $connect->createCommand(
                        'SELECT *'
                        . ' FROM j251_virtuemart_products_ru_ru p'
                        . ' JOIN j251_virtuemart_product_prices pp ON p.virtuemart_product_id = pp.virtuemart_product_id'
                        . ' JOIN j251_virtuemart_product_medias pm ON p.virtuemart_product_id = pm.virtuemart_product_id AND pm.ordering = 1'
                        . ' JOIN j251_virtuemart_medias m ON m.virtuemart_media_id = pm.virtuemart_media_id'
                        . ' JOIN j251_virtuemart_products  vp ON vp.virtuemart_product_id = p.virtuemart_product_id'
                        . ' JOIN j251_virtuemart_product_categories cp ON p.virtuemart_product_id = cp.virtuemart_product_id '
                        . ' WHERE p.virtuemart_product_id = '.$id
            )->queryOne();
        return $result;
    }
    
    /**
     * товар без изображений
     */
    public static function getNotImageProduct($id){
        $connect = self::Connection();
        $result = $connect->createCommand(
                        'SELECT *'
                        . ' FROM j251_virtuemart_products_ru_ru p'
                        . ' JOIN j251_virtuemart_product_prices pp ON p.virtuemart_product_id = pp.virtuemart_product_id'
                        . ' JOIN j251_virtuemart_products  vp ON vp.virtuemart_product_id = p.virtuemart_product_id'
                        . ' JOIN j251_virtuemart_product_categories cp ON p.virtuemart_product_id = cp.virtuemart_product_id '
                        . ' WHERE p.virtuemart_product_id = '.$id
            )->queryOne();
        return $result;
    }
    
    /**
     * товар без изображений и без категории
     */
    public static function getNotCategoriNotImage($id){
        $connect = self::Connection();
        $result = $connect->createCommand(
                        'SELECT *'
                        . ' FROM j251_virtuemart_products_ru_ru p'
                        . ' JOIN j251_virtuemart_product_prices pp ON p.virtuemart_product_id = pp.virtuemart_product_id'
                        . ' JOIN j251_virtuemart_products  vp ON vp.virtuemart_product_id = p.virtuemart_product_id'
                        . ' WHERE p.virtuemart_product_id = '.$id
            )->queryOne();
        return $result;
    }
    /**
     * товар без изображений и без категории
     */
    public static function getNot($id){
        $connect = self::Connection();
        $result = $connect->createCommand(
                        'SELECT *'
                        . ' FROM j251_virtuemart_products_ru_ru p'
//                        . ' JOIN j251_virtuemart_products  vp ON vp.virtuemart_product_id = p.virtuemart_product_id'
                        . ' WHERE p.virtuemart_product_id = '.$id
            )->queryOne();
        return $result;
    }
   
    /**
     * ищет пропущенніе товары
     */
    public static function setUpdateProduct(){
        $result = self::getOnliProduct();
        $i = 0;
        foreach ($result as $value) {
            $product = \app\models\Product::find()
                    ->where(['_original_id'=>$value['virtuemart_product_id']])
                    ->select('id, _original_id')
                    ->one();
             
            (++$i%100) != 0 ?: vd("i_$i"."_".$value['virtuemart_product_id'], false);;
            
            if(!$product){
                $productFull = self::getFullProduct($value['virtuemart_product_id']);
                $productFull = $productFull ?: self::getNotImageProduct($value['virtuemart_product_id']);
                $productFull = $productFull ?: self::getNotCategoriNotImage($value['virtuemart_product_id']);
                $productFull = $productFull ?: self::getNot($value['virtuemart_product_id']);
                
                
                if(!$productFull){
                    vd($value['virtuemart_product_id'], false);
                    return;
                }
                
                
                $model = new \app\models\Product();
                $model->name = $productFull['product_name'];
                $model->name_lat = $productFull['slug'];
                $model->description_short = $productFull['product_s_desc'];
                $model->description = $productFull['product_desc'];
                $model->category_id = isset($productFull['virtuemart_category_id']) ? $productFull['virtuemart_category_id'] : null;
                $model->price = isset($productFull['product_price']) ? $productFull['product_price'] : 0;

                $model->article = isset($productFull['product_sku']) ? $productFull['product_sku'] : 0;
                $model->in_stock1 = isset($productFull['product_in_stock_1']) ? $productFull['product_in_stock_1'] : 0;
                $model->in_stock2 = isset($productFull['product_in_stock_2']) ? $productFull['product_in_stock_2'] : 0;
                $model->in_res1 = isset($productFull['product_in_res_1']) ? $productFull['product_in_res_1'] : 0;
                $model->in_res2 = isset($productFull['product_in_res_2']) ? $productFull['product_in_res_2'] : 0;
                $model->available = isset($productFull['product_availability']) ? $productFull['product_availability'] : 0;
    //            $model->warranty_months = $productFull['custom_value'];

                $model->status = 1;
                $img = isset($productFull['file_url']) ? preg_replace('/images\/stories\/virtuemart\/product\//', '', $productFull['file_url']) : false;
                if($img){
                    $img = preg_replace('/\.\w+/', '', $img);
                    $model->logo = $img . "_150x150.jpg";
                }
                $model->_original_id = 1 * $productFull['virtuemart_product_id'];
                $model->_is_new = 1; //дозагруженные товары

                if (!$model->save()) {
                    return false;
                };
            }
        }
        
        
    }
    
    
    public static function getProduct() {
        $connect = self::Connection();
        return $connect->createCommand(
                        'SELECT *'
//                            . 'product_name, slug, product_s_desc, product_desc, virtuemart_category_id, product_price, product_sku, '
//                            . 'product_in_stock_1, product_in_stock_2, product_in_res_1, product_in_res_2, product_availability, custom_value, '
//                            . 'file_url, virtuemart_product_id'
                        . ' FROM j251_virtuemart_products_ru_ru p '
                        . ' JOIN j251_virtuemart_product_prices pp ON p.virtuemart_product_id = pp.virtuemart_product_id'
                        . ' JOIN j251_virtuemart_product_medias pm ON p.virtuemart_product_id = pm.virtuemart_product_id AND pm.ordering = 1'
                        . ' JOIN j251_virtuemart_medias m ON m.virtuemart_media_id = pm.virtuemart_media_id'
                        . ' JOIN j251_virtuemart_products  vp ON vp.virtuemart_product_id = p.virtuemart_product_id'
//                        . ' JOIN j251_virtuemart_product_customfields  pc ON pc.virtuemart_product_id = p.virtuemart_product_id '
//                        . ' JOIN j251_virtuemart_product_categories cp ON p.virtuemart_product_id = cp.virtuemart_product_id WHERE p.virtuemart_product_id <= 1000' //  AND pc.virtuemart_custom_id = 7
                        . ' JOIN j251_virtuemart_product_categories cp ON p.virtuemart_product_id = cp.virtuemart_product_id WHERE p.virtuemart_product_id BETWEEN ' . $_GET['f'] . ' AND ' . $_GET['l'] . '' // WHERE fld1  AND pc.virtuemart_custom_id = 7
                )->queryAll();
    }
    

    public static function setProduct() {
        $result = self::getProduct();
        foreach ($result as $value) {
//            $model = Product::find()->where(['_original_id' => $value['virtuemart_product_id']])->one();

            $model = new \app\models\Product();

            $model->name = $value['product_name'];
            $model->name_lat = $value['slug'];
            $model->description_short = $value['product_s_desc'];
            $model->description = $value['product_desc'];
            $model->category_id = $value['virtuemart_category_id'];
            $model->price = $value['product_price'];

            $model->article = $value['product_sku'];
            $model->in_stock1 = $value['product_in_stock_1'];
            $model->in_stock2 = $value['product_in_stock_2'];
            $model->in_res1 = $value['product_in_res_1'];
            $model->in_res2 = $value['product_in_res_2'];
            $model->available = $value['product_availability'] ? $value['product_availability'] : 0;
//            $model->warranty_months = $value['custom_value'];

            $model->status = 1;
            $img = preg_replace('/images\/stories\/virtuemart\/product\//', '', $value['file_url']);
            $img = preg_replace('/\.\w+/', '', $img);
            $model->logo = $img . "_150x150.jpg";
            $model->_original_id = 1 * $value['virtuemart_product_id'];

            if (!$model->save()) {
                return false;
            };
        }
        return true;
    }

    public static function getData() {
        $result = 'KeyCard Admin</div>Пица Успех</div>Sushi</div>Silver cafe</div>пароль</div>parol2</div>parol3</div>parol4</div>parol5</div>parol6</div>Sexshop </div>Калипсо</div>keycard</div>Название </div>Proba1 rus</div>Par1 Русс </div>Reg1 рус</div>proba2 hec</divproba3 русс</div>тест4</div>Proba4 русс</div>ffdfffff</div>тест4</div>Департамент охраны</div>пап</div>Beerwood</div>Планета Спорт</div>тест9</div>test8</div>Планета Спорт</div>Пица Успех</div>тест11</div>Shariki.ua</div>test12</div>Лайнэкс</div>lotsman</div>Hooligans Bar</div>tect 14</div>proba13 рус</div>Snapme Studio</div>Картмания</div>E-POOL</div>ДЕПАРТАМЕНТ ОХРАНЫ</div>MARINA YACHTING</div>INDIGO</div>Старт-борд</div>Parure</div>Мастерская дизайна "Мадиз"</div>TERRASPORT.UA</div>test13</div>тест15</div>VAK Akademy</div>VT HOLDING</div>partner</div>Сеть салонов VT SALONS</div>test</div>MODENA</div>asd';
        return explode("</div>", $result);
    }

    /**
     * обновление описаний товаров 
     * не у в сех товаров есть полнное описание
     */
    public static function getForUpdateProduct($id) {
        $connect = self::Connection();
        return $connect->createCommand(
                        'SELECT *'
                            . 'product_name, slug, product_s_desc, product_desc, virtuemart_category_id, product_price, product_sku, '
                            . 'product_in_stock_1, product_in_stock_2, product_in_res_1, product_in_res_2, product_availability, custom_value, '
                            . 'file_url, virtuemart_product_id'
                        . ' FROM j251_virtuemart_products_ru_ru p '
                        . ' JOIN j251_virtuemart_product_prices pp ON p.virtuemart_product_id = pp.virtuemart_product_id'
                        . ' JOIN j251_virtuemart_product_medias pm ON p.virtuemart_product_id = pm.virtuemart_product_id AND pm.ordering = 1'
                        . ' JOIN j251_virtuemart_medias m ON m.virtuemart_media_id = pm.virtuemart_media_id'
                        . ' JOIN j251_virtuemart_products  vp ON vp.virtuemart_product_id = p.virtuemart_product_id'
                        . ' JOIN j251_virtuemart_product_customfields  pc ON pc.virtuemart_product_id = p.virtuemart_product_id '
                        . ' JOIN j251_virtuemart_product_categories cp ON p.virtuemart_product_id = cp.virtuemart_product_id WHERE p.virtuemart_product_id <= 1000' //  AND pc.virtuemart_custom_id = 7
                        . ' JOIN j251_virtuemart_product_categories cp ON p.virtuemart_product_id = cp.virtuemart_product_id WHERE p.virtuemart_product_id BETWEEN '.$_GET['f'].' AND '.$_GET['l'].'' // WHERE fld1  AND pc.virtuemart_custom_id = 7
                        . ' WHERE p.virtuemart_product_id=' . $id // WHERE fld1  AND pc.virtuemart_custom_id = 7
                )->queryOne();
    }

    public static function updateProduct() {
        $product = self::getForUpdateProduct();
        $model = Product::find()->where(['description_short' => ''])
                ->andWhere(['between', 'id', 0, 5000])
                ->all();


        foreach ($model as $value) {
            $old = self::getForUpdateProduct($value->_original_id);
            if ($value->description_short != $old['product_s_desc'] || $value->description != $old['product_desc']) {
                echo 1;
            }
            $value->description_short = $old['product_s_desc'];
            $value->description = $old['product_desc'];
            $value->update();
        }
    }


    /**
     * Удаляет лишнее с описания
     */
    public static function replacePars($string){
        
        $string = preg_replace('/Делл /i', '', $string);
        $string = preg_replace('/белая/i', '', $string);
        $string = preg_replace('/серия/i', '', $string);
        $string = preg_replace('/серии/i', '', $string);
        $string = preg_replace('/Серии/i', '', $string);
        $string = preg_replace('/Серий/i', '', $string);
        $string = preg_replace('/двойной/i', '', $string);
        $string = preg_replace('/\(при изменении полярности\)/i', '', $string);
        $string = preg_replace('/с системой охлаждения/i', '', $string);
        $string = preg_replace('/без рамки/i', '', $string);
        $string = preg_replace('/с рамкой/i', '', $string);
        $string = preg_replace('/с подсветкой/i', '', $string);
        $string = preg_replace('/с подсветкой/i', '', $string);
        $string = preg_replace('/серебристая/i', '', $string);
        $string = preg_replace('/серая рамка/i', '', $string);
        $string = preg_replace('/чёрная рамка/i', '', $string);
        $string = preg_replace('/рамка имеет отличия по крпежам/i', '', $string);
        $string = preg_replace('/рамка/i', '', $string);
        $string = preg_replace('/Эйсер/i', '', $string);
        $string = preg_replace('/cерии/i', '', $string);
        $string = preg_replace('/глянцевая/i', '', $string);
        $string = preg_replace('/версия \d/i', '', $string);
        $string = preg_replace('/имеет отличия по крепежам/i', '', $string);
        $string = preg_replace('/с панелью/i', '', $string);
        $string = preg_replace('/с камерой/i', '', $string);
        $string = preg_replace('/серий/i', '', $string);
        $string = preg_replace('/система охлаждения 1/i', '', $string);
        $string = preg_replace('/система охлаждения 2/i', '', $string);
        $string = preg_replace('/\(не для NV55C05c\)/i', '', $string);
        $string = preg_replace('/\(ши.*?\)/i', '', $string);
        $string = preg_replace('/\(заглушки\)/i', '', $string);
        $string = preg_replace('/розовая/i', '', $string);
        $string = preg_replace('/коричневая/i', '', $string);
        $string = preg_replace('/для INTEL 945/i', '', $string);
        $string = preg_replace('/и др/i', '', $string);
        $string = preg_replace('/1660м/i', '1660M', $string);
        $string = preg_replace('/не совместим с .*?<\/li>/i', '', $string);
        $string = preg_replace('/\(Дискретная видеокарта\)/i', '', $string);
        $string = preg_replace('/<span.*?>/', '', $string);
        $string = preg_replace('/<div.*?>/', '', $string);
        $string = preg_replace('/<\/span>/', '', $string);
        $string = preg_replace('/<\/div>/', '', $string);
        $string = preg_replace('/(&quot;)*/', '', $string);
        $string = preg_replace('/&nbsp;*/', ' ', $string);
        $string = preg_replace('/((<h\d.*?>.*?<\/h\d>)|(<p.*?>.*<\/p>))/', '', $string);
        $string = preg_replace('/^.*<\/((h\d)|p)>/', '', $string);
        $string = preg_replace('/.*<\/p>/', '', $string);
        $string = preg_replace('/<h2(.*)?>.*$/ ', '', $string);
        $string = preg_replace('/(<ul.*?>|<\/ul>)/', '', $string);
        $string = preg_replace('/<\/li>/', '', $string);
        $string = preg_replace('/<li(.*?)>/', '|', $string);
        $string = preg_replace('/((<li(.)*>(\s)*)|(<br \/>\s*))/', '|', $string);
        $string = preg_replace('/<!--.*?-->/', '|', $string);
        $string = preg_replace('/^\|.*?с/', '|', $string);
        $string = preg_replace('/\s*?\|/', '|', $string);
        $string = preg_replace('/\|\s*/', '|', $string);
        $string = preg_replace('/((^\s*)|(\s*$))/', '', $string);
        return $string;
        $string = preg_replace('/\s*\./', '', $string);
    }
    
    /**
     * Запускает распарсивание товаров на модели и парт номера
     */
    public static function setListModel() {
        $model = Product::find()
                ->where(['not', ['description' => '']])
                ->andWhere(['like','article','07%', FALSE])
                ->andWhere([
                    'status'=>1,
                    'is_delete'=>0,
                ])
//                ->andWhere(['between', 'id', 30000, 40000])
                ->select('id, article, name, description')
                ->all();
        vd($model, false);
        foreach ($model as $value) {
            $value->_list_model = self::setParsModel($value->description, $value->id);
//            $value->_list_pn = self::setParsPN($value->description, $value->id);
            $value->update();
        }
        return true;
    }

    /**
     * парсит модели с описания товара
     */
    public static function setParsModel($description, $id) {
        ini_set("memory_limit","200M");
//            vd($description, false);
        if ($description) {
            $string = preg_replace('/\n/', '', $description);
            $serch = preg_match("/модел(.*)P\/N?/i", $string, $matches);// делим на две части описание
            if(!$serch){
                $serch = preg_match("/модел(.*)(С|с)овмес/i", $string, $matches);// делим на две части описание
            }
            
            if(!$serch){
                $serch = preg_match("/модел(.*)/i", $string, $matches);// делим на две части описание
//                vd($description, false);
//                echo "<br/>";
//                vd($matches, false);
//                die("<br/>end");
            }
            if(!isset($matches[1])){
                vd($id, false);
                vd($string, false);
                echo "<br/>";
                vd($description, false);
                echo "<br/>";
                vd($string, false);
                echo "<br/>";
                vd($matches, false);
//                die("<br/>end");
            }
            $string = self::replacePars($matches[1]);
            $string = preg_replace('/([а-я]*?|\(.*?[а-я]+.*?\))/i', '', $string); // удаляет русский текст с строки
            $string = preg_replace('/\.*\|/i', '|', $string); // удаляет русский текст с строки
            $string = preg_replace('/\s*\|/i', '|', $string); // удаляет русский текст с строки
            $string = preg_replace('/^\|/i', '', $string); // удаляет русский текст с строки
//            vd($string, false);
//            echo "<br/>";
//            echo "<br/>";
//            $string = preg_replace('/((\-.*?\|)|(\-.*$))/i', '|', $string);

//            $serch = preg_match("/модел.*?\|(.*?)#/i", $string, $matches);
//            $serch = !$serch ? preg_match("/модел.*?\|(.*)?Совм.*/i", $string, $matches) : 1;
//            $serch = !$serch ? preg_match("/модел.*?\|(.*?)\s$/i", $string, $matches) : 1;
            
            if ($serch) {
                $array = explode("|", $string);
                $array = array_filter ($array);
//                vd($array, false);
//                echo "<br/>";
//                if(!){
//                    vd($array, false);
//                    die("<br/>end");
//                    
//                }
//                if(error_reporting(0) == 24575){
//                    vd($description, false);
//                    echo "<br/>";
//                    echo "<br/>";
//                    vd($array, false);
//                    echo json_encode($array);
//                    die("<br/>end");
//                }
                if(error_reporting(0) != 0){
                    vd($id, false);
                    vd($description, false);
                    echo "<br/>";
                    echo "<br/>";
                    echo "error";
                    vd($array, false);
                }
                return json_encode($array);
            } else {
                vd($description, false);
                return json_encode([]);
            }
        }
    }
    
    /**
     * парсит парт номера
     */
    public static function setParsPN($description, $id) {
        ini_set("memory_limit","200M");
        if ($description) {
            $string = preg_replace('/\n/', '', $description);
            $serch = preg_match("/P\/N(.*)?/i", $string, $matches);// делим на две части описание
            if(!$serch){
                $serch = preg_match("/для(.*)/i", $string, $matches);// делим на две части описание
            }
//            $serch = preg_match("/p\/n.*?\|(.*)?/i", $string, $matches);
//            $serch = !$serch ? preg_match("/для.*?\|(.*)?/m", $string, $matches) : 1;
//            $serch = !$serch ? preg_match("/#.*?\|(.*)?#\s*/m", $string, $matches) : 1;
            
            vd($description, false);
            echo "<br/>";
            echo "<br/>";
            vd($description, false);
            vd($matches, false);
//            die("<br/>end");
            if ($serch) {
                $string = self::replacePars($matches[1]);
                $string = preg_replace('/ без/', '', $string);
                $string = preg_replace('/^\|/', '', $string);
//                vd($string, false);
//                echo "<br/>";
//                echo "<br/>";
//                echo "<br/>";
                 //ищет нет ли в тексте русских символов
                $kiril = preg_match("/([а-яА-я])/i", $string, $kirilitsa);
                if($kiril){
                    vd($id, false);
                    vd($matches[1], false);
                    echo "<br/>-------------------------------------------------------------------------<br/>";
                }
//                $string = preg_replace('/(\s*\|)/', '|', $matches[1]);
//                $string = preg_replace('/#.*/', '', $string);
//                $string = preg_replace('/<.*>/', '', $string);
//                $string = preg_replace('/\s*$/', '', $string);

                $array = explode("|", $string);
                $array = array_filter ($array);
//                vd($array, false);
                return json_encode($array);
            } else {
                return json_encode([]);
            }
        }
    }
    /**
     * создает (и проверяет на наличие) модели с распарсиных товаров
     */
    public static function setProductModel() {
        ini_set("memory_limit","200M");
        $model = Product::find()
                ->where(['not', ['description' => '']])
                ->andWhere(['between', 'id', 29433, 40000])
                ->all();

        foreach ($model as $value) {
            $listModel = json_decode($value->_list_model);
//            echo $value->id;
//            vd($listModel, false);
        
            foreach ($listModel as $item) {
                $result = \app\models\Model::setModel($value->producer_id, $item, $value->id);
                if (!$result) {
                    return false;
                }
            }
            return TRUE;
        }
    }
   
    
    
    public static function getManufacture() {
        $connect = self::Connection();
        return $connect->createCommand(
                        "SELECT virtuemart_manufacturer_id, mf_name, slug FROM j251_virtuemart_manufacturers_ru_ru"
                        . " WHERE virtuemart_manufacturer_id > 5"
                )->queryAll();
    }
    
    /**
     * копируем производителей с исходной joomly
     */
    public static function setProducer() {
        $model = self::getManufacture();
        foreach ($model as $value) {
            $producer = new Producer();
            $producer->_original_id = $value['virtuemart_manufacturer_id'];
            $producer->name_lat = $value['slug'];
            $producer->name = $value['mf_name'];
            if(!$producer->save()){
                return false;
            }
        }
        return TRUE;
    }
    
    /**
     * обновление проблемных товаров
     */
    public static function updateProblemProduct(){
        $model = Product::find()->where(['not', ['article' => '']])
                ->andWhere(['producer_id'=>0])
                ->all();
        
        foreach ($model as $value) {
            preg_match("/^(\d\d)\d+/", $value->article, $matches);
            $articleCat = $matches[1]; 
            switch ($articleCat){
                case '01': $value->category_id = 1; break;
                case '02': $value->category_id = 2; break;
                case '04': $value->category_id = 3; break;
                case '03': $value->category_id = 4; break;
                case '05': $value->category_id = 5; break;
                case '06': $value->category_id = 6; break;
                case '07': $value->category_id = 9; break;
                case '08': $value->category_id = 10; break;
                case '09': $value->category_id = 12; break;
            }
            
            $articleProducer = $matches[1]; 
            switch ($value->article){
                case '010006': $value->producer_id = 1; break;
                case '010025': $value->producer_id = 1; break;
                case '020017': $value->producer_id = 1; break;
                case '030075': $value->producer_id = 2; break;
                case '070216': $value->producer_id = 2; break;
                case '060146': $value->producer_id = 6; break;
                case '010436': $value->producer_id = 6; break;
                case '010448': $value->producer_id = 6; break;
                case '010485': $value->producer_id = 6; break;
                case '010492': $value->producer_id = 9; break;
                case '060223': $value->producer_id = 9; break;
                case '030139': $value->producer_id = 10; break;
                case '040051': $value->producer_id = 10; break;
                case '070216': $value->producer_id = 10; break;
                case '010469': $value->producer_id = 15; break;
                case '020318': $value->producer_id = 17; break;
                case '020316': $value->producer_id = 24; break;
                case '070216': $value->producer_id = 24; break;
                case '040048': $value->producer_id = 25; break;
                case '040048': $value->producer_id = 25; break;
                case '020340': $value->producer_id = 25; break;
                case '070028': $value->producer_id = 25; break;
                case '070029': $value->producer_id = 25; break;
                case '070155': $value->producer_id = 25; break;
                case '070257': $value->producer_id = 25; break;
                case '070258': $value->producer_id = 25; break;
                case 'PJ039B': $value->producer_id = 25; break;
            }
            vd($value->update(), false);
        }
    }
    
    /**
     * Создаем связи у товарах которые (не) имели описания 
     */
    public static function setModelCompatibleDescriptionEmpty() {
        ini_set("memory_limit","250M");
        $produser = \app\models\Producer::getProducerArray();
        $product = Product::find()
                ->select('producer_id, id, article, name')
                ->where(['not', ['producer_id' => 0]])
                ->andWhere(['_is_problem' => 0])
                ->andWhere(['like', 'article', "09%", false])//%Aspire ZA3%
//                ->andWhere(['not',['description' => '']])
//                ->andWhere(['article' => '010009'])
//                ->andWhere(['between', 'id', 30000, 40000])
                ->all();
        $i = 1;
        foreach ($product as $value) {
            vd($i++, false);
            if(isset($produser[$value->producer_id])){
                $modelName = self::getNameModel($produser[$value->producer_id], $value->name);
                if ($modelName) {
                    $modelName = self::cutModelInName($modelName);
                    $modelName = preg_replace("/\s*$/u", '', $modelName);
                    $modelName = preg_replace("/ (\()?[а-яА-Я]+(.*)?/u", '', $modelName);
                    $modelName = preg_replace("/\s[а-яА-Я]+/u", '', $modelName);
                    $modelName = preg_replace("/\s+\([а-яА-Я]+ (\d)?/u", '', $modelName);
                    $modelName = preg_replace("/С/u", 'C', $modelName);
                    $modelName = preg_replace("/с/u", 'c', $modelName);
                    $modelName = preg_replace("/м/u", 'm', $modelName);

                    vd($value->name, false);
                    echo "<br/>";
                    vd($modelName, false);
                    echo "<br/>";
                    echo "<br/>";
                    $model = \app\models\Model::findOne(['name' => $modelName]);
                    if ($model) {
                        $result = \app\models\ModelCompatible::setModelCompatible($model->id, $value->article, $value->id);
                        if (!$result) {
                            vd($value->id, false);
                        }
                    }
                    vd($value->id, false);
                } else {
                    echo "Не могу определить модель";
                    $value->_is_problem = 3;
                    vd($value->update(), false);
                }
            }
        }
    }

    /**
     * Заполняет таблицу связей Моделей и товаров
     * model_id -> article
     */
    public static function setModelCompatible(){
        ini_set("memory_limit","250M");
        $produser = \app\models\Producer::getProducerArray();
        $product = Product::find()->select('producer_id, id, article, _list_model, name')
                ->where(['not', ['_list_model' => null]])
                ->andWhere(['not', ['producer_id' => 0]])
                ->andWhere(['_is_problem' => 0])
//                ->andWhere(['article' =>'010005'])
                ->andWhere(['between', 'id', 30000, 40000])
                ->all();
        $i=1;
        foreach ($product as $value) {
            vd($i++, false);
            vd($value->id, false);
            echo "<br/>";
            echo "<br/>";

            $listModel = json_decode($value->_list_model);
            $listModel = $listModel ? $listModel : [];
            foreach ($listModel as $modelNmae) {
                $model = \app\models\Model::find()
                                    ->where(['name'=>$modelNmae])
                                    ->one();
                if($model){
                    $result = \app\models\ModelCompatible::setModelCompatible($model->id, $value->article, $value->id);
                    if (!$result) {
                        vd($value->id, false);
                        return false;
                    }
                }
            }
        }
    }
    
    
    /**
     * проверка на соответствие количества моделей товара 
     * и количество самих товаров соответственно артикулу
     * Создание новых товаров которых нет
     */
    public static function createProduct() {
        $articles = Product::find()
                ->where(['not', ['article' => 0]])
                ->andWhere('article IN (010002)')
                ->select('article')
                ->distinct()
                ->all();
        $i = 0;
        vd(count($articles), false);
        foreach ($articles as $value) {
            if($i > 1970 && $i <= 30000){
                vd($i, false);
                echo "<br/>";
                vd($value->article, false);
                $i++;
                $model = \app\models\ModelCompatible::find()
                                ->where(['article'=>$value->article])
                                ->all();
                $product = \app\models\Product::find()
                                ->where(['article'=>$value->article])
                                ->select('id, category_id, producer_id, price, name, logo, status, article, _original_id')
                                ->all();

                vd("p_".count($product)." m_". count($model), false);
                
                if(count($product) < count($model)){
                    //товаров < моделей
                    foreach ($model as $item) {
                        if($item->model){
                            $serchProduct = \app\models\Product::find()
                                    ->where(['producer_id'=>$item->model->producer_id, 'article'=>$item->article, 'name' => "LIKE %".$item->model->name."%"])
                                    ->one();
                            if(!$serchProduct){
                                $serchProduct = new \app\models\Product();

                                $product = Product::find()->where(['article'=>$item->article])->one(); // находим товар для получения с него недостающей информации
                                $serchProduct->in_stock1 = $product->in_stock1;
                                $serchProduct->in_stock2 = $product->in_stock2;
                                $serchProduct->in_res1 = $product->in_res1;
                                $serchProduct->in_res2 = $product->in_res2;
                                $serchProduct->trade_price = $product->trade_price;
                                $serchProduct->is_timed = $product->is_timed;
                                $serchProduct->category_id = $product->category_id;
                                $serchProduct->price = $product->price;
                                $serchProduct->name = Product::createProductName($item->article, $item->model->producer_id, $item->model->id);
                                $serchProduct->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($serchProduct->name);
                                $serchProduct->logo = $product->logo;
                                $serchProduct->available = $product->available;
                                $serchProduct->status = $product->status;
                                $serchProduct->_original_id = $product->id;
                                $serchProduct->createtime = time();
                                $serchProduct->_is_new = 2;//создан с модели
                                $serchProduct->article = $item->article;
                                $serchProduct->producer_id = $item->model->producer_id;
                                $serchProduct->save();
                            }
                            //создаем новые связи модели с артикулом
                            \app\models\ModelCompatible::setModelCompatible($serchProduct->id, $serchProduct->article, $serchProduct->_original_id);
                        }
                    }
                }else{
                    $i = 0;
                    //может существовать несколько товаров с одинаковыми названиями и артикулами (пример Aspire 3690)
                    //товаров > моделей
                    foreach ($product as $item) {
    //                    vd($i++, false);
                        $serchModel = \app\models\Model::find()
                                ->where(['producer_id'=>$item->producer_id, 'name'=>$item->modelName])
                                ->one();
    //                    echo "<br/>";
    //                    vd($i++." ".$item->modelName, false);

                        if(!$serchModel){
                            $serchModel = new \app\models\Model();
                            $serchModel->producer_id = $item->producer_id;
                            $serchModel->name = $item->modelName;
                            $serchModel->_id = 0; //$item->_original_id
                            $serchModel->save();
                        }
                        //создаем новые связи модели с артикулом
                        \app\models\ModelCompatible::setModelCompatible($serchModel->id, $item->article, $item->_original_id);
                    }

                }
            }
            
            
            
//            vd($model[0]->model, false);
//            echo "<br/>";
//            echo "<br/>";
//            vd($product[0]->model, false);
           
//            die("<br/>end");
        }
        return TRUE;
    }
    
    /**
     * Создание новых товаров из модели
     */
    public static function createProductModel() {
        $articles = Product::find()
                ->where(['not', ['article' => 0]])
                ->select('article')
                ->distinct()
                ->all();
        $i = 0;
        foreach ($articles as $value) {
            if ($i > 0 && $i <= 5) {
                echo "<br/>";
                vd($i, false);
                vd($value->article, false);
                $modelCompatible = \app\models\ModelCompatible::find()
                        ->where(['article' => $value->article])
                        ->all();
                foreach ($modelCompatible as $value) {
                    if ($value->model) {
                        $serchProduct = \app\models\Product::find()
                                ->select('id, category_id, producer_id, price, name, logo, status, article, _original_id, _is_problem')
                                ->andWhere(['like', 'name', "% ".$value->model->name."%", false])//%Aspire ZA3%
                                ->andWhere([
                                    'producer_id' => $value->model->producer_id,
                                    'article' => $value->article,
                                    '_is_problem' => 0
                                ])
                                ->one();
//                        if (!$serchProduct) {
//                            vd($value->model->name, false);
//                            $serchProduct = new \app\models\Product();
//
//                            $product = Product::find()->where(['article' => $value->article])->one(); // находим товар для получения с него недостающей информации
//                            $serchProduct->in_stock1 = $product->in_stock1;
//                            $serchProduct->in_stock2 = $product->in_stock2;
//                            $serchProduct->in_res1 = $product->in_res1;
//                            $serchProduct->in_res2 = $product->in_res2;
//                            $serchProduct->trade_price = $product->trade_price;
//                            $serchProduct->is_timed = $product->is_timed;
//                            $serchProduct->category_id = $product->category_id;
//                            $serchProduct->price = $product->price;
//                            $serchProduct->name = Product::createProductName($value->article, $value->model->producer_id, $value->model->id);
//                            $serchProduct->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($serchProduct->name);
//                            $serchProduct->logo = $product->logo;
//                            $serchProduct->available = $product->available;
//                            $serchProduct->status = $product->status;
//                            $serchProduct->_original_id = $product->id;
//                            $serchProduct->createtime = time();
//                            $serchProduct->_is_new = 2; //создан с модели
//                            $serchProduct->article = $value->article;
//                            $serchProduct->producer_id = $value->model->producer_id;
//                            $serchProduct->save();
//                        }
                    }
                }
            }
            $i++;
        }
    }

    /**
     * Создание новых моделей которые получаем с названия товара
     */
    public static function createNewModel(){
        ini_set("memory_limit","250M");
        $produser = \app\models\Producer::getProducerArray();
        $product = Product::find()
                ->select('producer_id, id, article, _list_model, name, category_id, _original_id')
                ->where(['not', ['producer_id'=>0]]) //'category_id' =>[4,12],
//                ->andWhere(['not', ['category_id' =>[4,12]]])
//                ->andWhere(['like','article','09%', FALSE])
                ->andWhere(['_is_problem' =>0])
//                ->andWhere(['article' =>'010015'])
                ->andWhere(['between', 'id', 20000, 40000])
                ->all();
        
        foreach ($product as $value) {
            vd($value->name, false);
            vd($value->producer_id, false);
            echo "<br/>";
            
            $modelName = self::getNameModel($produser[$value->producer_id], $value->name);
            $modelName = preg_replace('/ *?[а-я]+.*/i', '', $modelName);
            $serchPartNsme = PartNamber::findOne(['name'=>$modelName]);
            if($modelName && !$serchPartNsme){
                vd($value->id, false);
                vd($produser[$value->producer_id], false);
                echo "<br/>";
                $modelName = preg_replace('/\-(\w+|\d+).*/i', '',$modelName);
                $modelName = preg_replace('/\s*$/i', '',$modelName);
                vd($modelName, false);
                echo "<br/>";
                echo "<br/>";
//                создаем модели
               $model = \app\models\Model::setModel($value->producer_id, $modelName, $value->id);
                if(is_object($model)){
                    //создаем новые связи модели с артикулом
                    \app\models\ModelCompatible::setModelCompatible($model->id, $value->article, $value->_original_id);
                }
            }else{
                echo $value->name;
                if($serchPartNsme){
                    $value->_is_problem = 2; // парт номер в имени
                    $value->update();
                    vd($serchPartNsme, false);
                }
                echo "<br/>";
                echo "<br/>";
            }
        }
    }
    
    /**
    * Проверяет если распарсинные парт/номера и модели одинаковые то удаляем парт номера
    */
    public static function dublePartInProduct(){
        $product = Product::find()
                ->select('id, _list_model, _list_pn')
                ->where(['not', ['producer_id'=>0]]) //'category_id' =>[4,12],
                ->andWhere(['not', ['_list_model'=>null]])
                ->andWhere(['not', ['category_id' =>[4,12]]])
//                ->andWhere(['like','article','01%', FALSE])
                ->andWhere(['between', 'id', 0, 40000])
                ->all();
        foreach ($product as $value) {
            if($value->_list_model == $value->_list_pn){
                $value->_list_pn = null;
                vd($value, false);
                vd($value->update(), false);
            }
        }
    }
    
    /**
     * Создает парт номера в таблице с распарсиного товара
     */
    public static function createNewPartNamber() {
        ini_set("memory_limit","250M");
        $product = Product::find()
                ->select('producer_id, id, _list_pn, _original_id')
                ->where(['not', ['producer_id'=>0]]) //'category_id' =>[4,12],
                ->andWhere(['not', ['_list_pn'=>null]])
                ->andWhere(['not', ['category_id' =>[4,12]]])
                ->andWhere(['like','article','09%', FALSE]) // 07 не проверять!!!
//                ->andWhere(['between', 'id', 30000, 40000])
                ->all();
        foreach ($product as $value) {
            $partNamberArray = json_decode($value->_list_pn);
            foreach ($partNamberArray as $partNamberName) {
                $model = \app\models\PartNamber::setPartNamber($value->producer_id, $partNamberName, $value->id);
                vd($model->name, false);
            }
        }
    }
    
    /**
     * Создаем модель с распарсиного товара
     */
    public static function createToArticleModelinProduct($article){
        $modelCompatible = \app\models\ModelCompatible::find()
                ->where(['like', 'article', $article."%", FALSE])
//                ->where(['between', 'id', 0, 10])
                ->all();

        $product = \app\models\Product::find()
                ->where(['article'=>$article, '_is_problem'=>0])
                ->andWhere(['like', 'article', $article."%", FALSE])
                ->select('id, article, name, producer_id, _list_model, _original_id')
                ->all();
        
        $i=0;
        $count = count($modelCompatible);
        foreach ($product as $value) {
            vd($i++, false);
            vd($value->name, false);
            foreach ($modelCompatible as $key=>$item) {
//                vd('/.*? ('.$item->model->name.')( .*)?$/i', false);
//                echo "<br/>";
               $serch = preg_match('/.*? ('.$item->model->name.')( .*)?$/i', $value->name, $sought);
                if ($serch) {
                    vd($sought[1], false);
                    break;
                }
                // если достигнут конец масива и не найден в названиях товара модель 
                // то проверяем существует ли модель ели нет то создаем 
                // проверяем существует ли связь и создаем связь модели и артикула
                if($key == $count-1) {
                    if (!$serch) {
                        $modelName = self::getNameModel($value->producer->name, $value->name);
                        $modelName = preg_replace('/\-(\w+|\d+).*/i', '',$modelName);
                        $model = \app\models\Model::setModel($value->producer_id, $modelName, $value->id);
                        if($model){
                            //создаем новые связи модели с артикулом
                            \app\models\ModelCompatible::setModelCompatible($model->id, $value->article, $value->_original_id);
                        }
                        vd($value->name, false);
                        vd($modelName, false);
                    } 
                }
            }
            echo "<br/>";
        }
        echo "<br/>";
        echo "<br/>";
        echo "<br/>";
        $i=1;
        foreach ($modelCompatible as $value) {
            vd($i++, false);
            vd($value->model->name, false);
            echo "<br/>";
        }
        
    }

    /**
     * ображает список количества товаров и моделей по артикулам
     */
    public static function reviewCountModel(){
        $articles = Product::find()
                    ->where(['not', ['article' => null, 'model_id'=>null]])
                    ->andWhere(['_is_problem'=>0, 'is_delete'=>0, 'status'=>1])
                    ->select('article')
                    ->asArray()
                    ->distinct()
                    ->all();
        vd('articles_ : '.count($articles), false);
        echo "<br/>";
       
        $i=0;
        foreach ($articles as $key => $value) {
            $countProduct = Product::find()
                    ->where(['article'=>$value['article']])
                    ->andWhere(['not', ['article' => null, 'model_id'=>null]])
                    ->andWhere(['_is_problem'=>0, 'is_delete'=>0, 'status'=>1])
                    ->count();
            
            $count_Model =  \app\models\ModelCompatible::find()
                            ->where(['article'=>$value['article'], '_is_problem'=>0])
                            ->count();
            if($countProduct != $count_Model){
                echo "<br/>";
                echo vd($value['article']." : (p:".$countProduct."  m:".$count_Model.")", false);
            }
            if( $key > 100) return;
        }
    }
    
    /**
     * Массовое удаление товаров по артикулам
     */
    public static function dellDubleProduct() {
        $articles = Product::find()
                ->where(['not', ['article' => null]])
                ->andWhere(['article' => '010022'])
                ->select('article')
                ->distinct()
                ->all();
        foreach ($articles as $key => $value) {
            
            if($key >= 0 && $key < 10){
                vd($value->article, false);
                self::dellDubleProductArticle($value->article);
            }
        }
        
    }

    /**
     * удаляет дублирующие товары по артикулу
     */
    public static function dellDubleProductArticle($article){
        $product = Product::find()
                    ->where(['article'=>$article])
                    ->select('id, article, name, description')
                    ->all();
        $dellProduct = [];
        foreach ($product as $key=>$value) {
            $serchProduct = Product::find()
                ->where(['not', ['id' => $value->id]])
                ->andWhere(['name'=>$value->name, 'description' => ''])
                ->select('id, article, name, description')
                ->all();
            vd($serchProduct, false);

            foreach ($serchProduct as $item) {
                //проверка для того чтобы не было перекрестных удалений
                if(!in_array($value->id, $dellProduct)){
                    $dellProduct[] = $item->id; //добавляем удаленнй товар в масив

                    $image = \app\models\Image::find()
                            ->where(['product_id'=>$item->id])
                            ->all();
                    foreach ($image as $img) {
                        $img->delete();
                    }
                    vd($item->delete(), false);
                }
            }
        } 
    }
    
    public static function delProductDuble(){
        $product = Product::find()
                    ->where(['_original_id'=>$_GET['r']])
                    ->all();
        
        foreach ($product as $value) {
            $serch = Product::find()
                    ->where(['_original_id'=>$value->_original_id])
                    ->select('id, _original_id, name')
                    ->all();
            if(count($serch) > 1){
                foreach ($serch as $key => $value) {
                    if($key != 0){
                        vd($value->delete(), false);
                    }
                }
            }
        }
    }

    /**
     * показует название моделей и название товаров
     */
    public static function getNameProductNameModel($article){
        echo $article;
        echo "<br/>";
        echo "<br/>";
        $product = Product::find()
                    ->where(['article'=>$article, 'is_delete'=>0, '_is_problem'=>0])
                    ->select('id, article, name, _list_model')
                    ->all();
        $productArray = [];
        foreach ($product as $item) {
            $productArray[$item->id] = $item->name;
        }
        asort($productArray);
        
        
        $model = \app\models\ModelCompatible::find()
                    ->where(['article'=>$article])
                    ->select('model_id')
                    ->all();
        
        $modelArray = [];
        foreach ($model as $item) {
            $modelArray[$item['model_id']] = ['name'=>$item->model->name, 'producer'=>$item->model->producer->name];
        }
        asort($modelArray);

        $i=1;
        foreach ($productArray as $value) {
            vd($i++, false);
            echo $value;
            foreach ($modelArray as $item) {
               $serch = preg_match('/.*? ('.preg_quote($item['producer']." ".$item['name'], '/').')$/i', $value, $sought);
                if ($serch) {
                    vd($sought[1], false);
                }
            }
            echo "<br/>";
        }
        echo "<br/>";
        echo "<br/>";
        $i=1;
        foreach ($modelArray as $value) {
            vd($i++, false);
            echo $value['producer']." ".$value['name'];
            foreach ($productArray as $item) {
               $serch = preg_match('/.*?('.preg_quote($value['producer']." ".$value['name'], '/').'(\-.*|( .*))?)$/i', $item, $sought);
                if ($serch) {
                    vd($item, false);
                }
            }
            echo "<br/>";
        }
//        echo "<br/>";
//        echo "<br/>";
//        foreach ($model as $value) {
//            if($value->model){
//                vd($value->model->name, false);
//                echo "<br/>";
//            }else{
//                vd($value, false);
//            }
//        }
        
        
    }
    
    /**
     * удаляет (is_problem = 1) все товары для которых нет модели
     */
    public static function setProblemProduct() {
        ini_set("memory_limit","250M");
        $produser = \app\models\Producer::getProducerArray();
        $product = Product::find()
                ->select('producer_id, id, name')
                ->where(['not', ['producer_id' => 0]])
//                ->andWhere(['not', ['category_id' =>[4,12]]])
                ->andWhere(['_list_model'=>null])
                ->andWhere(['between', 'id', 30000, 40000])
                ->all();
        foreach ($product as $value) {
            $modelName = $value->name;
            $serch = preg_match("/".$produser[$value->producer_id]." (.*)?/i", $value->name, $matches);
            if($serch){
                $modelName = preg_replace('/ *?[а-я]+.*/i', '', $matches[1]);
                $isModel = \app\models\Model::isModel($modelName);
                if(!$isModel){
                    vd($value->name, false);
                    echo "<br/>";
                    vd($modelName, false);
                    $value->_is_problem = 1;
                    vd($value->update(), false);;
                    echo "<br/>";
                    echo "<br/>";
                }
            }
            
        }
    }

    /**
     * перебырает все модели, удаляет (\-.*?)
     * создает тестовую таблицу
     */
    public static function cresteModelCut(){
        ini_set("memory_limit","250M");
        $model = \app\models\Model::find()
                        ->andWhere(['between', 'id', 17180, 30000])
                        ->all();
        foreach ($model as $value) {
            vd($value->name, false);
            echo "<br/>";
                
            $name = self::cutModelInName($value->name);
           
            $model = ModelCut::findOne(['name'=>$name, 'producer_id'=>$value->producer_id]);
            if(!$model){
                $model = new ModelCut();
                $model->attributes =  $value->attributes;
                $model->name = $name;
                vd($model->save(), false);
            }
            vd($name, false);
            echo "<br/>";
        }
        return TRUE;
    }
    /**
     * Удаляет лишние символы после '-' c конца
     */
    public static function cutModelInName($name){
        $serch = preg_match("/ .*(\-.*?)$/i", $name, $matches);
        if($serch){
            $reg = preg_quote($matches[1], '/');
            return preg_replace("/$reg/i", '', $name);
        }else
            return $name;
    }

    /**
     * Возвращает модель с имени продукта
     */
    public static function getNameModel($producerName, $productName){
        $serch = preg_match("/".$producerName." (.*)?/i", $productName, $matches);
        if(!$serch){
            $modelName = preg_replace('/\-.*? /', ' ', $productName);
            $serch = preg_match("/".$producerName." (.*)?/i", $modelName, $matches);
        }
        
        if(!$serch){
            echo $productName;
            echo "<br/>";
            echo "<br/>";
            return false;
        }
        return $matches[1];
    }
    
    /**
     * Досоздает модели с распарсинного товара
     */
    public static function addModelInPars() {
        ini_set("memory_limit","250M");
        $product = Product::find()
                ->select('producer_id, article, id, _list_model, _original_id')
                ->where(['not', ['producer_id'=>0]])
                ->andWhere(['_is_problem'=>0])
                ->andWhere(['not', ['_list_model'=>null]])
//                ->andWhere(['not', ['category_id' =>[4,12]]])
                ->andWhere(['like','article','09%', FALSE]) // 07 не проверять!!!
//                ->andWhere(['between', 'id', 30000, 40000])
                ->all();
        foreach ($product as $value) {
            $modelArray = json_decode($value->_list_model);
            foreach ($modelArray as $modelName) {
                $modelName = preg_replace('/\-.*$/u', '', $modelName);
                if($modelName){
                    $model = \app\models\Model::createModel($value->producer_id, $modelName, $value->id);
                    if($model){
                        //создаем связи
                        $result = \app\models\ModelCompatible::setModelCompatible($model->id, $value->article, $value->id);
                    }
                    vd($model->name, false);
                }
            }
        }
        
    }
    
    

}
