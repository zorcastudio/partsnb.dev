<?php
namespace app\models;

class UserCreate extends \app\models\User {
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [];
    }
    
    public static function create($user, $form){
        $username = preg_replace("/@.*/i", '', $form['email']);
        
        $model = self::find()
                ->where(['email'=>"{$form['email']}"])
                ->orWhere(['username'=>"$username"])
                ->one();
                
        if(!$model){
            $model = new self();
//            $model->name = isset($form['_name']) ? $form['_name'] : null;
            $model->name = $username;
            $model->phone = isset($form['phone']) ? $form['phone'] : null;
            $model->email = isset($form['email']) ? $form['email'] : null;
            $model->country_id = isset($form['country_id']) ? $form['country_id'] : 1;
            $model->region_id = isset($form['region_id']) ? $form['region_id'] : null;
            $model->city_id = isset($form['city_id']) ? $form['city_id'] : null;
            $model->address = isset($form['address']) ? $form['address'] : null;
            $model->zip_code = isset($form['zip_code']) ? $form['zip_code'] : null;
            $model->status = 1;
            $model->username = $username;
            $model->password = rand(100000, 999999);
            $model->createtime = time();
            $model->article = "u_".time().rand(100, 999);;
            $model->price_type = 0;
            $model->save();
        }
        return $model;
    }
}
