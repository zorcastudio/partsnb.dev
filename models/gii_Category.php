<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $name_lat
 * @property integer $product_count
 * @property integer $is_delete
 *
 * @property CategoryFilter[] $categoryFilters
 * @property Filter[] $filters
 * @property Product[] $products
 * @property ProductFilterCount[] $productFilterCounts
 * @property FilterValue[] $filterValues
 */
class gii_Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'name', 'name_lat', 'product_count'], 'required'],
            [['parent_id', 'product_count', 'is_delete'], 'integer'],
            [['name', 'name_lat'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'name' => 'Name',
            'name_lat' => 'Name Lat',
            'product_count' => 'Product Count',
            'is_delete' => 'Is Delete',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryFilters()
    {
        return $this->hasMany(CategoryFilter::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilters()
    {
        return $this->hasMany(Filter::className(), ['id' => 'filter_id'])->viaTable('category_filter', ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductFilterCounts()
    {
        return $this->hasMany(ProductFilterCount::className(), ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilterValues()
    {
        return $this->hasMany(FilterValue::className(), ['filter_value_id' => 'filter_value_id'])->viaTable('product_filter_count', ['category_id' => 'id']);
    }
}