<?php
namespace app\models;

use yii;
use yii\data\ActiveDataProvider;



class OrderUser extends \app\models\Order {

    public function getUrl(){
        return '/user/order/'.$this->id;
    }
    
    public function rules() {
        return [
            [['payment_id'], 'in', 'range' => [1,2,3]],
            [['zip_code', 'status', 'user_id', 'delivery_id', 'payment_id', 'country_id', 'city_id', 'region_id'], 'integer'],
            [['phone'], 'match', 'pattern' => '/^[+()0-9]+$/u', 'message' => 'Недопустимый формат телефона допускается "+()0-9"'],
            [['id', 'price', 'description', 'createtime', 'full_name', 'address'], 'safe'],
            [['id', 'user_id', 'delivery_id', 'payment_id', 'price', 'country_id', 'city_id', 'region_id', 'full_name', 'address', 'status'], 'safe', 'on' => 'search'],
        ];
    }
    
    public function search($params)
    {
        $query = self::find()
                ->select("order.*, (order.price + delivery.price) AS sumPrice")
                ->join('INNER JOIN','delivery', 'order.delivery_id = delivery.id')
                ->where([
                    'order.is_delete' => 0,
                    'user_id'=>Yii::$app->user->id
                ]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'delivery_id' => $this->delivery_id,
            'payment_id' => $this->payment_id,
            'price' => $this->price,
            'country_id' => $this->country_id,
            'zip_code' => $this->zip_code,
            'createtime' => $this->createtime,
            'status' => $this->status,
//            'is_delete' => 0,
//            'user_id'=>Yii::$app->user->id
        ]);
        

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        return $dataProvider;
    }

}
