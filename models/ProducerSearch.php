<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;


class ProducerSearch extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "product_producer_search";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['article', 'producer_id'], 'unique', 'targetAttribute' => ['article', 'producer_id']],
            [['producer_id'], 'integer'],
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            //если была изменена серия - 
            if ($this->isNewRecord) {
                $this->createtime = time();
                return TRUE;
            }
        };
        return TRUE;
    }

    public function getProducer() {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }
    
    public static function addProducerSearch($id, $article){
        $model = self::find()
                ->where([
                    'article'=>$article, 
                    'producer_id'=>$id, 
                ])
                ->one();
        if(!$model){
            $model = new self;
            $model->article = $article;
            $model->producer_id = $id;
            if($model->save()){
                return [$model->producer->id => $model->producer->name];
            };
        }  else {
            return -1;
        }
    }
    
    public static function getProducerArticle($article){
        $producer = self::find()->where(['article'=>$article])->all();
        $result = [];
        foreach ($producer as $value) {
            $result[$value->producer_id] = $value->producer->name;
        }
        return $result;
    }
}
