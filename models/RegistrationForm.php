<?php

namespace app\models;

use Yii;
use yii\base\Model;


class RegistrationForm extends Model {

    public $verifyPassword;
    public $verifyCode;
    public $body = "Для подтверждения регистрации пройдите по ссылке";
    public static $_country_id = 1;
    
    public $country_id;
    public $region_id;
    public $username;

    public function rules() {
        $rules = [
            ['username, password, email, verifyPassword, country_id', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'message' => 'Пользователь с таким электронным адресом уже существует.'],
            ['username', 'unique', 'message' => 'Пользователь с таким логином уже существует.'],
            ['username', 'length', 'max' => 20, 'min' => 3, 'message' => 'Длина имени пользователя от 3 до 20 символов.'],
            ['username', 'unique', 'message' => 'Пользователь с таким именем уже существует.'],
            ['username', 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В имени пользователя допускаются только латинские буквы и цифры.'],
            ['name', 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В имени пользователя допускаются только буквы и цифры.'],
            ['middle_name', 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В отчестве пользователя допускаются только буквы и цифры.'],
            ['surname', 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В фамилия пользователя допускаются только буквы и цифры.'],
            ['phone, country_id', 'numerical'],
            ['password', 'length', 'max' => 128, 'min' => 4, 'message' => 'Минимальная длина пароля 4 символа.'],
            ['zip_code', 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В в почтовом индексе допускаются только латинские буквы и цифры.'],
            ['address', 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В в адресе допускаются только буквы и цифры.'],
            ['verifyPassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают.'],
        ];

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'verticalForm')
            return $rules;
        return $rules;
    }
    
     public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'activkey' => 'Activkey',
            'createtime' => 'Createtime',
            'lastvisit' => 'Lastvisit',
            'name' => 'Name',
            'surname' => 'Surname',
            'middle_name' => 'Middle Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'country_id' => 'Country ID',
            'region_id' => 'Region ID',
            'city_id' => 'City ID',
            'address' => 'Address',
            'zip_code' => 'Zip Code',
            'status' => 'Status',
        ];
    }
    
    public function getCountry(){
        return self::$_country_id;
    }

}
