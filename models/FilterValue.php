<?php
namespace app\models;

use Yii;

class FilterValue extends \yii\db\ActiveRecord {

    public $_category_id;
    public $checked;
    public $_product_count;
   
    
    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "filter_value";
    }

    public function rules() {
        return array(
            array(['name'], 'required'),
            array(['filter_value_id', 'filter_id'], 'integer'),
        );
    }

    public function attributeLabels() {
        return array(
            'name' => "Название фильтра",
            'filter_id' => "Значение фильтра",
        );
    }
    
    public function scopes() {
        return array(
            'list' => array('select' => 'filter_value_id, name'),
        );
    }
    
    public function getProduct_filter_value() {
        return $this->hasMany(ProductFilterValue::className(), ['filter_value_id' => 'filter_value_id']);
    }
   
    public function getFilter() {
        return $this->hasOne(Filter::className(), ['id' => 'filter_id']);
    }
   
    public function isExists(){
        return self::find()->where([
            'filter_id'=> $this->filter_id, 
            'name_lat'=> $this->name_lat,
//            'name'=> $this->name
        ])->exists();
    }
    
    public static function getListFilterValue() {
        return CHtml::listData(self::model()->list()->findAll(), 'filter_value_id', 'name');
    }
    
    
    /**
     * перед тем как удалить значение фильтра удаляем те значения в таблицах где они существовали
     */
    public function beforeDelete(){
        ProductFilterValue::deleteAll(['filter_value_id'=>$this->filter_value_id]);
        ProductFilterCount::deleteAll(['filter_value_id'=>$this->filter_value_id]);
        return parent::beforeDelete();
    }
    
    /**
     * 
     * @param type $category_id - категория фильтра
     * @param type $name - названия свойства фильтра
     * @param type $filter_n - модель фильтра
     * @return модель свойства фильтра
     */
    public static function createFilterValue($name, $filter_n){
        $model = FilterValue::find()
                            ->where([
                                'filter_id'=>$filter_n->id,
                                'name'=>$name,
                            ])
                            ->one();
        if(!$model){
            $model = new self();
            $model->filter_id = $filter_n->id;
            $model->name = $name;
            $model->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($model->name);
            $model->save();
        }
        return $model;
    }
    
    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('name', $this->name, TRUE);
        $criteria->compare('filter_value_id', $this->filter_value_id, TRUE);
 
        return new CActiveDataProvider('FilterValue', array(
            'pagination' => array(
                'pageSize' => isset($_GET['pagination']) ? $_GET['pagination'] * 1 : 10,
            ),
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'filter_value_id DESC',
            ),
        ));
    }
    
    public static function getNormFilter(){
        $result = [];
        $filterID = [];
        $filterValueID = [];
            foreach (Product::getActiveFilter() as $key => $value) {
                $result['filter_id'][] = $key;
                foreach ($value as $k=>$item) 
                    $result['filterValueID'][] = $k;
            }
        return $result;
    }
    
    /**
     * Верне артикули ті які по значенню фільтрів
     * @param type $active_f
     * @param type $get
     * @return type
     */
    public static function getArticle(){
        $filter = Product::getFilterArray();
        $i=0;
        $sqlFilter = '';
        $result = [];
        foreach ($filter as $key => $value) {
            if($i++ == 0)
                $sqlFilter = "pfv.filter_value_id IN({$value['id']})";
            else
                $sqlFilter .= " OR pfv.filter_value_id IN({$value['id']})";
        }
        
        if($sqlFilter){
           $sqlFilter = " AND ({$sqlFilter})";
        }
       
        if(!empty($sqlFilter)){
            $request = Yii::$app->request->get();
            $serie = '';
            $joinSerie = '';
            if(isset($request['serie'])){
                $serie = "AND s.name_lat = '{$request['serie']}'";
                $joinSerie = " INNER JOIN serie s ON  s.id = pfv.serie_id";
            }
            
            Yii::$app->db->createCommand('SET group_concat_max_len = 500000')->execute();
            $list = Yii::$app->db->createCommand("
                    SELECT
                        GROUP_CONCAT(DISTINCT pfv.`article` SEPARATOR '; ') AS article,
                        pfv.`filter_id`
                        FROM `product_filter_value` pfv
                        INNER JOIN category c ON  c.id = pfv.category_id
                        INNER JOIN producer pr ON  pr.id = pfv.producer_id
                        {$joinSerie}
                        WHERE c.name_lat = '{$request['category']}'
                        AND pr.name_lat = '{$request['producer']}'
                        {$serie}
                        {$sqlFilter}
                        GROUP BY pfv.`filter_id`
                        ORDER BY  pfv.`filter_id`
                        ")->queryAll();
            
            if($list){
                $result = explode('; ', $list[0]['article']);
            }
            foreach ($list as $value) {
                $result = array_intersect($result, explode('; ', $value['article']));
            }
        };
        
        if($filter == []){
            return TRUE;
        }
        
       return $result ? implode(',', $result) : '0';
    }
    
    public static function getForH1($filter){
        $result = '';
        foreach ($filter as $value) {
            foreach ($value as $key => $val) {
                if($key != 'id'){
                    $result .= $key." = ({$value[$key]}) "; 
                }
            }
        }
        return $result;
    }

}
