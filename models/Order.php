<?php
namespace app\models;

use yii\helpers\ArrayHelper;
use Yii;
use app\models\Delivery;
use app\models\Product;

class Order extends \yii\db\ActiveRecord {

    public $_name;
    public $_lastname;
    public $sumPrice;
    
    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "order";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['full_name', 'email', 'phone', 'accepted', 'delivery_id', 'country_id', 'payment_id'], 'required'],
//            [['phone'], 'match', 'pattern' => '/^\+\d{2} \(\d{3}\) \d{3} \d{4}+$/u', 'message' => 'Недопустимый формат телефона допускается "+99 (999) 999 9999"'],
//            [['phone'], 'string', 'max' =>12],
            [['zip_code','full_name', 'address', 'city_name'], 'string'],
            ['email', 'email'],
            [['delivery_id'], 'isZipCode'],
            [['status', 'user_id', 'delivery_id', 'payment_id', 'region_id', 'city_id', 'is_delete'], 'integer'],
            [['id', 'price', 'description', 'createtime'], 'safe'],
            [['id', 'user_id', 'delivery_id', 'payment_id', 'price', 'region_id', 'full_name', 'address'], 'safe', 'on' => 'search'],
        ];
    }

    public static function getAvailableRules() {
        return [0 => 'заказ', 1 => 'заказ завтра после 10:00', 2 => 'заказ послезавтра', 3 => 'заказ через 1-2 дня'];
    }
    
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update'] = [];
        return $scenarios;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'delivery_id' => "Доставка",
            'createtime' => "Дата создания",
            'payment_id' => "Тип оплаты",
            'full_name' => "Ф.И.О",
            'address' => 'Адрес',
            'zip_code' => 'Почтовый индекс',
            'country_id' => 'Cтрана',
            'region_id' => 'Город',
            'city_id' => 'Город',
            'city_name' => 'Город',
            'description' => 'Описание',
            'phone' => 'Телефон',
            'price' => 'Цена',
            'status' => 'Статус',
            '_name' => 'Имя',
            '_lastname' => 'Фамилия',
            'email' => 'Эл. почта',
            'accepted' => 'Я согласен с Условиями обслуживания(Условия обслуживания)',
            
        ];
    }
    
    public function defaultScope() {
        return [ 'condition' => 'is_delete = 0'];
    }
    
    public function getCountry(){
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getDelivery(){
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    public function getPayment(){
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    public function getRegion(){
        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
    }
    
    public function getCity(){
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    public function getOrder_p(){
        return $this->hasMany(OrderProduct::className(), ['order_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion0()
    {
        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion1()
    {
        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
    }
    
    /**
     * Верне сторынку на яку ссилаэться доставка
     * @return type
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id'])->viaTable('delivery', ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('order_product', ['order_id' => 'id']);
    }
    
    
    
    public function isZipCode(){
        if(empty($this->zip_code)){
            switch ($this->delivery_id){
                case 4 :
                case 5 :
                case 6 :
                case 10 :
                case 11 : $this->addError('zip_code', 'Не указан индекс'); return FALSE; 
                default : return TRUE;
            }
        }else
            return true;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFullName()
    {
        if($this->full_name){
            return $this->full_name;
        }else{
            if($this->user){
                $fullName ='';
                $fullName .= $this->user->surname ? $this->user->surname." " : '';
                $fullName .= $this->user->name ? $this->user->name." " : '';
                $fullName .= $this->user->middle_name ? $this->user->middle_name." " : '';
                return $fullName;
            }else
                return '';
        }
    }
    
    public static function getMailMeneger($id){
        $menegers = [
            0 => 'sales@partsnb.ru',
            1 => 'salesk@partsnb.ru',
            2 => 'saless@partsnb.ru',
            3 => 'reg@partsnb.ru',
//            0 => 'bikov_sc@mail.ru',
//            1 => 'bikov_sc@mail.ru',
//            2 => 'bikov_sc@mail.ru',
        ];
        switch ($id){
            case 1 : return $menegers[0];
            case 2 : return $menegers[1];
            case 4 : return $menegers[0];
            case 5 : return $menegers[3];
            case 6 : return $menegers[3];
            case 9 : return $menegers[2];
            case 10 : return $menegers[3];
            case 11 : return $menegers[3];
            default : return $menegers[0];
        }
    }






    public static function itemAlias($type, $code = NULL) {
        $_items = [
            'status' => [
                0 => 'В обработке',
                1 => 'Оплачен',
                2 => 'Доставлен',
                3 => 'Обработан',
            ],
        ];
        if (isset($code)) {
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        } else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
    
    public function afterSave($insert, $changedAttributes) {
        if ($this->status == 3) {
            foreach ($this->order_p as $value){
                $model = Product::findOne($value->product_id);
                for($i=1; $i <= $value->amount; $i++){
                    $model->pay_count++;
                }
                $model->update();
            }
            
        }
        parent::afterSave($insert, $changedAttributes);
    }
    
//    public function afterFind(){
//        parent::afterFind();
//        //добавляем цену доставки
//        $deliveryPrice = \app\models\Delivery::getDeliveryPrice();
//        $delivery = json_decode($deliveryPrice, true);
//        $delivery = $delivery[$this->delivery_id];
//        $this->price += 1*preg_replace('/( )?(\.00)/u', '', $delivery);
//    }

    public static function getListDelivery(){
        return ArrayHelper::map(Delivery::find()->all(),'id', 'name');
    }
    
    public function getUrl(){
        return '/admin/order/view/id/'.$this->id;
    }
    
    public static function getAvialebel($basket){
        $rules = self::getAvailableRules();
        $available_stock = Product::getAvailableStock($basket->positions);
        $stock = ['1' => $rules[$available_stock['stock1']], '9' => $rules[$available_stock['stock3']]];
        return $stock;
    }
}
