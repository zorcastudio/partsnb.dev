<?php
namespace app\models;

use Yii;

class ProductFilterCount extends  \yii\db\ActiveRecord{

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "product_filter_count";
    }
    
    /**
     * Создаети/зменяет модель количества товаров в фильтрах
     * @param type $category_id
     * @param type $producer_id
     * @param type $serie_id
     * @param type $filter_value_id
     * @param type $product_count
     * @return \self
     */
    public static function createProductFilterCount($producer_id, $serie_id, $filter_value_id, $product_count){
        $model = self::find()
                ->where([
                    'producer_id'=>$producer_id,
                    'serie_id'=>$serie_id,
                    'filter_value_id'=>$filter_value_id,
                ])
                ->one();
        
        $model ?: $model = new self();
        
        $model->producer_id = $producer_id;
        $model->serie_id = $serie_id;
        $model->filter_value_id = $filter_value_id;
        $model->product_count = $product_count;
        $model->update = 1;
        $model->save();
        return $model;
    }
}
