<?php

namespace app\models;

use Yii;

class NEWpartNamber extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "all_p_n";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [];
    }

    public static function getListDMPN() {
        ini_set("memory_limit","2000M");
        ignore_user_abort(true);
        set_time_limit(0);
        
        $list = Yii::$app->db->createCommand("
            SELECT  `article` ,  `_list_pn`, producer_id 
            FROM  `product` 
            WHERE  `_list_pn` !=  ''
            LIMIT 5000, 2000
        ")->queryAll();
      
        
        foreach ($list as $value) {
            $result = '';
            $json = json_decode($value['_list_pn']);
//            $model = self::find()
//                    ->where([
//                        'article'=>$value['article'],
//                        'name'=>$json,
//                    ])->count();

//            if($model != count($json)){
                foreach ($json as $key => $item) {
                    $pn = preg_replace('/&nbsp;/iu', ' ', $item);
                    $model = self::find()->where(['article'=>$value['article'], 'name'=>trim($pn)])->one();
                    if(!$model){
                        $model = new self();
                        $model->article = $value['article'];
                        $model->name = trim($pn);
                    }  else {
                        $model->producer_id = $value['producer_id'];
                    }
                    $model->save();
                }
//            }
            
        }
    }
    
    public static function createNoneMOdel(){
        ini_set("memory_limit","2000M");
        ignore_user_abort(true);
        set_time_limit(0);
        
        $list = Yii::$app->db->createCommand("
            SELECT a.id, a.producer_id,
            CASE a.producer_id
                WHEN 1 THEN 471
                WHEN  2 THEN 473
                WHEN  13 THEN 472
                WHEN  22 THEN 474
                WHEN  24 THEN 475
                WHEN  16 THEN 476
                WHEN  18 THEN 477
                WHEN  17 THEN 478
                WHEN  23 THEN 479
                WHEN  30 THEN 480
                WHEN  13 THEN 472
                WHEN  3 THEN 481
                WHEN  21 THEN 482
                WHEN  20 THEN 483
                WHEN  13 THEN 472
                WHEN  14 THEN 484
                WHEN  4 THEN 485
                WHEN  27 THEN 486
                WHEN  5 THEN 487
                WHEN  6 THEN 488
                WHEN  10 THEN 489
                WHEN  8 THEN 490
                WHEN  33 THEN 491
                WHEN  34 THEN 492
                WHEN  29 THEN 493
                WHEN  9 THEN 494    
                WHEN  31 THEN 495
                WHEN  25 THEN 496
                WHEN  7 THEN 497
                WHEN  35 THEN 498
                WHEN  19 THEN 499
                WHEN  11 THEN 500
                WHEN  12 THEN 501
                WHEN  32 THEN 502
                WHEN  15 THEN 503
                WHEN  36 THEN 504
                WHEN  28 THEN 505
                WHEN  26 THEN 506
                WHEN  37 THEN 507
            END AS serie_id, a .name
            FROM `all_p_n` a
            LEFT JOIN model m ON m.name=a.name
            WHERE m.name IS null
        ")->queryAll();
        
        foreach ($list as $item) {
            $model = Model::find()->where([
                'producer_id'=>$item['producer_id'],
                'serie_id'=>$item['serie_id'],
                'name'=>$item['name'],
            ])->one();
            
            if(!$model){
                $model = new Model();
                $model->producer_id = $item['producer_id'];
                $model->serie_id = $item['serie_id'];
                $model->name = $item['name'];
                $model->save();
            } 
            
//            SELECT a.article, a.name, m.id, m.name, p.article, p.name 
//            FROM `all_p_n` a
//            INNER JOIN model m ON m.name=a.name
//            LEFT JOIN product p ON (p.model_id = m.id AND a.article=p.article)
//            WHERE p.article IS NULL
        }
        
        
    }
}
