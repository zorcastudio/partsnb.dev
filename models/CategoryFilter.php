<?php
namespace app\models;

use Yii;

class CategoryFilter extends  \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "category_filter";
    }

    public function rules() {
        return [
            [['filter_id', 'category_id'], 'integer'],
            [['filter_id', 'category_id'], 'unique', 'targetAttribute' => ['filter_id', 'category_id'], 'message' => 'Поєднання filter_id і category_id вже були створені.']
        ];
    }
    
    public function getValue(){
        return $this->hasMany(FilterValue::className(), ['filter_id' => 'id'])->viaTable('filter', ['id' => 'filter_id']);
    }
   
   
    public function getValue_all(){
        return $this->hasOne(FilterValue::className(), ['filter_id' => 'filter_id']);
    }
    
    
    public function getCategory(){
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getFilter(){
        return $this->hasOne(Filter::className(), ['id' => 'filter_id']);
    }
   

    public function attributeLabels() {
        return array(
            'filter_id' => "Фильтр",
            'category_id' => "Категория",
        );
    }
}
