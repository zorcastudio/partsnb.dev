<?php
namespace app\models;

use Yii;

use yii\helpers\Url;
use app\models\User;
use app\modules\admin\components\TranslitFilter;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;


class Category extends \yii\db\ActiveRecord {

    public $_cat_array;
    public $_cat_active_array;
    const SEPARATOR = '_';
    const MAX_FILTER_COUNT = 10;
    public $filter_set;
    public $_list;
    public $_sort;

    public function __construct(){
        
        //устанавливаем вид списка товаров
        if(Yii::$app->request->get('list')){
            $this->_list = Yii::$app->request->get('list');
            User::setCookes('list', $this->_list);
        }else{
            if(Yii::$app->request->cookies['list']){
                $this->_list = Yii::$app->request->cookies['list']->value;
            }else{
                $this->_list = 1;
                User::setCookes('list', $this->_list);
            }
        }
        
        //устанавливаем сортировку списка товаров
        if(Yii::$app->request->get('sort')){
            $this->_sort = Yii::$app->request->get('sort');
            User::setCookes('sort', $this->_sort);
        }else{
            if(Yii::$app->request->cookies['sort']){
                $this->_sort = Yii::$app->request->cookies['sort']->value;
            }else{
                $this->_sort = 0;
                User::setCookes('sort', $this->_sort);
            }
        }
        parent::__construct();
    } 

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "category";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['name_lat', 'name'], 'unique'],
            [['id', 'parent_id', 'is_delete', 'is_published'], 'integer'],
            [['name', 'name_lat'], 'string', 'max' => 255],
            [['description', 'description_product'], 'safe'],
            [['name', 'name_category'], 'match', 'pattern' => '/^[a-zа-я0-9\-\(\)ё ]+$/ui','message' => 'В названии категории допускаются только буквы и цифры, знак "-".'],
            [['name_lat'], 'match', 'pattern' => '/^[a-z0-9 \-]+$/ui','message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-".'],
            ['filter_set',function () {
                if(!is_array($this->filter_set)){
                    $this->addError('filter_set','Список фильтров не масив');
                }
            }],
//            
        ];
    }
    
    public function relations() {
       return [
           'filter'=>array(self::MANY_MANY, 'Filter',
                'category_filter(filter_id, category_id)'),
           
       ];
   }
   
    public function getCat_filter(){
        return $this->hasMany(CategoryFilter::className(), ['category_id' => 'id']);
    }
    
    public function getProduct(){
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }
    
    public function getFilter(){
        return $this->hasMany(Filter::className(), ['category_id' => 'id']);
    }
    
    public function getFilterSearch(){
        return $this->hasMany(Filter::className(), ['category_id' => 'id'])->andWhere(['search_published'=>1]);
    }
    
    
    public function getFilterValues()
    {
        return $this->hasMany(FilterValue::className(), ['filter_value_id' => 'filter_value_id'])->viaTable('product_filter_count', ['category_id' => 'id']);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'parent_id' => "Родительская категория",
            'description' => "Описание категории",
            'description_product' => "Описание в товаре",
            'name' => "Название категории меню",
            'name_category' => "Название категории в h1",
            'is_delete' => "Удалить",
            'name_lat' => "Имя для ссылки",
            'filter_set' => "Список фильтров",
            'is_published' => "Опубликовано",
            '_sort' => "Сортировать",
        ];
    }
    
    public function getListFilter() {
        $filter = Filter::findAll(['category_id'=>$this->id]);
        return ArrayHelper::map($filter, 'id', 'name');
    }
    
    public function children(){
        return self::find()
            ->where(['parent_id' => $this->id, 'is_delete'=>0])
            ->all();
    }
    
    public static function getListAll(){
        return self::find()
            ->where(['is_delete' => 0])
            ->select('id, name')
            ->all();
    }
    
    
    
    public function defaultScope() {
        return [ 'condition' => 'is_delete = 0'];
    }
    
    public static function find() {
        return new \app\models\MyActiveQuery(get_called_class());
    }
    
     public static function itemAlias($type, $code = NULL) {
        $_items = array(
            '_sort' => array(
                1 => 'Новые',
                2 => 'Старые',
                3 => 'Популярные',
//                4 => 'От дешевых к дорогим',
//                5 => 'От дорогих к дешевым',
            ),
        );
        if (isset($code)) {
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        } else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
    

    /*
     * Перед тек как сохранить в БД
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (!$this->name_lat) {
                $this->name_lat = TranslitFilter::translitUrl($this->name);
            }else{
                $model = self::findOne($this->id);
                if($model && $model->name != $this->name && $model->name_lat == $this->name_lat)
                    $this->name_lat = TranslitFilter::translitUrl($this->name);
            }
            return TRUE;
        } else
            return false;
    }
    
    public function afterSave($insert, $changedAttributes) {
        if (parent::afterSave($insert, $changedAttributes)) {
            if(!$this->isNewRecord){
                //перещитваем товары в фильтрах
                FilterWorkingCount::setCountProductFilter();
                //перещитаем количество товаров в категориях
                ProductWorkingCount::setCountProductCategory();
            }
            return  TRUE;
         }else
            return  false;
    }
    
    /**
     * ищем в тексте категорию
     * возвращаем его id и название
     * @param type $serch
     * @return type
     */
    public static function seartchCategory($serch) {
        if(preg_match("/c=(\d+)$/iu", $serch, $matches)){
            $category  = self::findOne($matches[1]);
            
            $key = explode(',', $category['id']);
            return empty($category) ? false : ['key'=>  $key, 'name'=>$category->name]; 
        }
//        vd($serch, false);
//        preg_match("/^([а-яё ]*)?.*([а-яё ]*)?$/ui", $serch, $matches1);
//        vd($matches1, false);
        
        if(preg_match("/([а-яё ]*).*/iu", $serch, $matches) && $matches[1]){
            
            //якщо кылькысть символыв меньше 5 то невизначенный пошук кириличных символів
            if (strlen($matches[1]) < 5){
                return false;
            }
            $old_search = $matches[1];
            $serch = trim($matches[1]);
            $result = [];
            foreach (explode(" ", $serch) as $value) {
                $result[] = substr($value, 0, -2);
            }
            $serch = implode(' ', $result);
            $serch = preg_replace("/\s/iu", '%', $serch);
            
            $category  = self::find()
                ->select(["
                    GROUP_CONCAT(id SEPARATOR ',') AS id,
                    GROUP_CONCAT(name SEPARATOR '; ') AS name
                "])
                ->where(['like', 'name', "%{$serch}%", false])
                ->asArray()
                ->one();
            $key = $category['id'] ? explode(',', $category['id']) : null;
            return !$category['id'] ? false : ['key'=>  $key, 'name'=>trim($old_search)]; 
        } 
        
        return FALSE;
    }
    
    /**
     * Список категорий для siteMap
     */
    public static function getCategoryListUrl(){
        $model = self::find()
                ->where(['is_delete'=>0, 'is_published'=>1])
                ->select('name_lat, id')
                ->andWhere(['not', ['name_lat'=>'']])
                ->asArray()
                ->all();
        $result = [];
        foreach ($model as $value) {
            $result[$value['id']] = $value['name_lat'];
        }
        return $result;
    }
    
    public static function isChildren($url){
        $category = self::getLastCat($url);
        if($category){
            $model = self::find()->where(['name_lat'=>$category])->one();
            return ($model && $model->children() == null) ? $model : FALSE;
        }else{
            return false;
        }
    }

    public static function getCategories(){
        $categori = self::find()->all();
        return $categori ? ArrayHelper::map($categori,'id', 'name') : [];
    }
    
    /**
     * Вернет все категории которые не являются родителями
     */
    public static function getCategoriesLast(){
        $categori = self::find()->where(['is_delete'=>0])->all();
        $result = [];
        foreach ($categori as $value) {
//            $children = self::find()->where(['parent_id'=>$value->id, 'is_delete'=>0])->one();
//            $children ?: $result[$value->id] = $value->name;
            $result[$value->id] = $value->name;
        }
        return $result;
    }
    

    public static function getCategoryListWithoutChield(){
        $return = array();
        foreach (self::getListAll() as $value) {
            if(!self::findAll(['parent_id' => $value->id])) {
                $return[$value->id] = $value->name;
            }
        }
        
        return  $return;
    }
    
    public function filterList(){
        $result = array();
        foreach ($this->filter as $value)
            $result[]=$value->id;
        return  $result;
    }

    public function getCountProduct(){
        return ;// Product::find()->where(['category_id' => $this->id])->count();
    }

    /**
     * масив категорий к дереву
     */
    public static function getList(){
        $result = [];
        $categorys =  self::find()
                ->where(['parent_id'=>null, "is_delete"=>0, 'is_published'=>1])
                ->all();
        
        foreach ($categorys as $key => $value){
            $result[$key] = [
                'label'=> $value->name, //." (".$value->countProduct.")", 
                'url'=> Url::toRoute("/".$value->getUrl()),
                'active'=>$value->isActive()
            ];
//            $result[$key] +=array('items'=>Producer::getProducer($value->id));
        }
        return $result;
    }
    
    
    /**
     * показывает есть ли в 
     * запрашуемой категории 
     * активная ссылка
     * expand
     */
    public static function Expand ($value) {
        $array = self::getCategoryArray();
        return in_array($value->name_lat, $array);
    }
    
    /**
     * Пзоверяет активна ли категория,
     * функция принимает значение активной категории
     */
    public static function isActiveCat($cat, $model){
        if($cat == $model->name_lat){
            return TRUE;
        }else{
            $model = self::model()->findByAttributes(array('parent_id'=>$model->id));
            return $model ? self::isActiveCat($cat, $model) :  false;
        }
    }
    
    public function isActive(){
        if(isset($_GET['category'])){
            return $_GET['category'] == $this->name_lat ? 'active' : null;
        }
        return Category::getLastCat() == $this->name_lat ? 'active' : null;
    }

        public function setCatActiveArray($result = array(), $category = null){
        if(empty($result)){
            $cat = self::getLastCat(Yii::$app->request->requestUri);
            $result[] =  $cat;
            $category = self::model()->findByAttributes(array('name_lat'=>$cat));
        }
        
        $category = self::model()->findByAttributes(array('id'=>$category->parent_id));
        if($category){
            $result[] =  $category->name_lat;
            if($category->parent_id != NUll)
                return self::setCatActiveArray($result, $category);
            else
                return $result;
        }else
            return $result;
        
        $this->_cat_active_array = $result;
    }
    
    /**
     * ищем главного родителя категории
     */
    public function getParent() {
        $model = self::findOne($this->parent_id);
        if (!$model)
            return $this;

        return $model->parent_id != NUll ? $model->getParent() : $model;
    }
    
    /**
     * формирует ссылки в меню категории
     */
    public static function genegalLink($tree) {
        if (isset($_GET['setcategory']) && $_GET['setcategory'] == $tree->name_lat) {
            $link_p = "<a class='active' href='/".$tree->url."'>" . $tree->name . " (".$tree->product_count.")</a>";
        } else
            $link_p = "<a href='/".$tree->url."'>" . $tree->name . " (".$tree->product_count.")</a>";
        return $link_p;
    }
    
    public static function getHeadCat(){
        $categorys = self::model()->findAllByAttributes(['parent_id'=>null]);
        
        foreach ($categorys as $key => $value){
            $list = self::model()->findAllByAttributes(['is_delete' => 0, 'parent_id'=>$value->id]);
            $result[$key] = array('text'=> CHtml::link($value->name." (".$value->product_count.")", Yii::$app->createAbsoluteUrl($value->getUrl()), array('class'=>$value->isActive())));
            if($list){
                $category = self::getLastCat(Yii::$app->request->requestUri);
//                $result[$key] +=array('expanded'=> self::Expand($value));
//                $result[$key] +=array('children'=>self::getList ($list));
            }
        }
        return $result;
    }
    
    /**
     * 
     *есть ли дети в категории
     */
    public static function isChild($id) {
        $children = self::find()
                ->where(['parent_id' => $id])
                ->count();
        return $children ? TRUE : FALSE;
    }
    
    /**
     * показывает есть ли в 
     * запрашуемой категории 
     * активная ссылка
     */
    public static function  isActiveLink($setcategory, $model){
        if ($model->name_lat == $setcategory){
            return TRUE;
        }else{
            $model = self::model()->findByAttributes(array("parent_id"=>$model->id));
            return $model ? self::isActiveLink ($setcategory, $model) : false;
        }
    }

    /**
     * устанавливает количество товаро в категории
     */
    public function setproductCount(){
        $parent = $this->getParent();
        foreach ($parent->getChildren() as $id){
            $model = self::findByPk($id);
            $model->product_count = $model->getCount();
            $model->update();
        }
    }
    
    /*
     * щитает количество товаров в категории
     */
    public function getCount(){
        
        $product = Product::model()->findAllByAttributes(array('category_id'=>$this->id, 'status' => 1, 'is_published'=>1, 'is_delete' => 0));
        $count = count($product);
        
        $categories = self::model()->findAllByAttributes(array('parent_id'=>$this->id));
        
        foreach ($categories as $value) {
            $count += $value->getCount();
        }
        return $count;
    }
    
       
    /*
     * Формирует ссылку (категории) в виде хлебных крошок
     */
    public function getUrl($result = null) {
       if(!$result)
            $result = $this->name_lat;

        $model = self::findOne(['id'=>$this->parent_id]);
        if ($model) {
            $result = $model->name_lat.self::SEPARATOR.$result;
           
            if ($model->parent_id != null) {
//                return $model->getUrl($result);
            }
        }
        return $result;   
    }
    
    
    
    /*
     * возвращаем последнюю категорию
     */
    public static function getLastCat(){
        $array = self::getCategoryArray();
        return array_pop($array);
    }
    
    /**
     * возвращаем последнюю категорию
     */
    public static function FirstCat(){
        $array = self::getCategoryArray();
        return $array[0];
    }
    
    /**
     * массив категорий полученный с адресса
     */
    public static function getCategoryArray(){
        $url = preg_replace('/\?.*/', '', Yii::$app->request->url);
        $arr_url = explode("/", $url);
        $url = $arr_url[1];
        $array = explode(self::SEPARATOR, $url);
        return $array;
    }


    /*
     * возвращает всех детей категории
     */
    public function getChildren($result=array()){
        if(empty($result))
            $result[]= $this->id;
        
        $model = self::find()
            ->where(['parent_id' => $this->id, "is_delete"=>0])
            ->all();

        foreach ($model as $key => $value) {
            $result[] = $value->id;
            $result += $value->getChildren($result);
        }
        return $result;
    }

    /*
     * возвращает всех детей категории
     */
    public static function getLastCategory($result = array(), $item = null){
        if(empty($result)){
            $model = self::model ()->findAllByAttributes(array('parent_id'=>null));
        }else{
            $model = self::model()->findAllByAttributes(array('parent_id'=>$item->id));
        }
        
        foreach ($model as $value) {
            self::getLastCategory($result, $value);
        }
        return;
    }
    
    
    
    public static function getAllCategory($result=[], $value = null){
        if(empty($result))
            $model = self::model()->findAllByAttributes(['parent_id'=>null]);
        else    
            $model = self::model()->findAllByAttributes(['parent_id'=>$value->id]);

        foreach ($model as $key => $value) {
            $result[] = $value->id;
            $result += self::getAllCategory($result, $value);
        }
        return $result;
    }


    /**
     * хлбные крошки
     */
    public static function getBreadCrumbs($model, $scenario){
        $cat_array = $model->getCatArray();
        
        $count = count($cat_array);
        $result = [];
        $item = '';
        $i = 0;
        foreach ($cat_array as $key=>$value){
            if(++$i != $count || $scenario == 'product'){
                $item .= ($i==1) ? '/'.$value : self::SEPARATOR.$value;
                $result[$i-1] = ['label' => $key, 'url' => [$item]];
            }else{
                $result[] = $key;
            }
        }
        return $result;
    }

    public function getCatArray($result = array()) {
        if (!$this->_cat_array) {
            if (empty($result))
                $result[$this->name] = $this->name_lat;

            $model = self::find()
                    ->where(['id' => $this->parent_id])
                    ->one();
            
            if ($model) {
                $result[$model->name] = $model->name_lat;
                if ($model->parent_id != null) {
                    return $model->getCatArray($result);
                }
            }
            return $this->_cat_array = array_reverse($result);
        } else {
            return $this->_cat_array;
        }
    }
    
    /**
     * возвращает список родительских категорий в массиве
     */
    public static function grtCategoryArray(){
        $category = self::find()
                ->where(['parent_id'=>null, 'is_delete'=>0])
                ->select('name_lat, name')
                ->all();
        return ArrayHelper::map($category, 'name_lat', 'name');
    }


    public function getSort(){
        $sort = 'id DESC'; 
        switch ($this->_sort){
            case 0: break;
            case 1: $sort = 'createtime DESC'; break;
            case 2: $sort = 'createtime ASC'; break;
            case 3: $sort = 'pay_count DESC'; break;
        }
        return  $sort;
    }

    public function getNameParent() {
        $model = self::findOne($this->parent_id);
        return $model ? $model->name : null;
    }
    
    /*
     * Получаем главные категории
     */
    public static function getParentCat(){
        return self::findAll(['parent_id' => null, 'is_delete' => 0]);
    }
    
    /**
     * вернет id категории полученный с артикла
     */
    public static function getIdCstegory($article) {
        if (preg_match("/^(\d\d)\d+/", $article, $articleCat)) {
            switch ($articleCat[1]) {
                case '01': return 1; //Клавиатуры
                case '02': return 2; //Вентиляторы //Система охлаждения
                case '03': return 3; //Аккумулятор
                case '04': return 4; //Блоки питания //Сетевой кабель
                case '05': return 5; //Петли для матриц //Направляющие для матриц
                case '06': return 6; //Шлейфы для матриц
                case '07': return 9; //Разъёмы питания
                case '08': return 10;//Матрицы
                case '09': return 12;//Расходные материалы
                default : return null;
            }
        }
    }
    
    /**
     * по category_id вернет название категории
     */
    public static function getNameInCategory($article){
        if (preg_match("/^(\d\d)\d+/", $article, $articleCat)) {
            switch ($articleCat[1]){
                case '01': $categoryName = 'Клавиатура'; break;
                case '02': $categoryName = 'Вентилятор'; break;
                case '03': $categoryName = 'Аккумулятор'; break;
                case '04': $categoryName = 'Блок питания'; break;
                case '05': $categoryName = 'Петля для матрицы'; break;
                case '06': $categoryName = 'Шлейф для матрицы'; break;
                case '07': $categoryName = 'Разъём питания'; break;
                case '08': $categoryName = 'Матрица'; break;
                case '09': $categoryName = 'Расходный материал'; break;
                default : $categoryName = null;  
            }
        }
        return $categoryName;
    }
    /**
     * по category_id вернет название категории
     */
    public static function getNameCategory($id){
            switch ($id){
                case '1': $categoryName = 'Клавиатура'; break;
                case '2': $categoryName = 'Вентилятор'; break;
                case '3': $categoryName = 'Аккумулятор'; break;
                case '4': $categoryName = 'Блок питания'; break;
                case '5': $categoryName = 'Петля для матрицы'; break;
                case '6': $categoryName = 'Шлейф для матрицы'; break;
                case '9': $categoryName = 'Разъём питания'; break;
                case '10': $categoryName = 'Матрица'; break;
                case '12': $categoryName = 'Расходный материал'; break;
                case '23': $categoryName = 'Запчасть для apple'; break;
                default : $categoryName = null;  
            }
        return $categoryName;
    }
    
    public static function getCategoryOfArticle($article){
        if (preg_match("/^(\d{2})\d{4}$/", $article, $articleCat)) {
            switch ($articleCat[1]){
                case '01': return 1;
                case '02': return 2;
                case '03': return 3;
                case '04': return 4;
                case '05': return 5;
                case '06': return 6;
                case '07': return 9;
                case '08': return 10;
                case '09': return 12;
                default : return null;  
            }
        }
    }

    

    /**
     * Вернет название категории по артикулу
     */
    public static function getCategoryArticle($article){
       $category_id = self::getIdCstegory($article);
       return self::find()->where(['id'=>$category_id])->one();
    }
    
    public static function updateCategoryArticle(){
        $model = ProductCreate::find()
                ->select('*, 
                    (CASE 
                        WHEN article  RLIKE "^01" THEN 1
                        WHEN article  RLIKE "^02" THEN 2
                        WHEN article  RLIKE "^03" THEN 3
                        WHEN article  RLIKE "^04" THEN 4
                        WHEN article  RLIKE "^05" THEN 5
                        WHEN article  RLIKE "^06" THEN 6
                        WHEN article  RLIKE "^07" THEN 9
                        WHEN article  RLIKE "^08" THEN 10
                        WHEN article  RLIKE "^09" THEN 12
                    END)  AS _category_id'
                )
//                ->where('updatetime > 1450167182')                
                ->having('_category_id != category_id')
                ->limit(500)
                ->all();
        
        foreach ($model as $value) {
            vd($value->category_id, false);
            $value->category_id = $value->_category_id;
            vd($value->article.' '.$value->category_id, false);
            vd($value->name, false);
            
            $value->name = preg_replace("/^([а-я]* )*/iu", self::getNameCategory($value->_category_id).' ', $value->name);
            $value->original_name_lat .= ','.$value->name_lat;
            $value->name_lat = TranslitFilter::translitUrl($value->name);

            vd($value->name, false);
            vd($value->update(), false);
            echo "<br/>";
        }
    }

    public function getUrlBrandPath() {
        $producer = Yii::$app->request->get('producer');
        $model = Yii::$app->request->get('model');
        $serie = Yii::$app->request->get('serie');
        return '/'.$this->name_lat.'/'.$producer.'/'.$serie.'/model/'.$model;
    }



}
