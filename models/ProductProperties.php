<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;


class ProductProperties extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "product_properties";
    }
    
    public function getProperties() {
        return $this->hasOne(Properties::className(), ['id' => 'properties_id']);
    }
   
    public function getProductForArticle() {
        return $this->hasOne(Product::className(), ['id' => 'product_id'])->select('id, article');
    }
    
    public function getArticleProperties() {
        return $this->hasMany(ProductProperties::className(), ['article' => 'article']);
    }
    
    public function getSerieInProduct() {
        return $this->hasMany(Product::className(), ['article' => 'article'])->select('article, serie_id')->where(['not', ['serie_id'=>null]])->distinct();
    }
    
    /**
     * создает свойтсва для дочерных товаров
     */
    public static function createProperties($s, $f){
        $productProperties = self::find()
                ->select('product_id')
                ->where(['between', 'id', $s, $f])
                ->distinct()
                ->asArray()
                ->all();
        $product = [];
        $result  = [];
        foreach ($productProperties as $value) {
            $product[] = $value['product_id'];
        }
        foreach ($product as $id) {
            $model = self::find()
                    ->where([
                        'product_id'=>$id,
                        'properties_id'=>[1,2,3,6,7,8,9,10,11,12,17,20,22,23],
                        'ordering'=>[0,1,2,3,4]
                    ])
                    ->asArray()
                    ->all();
             
            $productArticle = Product::find()
                    ->where(['id'=>$id])
                    ->select('article')
                    ->asArray()
                    ->one();
            
            //ищет все товары по заданому артикулу
            $productScope = Product::find()
                    ->where(['article'=>$productArticle['article']])
                    ->select('id')
                    ->asArray()
                    ->all();
            
            foreach ($productScope as $value) {
                foreach ($model as $item) {
                    $productProperties = self::find()
                            ->where([
                                'product_id'=>$value['id'],
                                'properties_id'=>$item['properties_id'],
                                'value'=>$item['value'],
                                'ordering'=>$item['ordering'],
                            ])
                            ->one();
                    if(!$productProperties){
                        $productProperties = new self();
                        $productProperties->product_id = $value['id'];
                        $productProperties->properties_id = $item['properties_id'];
                        $productProperties->value = $item['value'];
                        $productProperties->ordering = $item['ordering'];
                        $result [$value['id']] = $productProperties->save();
                    }
                }
            }
        }
        return $result;
    }
    
    
     /**
     * добавляет к таблице артикуль вместо id_product
     */
    public static function setArticle(){
        $productProperties = self::find()
                ->andWhere(['product_properties.article'=>null])
                ->joinWith('productForArticle')
                ->limit(1000)
                ->all();
      
        foreach ($productProperties as $value) {
            $model = self::find()
                    ->where([
                        'article'=>$value['productForArticle']->article,
                        'properties_id'=>$value['properties_id'],
                        'value'=>$value['value'],
                        'ordering'=>$value['ordering'],
                    ])
                    ->one();
            if(!$model){
                $value->article = $value['productForArticle']->article;
                vd($value->update(), false);
            }else{
                vd($value->delete(), false);
            }
        }
    }
}
