<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;


class Region extends  \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "region";
    }

    public static function getRegion($country_id, $COUNTRY_ID = null){
        if(!$country_id)
            $country_id = $COUNTRY_ID;
        
        $model = self::find()
                    ->where(['country_id'=>$country_id])
                    ->select("region_id, name")
                    ->all();
        
        return ArrayHelper::map($model,'region_id', 'name');
    }
    
    public static function getCountry($region_id){
        $model = self::find()
                    ->where(['region_id'=>$region_id])
                    ->one();
        
        return $model ? $model->country_id : false;
    }
    
    public static function Name($id){
        $region = self::findOne(['region_id'=>$id]);
        return $region ? $region->name : null;
    }
}
