<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Image;
use app\modules\admin\components\TranslitFilter;



class GenereteProduct extends Product{

    public $_nameOne;
    public $_nameEnd;
    public $_compatible;
    public $_modelSearch;
    public $_pnSearch;
    public $logo_temp;
    public $images_temp;
    public $images;
    public $producer_search;
    public $partNamber = [];
    public $children = [];
    public $list_producer = [];


    /**
     * @return string the associated database table name
     */
    public static function tableName() {
//        if(in_array($_SERVER["REMOTE_ADDR"], ['193.107.106.42'])){
//            return "product2";
//        }
        return "product";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['article', '_nameOne'], 'required'], //_compatible 'category_id', 
            [['category_id', 'price', 'createtime', 'article', 'in_stock1', 'in_stock2', 'in_stock3', 'is_published', 'ya_market', 'warranty'], 'integer'],
            [['producer_id'], 'integer'],
//            [['name_lat'], 'unique', 'message' => 'Такая ссылка уже существует'],
            [['name_lat'], 'uniqueMy'],
            [['model_id'], 'isUnique'],
            [['_compatible'], 'isArray'],
            [['in_res1', 'in_res2', 'in_res3', 'trade_price', 'is_timed', 'delivery_date', 'warranty_months',  'trade_price1', 'trade_price2', 'trade_price3', 'logo', 'available', 'is_delete', 'status', 'pay_count', 'warranty', '_is_problem'], 'safe'],
            ['article', 'isIssetArticle'],
            [['_nameOne', '_nameEnd'], 'match', 'pattern' => '/[A-Za-zА-Яа-яёЁ0-9\. \-\(\)"\[\]]+$/u', 'message' => 'В названии товара допускаются только буквы и цифры, точки, знак "-" и пробелы.'],
        ];
    }
    
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'category_id' => "Категория товара",
            'producer_id' => "Производитель товара",
            'price' => "Цена",
            '_nameOne' => "Коректировочное название товаров",
            '_nameEnd' => "Коректировочное название окончания",
            '_compatible' => "Модели",
            '_modelSearch' => "Список совместимых моделей для поиска",
            '_pnSearch' => "Список совместимых парт-номеров для поиска",
            'logo' => "Логотип товара",
            'logo_temp' => "Логотип товара",
            'images' => "Изображения",
            'article' => "Артикль",
            'in_stock1' => "Количество товаров на складе 1",
            'in_stock2' => "Количество товаров на складе 2",
            'in_stock3' => "Количество товаров на складе 3",
            'is_published' => "Опубликован на сайте",
            'ya_marcet' => "yandex market",
            'producer_search' => "Добавить группу моделей для производителя",
        );
    }
    
    
    public function uniqueMy(){
        $model = self::find()
                ->where(['name_lat'=>$this->name_lat])
                ->andWhere(['!=', 'id', $this->id])
                ->one();
        if ($model) {
            $model2 = self::findOne(['article'=>$this->article, 'name_lat'=>$this->name_lat]);
            if($model2){
                $this->addError('name_lat', 'Такая ссылка в этом артикуле уже существует');
            }else{
                $url = TranslitFilter::translitUrl("{$this->name} 1");
                $this->name_lat = self::createAbsoluteUrl($this, $url, $i=1);
                return true;
            }
            return FALSE;
        }
        return TRUE;
    }
    
    public function isUnique(){
        $model = Yii::$app->db->createCommand("
                SELECT p.id, CONCAT('partsnb.ru','/',c.name_lat, '/', pr.name_lat, '/', s.name_lat, '/', p.name_lat) AS name_lat  
                FROM `product` p 
                INNER JOIN category c ON c.id = p.category_id
                INNER JOIN producer pr ON pr.id = p.producer_id
                INNER JOIN serie s ON s.id = p.serie_id
                WHERE p.`article`='{$this->article}'
                    AND p.`producer_id`= '{$this->producer_id}'
                    AND p.`model_id`='{$this->model_id}'
                    AND p.`id` != '{$this->id}'
        ")->queryOne();
        
        if ($model) {
            $this->addError('model_id', "{$model['id']} Такой товар в этом артикуле уже существует  :  {$model['name_lat']}");
            return FALSE;
        }
        return TRUE;
    }
    
    public function isIssetArticle(){
        if(isset($this->oldAttributes['article']) && $this->oldAttributes['article'] == ''){
            $model = self::findOne(['article'=>$this->article, 'is_delete'=>0, 'is_published'=>1,]);
            if($model){
                $this->addError('article', 'Такой артикль уже существует');
                $this->article = '';
                return FALSE;
            }
            return true;
        }
    }
    
    public function isArray(){
        if(!is_array($this->_compatible)){
            $this->addError('_compatible', 'Не выбрана модель для товаров');
            return FALSE;
        }else
            return TRUE;
    }
    
    public function afterFind() {
        $this->_nameOne = $this->name;
        $this->_compatible = \app\models\Model::getModelCheked($this->article);
        
        $this->_modelSearch = \app\models\SearchModel::getModelArrayId($this->article);
        $this->_pnSearch = \app\models\ProductPnSearch::getPNArrayId($this->article);
        parent::afterFind();
    }
    
    
    
    public function beforeSave($insert) {
        // Створення звязків перенесено на БД
//        if ($this->model_id != NULL && $this->article != NULL && $this->isNewRecord) {
//            $modelCompatible = new ModelCompatible();
//            $modelCompatible->article = $this->article;
//            $modelCompatible->model_id = $this->model_id;
//            $modelCompatible->save();
//        }
        return parent::beforeSave($insert);
    }
    
    /**
     * Возвращаем весь список существующих моделей для даного товара
     */
    public  function getModelInProduct(){
        $list = Yii::$app->db->createCommand("
                    SELECT model_id, m.name
                    FROM product p
                    INNER JOIN model m ON m.id = model_id
                    WHERE p.article = '$this->article' 
                        AND _is_problem=0 
                        AND status=1 
                        AND is_delete=0 
                        AND is_published = 1
                ")->queryAll();
        
        $result = [];
        foreach ($list as $value) {
            $result[$value['model_id']] = $value['name'];
        }
        return $result;
    }

    public function getSeries() {
        return $this->hasOne(Serie::className(), ['id' => 'serie_id']);
    }
    
    /**
     * Возвращаем весь список расширенных моделей по производителям
     */
    public  function setProducerModel(){
        Yii::$app->db->createCommand('SET group_concat_max_len = 500000')->execute();
        $list = Yii::$app->db->createCommand("
            SELECT m.producer_id, pr.name,
            GROUP_CONCAT( CONCAT(m.id, ':', m.name, ':', is_pn ) ORDER BY m.name SEPARATOR '; ' ) AS children
            FROM `model_compatible` mc
            INNER JOIN model m ON m.id = mc.model_id
            LEFT JOIN producer pr ON pr.id = m.producer_id
            WHERE mc.article = '{$this->article}'
            GROUP BY m.producer_id
            HAVING children IS NOT NULL 
        ")->queryAll();
            
        $producer_list = [];
        if(!empty($list)){
            foreach ($list as $key => $value) {
                $this->children[$value['producer_id']]['model']['id'] = [];
                $this->children[$value['producer_id']]['part_namber']['id'] = [];
                if($value['children']){
                    $producer_list[] = $value['producer_id'];
                    foreach (explode("; ", $value['children']) as $item) {
                        $model = explode(":", $item);
                        if(isset($model[1])){
                            $this->children[$value['producer_id']]['model']['name'] = $value['name'];
                            $this->children[$value['producer_id']]['model']['p_id'] = $value['producer_id'];
                            if($model[2]==0){
                                $this->children[$value['producer_id']]['model']['id'][$model[0]] = $model[1]; 
                            }else{
                                $this->children[$value['producer_id']]['part_namber']['id'][$model[0]] = $model[1]; 
                            }
                        }
                    }
                }else
                    $this->children = [];
            };
        }
            //Для випадаючого списку добавлення нової группи моделей виробника
            $list = Producer::find()
                    ->select(['id', 'name'])
                    ->where(['NOT', ['id'=>$producer_list]])
                    ->asArray()
                    ->all();

            $result['list']=[];
            foreach ($list as $value) {
                $this->list_producer[$value['id']]=$value['name'];
            };
        return TRUE;
    }
    
    /**
     * Створення/обновлення/видаленя - товарів з парт. номерами
     * @param type $partNamber
     * @param type $model
     */
    public static function CUDpnProduct($POST, $model){
        $postPN = isset($POST['PartNamber']) ? $POST['PartNamber'] : [];
        $PNArray = self::getPNnameArray($postPN, 1);

        //створюємо новітовари з парт номеру
        foreach ($postPN as $key => $PN) {
            $setPN = isset($model->children[$key]['part_namber']['id']) ? array_keys($model->children[$key]['part_namber']['id']) : [];
            foreach (array_diff($PN, $setPN) as $id) {
                $result = self::createProduct($model, $PNArray[$id], $id);
                if(!$result)
                    return FALSE;
            }
        }
        
        $postPN = self::getArrayOne($postPN);
        $delete = self::getArrayOneInModel($model->children, 'part_namber');
        //Видаленнятоварів парт номеру
        $product = Product::find()->where(['article'=>$model->article,'model_id'=>array_diff($delete, $postPN)])->all();
        foreach ($product as $value) {
            $value->delete();
        }
        self::updateProduct($model);
        return TRUE;
    }

    public static function updateProduct($model){
        self::updateAll(
                [
                    'price'         =>$model->price,
                    'trade_price1'  =>$model->trade_price1,
                    'trade_price2'  =>$model->trade_price2,
                    'trade_price3'  =>$model->trade_price3,
                    'in_stock1'     =>$model->in_stock1,
                    'in_stock2'     =>$model->in_stock2,
                    'in_stock3'     =>$model->in_stock3,
                    'in_res1'       =>$model->in_res1,
                    'in_res2'       =>$model->in_res2,
                    'in_res3'       =>$model->in_res3,
                    'is_timed'      =>$model->is_timed,
                    'delivery_date' =>$model->delivery_date,
                    'warranty'      =>$model->warranty,
                    'available'     =>$model->available,
                    'logo'          =>$model->logo,
                    'ya_market'     =>$model->ya_market,
                    'is_published'  =>$model->is_published,
                    'status'        =>$model->status,
//                    'is_delete'     =>$model->is_delete,
                    'updatetime' =>  time(),
                ], 
                ["article" => $model->article, 'model_id'=>'IS NOT NULL']
            );
        //обновляєм закінчення назви товарів 
        Yii::$app->db->createCommand("
            UPDATE product p
            LEFT JOIN model m ON m.id = p.model_id
            SET p.name = CONCAT(MID(p.name, 1, LOCATE(m.name, p.name) + LENGTH(m.name)), ' {$model->_nameEnd}')
            WHERE p.article = '{$model->article}';
        ")->execute();
            
        Yii::$app->db->createCommand("
            UPDATE product p
            INNER JOIN producer pr ON pr.id = p.producer_id
            INNER JOIN model m ON m.id = p.model_id
            SET p.name = REPLACE('{$model->_nameOne} {producer model} {$model->_nameEnd}', '{producer model}', CONCAT(pr.`name`, ' ', m.`name`))
            WHERE p.article = '{$model->article}'
            AND p.`name` IS NULL
        ")->execute();
        
        
//        $list = Yii::$app->db->createCommand("
//                        SELECT  p.id
//                        FROM  `product` p
//                        LEFT JOIN  image i ON i.product_id = p.id
//                        WHERE p.article = '{$model->article}' 
//                        AND i.product_id IS  NULL
//                    ")->queryAll();
                        
//        $array_id =  Yii::$app->db->createCommand("SELECT id FROM product WHERE article = '{$model->article}' AND id != '{$model->id}'")->queryAll();
//        $list = Yii::$app->db->createCommand("
//            SELECT product_id,  article, meta, url, thumb, title, description, is_delete,
//                GROUP_CONCAT(product_id SEPARATOR '; ') AS product_id,
//                COUNT(*) AS _count
//            FROM `image`
//            WHERE product_id IN (SELECT id  FROM product WHERE article = '{$model->article}')
//            GROUP BY url
//            HAVING _count = 1
//        ")->queryAll();
//            
//        if($array_id && $list){
//            foreach ($array_id as $id){
//                foreach ($list as $item) {
//                    $image = new Image();
//                    $image->attributes = $item;
//                    $image->product_id = $id['id'];
//                    if (!$image->save())
//                        return FALSE;
//                }
//            }
//
//        }
    }


    /**
     * Створюе новий товар
     * @param type $model - модель батькывського товру
     * @param type $Array - Данны з моделі
     * @param type $id - id моделі
     * @param type $imageList - Список зображень батькывського товару
     * @return boolean
     */
    public static function createProduct($model, $Array, $id) {
        
        if($model->model_id == NULL){
            $product = $model;
        }else{
            $product = new self();
        }
        
        $product->attributes = $model->attributes;
        $product->_nameOne = $model->_nameOne;
        
        $product->producer_id = $Array['producer_id'];
        $product->serie_id = $Array['serie_id'];
        $product->model_id = $id;
        
        $serch = $model->producer ? preg_match('/^(.*) '.preg_quote($model->producer->name).'/i', $model->_nameOne, $sought) : null;
        $categoryName = $serch ? $sought[1] : $product->_nameOne;
        $product->name = $categoryName." ".$Array['p_name']." ".$Array['name'];
        if(!empty($model->_nameEnd)) {
            $product->name .= ' '.$model->_nameEnd;
        }
        
        $product->name_lat = TranslitFilter::translitUrl($product->name);
        $product->_nameOne = $model->_nameOne;
        
//        vd($Array, false);
//        echo "<br/>";
//        vd($id, false);
//        echo "<br/>";
        
//        vd($product->validate(), false);
//        die("<br/>end");
        
        if($product->save()){
        //Звязок роблю в БД
//            //створюємо звязки
//            $modelCompatible = new ModelCompatible();
//            $modelCompatible->attributes = $product->attributes;
//            if(!$modelCompatible->save()) return FALSE;
            return TRUE;
        }
//        else
//            return false;
        
    }

    public static function getPNnameArray($partNamber, $is_pn) {
        $resiult = []; 
        $list = Model::find()
                ->select(['m.id', 'm.producer_id', 'm.serie_id', 'UPPER(pr.name) AS p_name', 'm.name'])
                ->from('model m')
                ->innerJoin('producer pr', 'm.producer_id = pr.id')
                ->where(['m.is_pn'=>$is_pn, 'm.id'=>self::getArrayOne($partNamber)])
                ->asArray()
                ->all();
       
        foreach ($list as $value) {
            $id = $value['id'];
            unset($value['id']);
            $resiult[$id] = $value;
        }
        return $resiult;
    }

    /**
     * перетворює двумірний масив в одномірний
     * @param type $array
     * @return type
     */
    public static function getArrayOne($array){
        $result = [];
        foreach ($array as $value) {
            foreach ($value as $item) {
                $result[]= $item*1;
            }
        }
        return  $result;
    }
    
    public static function getArrayOneInModel($array, $column){
        $result = [];
        foreach ($array as $value) {
            if(isset($value[$column])){
                foreach ($value[$column]['id'] as $key => $item) {
                    $result[]= $key;
                }
            }
        }
        return  $result;
    }
    
    /**
     * Обновить сортування артикулу
     * @param type $article
     * @return type
     */
    public static function updateSort($article, $table){
        return Yii::$app->db->createCommand("
                SET @i:= 0;
                SET @article := '{$article}';

                UPDATE {$table} p 
                LEFT JOIN (
                    SELECT `id`, `article`,
                    CASE 
                        WHEN @article NOT LIKE article
                        THEN  @i:= 1
                        ELSE  @i:=@i+1
                    END AS i, @article := `article`
                    FROM {$table} 
                    WHERE `article` = @article
                    ORDER BY `id`
                )  p2 ON p.id = p2.id
                SET p.sort = p2.i
                WHERE p.article = @article
            ")->execute();
    }
    
    
    /**
     * Обновляэ всі логотипи товари данного артикула 
     * @return type
     */
    public function updateLogoGroup(){
        return Yii::$app->db->createCommand("
                UPDATE product  
                SET logo = '{$this->logo}'
                WHERE article = '{$this->article}'
            ")->execute();
        
    }
    
    /**
     * заповняє кількість товарів на складі для головного товару артикула
     */
    public function setStok(){
        $this->attributes = Yii::$app->db->createCommand("
            SELECT MAX(p.in_stock1) AS in_stock1, MAX(p.in_stock2) AS in_stock2, MAX(p.in_stock3) AS in_stock3
            FROM product p
            WHERE article = '{$this->article}'
            GROUP BY article;
        ")->queryOne();
    }
    
}
