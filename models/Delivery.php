<?php
namespace app\models;
use Yii;

use yii\helpers\ArrayHelper;

class Delivery extends \yii\db\ActiveRecord{

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "delivery";
    }

    public function rules() {
        return [
            [['name'], 'required'],
            [['id', 'status', 'is_delete', 'page_id'], 'integer'],
            ['price','double'],
            [['description', 'adress','phones','email_info'], 'string'],
            [['id', 'name', 'description', 'status'], 'safe', 'on' => 'search'],
        ];
    }

    public function attributeLabels() {
        return array(
            'name' => "Тип доставки",
            'description' => "Описание",
            'status' => "Статус",
            'price' => 'Цена доставки',
            'page_id' => 'Ссылается на страницу',
            'adress' => 'Адрес',
            'phones' => 'Контактные телефоны',
            'email_info' => 'Дополнительная информация в письме о заказе',
        );
    }
    
     public static function itemAlias($type, $code = NULL) {
        $_items = [
            'status' => [
                1 => 'активный',
                0 => 'не активный',
            ],
        ];
        if (isset($code)) {
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        } else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
    
    public function scopes() {
        return array(
            'list' => ['select' => 'id, name', 'condition' => 'status = 1'],
        );
    }
    
    
    public function getOrders(){
        return $this->hasMany(Order::className(), ['delivery_id' => 'id']);
    }
    
    public function getPage(){
        return $this->hasOne(Page::className(), ['page_id' => 'id']);
    }

    public static function getListDelivery() {
        $model = self::find()
               ->where(['status' => 1, 'is_delete'=>0])
               ->select('id, name')
               ->orderBy('sort ASC')
               ->all();
       
       $result = [];
       foreach ($model as $value) {
           $result[$value->id] = $value->name;
       }
        return $result;
    }
    
    public static function getListStatus() {
        return [0=>"выключен", 1=>'включен'];
    }
    
    public function getStatus() {
        return $this->status ? "Да" : "Нет";
    }
    
    /**
     * Верне телефони якщо не для даної відправки товару не вказані телефони
     * @return type
     */
    public function getTelefone($payment = null) {
        if($payment && $payment == 8){
            $model = self::find()
                ->where(['page_id'=>26])
                ->andWhere(['NOT', ['phones'=>null]])
                ->one();
            return $model ? $model->phones : '';
        }
        
        if(empty($this->phone)){
            $model = self::find()
                ->where(['page_id'=>$this->page_id])
                ->andWhere(['NOT', ['phones'=>null]])
                ->one();
            return $model ? $model->phones : '';
        }else{
            return $this->phone;
        }
    }
    
    public function getUrl() {
        return $this->name;
    }
    
    public static function getDeliveryPrice(){
        $model = self::find()
                ->asArray()
                ->select('id, price')
                ->all();
        $result=[];
        foreach ($model as $value) {
            $result[$value['id']]=$value['price'];
        }
        return json_encode($result);
    }
}
