<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class Registration extends \app\models\User{

    public $verifyPassword;
    public $verifyCode;
    public $body = "Для подтверждения регистрации пройдите по ссылке";
    public static $_country_id = 1;
    
    public $rememberMe = true;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            [['username', 'password', 'email', 'verifyPassword', 'country_id'], 'required'],
            [['createtime', 'lastvisit', 'country_id', 'region_id', 'city_id', 'zip_code', 'status'], 'integer'],
            [['username'], 'string', 'max' => 20, 'message' => 'Длина имени пользователя от 3 до 20 символов.'],
            [['article'], 'unique'],
            [['email'], 'email'],
            [['email'], 'unique', 'message' => 'Пользователь с таким электронным адресом уже существует.'],
            [['username'], 'unique', 'message' => 'Пользователь с таким логином уже существует.'],
            [['username'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В имени пользователя допускаются только латинские буквы и цифры.'],
            [['name'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В имени пользователя допускаются только буквы и цифры.'],
            [['middle_name'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В отчестве пользователя допускаются только буквы и цифры.'],
            [['surname'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В фамилия пользователя допускаются только буквы и цифры.'],
            [['password'], 'string', 'max' => 128, 'min' => 4, 'message' => 'Минимальная длина пароля 4 символа.'],
            [['zip_code'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В в почтовом индексе допускаются только латинские буквы и цифры.'],
            [['address'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В в адресе допускаются только буквы и цифры.'],
            [['verifyPassword'], 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают.'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($password) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion0()
    {
        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
    }
    
    public function beforeSave($insert){
        if ($this->isNewRecord){
            $this->article = "u_".time().rand(100, 999);
        }
        return parent::beforeSave($insert);
    }
}

