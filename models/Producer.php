<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\admin\components\TranslitFilter;
use yii\db;

class Producer extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "producer";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['description','description_product'], 'safe'],
            [['name_lat', 'name'], 'unique'],
            [['name', 'name_lat'], 'string', 'max' => 255],
            [['name_lat'], 'match', 'pattern' => '/^[A-Za-z0-9 \-]+$/u', 'message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-".'],
            [['name'], 'match', 'pattern' => '/^[A-Za-zА-Яа-я0-9\- ]+$/u', 'message' => 'В названии категории допускаются только буквы и цифры, знак "-".'],
        ];
    }

    public function attributeLabels() {
        return array(
            'name' => "Наименование",
            'name_lat' => "Имя для ссылки",
            'description' => "Описание производителя",
            'description_product' => "Описание в товаре",
        );
    }

    public function getModel() {
        return $this->hasMany(Model::className(), ['producer_id' => 'id'])->where(['_ok' => 1]);
    }

    public function getSeries() {
        return $this->hasMany(Serie::className(), ['producer_id' => 'id']);
    }

    public function getSeriesByModel() {
        return $this->hasMany(Serie::className(), ['producer_id' => 'id'])
            ->leftJoin(Model::tableName().' m', 'm.serie_id = serie.id')
            ->andWhere('m.is_pn = 0');
    }

    public function getUrlBrandPath() {
        return '/brands/'.$this->name_lat;
    }

    /*
     * Перед тек как сохранить в БД
     */

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (!$this->name_lat) {
                $this->name_lat = TranslitFilter::translitUrl($this->name);
            } else {
                $model = self::findOne($this->id);
                if ($model && $model->name != $this->name && $model->name_lat == $this->name_lat)
                    $this->name_lat = TranslitFilter::translitUrl($this->name);
            }
            return TRUE;
        } else
            return false;
    }

    /**
     * Список производителей для siteMap
     */
    public static function getProducerListUrl() {
        /*$query = new \yii\db\Query();
        $query->select(['m.name', 'm.name_lat', 'm.id'])
            ->from ([self::tableName().' m'])
            ->leftJoin(Product::tableName().' p','m.id = p.producer_id')
            ->distinct()
            ->where([
                'p._is_problem' => 0,
                'p.is_delete' => 0,
                'p.is_published' => 1,
            ])
            ->andWhere(['not', ['m.name_lat' => '']]);

        $model =  $query->all();*/


        $model = self::find()
            ->select(['producer.name', 'producer.name_lat', 'producer.id'])
            ->leftJoin(Product::tableName().' p','producer.id = p.producer_id')
            ->distinct()
            ->where(['not', ['producer.name_lat' => '']])
            ->andWhere([
                'p._is_problem' => 0,
                'p.is_delete' => 0,
                'p.is_published' => 1,
            ])
            //->asArray()
            ->all();

        $result = [];
        foreach ($model as $value) {
            $result[$value['id']] = ['slug' => $value['name_lat'], 'name' => $value['name']];
        }

        return $result;
    }

    /**
     * Вернет масив всех производителей
     */
    public static function getProducerArray() {
        return \yii\helpers\ArrayHelper::map(self::find()->orderBy('name asc')->all(), 'id', 'name');
    }

    /**
     * ищем в тексте производителя
     * возвращаем его id
     * @param type $serch
     * @return type
     */
    public static function seartchProducer($serch) {
        if(preg_match("/p=([\d,]+)/iu", $serch, $matches)){
            $list = Yii::$app->db->createCommand("
                        SELECT  
                            GROUP_CONCAT(`id` SEPARATOR ',') AS `id`,
                            GROUP_CONCAT(name SEPARATOR ',') AS `name`
                        FROM  `producer`
                        WHERE `id` IN ({$matches[1]})
                    ")->queryOne();
           
            foreach (explode(',', $list['name']) as $value) {
               $serch = trim(preg_replace("/{$value}/iu", '', $serch));
            }
            
            $serch = trim(preg_replace("/p=([\d,]+)/iu", '', $serch));
            return ['key' => $list['id'], 'name' => $list['name'], 'request'=>$serch];
        }
        
        $list = Yii::$app->db->createCommand("
                    SELECT 
                        GROUP_CONCAT(`id` SEPARATOR ',') AS `id`,
                        GROUP_CONCAT(name SEPARATOR ',') AS `name`
                    FROM `producer`
                    WHERE  '{$serch}' RLIKE CONCAT(name)
                ")->queryOne();
                    
        if($list['id'] != NULL){
            foreach (explode(',', $list['name']) as $value) {
                $serch = trim(preg_replace("/{$value}/i", '', $serch));
            }
            return ['key' => $list['id'], 'name' => $list['name'], 'request'=>$serch];
        }
        
                    
//        foreach (self::getProducerArray() as $key => $value) {
//            if (preg_match("/^($value)/i", $serch, $march)) {
//                $serch = trim(preg_replace("/{$march[1]}/iu", '', $serch));
//                return ['key' => $key, 'name' => $value, 'request'=>$serch];
//            }
//        }
        return FALSE;
    }

    /**
     * Возвращает только тех производителей у которых есть товары данной категории
     */
    public static function getProducerInCategory($category) {
        $product = Product::find()
                ->where([
                    'category_id' => $category->id,
                    '_is_problem' => 0,
                    'is_delete' => 0,
                    'is_published' => 1,
                ])
                ->select('producer_id')
                ->asArray()
                ->distinct()
                ->all();
        $result = [];
        foreach ($product as $value) {
            $result[] = $value['producer_id'];
        }
        return self::find()->where(['id' => $result])->all();
    }

    /**
     * Возвращает только те категории, у которых есть товары данного производителя
     */
    public function getCategoriesByBrand($brand) {
        $product = Product::find()
            ->select('category_id')
            ->andWhere([
                'product._is_problem' => 0,
                'product.is_published' => 1,
                'product.producer_id' => $brand->id
            ])
            ->asArray()
            ->distinct()
            ->all();
        $result = [];
        foreach ($product as $value) {
            $result[] = $value['category_id'];
        }
        return Category::find()->where(['id' => $result])->all();
    }

    public function getCountProduct($category_id) {
        return '#'; // Product::find()->where(['producer_id' => $this->id, 'category_id'=>$category_id])->count();
    }

    /**
     * масив производитнлнй для левого меню
     */
    public static function getProducer($category_id) {
        $result = [];
        $producer = self::find()->all();

        foreach ($producer as $key => $value) {
            $count = $value->getCountProduct($category_id);
            if ($count) {
                $result[$key] = [
                    'label' => $value->name . " (" . $count . ")",
                    'url' => Url::toRoute(["/product/producer", 'cat' => $category_id, 'prod' => $value->id]),
                        //                'active'=>$value->isActive()
                ];
            }
            $result[$key] += ['items' => Model::getModel($category_id, $value->id)];
        }
        return $result;
    }

    /**
     * возвращает список всех производителей
     */
    public static function grtProducerArray() {
        $producer = self::find()
                ->select('name_lat, name')
                ->all();
        return ArrayHelper::map($producer, 'name_lat', 'name');
    }

    /**
     * Возвращает список производителейи и его модели
     */
    public static function getProducerMOdel() {
        $model = self::find()
                ->asArray()
                ->select('model.name, producer.id, producer.name')
                ->joinWith('model')
                ->all();
        $i = 1;
        foreach ($model as $key => $value) {
            $producer[$key] = [
                'title' => $value['name'],
                'key' => $i++
            ];
            foreach ($value['model'] as $item) {
                $producer[$key] += ['folder' => true];
                $producer[$key]['children'][] = ['title' => $item['name'], 'key' => $i++];
            }
        }
        return $producer;
    }

    /**
     * повертає id виробника шукаючи його в назві товару name
     * @param type $producers
     * @param type $name
     * @return type
     */
    public static function getIdProducerInName($producers, $name) {
        $result = [];
        $key = NULL;
        $srt = '';
        foreach ($producers as $producer) {
            if (preg_match("/($producer[name])/i", $name, $matches)) {
                $result[$producer['id']] = $matches[1];
                $key = $producer['id'];
            }
        }
        if(empty($result)){
            return ['id'=>null, 'name'=>null];
        }else{
            if (count($result) > 1) {
                foreach ($result as $k => $val) {
                    if (strlen($srt) < strlen($val)) {
                        $key = $k;
                        $srt = $val;
                    }
                }
            }else
                $srt = $result[$key];
            
            return ['id'=>$key, 'name'=>$srt];
        }
    }

//    public function search($category, $producer) {
//        $query = Product::find();
//        $query->andFilterWhere([
//            'category_id' => $category->id,
//            'producer_id' => $this->id,
//            '_is_problem' => 0,
//            'status' => 1,
//        ]);
//
//        $dataProvider = new \yii\data\ActiveDataProvider([
//            'query' => $query,
//            'pagination' => [
//                'pageSize' => 8,
//            ],
//        ]);
//        return $dataProvider;
//    }
}
