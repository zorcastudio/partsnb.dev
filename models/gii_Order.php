<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $delivery_id
 * @property integer $payment_id
 * @property double $price
 * @property integer $currency_id
 * @property string $description
 * @property string $full_name
 * @property string $address
 * @property string $phone
 * @property integer $country_id
 * @property integer $city_id
 * @property integer $region_id
 * @property integer $zip_code
 * @property integer $is_delete
 * @property integer $createtime
 * @property integer $status
 *
 * @property Country $country
 * @property Currency $currency
 * @property User $user
 * @property Delivery $delivery
 * @property Payment $payment
 * @property City $city
 * @property Region $region
 * @property Region $region0
 * @property Region $region1
 * @property OrderProduct[] $orderProducts
 * @property Product[] $products
 */
class gii_Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'delivery_id', 'payment_id', 'price', 'currency_id', 'description', 'full_name', 'address', 'phone', 'country_id', 'is_delete', 'createtime', 'status'], 'required'],
            [['user_id', 'delivery_id', 'payment_id', 'currency_id', 'country_id', 'city_id', 'region_id', 'zip_code', 'is_delete', 'createtime', 'status'], 'integer'],
            [['price'], 'integer'],
            [['description', 'address'], 'string', 'max' => 255],
            [['full_name'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'delivery_id' => 'Delivery ID',
            'payment_id' => 'Payment ID',
            'price' => 'Price',
            'currency_id' => 'Currency ID',
            'description' => 'Description',
            'full_name' => 'Full Name',
            'address' => 'Address',
            'phone' => 'Phone',
            'country_id' => 'Country ID',
            'city_id' => 'City ID',
            'region_id' => 'Region ID',
            'zip_code' => 'Zip Code',
            'is_delete' => 'Is Delete',
            'createtime' => 'Createtime',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion0()
    {
        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion1()
    {
        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProducts()
    {
        return $this->hasMany(OrderProduct::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('order_product', ['order_id' => 'id']);
    }
}