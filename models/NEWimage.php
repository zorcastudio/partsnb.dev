<?php
namespace app\models;

use Yii;

class NEWimage extends  \yii\db\ActiveRecord{

    public $images_temp;
    
 

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "NEWimage";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            
            [['url', 'product_id','meta','thumb', 'title', 'description', 'is_delete'], 'safe'],
            [['url'], 'string', 'max' => 255],
//            [['product_id', 'is_delete', 'url'], 'safe', 'on' => 'search'],
        ];
    }

      
    public function getProduct(){
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    
    public function scopes() {
        return array(
            'active' => ['condition' => 'image.is_delete = 0'],
        );
    }
    
    public function defaultScope() {
        return [ 'condition' => 'image.is_delete = 0'];
    }
    
    /**
     * генерує унікальні силки зображень у дочірніх товарів 
     * створює зображення товарів для дочірніх з батьківського товару
     */
    public static function setUniqeUrl(){
        $listProduct = Yii::$app->db->createCommand("
                        SELECT  p.id ,  p.article, i.url, ni.url AS new
                        FROM  `product` p
                        LEFT JOIN  image i ON i.product_id = p.id
                        LEFT JOIN NEWimage ni ON ni.product_id=p.id
                        WHERE p.article IS NOT NULL 
                        AND p.article !=  '' 
                        AND i.url IS NOT NULL
                        AND ni.url IS NULL
                        GROUP BY p.article
                        LIMIT 10
                    ")->queryAll();
        
        $result = [];
        foreach ($listProduct as $value) {
            //зображення товару унікальне
            $images = Image::find()
                    ->where(['product_id'=>$value['id']])
                    ->asArray()
                    ->all();
           
            $product = Product::find()
                    ->select('id, article')
                    ->where(['article'=>$value['article']])
                    ->asArray()
                    ->all();
            
            foreach ($product as $item) {
                foreach ($images as $im) {
                    $image = new NEWimage();
                    $image->product_id = $item['id'];
                    $image->article = $item['article'];
                    $image->meta = $im['meta'];
                    $image->url = $im['url'];
                    $image->thumb = $im['thumb'];
                    $image->title = $im['title'];
                    $image->description = $im['description'];
                    $image->is_delete = $im['is_delete'];
                    $result[$value['article']][$item['id']][]= $image->save();
                }
            }
        }
        return $result;
    }

    
    /**
     * Создает изображения для товаров с старой таблицы joomla
     */
    public static function setImagesFoProduct($article){
        $list = Yii::$app->db->createCommand("
                        SELECT  p.id, p.article, `_original_id` ,file_url, file_meta, file_description, file_url_thumb
                        FROM `product` p
                        LEFT JOIN j251_virtuemart_product_medias jpm ON jpm.virtuemart_product_id = p._original_id
                        LEFT JOIN j251_virtuemart_medias  jm USING(virtuemart_media_id)
                        WHERE p.article = '$article' AND  jpm.virtuemart_media_id IS NOT NULL
                        GROUP BY _original_id, file_url
                    ")->queryAll();
        
        $result = [];
        if(!empty($list)){
            foreach ($list as $value) {
                $url = preg_replace('/\w*\//i', '', $value['file_url']);
                $image = self::find()->where(['product_id'=>$value['id'], 'url'=>$url]);
                if(!$image->one()){
                    $image = new Image();
                    $image->product_id = $value['id'];
                    $image->article = $value['article'];
                    $image->title = $value['file_meta'];
                    $image->url = $url;
                    $image->thumb = $value['file_url_thumb'];
                    $image->description = $value['file_description'];
                    $result[$value['id']][] = $image->save();
                }
            }
        }else{
            $list = Yii::$app->db->createCommand("
                        SELECT  jp.virtuemart_product_id AS id, jp.product_sku AS article, 
                                TRIM(LEADING 'images/stories/virtuemart/product/' FROM file_url) AS url,   
                                TRIM(LEADING 'images/stories/virtuemart/product/resized/' FROM file_url_thumb) AS thumb,
                                file_meta AS title, 
                                file_description AS description   
                        FROM `j251_virtuemart_products` jp
                        LEFT JOIN j251_virtuemart_product_medias jpm USING(virtuemart_product_id)
                        LEFT JOIN j251_virtuemart_medias jm USING(virtuemart_media_id)
                        WHERE `product_sku` LIKE '%$article%'
                        GROUP BY article, url
                    ")->queryAll();
            if(!empty($list)){
                $product = Product::find()->where(['article'=>$list[0]['article']])->one();
                if($product){
                    foreach ($list as $value) {
                        $image = self::find()->where(['product_id'=>$product->id, 'url'=>$value['url']]);
                        if(!$image->one()){
                            $image = new Image();
                            $image->product_id = $product->id;
                            $image->article = $value['article'];
                            $image->title = $value['title'];
                            $image->thumb = $value['thumb'];
                            $image->url = $value['url'];
                            $image->description = $value['description'];
                            $result[$product->id][] = $image->save();
                        }
                    }
                    $product->_original_id = $list[0]['id'];
                    $result['id'] = $product->update();
                }
            }
        }
        return $result;
    }
    
    /**
     * Догенерирует изображения к товарам если были раннее созданы изображения по артикулу
     */
    public static function genereteImagesFoArticle($article){
        $product = Yii::$app->db->createCommand("
                        SELECT p.id, p.article, i.url
                        FROM `product` p
                        LEFT JOIN image i ON product_id = p.id
                        WHERE p.article = '$article' AND  i.url IS NULL
                    ")->queryAll();
        
        $result = [];
        if(!empty($product)){
            $iamgeOne = Yii::$app->db->createCommand("
                        SELECT i.*
                        FROM `image` i
                        WHERE i.article = '$article'
                    ")->queryOne();
            if($iamgeOne){
                $iamges = Yii::$app->db->createCommand("
                            SELECT i.*
                            FROM `image` i
                            WHERE i.article = '$article' AND product_id = $iamgeOne[product_id]
                        ")->queryAll();
                foreach ($product as $value) {
                    foreach ($iamges as $item) {
                        $image = self::find()->where(['product_id'=>$value['id'], 'url'=>$item['url']]);
                        if(!$image->one()){
                            $image = new Image();
                            $image->product_id = $value['id'];
                            $image->article = $value['article'];
                            $image->title = $item['title'];
                            $image->thumb = $item['thumb'];
                            $image->url = $item['url'];
                            $image->description = $item['description'];
                            $result[$value['id'].' '.$value['article']][] = $image->save();
                        }
                    }
                }
            }
            
        }else{
            return -1;
        }
        return $result;
    }
    
    
    /**
     * Створення зображень з батьківских товарів
     * -2 відсутні зо
     */
    public static function createImageInParentProduct(){
        $listProduct = ProductCreate::find()
//                ->select('product.id,  product.article, i.product_id, i.url')
                ->leftJoin('image i', 'i.product_id = product.id')
                ->where('product.article !=  "" AND  i.product_id IS NULL AND is_published != -2 ')
                ->limit(100)
                ->all();
        
        if(empty($listProduct))
            return -1;
        
        foreach ($listProduct as $value) {
            $image = Image::find()->where(['article'=>$value['article']])->one();
            if(!$image){
                $value->is_published = -2;
                $result[$value->id] = $value->save();
            }else{
                $imageParent = Image::find()->where(['article'=>$value['article']])->one();
                $imageParentArray = Image::find()->where(['product_id'=>$imageParent->product_id])->all();
                foreach ($imageParentArray as $item) {
                    $image = new Image();
                    $image->product_id = $value->id;
                    $image->article = $value->article;
                    $image->title = $item['title'];
                    $image->thumb = $item['thumb'];
                    $image->url = $item['url'];
                    $image->description = $item['description'];
                    $result[$value->id.' '.$value['article']][] = $image->save();
                }
            }
        }
        return $result;
    }
    
    /**
     * Генерує зображення до товарів отримавши данні товарів які були проаналізовані на відповідь 404
     * @return type
     */
     public static function createImageInProductInErrorAnsver(){
        $listProduct = Yii::$app->db->createCommand("
                    SELECT  `ansver`,  `url_test`, p.`id`, p.article, p.`name`, p.`is_delete`, `status`, `is_published`  
                    FROM `_urls` u
                    LEFT JOIN product p ON u.url_test = p.name_lat
                    WHERE ansver = 404 
                    AND u.article = p.article
                    AND is_published != 1
                    AND category_id IS NOT NULL
                    AND producer_id IS NOT NULL
                    AND model_id IS NOT NULL
                    AND serie_id IS NOT NULL
                ")->queryAll();
        
        if(empty($listProduct))
            return -1;
        
        foreach ($listProduct as $value) {
            $image = Image::find()->where(['article'=>$value['article']])->one();
            $product = ProductCreate::findOne($value['id']);
            if(!$image){
                $product->is_published = -2;
                $result[$product->id] = $product->save();
            }else{
                $productImage = Image::find()->where(['product_id'=>$value['id']]);
                if($productImage->one()){
                    $product->is_published = 1;
                    $result[$product->id] = $product->update();
                }else{
                    $imageParentArray = Image::find()->where(['product_id'=>$image->product_id])->all();
                    foreach ($imageParentArray as $item) {
                        $image = new Image();
                        $image->product_id = $product->id;
                        $image->article = $product->article;
                        $image->title = $item['title'];
                        $image->thumb = $item['thumb'];
                        $image->url = $item['url'];
                        $image->description = $item['description'];
                        $result[$product->id.' '.$value['article']][] = $image->save();
                    }
                }
            }
        }
        return $result;
    }
    
    /**
     * Створюємо фото для нових товарів з таблиці joomla
     * використовуючи _original_id оригынальны id
     */
    static function createImageForProduct() {
        $list = Yii::$app->db->createCommand("
                    SELECT p.`id` , p.`article` , p.`category_id` , p.`producer_id` , p.`model_id` , p.`serie_id` , p.`name` , p.`name_lat` , p.`is_published` , p.`_original_id`, GROUP_CONCAT( p.id SEPARATOR ',') AS group_id
                    FROM  `product` p
                    LEFT JOIN  `image`  `i` ON i.product_id = p.id
                    WHERE p.article !=  ''
                    AND is_published = -2
                    AND i.product_id IS NULL 
                    AND p.category_id IS NOT NULL 
                    AND p.producer_id IS NOT NULL 
                    AND p.model_id IS NOT NULL 
                    AND p.serie_id IS NOT NULL 
                    AND p._original_id IS NOT NULL
                    GROUP BY article
                ")->queryAll();
        
        foreach ($list as $value) {
            $images = Yii::$app->db->createCommand("
                    SELECT jp.virtuemart_product_id AS id, jp.product_sku AS article, 
                        TRIM( LEADING  'images/stories/virtuemart/product/'FROM file_url ) AS url, 
                        TRIM( LEADING  'images/stories/virtuemart/product/resized/'FROM file_url_thumb ) AS thumb, 
                        file_meta AS title, file_description AS description
                    FROM  `j251_virtuemart_products` jp
                    LEFT JOIN j251_virtuemart_product_medias jpm
                    USING ( virtuemart_product_id ) 
                    LEFT JOIN j251_virtuemart_medias jm
                    USING ( virtuemart_media_id ) 
                    WHERE jp.virtuemart_product_id = $value[_original_id]
                        AND file_url IS NOT NULL
                ")->queryAll();
            foreach (explode(",", $value['group_id']) as $item) {
                foreach ($images as $v) {
                    $image = new Image();
                    $image->product_id = $item;
                    $image->article = $value['article'];
                    $image->title = $v['title'];
                    $image->thumb = $v['thumb'];
                    $image->url = $v['url'];
                    $image->description = $v['description'];
                    $image->save();
                }
                $product = ProductCreate::findOne($item);
                $product->is_published = 1;
                $product->update();
            }
        }
    }

}
