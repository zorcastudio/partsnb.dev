<?php

namespace app\models;

use Yii;
use app\modules\admin\components\TranslitFilter;
use yii\helpers\ArrayHelper;

class Novosti extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'novosti';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'is_published', 'text', 'text_short'], 'required'],
            [['name_lat', 'name'], 'unique'],
            [['is_published', 'createtime'], 'integer'],
            [['name'], 'match', 'pattern' => '/^[A-Za-zА-Яа-я0-9 \-\"\'\.]+$/u', 'message' => 'В названии новости допускаются только буквы и цифры, знак "-" и пробелы.'],
            [['name_lat'], 'match', 'pattern' => '/^[A-Za-z0-9 \-]+$/u', 'message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-" и пробелы.'],
            [['text', 'description', 'keywords', 'text_short'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'name' => "Наименование новости",
            'text' => "Текст новости",
            'text_short' => "Краткое описание",
            'name_lat' => "Имя для ссылки",
            'is_published' => "Опубликовано",
            'description' => "Мета тег Description",
            'keywords' => "Мета тег Keywords",
            'createtime' => "Создан",
        ];
    }
    
     public static function itemAlias($type, $code = NULL) {
        $_items = [
            'NoYes' => [
                1 => 'Да',
                0 => 'Нет'
            ]];
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    /*
     * Перед тек как сохранить в БД
     */

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (!$this->name_lat) {
                $this->name_lat = TranslitFilter::translitUrl($this->name);
            } else {
                $model = self::findOne($this->id);
                if ($model->name != $this->name && $model->name_lat == $this->name_lat)
                    $this->name_lat = TranslitFilter::translitUrl($this->name);
            }
            return TRUE;
        } else
            return false;
    }

    public function getUrl() {
        return "/novosti/" . $this->name_lat;
    }


    /**
     * хлбные крошки
     */
    public function getBreadCrumbs() {
        $result[] = ['label' => 'Новости', 'url' => ["/novosti"]];
        $result[] = $this->name;
        return $result;
    }
    
    /**
     * Список кратных новостей
     */
    public function geShort() {
        $model = self::find()
                ->select('name, name_lat, text_short, createtime')
                ->where(['is_published'=>1])
                ->orderBy('createtime DESC')
                ->asArray()
                ->limit(4)
                ->all();
        return $model;
    }

}
