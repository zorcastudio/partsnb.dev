<?php

namespace app\models;

use Yii;

class TempProduct extends \app\models\Product {

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['_original_id', 'article', 'name', 'name_lat', 'logo', 'in_stock1', 'in_stock2', 'in_stock3', 'in_stock3', 'in_res1', 'in_res2', 'in_res3', 'category_id', 'trade_price', 'trade_price1', 'trade_price2', 'trade_price3', 'price', 'available', 'is_published', 'ya_market', 'description_short', 'description'], 'safe'],
        ];
    }

    /**
     * створює товари які існують в БД jooml але не існують в product 
     * пошук здійснюється по url
     * 17.11.15
     */
    public static function createProductNotInTable() {
//        1447758470
        $list = Yii::$app->db->createCommand("SELECT jpr.`virtuemart_product_id` AS _original_id, product_sku AS article, jpr.`product_name` AS name, jpr.`slug` AS name_lat, product_in_stock AS in_stock1, product_in_stock_1 AS in_stock2, product_in_stock_2 AS in_stock3, product_in_res_1 AS in_res1, product_in_res_2 AS in_res2, product_availability AS available, published AS is_published, yandex AS ya_market, product_s_desc AS description_short, product_desc AS description
                FROM `j251_virtuemart_products_ru_ru` jpr
                LEFT JOIN product p ON p.name_lat = jpr.slug
                LEFT JOIN j251_virtuemart_products  jp USING(virtuemart_product_id)
                WHERE p.name_lat IS NULL AND product_sku IS NOT NULL
                LIMIT 500"
                )->queryAll();
        if (empty($list))
            return -1;

        $result = [];
        foreach ($list as $value) {
            $productParent = self::find()->where(['article' => $value['article']])->one();
            $model = new self();
            $model->createtime = time();

            if ($productParent)
                $model->attributes = $productParent->attributes;

            $model->attributes = $value;
            $result[] = $model->save() ? $model->id : $model->getErrors();
        }
        return $result;
    }
    
    /**
     * З таблиць джумли формує новий товар артикула якого не було
     * також заповнює його зображеннями
     * 24.12.2015
     * @return boolean
     */
    public static function createProductNotInArticle() {
        $producers = Yii::$app->db->createCommand("SELECT id, name FROM producer")->queryAll();
        $list = Yii::$app->db->createCommand("
            SELECT 
                vp.virtuemart_product_id AS ID, 
                vp.product_sku AS article, 
                vp.`product_in_stock_1` AS in_stock1,
                vp.`product_in_stock_2` AS in_stock2,
                vp.`product_in_stock_3` AS in_stock3,
                vp.`product_in_res_1` AS in_res1,
                vp.`product_in_res_2` AS in_res2,
                vp.`product_in_res_3` AS in_res3,
                vp.`product_availability` AS available,
                vp.`published` AS is_published,
                vp.`yandex` AS ya_market,
                vpr.product_s_desc AS  description_short,
                vpr.product_desc AS  description,
                vpr.product_name AS name,
                vpr.slug AS original_name_lat,
                vm.file_title AS logo,
                GROUP_CONCAT(CONCAT(vm.file_meta,':',TRIM(LEADING 'images/stories/virtuemart/product/' FROM vm.file_url)) SEPARATOR '; ') AS image
            FROM `j251_virtuemart_products` vp
            LEFT JOIN product p ON p.article = vp.product_sku
            INNER JOIN j251_virtuemart_products_ru_ru vpr USING(virtuemart_product_id)
            INNER JOIN j251_virtuemart_product_medias vpm USING(virtuemart_product_id)
            INNER JOIN j251_virtuemart_medias  vm USING(virtuemart_media_id)
            WHERE vp.product_sku != ''
                AND  p.article IS NULL 
            -- AND vpm.ordering = 1 -- відпала потреба так як при групіровці лишні відсіялись самі
            GROUP BY vpm.virtuemart_product_id
            ")->queryAll();
        
        if (empty($list))
            return -1;

        $result = [];
        foreach ($list as $value) {
            $model = new self();
            $model->createtime = time();
            $model->attributes = $value;
            $model->original_name_lat = $value['original_name_lat'];
            $model->category_id = Category::getIdCstegory($value['article']);
            $producerArray = Producer::getIdProducerInName($producers, $value['name']);
            $model->producer_id = $producerArray['id'];
            $model->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($model->name);
            
            if($model->save()){
                foreach (explode(';', $value['image']) as $item) {
                    $image = new Image();
                    $j_img  = explode(':', $item);
                    
                    $image->product_id = $model->id;
                    $image->article = $model->article;
                    $image->url = trim($j_img[1]);
                    $image->title = trim($j_img[0]);
                    if(!$image->save()){
                        return -1;
                    }
                }
            }else
                return -1;
        }
        return true;
    }
    
    
    

    /**
     * видаляє дублюючі url
     */
    public static function deleteDubleUrl() {
        $list = Yii::$app->db->createCommand("SELECT  `id` ,  `name` ,  `name_lat` ,  COUNT( id ) AS _count
                    FROM  `product` 
                    GROUP BY name_lat
                    HAVING _count >1"
                )->queryAll();

        foreach ($list as $value) {
            $product = self::find()
                    ->where(['name_lat' => $value['name_lat']])
                    ->all();
            foreach ($product as $key => $item) {
                if ($key != 0) {
                    vd($item->id . " " . $item->name_lat, false);
                    vd($item->delete(), false);
                }
            }
        }
    }

    /**
     * Обновляє товар визначає виробника, категорію, модель та серію
     */
    public static function setCatProdModSerie() {
        $producers = Yii::$app->db->createCommand("SELECT id, name FROM producer")->queryAll();
        $series = Yii::$app->db->createCommand("SELECT id, name FROM serie ORDER BY `name` ASC")->queryAll();

        $products = self::find()
//            ->select('`id`, `article`, `category_id`, `producer_id`, `model_id`, `serie_id`, `name`')
                ->where('producer_id IS NULL AND model_id IS NULL AND serie_id IS NULL AND name != ""')
//            ->where('id IN (86780, 86765, 86747, 86703)')
                ->andWhere(['NOT', ['is_published' => -1]])
                ->andWhere(['NOT', ['article' => null]])
                ->limit(100)
                ->all();

        if (empty($products))
            return -1;

        $result = [];
        foreach ($products as $value) {
            $category_id = Category::getIdCstegory($value['article']);
            $value->category_id = $category_id;

            $name = $value->name;
            $name = preg_replace('/\([а-яА-ЯёЁ\d\-\. ]*\)/u', '', $name);
            $name = preg_replace('/\[[а-яА-ЯёЁ\d\-\. ]*\]/u', '', $name);
            $name = preg_replace('/[а-яА-ЯёЁ]*/u', '', $name);
            $name = trim($name);

            //поверне id виробника
            $producerArray = Producer::getIdProducerInName($producers, $name);
            $value->producer_id = $producerArray['id'];

            if ($producerArray['id'] != null) {
                $name = preg_replace("/$producerArray[name]/u", '', $name);
                $name = trim($name);


                //якщо в назві моделі не буде присутній парт номер
                if (!preg_match("/([a-z\d]*\-[a-z\d]*|([a-z\d]*\.[a-z\d]*)+)/i", $name)) {
                    if (!ProductPnSearch::find()->where(['name' => $name])->one()) {
                        //поверне id серії
                        $value->serie_id = Serie::getIdSerieInName($series, $name);
                        $value->model_id = Model::getIdModelInName($value, $name);
                        if ($value->model_id && $value->save())
                            $result[$value->id] = true;
                        else {
                            $errors = $value->getErrors();
                            if (isset($errors['model_id'])) {
                                $product = Product::find()->where([
                                            'article' => $value->article,
                                            'model_id' => $value->model_id,
                                        ])->one();
                                $product->original_name_lat .=",$value->name_lat";
                                $result[$value->id] = $value->delete() ? 'Удален' : 'Не могу удалить';
                            } else {
                                $value->is_published = -1;
                                $value->update();
                                $result[$value->id] = 'Другая ошыбка';
                            }
                        }
                    } else {
                        $value->is_published = -1;
                        $value->save();
                        $result[$value->id] = $value->name;
                    }
                } else {
                    $value->is_published = -1;
                    $value->save();
                    $result[$value->id] = $value->name;
                }
            } else {
                $value->is_published = -1;
                $value->save();
                $result[$value->id] = $value->name;
            }
        }
        return $result;
    }
    
    /**
     * Видаляє дублюючі товари, 
     * добавляе ссылку для пошуку
     */
    public static function deleteLikeProduct(){
        $list = Yii::$app->db->createCommand("
                SELECT  `id` ,  `article` ,  `category_id` ,  `producer_id` ,  `model_id` ,  `serie_id` ,  `name` ,  `name_lat` ,  `original_name_lat` ,  `is_published` , COUNT( id ) AS _count
                FROM  `product` 
                WHERE -- is_published = -1 
                article IS NOT NULL 
                AND category_id IS NOT NULL 
                AND  `producer_id` IS NOT NULL 
                AND  `model_id` IS NOT NULL 
                GROUP BY article,  `category_id` ,  `producer_id` ,  `model_id` 
                HAVING _count >1
                ORDER BY  `_count` ASC 
                LIMIT 100
            ")->queryAll();
        
        foreach ($list as $item) {
            $products = self::find()
                    ->select('`id` ,  `article` ,  `category_id` ,  `producer_id` ,  `model_id` ,  `serie_id` ,  `name` ,  `name_lat` ,  `original_name_lat` ,  `is_published` ')
                    ->where(['LIKE', 'article', $item['article']])
                    ->andWhere([
                        'category_id'=>$item['category_id'],
                        'producer_id'=>$item['producer_id'],
                        'model_id'=>$item['model_id'],
                    ])
                    ->all();
            $name_lat = '';
            foreach ($products as $key => $value) {
                if($key !=0){
                    $name_lat .= ",".$value->name_lat;
                    $value->delete();
                }
            }
            $products[0]->original_name_lat .= $name_lat;
            $products[0]->save();
        }
    }

}
