<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\modules\admin\components\TranslitFilter;
use app\components\FilterWorkingCount;

class Model extends \yii\db\ActiveRecord {

    public $is_model = 1;
    public $_task = 0;
    public $child;
    public $_children = [];
    public $p_name;

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "model";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['name', 'producer_id', 'serie_id', 'name_lat'], 'required'],
            [['name'], 'isUnique'],
            [['name'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,\.\-\(\)_\/]+$/u', 'message' => 'В имени модели допускаются только буквы и цифры.'],
            [['child'], 'isArray'],
            [['producer_id', 'serie_id','delete'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['name_lat'], 'string', 'max' => 255],
            [['description'], 'string'],
        ];
    }

//    public function scenarios() {
//        $scenarios = parent::scenarios();
//        $scenarios['createModel'] = ['name', 'producer_id', 'serie_id'];
//        $scenarios['updateModel'] = ['name', 'producer_id', 'serie_id'];
//        return $scenarios;
//    }

    public function isUnique() {
        if ($this->isNewRecord) {
            $search = self::find()
                    ->where(['name' => $this->name, 'producer_id' => $this->producer_id])
                    ->one();
            if ($search) {
                $this->addError('name', 'Название модели и производитель должны быть уникальными');
                return false;
            } else
                return TRUE;
        }else {
            $search = self::find()
                    ->where(['name' => $this->name, 'producer_id' => $this->producer_id])
                    ->select('id')
                    ->one();
            if ($search && $search->id != $this->id) {
                $this->addError('name', 'Название модели и производитель должны быть уникальными');
                return false;
            } else
                return TRUE;
        }
        return TRUE;
    }

    public function isArray($attribute, $params) {
        if (!is_array($this->$attribute)) {
            $this->addError($attribute, '"Это не масив');
            return FALSE;
        } else
            return TRUE;
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'id' => "ID",
            'producer_id' => "Производитель",
            'serie_id' => "Серия",
            'name' => "Название модели",
            '_task' => "Задача",
            'child' => "Расширенный список моделей",
            'is_pn' => "Парт номер",
            'createtime' => "Создан",
            'delete' => "Удален",
            'name_lat' => "Альяс",
            'description' => 'Описание модели'
        ];
    }

    public function getProducer() {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }
    
  
    public function getProduct() {
        return $this->hasOne(\app\modules\admin\models\ProductSet::className(), ['id' => '_id']);
    }

    public function getSerie() {
        return $this->hasOne(Serie::className(), ['id' => 'serie_id']);
    }

    public function getCompotible() {
        return $this->hasMany(ModelCompatible::className(), ['model_id' => 'id']);
    }

    public function getSerieForModel() {
        return $this->hasMany(Serie::className(), ['producer_id' => 'producer_id']);
//                    ->andWhere(['like','serie.name', "model.name"]);
    }

    public function getUrlBrandPath() {
        $producer = Yii::$app->request->get('producer');
        $serie = Yii::$app->request->get('serie');
        return '/brands/'.$producer.'/'.$serie.'/'.$this->name_lat;
    }

    /**
     * Перед тек как сохранить в БД
     */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->createtime = time();
            }
            //если была изменена серия - 
            if (!$this->isNewRecord && $this->attributes['serie_id'] != $this->oldAttributes['serie_id']) {
                $this->updateSerie();
                return TRUE;
            }
            //встановить is_pn
            self::itIsPN();

            if (!$this->name_lat) {
                $this->name_lat = TranslitFilter::translitUrl($this->name);
            } else {
                $model = self::findOne($this->id);
                if ($model && $model->name != $this->name && $model->name_lat == $this->name_lat)
                    $this->name_lat = TranslitFilter::translitUrl($this->name);
            }
        };
        return TRUE;
    }

    public function afterSave($insert, $changedAttributes) {
        if (!$this->isNewRecord) {
            //после пересохранения модели с серией
            $list = Yii::$app->db->createCommand("
                    SELECT 
                    GROUP_CONCAT(id SEPARATOR ', ') AS id,
                    GROUP_CONCAT(article SEPARATOR ', ') AS article
                    FROM `product` 
                    WHERE       (`serie_id` IS NULL AND `model_id`='{$this->id}' AND `producer_id`='{$this->producer_id}') 
                            OR  (`model_id`='{$this->id}' AND `producer_id`='{$this->producer_id}')
                    HAVING id IS NOT NULL
                    ")->queryOne();
            if($list){
                //обновляем товар у которого была эта модель бе серии или обновляем серию
                Product::updateAll(['serie_id' => $this->serie_id], "id IN ({$list['id']})");
                
                //обновляем фильтры
                ProductFilterValue::updateAll(
                        ['serie_id' => $this->serie_id], "article IN ({$list['article']}) AND producer_id = '{$this->producer_id}' AND serie_id='{$this->oldAttributes['serie_id']}'");
            }
            
            $is_pn = Serie::find()
                    ->where(['id'=>$this->serie_id])
                    ->andWhere(['LIKE', 'name', 'P/N'])
                    ->exists();
            if($is_pn)
                $this->is_pn = 1;
               
            
        }else{
            $this->createtime = time();
        }
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * перед тем как удалить модель удаляем те значения в таблицах где они были
     */
    public function beforeDelete() {
        //удаляем связи
        ModelCompatible::deleteAll(['model_id' => $this->id]);
        $model = Product::find()
                ->where(['model_id' => $this->id])
                ->asArray()
                ->all();
        
        //УДАЛЯЭМО РОЗШИРЯЭМІ МОДЕЛІ
        ModelChild::deleteAll(['model_id'=>  $this->id]);
        
        $product_id = [];
        $article = [];
        foreach ($model as $value) {
            $product_id[] = $value['id'];
            $article[] = $value['article'];
        }
        
        $article = array_unique($article);
        //удаляем изображения для товаров
        Image::deleteAll(['product_id' => $product_id]);
        OrderProduct::deleteAll(['product_id' => $product_id]);
        ProductProperties::deleteAll(['article' => $article]);
        Product::deleteAll(['model_id' => $this->id]);
        //если товар был удален то перещитываем товары в фильтрах
//        if (Product::deleteAll(['id' => $product_id])) {
//            FilterWorkingCount::setCountProductFilter();
//        }
        return parent::beforeDelete();
    }
    
    
    /**
     * Встановлює моделі чи модель являється парт номером
     */
    public function itIsPN(){
        $is_pn = Serie::find()->where(['id'=>$this->serie_id, 'name'=>'P/N'])->exists();
        if($is_pn){
            $this->is_pn = 1;
        }
    }

    /**
     * Обновляем сериии в таблицах
     * @return type
     */
    public function updateSerie() {
        $serie = Yii::$app->db->createCommand("
            SELECT  name
            FROM  serie
            WHERE id = {$this->serie_id}
        ")->queryOne();
        $this->is_pn = ($serie['name'] == "P/N") ? 1 : 0;
        return Product::updateAll(['serie_id' => $this->serie_id], ['model_id' => $this->id]) ? TRUE : FALSE;
    }
   
    
    public function setChildren() {
        $result = [];
        if($this->_children){
            foreach (explode(';', $this->_children) as $value) {
                $item = explode(':', $value);
                $result[$item[0]] = preg_replace('/^( |&nbsp;)*/', '', $item[1]);
            }
            $this->_children = $result;
        }else
            $this->_children = [];
        return TRUE;
    }

    /**
     * ищем в тексте модель если есть производитель
     * возвращаем его id
     * @param type $serch
     * @return type
     */
    public static function seartchModel($serch, $producer = false) {
        $serch = preg_replace("/[а-яА-Я]*/u", '', $serch);
        $serch = preg_replace("/^\s*/u", "", $serch);
        if ($producer) {
            $serch = preg_replace("/$producer[name]/i", "", $serch);
            $serch = preg_replace("/^\s*/u", "", $serch);
            if ($serch != "") {
                $model = self::find()
                        ->where(['producer_id' => $producer])
                        ->andWhere(['like', 'name', "%" . $serch . "%", false])
                        ->select('id, producer_id, name')
                        ->asArray()
                        ->all();

                $result = [];
                foreach ($model as $key => $value) {
                    $result['key'][] = $value['id'];
                    $result['name'][] = $value['name'];
                }
                return $result;
            } else
                return false;
        } else {
            $model = self::find()
                    ->where(['like', 'name', "%" . $serch . "%", false])
                    ->select('id, name')
                    ->all();
            if (!empty($model)) {
                $result = [];
                foreach ($model as $value) {
                    $result['key'][] = $value->id;
                    $result['name'][] = $value->name;
                }
                return $result;
            } else
                return false;
        }
    }

    public function getCountProduct($category_id) {
        return '#'; // Product::find()->where(['producer_id' => $this->id, 'category_id'=>$category_id])->count();
    }

    /**
     * Существует ли данная модель
     */
    public function isExists() {
        return self::find()
                        ->where(['producer_id' => $this->producer_id, 'name' => $this->name])
                        ->exists();
    }

    /**
     * масив моделей для левого меню
     */
    public static function getModel($category_id, $model_id) {
        $result = [];
        $model = self::find()
                ->where(['producer_id' => $model_id])
                ->andWhere(['producer_id' => 1])
                ->all();

        foreach ($model as $value) {
            $result[$value->id] = [
                'label' => $value->name . " (#)",
                'url' => Url::toRoute(["/product/model", 'cat' => $category_id, 'prod' => $model_id, 'model' => $value->id]),
//                        //                'active'=>$value->isActive()
            ];
        }
//        vd($result, false);
//        foreach ($model as $key => $value) {
//            $count = $value->getCountProduct($category_id);
////            if ($count) {
//                $result[$value->id] = [
//                    'label' => $value->name . " (" . $count . ")",
//                    'url' => Url::toRoute(["/product/model", 'cat' => $category_id, 'prod' => $model_id, 'model' => $value->id]),
//                        //                'active'=>$value->isActive()
//                ];
////            }
//        }
//        vd($result, false);

        return $result;
    }

    public function findModels($brand, $series, $model=null, $category=null) {

        $type = 'all';
        $query = self::find()
            ->where([
                'producer_id' => $brand->id,
                'serie_id' => $series->id,
                'delete' => 0,
                'is_pn' => 0
            ]);
        if($model) {
            $query->andWhere(['like', 'name', $model, false]);
            $type = 'one';
        }
         $list = $query->$type();

        /*$list = Yii::$app->db->createCommand("
                SELECT  m.id, m.name AS text
                FROM product p
                LEFT JOIN producer pr ON pr.id = p.producer_id
                LEFT JOIN serie s ON s.id = p.serie_id
                LEFT JOIN category c ON c.id = p.category_id
                LEFT JOIN model m ON m.id = p.model_id
                WHERE pr.name_lat = '{$brand}'
                AND s.name_lat = '{$series}'
                AND c.name_lat = '{$category}'
        ")->queryAll();*/

//        return \yii\helpers\ArrayHelper::map($list, 'id', 'name');
        return $list;
    }

    /**
     * возвращает список моделей с сериями
     */
    public static function getModelSeries($models) {
        $result = [];
        if (isset($models['Compatible'])) {
            foreach ($models['Compatible'] as $value) {
                $model = self::findOne(['id' => $value]);
                $result[$value] = $model->serie_id;
            }
        } else
            $result = [];
        return $result;
    }

    /**
     * Список моделей по артикулу
     */
    public static function getListModelArticles($articles) {
        $modelCompatible = ModelCompatible::find()
                ->where(['article' => $articles])
                ->select('model_id')
                ->asArray()
                ->all();

        $result = [];
        foreach ($modelCompatible as $value) {
            $result[] = $value['model_id'];
        }
        $model = self::find()
                ->where(['id' => $result])
                ->select('id, name')
                ->asArray()
                ->all();
        $result = [];
        foreach ($model as $value) {
            $result[] = $value['name'];
        }
        return $result;
    }
    
    public static function getAllModel(){
        $query = self::find()
                    ->join('INNER JOIN', 'product', "model.id = product.model_id")
                    ->where("model.name LIKE 'idea%'")
                    ->limit('10');
        foreach ($query->all() as $value) {
            $result[] = $value->name;
        }
        return $result;
    }

    /**
     * список моделей с поиска
     */
    public static function getModelSerchArray($data = false) {
        if ($data) {
            $category = Category::findOne(['name_lat' => $_GET['category_id']]);
            $producer = Producer::findOne(['name_lat' => $_GET['producer_id']]);

            $serie = \app\models\Serie::find()
                    ->where(["producer_id" => $producer->id])
                    ->andWhere(['LIKE', 'name_lat', "$_GET[serie_id]", false])
                    ->asArray()
                    ->select('id, name')
                    ->one();

//        $search = explode(" ", $_GET['term']);
//        $search = array_filter ($search);
//        if(count($search) > 1){
//            $term = '';
//            foreach ($search as $key => $value) {
//                if($key == 0)
//                    $term = "model.name LIKE '".$value."%'";
//                else
//                    $term .= " AND model.name LIKE '%".$value."%'";
//            }
//        }else
//            $term = "model.name LIKE '".$search[0]."%'";


            $query = self::find()
                    ->join('INNER JOIN', 'product', "model.id = product.model_id")
                    ->where([
                        'model.producer_id' => $producer->id,
                        'model.serie_id' => $serie['id'],
                        'model.is_pn' => 0, //шукаєм тільки реальні моделі а не парт номера
                        'product.category_id' => $category->id,
                    ])
//                    ->andWhere($term)
                    ->andWhere("model.name LIKE '$_GET[term]%' OR model.name LIKE '% $_GET[term]%' ")
//                    ->andWhere(['LIKE', 'model.name', "%".$_GET['term']."%", false])
                    ->limit('10');

//            $query = Product::find()
//                    ->where([
//                        'producer_id'=>$producer->id, 
//                        'serie_id'=>$serie,
//                        'category_id' => $category->id,
//                        'is_published' => 1,
//                    ])
//                    ->andWhere(['LIKE', 'name', "%".$_GET['term']."%", false])
//                    ->limit('10');

            $result = [];
            foreach ($query->all() as $value) {
                $result[] = $value->name;
            }
            return $result;
        }
    }

    public static function getModelCheked($article) {
        if ($article) {
            $modelCompatible = ModelCompatible::find()
                    ->where(['article' => $article])
                    ->select('model_id')
                    ->asArray()
                    ->all();
            $result = [];
            foreach ($modelCompatible as $value) {
                $result[] = $value['model_id'];
            }
            return $result;
        } else {
            return [];
        }
    }

    public static function getModelArray($produser_id) {
        if (empty($produser_id) && isset($_POST['GenereteProduct']) && isset($_POST['GenereteProduct']['producer_id'])) {
            $produser_id = $_POST['GenereteProduct']['producer_id'];
        };

        if ($produser_id) {
            return ArrayHelper::map(self::findAll(['producer_id' => $produser_id]), 'id', 'name');
        } else {
            return [];
        }
    }

    public static function getModelArrayOll() {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }

    /**
     * Проверяет существует ли такая модель 
     */
    public static function isModel($modelName) {
        $serch = self::find()
                ->where(['name' => $modelName])
                ->one();
        return $serch ? $serch : false;
    }

    /**
     * Распарсивает имя товара и возвращает его модель
     */
    public static function setModel($producer_id, $name, $_id) {
//        очищаем от русских символов
        $name = preg_replace('/[а-яА-Я]*/u', '', $name);

        $model = self::find()
                ->where(['producer_id' => $producer_id, 'name' => "$name"])
                ->one();
        if (!$model) {
            $model = new self;
            $model->name = $name;
            $model->producer_id = $producer_id;
            $model->_id = $_id;
            if (!$model->save()) {
                return false;
            } else
                return $model;
        }
        return $model;
    }

    public static function createModel($producer_id, $modelName, $_id) {
        $model = \app\models\Model::findOne(['name' => $modelName, 'producer_id' => $producer_id]);
        if (!$model) {
            $model = new self;
            $model->name = $modelName;
            $model->producer_id = $producer_id;
            $model->_id = $_id;
            if (!$model->save()) {
                return FALSE;
            }
        }
        return $model;
    }

    public static function updateModelCut() {
        $model = self::find()
                ->where(['like', 'name', '%-%', FALSE])
                ->all();
        foreach ($model as $value) {
            $name = preg_replace('/\-.*$/u', '', $value->name);
            if ($value->name != $name) {
                $value->name = $name;
//                vd($value->name, false);
                $value->update();
            }
        }
    }

    /**
     * Находим в моделе производителя и правим название модели
     */
    public static function deleteModelProducer() {
        $producer = Producer::find()->asArray()->all();
        foreach ($producer as $value) {
            $model = self::find()
                    ->where(['like', 'name', "%$value[name] %", FALSE])
                    ->all();
            foreach ($model as $item) {
                $name = preg_replace("/$value[name] /i", '', $item->name);
//                vd($item->name, false);
//                echo "<br/>";
//                vd($name, false);
//                echo "<br/>";
                $item->name = $name;
                $item->save();
            }
        }



        foreach ($model as $value) {
            $value->delete();
        }
    }

    /**
     * удаляем модели что повторяются
     */
    public static function setDableProduct() {
        $models = self::find()
                ->select('`id`,name, producer_id, Count(name) as Count')
                ->groupBy(['name'])
                ->having('count(`name`)>1')
                ->all();
        foreach ($models as $value) {
            $model = self::find()
                    ->where(['producer_id' => $value->producer_id, 'name' => $value->name])
                    ->select('id, name, producer_id')
                    ->all();
            if (count($model) > 1) {
                foreach ($model as $key => $item) {
                    if ($key > 0) {
                        $item->delete();
                    }
                }
            }
        }
    }

    /**
     * Создает связи модели и товара
     */
    public static function createCompatibleInMOdel() {
        $model = self::find()
                ->select('id, producer_id, name')
                ->andWhere(['between', 'id', 0, 500])
                ->asArray()
                ->all();
        foreach ($model as $value) {
            $product = Product::find()
                    ->where(['model_id' => $value['id'], 'producer_id' => $value['producer_id']])
                    ->select('model_id, article, producer_id, name')
                    ->asArray()
                    ->all();
            if (!empty($product)) {
                foreach ($product as $item) {
                    $compatible = ModelCompatible::find()
                            ->where(['model_id' => $item['model_id'], 'article' => $item['article']])
                            ->select('model_id, article')
                            ->asArray()
                            ->one();
                    if (!$compatible) {
                        $compatible = new ModelCompatible();
                        $compatible->model_id = $item['model_id'];
                        $compatible->article = $item['article'];
                        $compatible->save();
                    }
                }
            } else {
                $serchProduct = new \app\models\Product();

//                $product = Product::find()
//                            ->where(['like','_list_model',"%$value[name]%", FALSE])
//                            ->asArray()
//                            ->one(); // находим товар для получения с него недостающей информации
////                    vd($product, false);
////                    die("<br/>end");
//                if($product){
//                    $serchProduct->in_stock1 = $product['in_stock1'];
//                    $serchProduct->in_stock2 = $product['in_stock2'];
//                    $serchProduct->in_res1 = $product['in_res1'];
//                    $serchProduct->in_res2 = $product['in_res2'];
//                    $serchProduct->trade_price = $product['trade_price'];
//                    $serchProduct->is_timed = $product['is_timed'];
//                    $serchProduct->category_id = $product['category_id'];
//                    $serchProduct->price = $product['price'];
//                    $serchProduct->name = Product::createProductName($product['article'], $value['producer_id'] , $value['id']);
//                    $serchProduct->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($serchProduct->name);
//                    $serchProduct->logo = $product['logo'];
//                    $serchProduct->available = $product['available'];
//                    $serchProduct->status = $product['status'];
//                    $serchProduct->_original_id = $product['id'];
//                    $serchProduct->createtime = time();
//                    $serchProduct->_is_new = 2; //создан с модели
//                    $serchProduct->article = $product['article'];
//                    $serchProduct->producer_id = $value['producer_id'];
//                    if($serchProduct->save()){
//                        $compatible = new ModelCompatible();
//                        $compatible->model_id = $item['model_id'];
//                        $compatible->article = $item['article'];
//                        vd($compatible->save(), false);
//                        vd($compatible, false);
//                    }
//                    vd($serchProduct, false);
//                    die("<br/>end");
//                }
            }
        }
    }

    /*     * 16.07.12
     * Прсит название товара и создает модель со связью
     */

    public static function createModelCompatibleIsProduct() {
        $product = Product::find()
                ->where(['_is_problem' => 0])
                ->andWhere(['not', ['producer_id' => 0]])
                ->select('id, name, producer_id, model_id, article')
                ->andWhere(['between', 'id', 45000, 50000])
                ->all();

        foreach ($product as $value) {
            if ($value->producer) {
                preg_match("/^.*" . $value->producer->name . " (.*)$/i", $value->name, $matches);
                if (isset($matches[1])) {
                    vd($matches, false);
                    echo "<br/>";
                    $nameMoel = preg_replace('/[а-яА-Я]*/u', '', $matches[1]);
                    $nameMoel = preg_replace('/\([а-яА-Я]*\)?/u', '', $nameMoel);
                    $nameMoel = preg_replace('/\s*$/u', '', $nameMoel);
                    $nameMoel = preg_replace('/ё/u', '', $nameMoel);
                    $nameMoel = preg_replace('/ \d$/u', '', $nameMoel);
                    $model = self::createModel($value->producer_id, $nameMoel, $value->id);
                    vd($nameMoel, false);
                    echo "<br/>";
                    if ($model) {
                        //создаем связи
                        $result = \app\models\ModelCompatible::setModelCompatible($model->id, $value->article, $value->id);
                        vd($result, false);
                        echo "<br/>";
                        $value->model_id = $model->id;
                        $value->update();
                        //                    vd($value, false);
                        echo "<br/>";
                        echo "<br/>";
                    }
                } else {
                    $value->_is_problem = 3;
                    $value->update();
                }
            }
        }
    }

    /**
     * Удаляем модели что дублируются вместе с связями 
     */
    public static function setDuplicateModel() {
        $listModel = self::find()
                ->select('`id`, producer_id,`name`, Count(name) as Count')
                ->groupBy(['name'])
                ->having('count(`name`)>1')
                ->asArray()
                ->all();
        foreach ($listModel as $value) {
            $model = self::find()
                    ->where(['producer_id' => $value['producer_id'], 'name' => $value['name']])
                    ->select('id, name, producer_id')
                    ->all();

            if (count($model) > 1) {
                foreach ($model as $key => $item) {
                    if ($key > 0) {
                        vd($item, false);
                        echo "<br/>";
                        $modelCompatible = ModelCompatible::findAll(['model_id' => $item->id]);
                        foreach ($modelCompatible as $compatible) {
                            $compatible->delete();
                        }
                        vd($item->delete(), false);
                        vd($modelCompatible, false);
                    }
                }
            }
        }
//        vd($produkt, false);
    }

    /**
     * Устанавливаем серию для моделей
     */
    public static function setSerie() {
        $producer_id = $_GET['pr'];

        $series = Serie::find()
                ->where(['serie.producer_id' => $producer_id])
                ->select('serie.id, serie.name, serie.producer_id')
                ->asArray()
                ->orderBy('serie.name')
//                ->andWhere(['between', 'id', 1000, 2000])
                ->all();
        foreach ($series as $value) {
            echo $value['name'];
            $model = self::find()
                    ->where(['producer_id' => $value['producer_id']])
                    ->andWhere(['like', 'name', "$value[name]%", FALSE]) // 07 не проверять!!!
                    ->all();
            foreach ($model as $item) {
                if (preg_match("/^($value[name])/i", $item->name)) {
//                    vd($item->name, false);
//                    echo "<br/>";
                    $item->serie_id = $value['id'];
                    vd($item->update(), false);
                }
            }
//            echo "<br/>";
//            echo "<br/>";
        }
//        vd($series, false);
    }

    /**
     * вернет все модели для которых не нашел серию
     */
    public static function getModelSerieIsNull() {
        $model = self::find()
                ->where(['serie_id' => NULL])
                ->asArray()
//                ->limit(100)
                ->count();
        vd($model, false);
    }

    /**
     * Устанавливает серии для моделей с EXEL
     */
    public static function setSerieExel() {
        $csv = file_get_contents(Yii::$app->basePath . "/web/exel/Model +series.csv");
        foreach (explode("\n", $csv) as $value) {
            $item = explode(";", $value);
            $model = Model::find()
                    ->where(['model.id' => trim($item[0])])
                    ->andWhere('serie_id IS NULL')
                    ->joinWith('producer')
                    ->one();

            if ($model) {

                $serie = Serie::find()
                        ->where([
                            'producer_id' => $model->producer->id,
                            'name' => 'Другая',
                        ])
                        ->one();
                if ($serie) {
                    $model->serie_id = $serie->id;
                    vd($model->update(), false);
                }
            }
        }
    }

    /**
     * Удаляет не нужные модели без серии
     */
    public static function deleteModel() {
        $model = self::find()
                ->where('model.serie_id IS NULL')
//                ->join('LEFT JOIN', 'model_compatible mc', 'model.id=mc.model_id')
//                ->join('LEFT JOIN', 'product p', 'model.id=p.model_id')
//                ->andWhere('mc.model_id IS NULL') //тільки ті моделі яких немае в звязуючій таблиці
//                ->andWhere('p.model_id IS NULL') //тільки ті моделі яких немае в товарах
                ->all();
        $i = 0;
        $result = [];
        foreach ($model as $value) {
            $result[$value->id] = $value->delete();
            if ($i++ > 2)
                break;
        }
        return $result;
    }

    /**
     * Показать все товары 
     */
    public static function setProductSerie() {
        $product = Product::find()
                ->where('product.serie_id IS NULL')
                ->andWhere(['not', ['model_id' => null]])
                ->joinWith('model')
//                ->join('LEFT JOIN', 'model_compatible mc', 'model.id=mc.model_id')
//                ->join('LEFT JOIN', 'product p', 'model.id=p.model_id')
//                ->andWhere('mc.model_id IS NULL') //тільки ті моделі яких немае в звязуючій таблиці
//                ->andWhere('p.model_id IS NULL') //тільки ті моделі яких немае в товарах
                ->all();

//        foreach ($product as $value) {
//            $value->serie_id = $value->model->serie_id;
//            vd($value->update(), false);
//        }
        vd($product, false);
        die("<br/>end");
    }

    /**
     * Шукаємо або створюємо модель з назви товару
     */
    public static function getIdModelInName($product, $name) {
        if ($product->producer_id && $product->serie_id) {
            $model = self::find()
                    ->where([
                        'producer_id' => $product->producer_id,
//                        'serie_id'=>$product->serie_id,
                        'name' => "$name",
                    ])
                    ->one();
            if (!$model) {
                $model = new self;
                $model->name = $name;
                $model->producer_id = $product->producer_id;
                $model->serie_id = $product->serie_id;
                $model->_id = $product->id;
                $model->createtime = time();

                if (!$model->save()) {
                    return false;
                }

                //добавимо модель в таблицю зв'язку
                ModelCompatible::createMCforProduct($product, $model->id);
                //добавимо модель в таблицю для пошуку
                SearchModel::createModelSearch($product, $name);
            }
            return $model->id;
        } else
            return null;
    }
    
    /**
     * Створює або удаляє сумістні моделі
     * @return boolean
     */
    public function createChildren(){
        $child =  array_flip($this->children);
        if(empty($this->child))$this->child = [];
        //Удаляем те модели которых нет в приходящем масиве
        $delete = array_values(array_diff($child, $this->child));
        ModelChild::deleteAll(['id'=>$delete]);
        
        //створьємто тільки нові моделі яких не було в масиві БД
        foreach (array_diff($this->child, $child) as $value) {
            $model = new ModelChild();
            $model->model_id = $this->id;
            $model->name = $this->name.$value;
            if(!$model->save()) return false;
        }
        return TRUE;
    }
    
    public function getChild(){
        $this->child = array_keys($this->children);
    }
    
    public function getChildren(){
        return ArrayHelper::map(ModelChild::find()->where(['model_id'=>$this->id])->all(), 'id', 'name');
    }
    
    public static function getPnMOdel($article){
        $model = Product::find()
                ->select(['product.id','product.name_lat', 'm.name'])
                ->innerJoin('model m', 'm.id = product.model_id')
                ->where(['article'=>$article])
                ->andWhere(['m.is_pn'=>1])
                ->asArray()
                ->all();
        $result = [];
        foreach ($model as $value) {
            $result[$value['id']] = $value['name'];
        }
        return $model ? $result : [];
    }
    
    
    public static function updateModelName(){
        $list = Yii::$app->db->createCommand("
                    SELECT m.*, p.id AS _p_id
                    FROM model m
                    LEFT JOIN product p ON p.model_id = m.id
                    WHERE m.name LIKE '%_!_%' 
                    AND p.model_id = m.id
                ")->queryAll();
        
        foreach ($list as $value) {
            $model = self::find()
                    ->where(['name'=>preg_replace('/_!_/', '', $value['name'])])
                    ->one();
            
            $model_d = self::findOne($value['id']);
            if($model){
                $product = ProductCreate::findOne($value['_p_id']);
                $product->model_id = $model->id;
                vd($product->save(), false);
                vd($model_d->delete(), false);
            }
        }
        die("<br/>end");
        
        $list = self::find()
                ->where(['LIKE', 'name','_!_'])
                ->groupBy(["TRIM(replace (name, '_!_', ' '))"])
                ->having('COUNT( * ) = 1')
                ->orderBy('name')
//                ->asArray()
                ->all();
        
        foreach ($list as $value) {
            $value->name = preg_replace('/_!_/', '', $value->name);
            vd($value->save(), false);
            vd($value->getErrors(), false);
        }
        
        vd($list, false);
        
    }
}
