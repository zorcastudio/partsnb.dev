<?php
namespace app\models;
use Yii;

class ProductCreate extends \app\models\Product {
    /**
     * @return array validation rules for model attributes.
     */
    public $p_name;
    public $_category_id;

    
    
    public function rules() {
        return [];
    }
      
    public function getProductFilterValue() {
        return $this->hasMany(ProductFilterValue::className(), ['article' => 'article'])
                    ->joinWith('filter')
                    ->joinWith('filterValue');
    }
    
    /**
     * Обновляем или создаем новый товар
     * @param type $data
     * @return type
     */
    public static function updateProductTheXml($data, $time){
        $model = \app\models\Product::findOne(['article'=>$data['article']]);
        if(!$model){
            $model = new self();
            $model->article = isset($data['article']) ? $data['article'] : null;
            $model->price = isset($data['price']) ? $data['price'] : 0;
            $model->trade_price1 = isset($data['trade_price1']) ? $data['trade_price1'] : 0;
            $model->trade_price2 = isset($data['trade_price2']) ? $data['trade_price2'] : 0;
            $model->trade_price3 = isset($data['trade_price3']) ? $data['trade_price3'] : 0;
            $model->in_stock1 = isset($data['product_in_stock1']) ? $data['product_in_stock1'] : 0;
            $model->in_stock2 = isset($data['product_in_stock2']) ? $data['product_in_stock2'] : 0;
            $model->in_stock3 = isset($data['product_in_stock3']) ? $data['product_in_stock3'] : 0;
            $model->in_res1 = isset($data['product_in_res1']) ? $data['product_in_res1'] : 0;
            $model->in_res2 = isset($data['product_in_res2']) ? $data['product_in_res2'] : 0;
            $model->in_res3 = isset($data['product_in_res3']) ? $data['product_in_res3'] : 0;
            $model->is_timed = isset($data['is_timed']) ? $data['is_timed'] : null;
            $model->delivery_date = isset($data['delivery_date']) ? $data['delivery_date'] : null;
            $model->warranty = isset($data['warranty']) ? $data['warranty'] : null;
            $model->is_published = 0;
            $model->createtime = time();
//            $model->model_id = 51607; //неопределена модель
            $result = $model->save() ? 2 : $model->getErrors();
        }else{
            $stock = 0;
            $stock += isset($data['product_in_stock1']) ? $data['product_in_stock1'] : 0;
            $stock += isset($data['product_in_stock2']) ? $data['product_in_stock2'] : 0;
            $stock += isset($data['product_in_stock3']) ? $data['product_in_stock3'] : 0;
            
            $res = 0;
            $res += isset($data['product_in_res1']) ? $data['product_in_res1'] : 0;
            $res += isset($data['product_in_res2']) ? $data['product_in_res2'] : 0;
            $res += isset($data['product_in_res3']) ? $data['product_in_res3'] : 0;
            
            $notNeedBeUpdate = Yii::$app->db->createCommand("
                    UPDATE `product` 
                    SET 
                        `price`={$data['price']}, 
                        `trade_price1`=".(isset($data['trade_price1']) ? $data['trade_price1'] : 0).", 
                        `trade_price2`=".(isset($data['trade_price2']) ? $data['trade_price2'] : 0).", 
                        `trade_price3`=".(isset($data['trade_price3']) ? $data['trade_price3'] : 0).", 
                        `in_stock1`=".(isset($data['product_in_stock1']) ? $data['product_in_stock1'] : 0).", 
                        `in_stock2`=".(isset($data['product_in_stock2']) ? $data['product_in_stock2'] : 0).", 
                        `in_stock3`=".(isset($data['product_in_stock3']) ? $data['product_in_stock3'] : 0).", 
                        `in_res1`=".(isset($data['product_in_res1']) ? $data['product_in_res1'] : 0).", 
                        `in_res2`=".(isset($data['product_in_res2']) ? $data['product_in_res2'] : 0).", 
                        `in_res3`=".(isset($data['product_in_res3']) ? $data['product_in_res3'] : 0).", 
                        `is_timed`=".(isset($data['is_timed']) ? $data['is_timed'] : NULL).", 
                        `delivery_date`='".(isset($data['delivery_date']) ? $data['delivery_date'] : NULL)."', 
                        `available`=".($stock - $res).", 
                        `updatetime`= ".time().
                        (isset($data['warranty']) ? ", `warranty` = ".(int)$data['warranty'] : ''). "
                    WHERE article = '{$data['article']}' AND model_id IS NOT NULL 
                ")->execute();
            
//            $notNeedBeUpdate = self::updateAll(
//                [
//                    'price'=>$data['price'],
//                    'trade_price1'  =>isset($data['trade_price1']) ? $data['trade_price1'] : 0,
//                    'trade_price2'  =>isset($data['trade_price2']) ? $data['trade_price2'] : 0,
//                    'trade_price3'  =>isset($data['trade_price3']) ? $data['trade_price3'] : 0,
//                    'in_stock1'     =>isset($data['product_in_stock1']) ? $data['product_in_stock1'] : 0,
//                    'in_stock2'     =>isset($data['product_in_stock2']) ? $data['product_in_stock2'] : 0,
//                    'in_stock3'     =>isset($data['product_in_stock3']) ? $data['product_in_stock3'] : 0,
//                    'in_res1'       =>isset($data['product_in_res1']) ? $data['product_in_res1'] : 0,
//                    'in_res2'       =>isset($data['product_in_res2']) ? $data['product_in_res2'] : 0,
//                    'in_res3'       =>isset($data['product_in_res3']) ? $data['product_in_res3'] : 0,
//                    'is_timed'      =>isset($data['is_timed']) ? $data['is_timed'] : null,
//                    'delivery_date' =>isset($data['delivery_date']) ? $data['delivery_date'] : null,
//                    'warranty' =>isset($data['warranty']) ? $data['warranty'] : null,
//                    'available'=> $stock - $res,
//                    'updatetime' =>  time(),
//                ], 
//                "article = {$data['article']} AND model_id_ IS NOT NULL" //AND updatetime > $time
//            );
        }
        if(isset($notNeedBeUpdate)){
            return $notNeedBeUpdate ? 1 : -1;
        }else {
            return  $result;
        }
    }


    /**
     * Исправляет url если они одинаковые в одной категории одного производителя
     */
    public static function updateUrl(){
        $product = self::find()
                ->select('name_lat, Count(name_lat) as Count')
                ->where(['_is_problem'=>0])
                ->groupBy(['name_lat'])
                ->having('count(name_lat)>1')
                ->asArray()
                ->all();
        
        foreach ($product as $value) {
            $model = self::find()
                    ->where(['name_lat'=>$value['name_lat']])
//                    ->andWhere(['original_name_lat'=>NULL])
                    ->select('id, name, name_lat, original_name_lat, old_name_lat')
//                    ->limit(2)
                    ->all();
            foreach ($model as $item) {
                $item->old_name_lat = $item->name_lat;
                $item->name_lat = self::createAbsoluteUrl($item, $value['name_lat']);
                vd($item->save(), false);
            }
//            die("<br/>end");
        }
    }
    
    /**
     * Переносит url с оригинального сайта
     */
    public static function updateUrlOriginal(){
//        vd(date('d.m.Y H:i', '1445598153'), false);
//        die("<br/>end");
        $product = self::find()
                ->select('id, article, name, name_lat, original_name_lat, old_name_lat, updatetime')
//                ->where(['article'=>['030075','020340','060146','060223','010006','060092','020318','080004','080001','080003','010492','090002','090001']])
                ->where('updatetime > 1445598153')
                ->andWhere(['not', ['original_name_lat'=>null]])
                ->all();
        
        foreach ($product as $value) {
            $value->name_lat = $value->original_name_lat;
            $value->updatetime = time();
            vd($value->save(), false);
        }
//        vd($product, false);
        die("<br/>end");
    }
    
    
    
    
    
    /**
     * Просматривает количество изображений у дочерных товаров
     * и проверяет с фотографиями главных товаров
     */
    public static function createImageEqualImageProduct(){
        $product = self::find()
            ->select('product.id, article')
//            ->where(['not', ['product._list_model'=>null]])
//            ->andWhere(['not', ['product._list_model'=>'']])
            ->andWhere(['article'=>$_POST['article']])//
            ->joinWith('images', true, "INNER JOIN")
            ->asArray()
            ->one();
    
        $result = [];
        if($product){
            $headImage = [];
            foreach ($product['images'] as $value) {
                if($value['is_delete']==0){
                    $headImage[] = $value;
                }
            }
            $countImage = count($headImage);
            $childrenProduct = self::find()
                    ->select('product.id')
                    ->where(['article'=>$product['article']])
                    ->andWhere(['not', ['product.id'=>$product['id']]])
                    ->joinWith('images', true, 'INNER JOIN')
                    ->asArray()
                    ->all();
            foreach ($childrenProduct as $value) {
                
                $childrenImage = [];
                $i=0;
                foreach ($value['images'] as $item) {
                    if($item['is_delete']==0){
                        $childrenImage[] = $item;
                    }
                }
                if($countImage != count($childrenImage)){
//                    vd($countImage .' = '. count($childrenImage), false);
//                    vd($value['images'], false);
//                    vd($childrenImage, false);
//                    die("<br/>end");
                    Image::deleteAll(['product_id'=>$value['id']]);
                    foreach ($product['images'] as $imag) {
                        if($imag['is_delete'] == 0){
                            $image = new Image();
                            $image->product_id = $value['id'];
                            $image->meta = $imag['meta'];
                            $image->url = $imag['url'];
                            $image->thumb = $imag['thumb'];
                            $image->title = $imag['title'];
                            $image->description = $imag['description'];
                            $result[$value['id']][]= $image->save();
//                        vd($image, false);
                        }
                    }
                }else{
                    $result[$value['id']][] = '*';
                }
            }
        }
        return $result;
    }
    
    /**
     * артикули товаров у которых не определен главный товар
     */
    public static function getProductNotModel(){
        $article = \app\models\ModelCompatible::find()
                ->where(['not',['article'=>null]])
                ->select('article')
                ->asArray()
                ->distinct()
                ->all();
        $result = [];
        foreach ($article as $value) {
            $result[] = $value['article'];
        }
        
        $res = [];
        foreach ($result as $article) {
            $product = self::find()
                ->select('product.id, article')
                ->where(['not', ['product._list_model'=>null]])
                ->andWhere(['not', ['product._list_model'=>'']])
                ->andWhere(['article'=>$article])
                ->joinWith('images', true, "INNER JOIN")
                ->asArray()
                ->one();
            if($product === NULL){
               $res[] = $article;
            }
        }
        echo json_encode($res);
    }
    
    
    
    public static function setUrlPoint(){
        $model = self::find()
                ->select('id, name, name_lat, original_name_lat, _original_id, old_name_lat, updatetime')
                ->where(['NOT', ['_original_id'=>null]])
                ->andWhere('updatetime < 1445496473')
                ->limit(1000)
                ->all();
//        vd(time(), false);
//        die("<br/>end");
        foreach ($model as $value) {
//            $value->old_name_lat = $value->name_lat;
            $value->name_lat = $value->original_name_lat;
            $value->updatetime = time();
//            $value->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($value->name);
            vd($value->save(), false);;
            
            echo " ".$value->original_name_lat;
//            vd($value->name, false);
//            vd("   ".$value->name_lat, false);
//            vd("   ".$value->old_name_lat, false);
            echo "<br/>";
        }
//        vd($model, false);
    }
    
    public static function updateUrlDuble(){
        $model = self::find()
                ->select('name_lat, COUNT( name_lat ) AS _count')
//                ->where(['NOT', ['_original_id'=>null]])
                ->asArray()
                ->groupBy('name_lat')
                ->having('_count > 1')
                ->all();
//        vd($model, false);
//        die("<br/>end");
        
        foreach ($model as $value) {
            $product = self::find()
                    ->select('id, name_lat, original_name_lat, old_name_lat')
                    ->where(['name_lat'=>$value['name_lat']])
                    ->all();
            unset($product[0]);
            foreach ($product as $item) {
                $item->old_name_lat = $item->name_lat;
                $item->updatetime = time();
                $item->name_lat = self::createAbsoluteUrl($item, $value['name_lat']);
                vd($item->save(), false);
            }
        }
    }
    
    /**
     * Востанавливаем удаленные товари по артикула
     */
    public static function reestablishProductIsDelete($article){
        $model = self::find()
                ->where(['article'=>$article, 'is_delete'=>1])
                ->all();
        
        foreach ($model as $value) {
            $value->is_delete = 0;
            vd($value->update(), false);
        }
//        vd($model, false);
    }
    
    /**
     * Пересмотреть все товары и обновить его модели без пробелов
     */
    public static function updateProductModelDuble(){
       $list = Yii::$app->db->createCommand("SELECT p.id,  p.article, p.category_id, p.producer_id, m.name AS model_n, p.serie_id, p.name
                                                FROM   `product` p
                                                INNER JOIN model m ON m.id = p.model_id
                                                WHERE m.name LIKE ' %' LIMIT 100"
                                        )->queryAll();
//        p.article =080002
//        AND p.category_id =10
//        AND p.producer_id =1
//        vd($list, false);
//        die("<br/>end");
        foreach ($list as $value) {
            $model = Model::find()
                    ->select('id, name')
                    ->where(['name'=>trim($value['model_n'])])
                    ->asArray()
                    ->one();
            
            $product = self::findOne($value['id']);
            $product->model_id = $model['id'];
            vd($product->save(), false);
                    
            vd($value['name'], false);
            vd($model['name'], false);
            echo "<br/>";
            echo "<br/>";
        }
//        vd($product, false);
        die("<br/>end");
    }
    
    /**
     * Удаляет ненужные модели и связи
     */
    public static function updateModelCompatibleDuble(){
       $list = Yii::$app->db->createCommand("SELECT mc.model_id, mc.article
                                                FROM  `model_compatible` mc
                                                LEFT JOIN product p 
                                                USING ( model_id ) 
                                                WHERE p.model_id IS NULL"
                                        )->queryAll();
        foreach ($list as $value) {
            $model = Model::find()
                    ->select('id, name')
                    ->where(['id'=>$value['model_id']])
                    ->one();
            vd($model->delete(), false);
        }
    }
    
    /**
     * Удаляет дублирующие товары
     */
    public static function deleteProductDuble(){
       $list = Yii::$app->db->createCommand("SELECT  `id` ,  `article` ,  `category_id` ,  `producer_id` ,  `model_id` ,  `serie_id` ,  `name` , COUNT( id ) AS _count
                                                FROM  `product`
                                                GROUP BY  `article` ,  `category_id` ,  `producer_id` ,  `model_id` ,  `serie_id`
                                                HAVING _count >1"
                                        )->queryAll();
        foreach ($list as $value) {
            $product = self::find()
                    ->where([
                        'article'=>$value['article'],
                        'category_id'=>$value['category_id'],
                        'producer_id'=>$value['producer_id'],
                        'model_id'=>$value['model_id'],
                        'serie_id'=>$value['serie_id'],
                    ])
                    ->all();
            foreach ($product as $key => $value) {
                if($key != 0){
                    vd($value->delete(), false);
                }
            }
        }
    }
    
    
    /**
     * Видаляем дублюючі товари 
     */
    public static function addArticleToImge(){
        $list = Yii::$app->db->createCommand("SELECT  `id` ,  `article` ,  `category_id` ,  `producer_id` ,  `model_id` ,  `serie_id` ,  `name` , COUNT( id ) AS _count
                                                FROM  `product`
                                                GROUP BY  `article` ,  `category_id` ,  `producer_id` ,  `model_id` ,  `serie_id`
                                                HAVING _count >1"
                                        )->queryAll();
       $image = self::find()
               ->where(['image.article'=>''])
               ->joinWith('product')
               ->limit(5000)
               ->all();
      
       foreach ($image as $value) {
           $value->article = $value->product->article;
           vd($value->update(), false);;
       }
    }
    
    
    /**
     * З таблиці product_pn_search витягуємо парт номера та генеруємо товар та всі звязки
     */
    public static function createProductInSearchPN(){
        $list = Yii::$app->db->createCommand("
            SELECT pn.article AS _article , pn.name AS _name, pr.name, pr.id AS _producer_id, pr.name AS p_name,
            (SELECT id FROM `serie` s WHERE s.producer_id = _producer_id  AND `name`='P/N') AS _serie_id 
            FROM `product_pn_search` pn
            LEFT JOIN product p ON p.article = pn.article AND p.name LIKE CONCAT('%',pn.name,'%')
            LEFT JOIN producer pr ON pr.id = (SELECT producer_id  FROM `product` WHERE `article` LIKE pn.article AND `_list_pn` LIKE CONCAT('%',pn.name,'%') LIMIT 1 )
            WHERE  p.name IS NULL
            HAVING _producer_id IS NOT NULL 
        ")->queryAll();
       
        foreach ($list as $value){
            $model = Model::find()
                    ->where(['name'=>$value['_name'], 'producer_id'=>$value['_producer_id']])
                    ->one();
           
            //створюємо нові моделі (парт номера)
            if(!$model){
                $model = new Model();
                $model->name = $value['_name'];
                $model->producer_id = $value['_producer_id'];
                $model->serie_id = $value['_serie_id'];
                $model->is_pn = 1;
                $model->save();
            }
            
            $model->p_name = $value['p_name'];
           
            $product =  self::find()->where(['article'=>$value['_article']])->one();
            $product->_nameOne = Category::getNameInCategory($value['_article']); 
            $imageList = Image::findAll(['product_id'=>$product['id']]);
            
            GenereteProduct::createProduct($product, $model, $model['id'], $imageList);
        }
        return TRUE;
        
    }
}
