<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

class ModelCut extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "model_cut";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
              [['producer_id', '_id'], 'safe'],
        ];
    }

    public function getCountProduct($category_id) {
        return '#'; // Product::find()->where(['producer_id' => $this->id, 'category_id'=>$category_id])->count();
    }

    /**
     * масив моделей для левого меню
     */
    public static function getModel($category_id, $model_id) {
        $result = [];
        $model = self::find()
                ->where(['producer_id' => $model_id])
                ->andWhere(['producer_id'=>1])
                ->all();

        foreach ($model as $value) {
            $result[$value->id] =[
                'label' => $value->name . " (#)",
                'url' => Url::toRoute(["/product/model", 'cat' => $category_id, 'prod' => $model_id, 'model' => $value->id]),
//                        //                'active'=>$value->isActive()
            ];
        }
//        vd($result, false);


//        foreach ($model as $key => $value) {
//            $count = $value->getCountProduct($category_id);
////            if ($count) {
//                $result[$value->id] = [
//                    'label' => $value->name . " (" . $count . ")",
//                    'url' => Url::toRoute(["/product/model", 'cat' => $category_id, 'prod' => $model_id, 'model' => $value->id]),
//                        //                'active'=>$value->isActive()
//                ];
////            }
//        }
//        vd($result, false);

        return $result;
    }

    public static function setModel($producer_id, $name, $_id) {
        $nameSerch = self::find()
                ->where(['producer_id' => $producer_id, 'name' => "$name"])
//                ->andWhere(['like', 'name', '%' . $name . '%', false])
                ->one();
        if (!$nameSerch) {
            $model = new self;
            $model->name = $name;
            $model->producer_id = $producer_id;
            $model->_id = $_id;
            if (!$model->save()) {
                return false;
            };
        }
        return TRUE;
    }

}
