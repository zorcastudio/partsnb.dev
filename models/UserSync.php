<?php
namespace app\models;
use yii\helpers\VarDumper;

use Yii;
use app\models\User;
class UserSync extends User{

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['username'], 'required'],
            [['zip_code', 'id', 'country_id', 'region_id', 'city_id', 'price_type', 'lastvisit'], 'integer'],
            [['username'], 'unique', 'message' => 'Пользователь с таким логином уже существует.'],
            [['email'], 'unique', 'message' => 'Пользователь с таким электронным адресом уже существует.'],
            [['phone'], 'string'],
            [['username'], 'string', 'max' => 40, 'min' => 3, 'message' => 'Длина имени пользователя от 3 до 20 символов.'],
//            [['username'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,\-\.\_ ]+$/u', 'message' => 'В имени пользователя допускаются только латинские буквы и цифры.'],
//            [['name'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-яёЁ\s, \-\_\.\(\)]+$/u', 'message' => 'В имени пользователя допускаются только буквы и цифры.'],
//            [['middle_name'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В отчестве пользователя допускаются только буквы и цифры.'],
//            [['surname'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В фамилия пользователя допускаются только буквы и цифры.'],
//            [['address'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,\.]+$/u', 'message' => 'В адресе допускаются только латинские буквы и цифры.'],
            [['status'], 'in', 'range' => [self::STATUS_NOACTIVE, self::STATUS_ACTIVE, self::STATUS_BANED, self::STATUS_MODERATOR, self::STATUS_ADMIN]],
            [['name', 'surname', 'middle_name'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 12],
            [['address'], 'string', 'max' => 255],
        ];
    }
    
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public function validatePassword($password)
    {
        return $this->password === md5($password);
    }
    
    
    public function beforeSave($insert) {
        if($this->permit){
            $this->updatetime = time();
        }
        return parent::beforeSave($insert);
    }
    
    public static function updateUserTheXml($data, $time){
        $model = self::find()
                ->where(['article'=>$data['article']])
                ->one();
        if(!$model){
            $model = new self();
            $model->article     = !empty($data['article']) ? $data['article'] : null;
            $model->username    = !empty($data['username']) ? $data['username'] : $data['article'].' no username';
            $model->password    = !empty($data['password']) ? $data['password'] : 'no pasword' ;
            $model->name        = !empty($data['name']) ?  $data['name'] : 'no name';
            $model->phone       = !empty($data['phone']) ? $data['phone'] : 'no phone';
            $model->middle_name = !empty($data['lastname']) ? $data['lastname'] : 'no lastname';
            $model->zip_code    = !empty($data['zipcode']) ? $data['zipcode'] : null;
            $model->email       = !empty($data['email']) ? $data['email'] : $data['article'].'@partsnb.ru';
            $model->address     = !empty($data['adress']) ? $data['adress'] : 'no adress';
            $model->price_type  = !empty($data['price_type']) ? $data['price_type'] : 0;
            $model->updatetime  = time();
            $model->country_id  = 1;
            $model->status  = 1;
            $model->createtime = time();
        }elseif ($model->updatetime > $time) {
             if(!empty($data['article']))
                $model->article  =  $data['article'];
            
            if(!empty($data['username']))
                $model->username = $data['username'];
            
            if(!empty($data['username']))
                $model->password = $data['password'];
            
            if(!empty($data['name']))
                $model->name = $data['name'];
            
            if(!empty($data['phone']))
                $model->phone = $data['phone'];
            
            if(!empty($data['lastname']))
                $model->middle_name = $data['lastname'];
            
            if(!empty($data['zipcode']))
                $model->zip_code = $data['zipcode'];
            
            if(!empty($data['email']))
                $model->email = $data['email'];
            
            if(!empty($data['adress']))
                $model->address = $data['adress'];
            
            if(!empty($data['price_type']) && (int)$data['price_type'])
                $model->price_type = $data['price_type'];
            $model->updatetime  = time();
            $model->country_id  = 1;
        }
        
        if($model->isNewRecord)
            return $model->save() ? TRUE : $model->getErrors();
        else
            return $model->update() ? TRUE : $model->getErrors();
        
    }
}
