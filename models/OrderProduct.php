<?php
namespace app\models;

class OrderProduct extends \yii\db\ActiveRecord {


    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "order_product";
    }
    
    public function rules() {
        return [
            [['order_id', 'product_id', 'amount'], 'integer'],
            [['order_id', 'product_id'], 'unique', 'targetAttribute' => ['order_id', 'product_id'], 'message' => 'Сочетание order_id и product_id уже существуют.']
        ];
    }
    
    
    public function getOrder(){
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    public function getProduct(){
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
