<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "filter".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_lat
 *
 * @property CategoryFilter[] $categoryFilters
 * @property Category[] $categories
 * @property FilterValue[] $filterValues
 * @property ProductFilterValue[] $productFilterValues
 * @property Product[] $products
 */
class gii_Filter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'filter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'name_lat'], 'required'],
            [['name', 'name_lat'], 'string', 'max' => 20],
            [['name_lat'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'name_lat' => 'Name Lat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryFilters()
    {
        return $this->hasMany(CategoryFilter::className(), ['filter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('category_filter', ['filter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilterValues()
    {
        return $this->hasMany(FilterValue::className(), ['filter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductFilterValues()
    {
        return $this->hasMany(ProductFilterValue::className(), ['filter_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('product_filter_value', ['filter_id' => 'id']);
    }
}