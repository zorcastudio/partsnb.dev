<?php
namespace app\models;
use yii\helpers\ArrayHelper;


use Yii;

class Payment extends \yii\db\ActiveRecord {

    public $logo_temp;
    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "payment";
    }

    public function rules() {
        return [
            [['name','type'], 'required'],
            [['status', 'type'], 'integer'],
            [['name', 'description','logo'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels() {
        return array(
            'name' => "Тип оплаты",
            'type' => "ЮР/ФИЗ лицо",
            'description' => "Описание",
            'status' => "Статус",
            'logo_temp' => "Изображение",
        );
    }
    
    public static function itemAlias($type, $code = NULL) {
        $_items = array(
            'UR/FIZ' => array(
                1 => 'Физ. лица',
                2 => 'Юр. лица',
            ),
        );
        if (isset($code)) {
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        } else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
    
    public function getOrders(){
        return $this->hasMany(Order::className(), ['payment_id' => 'id']);
    }

    public static function getListPayment($paiment_id=false) {
        $model = self::find()
                ->select('id, name')
                ->where(['status' => 1])
                ->all();
        if (!$model && !$paiment_id) return [];
        $result = ArrayHelper::map($model,'id', 'name');
        
        if($paiment_id)
            if(!isset($result[$paiment_id])) return NULL;
        else
            return NULL;

        return ($paiment_id) ? $result[$paiment_id] : $result;
    }
    
    public static function getListForOrder() {
        $model = self::find()
                ->where(['status' => 1])
                ->all();
        $result = [];
        foreach ($model as $value) {
            $result[self::itemAlias("UR/FIZ",$value->type)][] = [
                    'id'=>$value->id,
                    'name'=>$value->name,
                    'logo'=>$value->logo,
                ] ;
        }
       return $result;
    }
    
    
    

    public static function getListStatus() {
        return array(0=>"выключен", 1=>'включен');
    }
    
    public function getStatus() {
        return $this->status ? "Да" : "Нет";
    }
    
    public function getUrl() {
        return $this->name;
    }
    
     /*
     * сохраняем логотип
     * возвращаем вид сохранненых изображений
     */
    public static function saveLogo($logo) {
            $logo_temp = \yii\web\UploadedFile::getInstanceByName('Payment[logo_temp]');
            $public_path = Yii::$app->params['public_path'];
            $serch = preg_match("/\.\w{3}$/u", $logo['name']['logo_temp'], $extension);
            $name = $serch ? time() . mb_strtolower($extension[0]) : time();
            
            $path = Yii::$app->basePath . "/$public_path/images/view/" . $name;
            $temp = $logo_temp->saveAs($path, false);
        
            return json_encode(array(
                'initialPreview' => "<img src='/images/view/$name' class='file-preview-image'>",
                'initialPreviewConfig' => array('caption' => $name, 'width' => '120px'),
                'append' => false
            ));
    }
}
