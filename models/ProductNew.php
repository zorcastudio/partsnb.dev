<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use app\models\Category;
use app\models\Filter;
use app\models\ProductFilterValue;
use app\modules\admin\components\TranslitFilter;
use app\components\FilterWorkingCount;
use app\components\ProductWorkingCount;
use app\models\SearchModel;
use app\models\ProductPnSearch;
use yz\shoppingcart\ShoppingCart;
use yii\helpers\ArrayHelper;


class ProductNew extends \yii\db\ActiveRecord {

   
    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "product_new";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [];
    }
}
