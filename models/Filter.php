<?php
namespace app\models;
use Yii;
use yii\helpers\ArrayHelper;
use app\modules\admin\components\TranslitFilter;

class Filter extends \yii\db\ActiveRecord {

    
    public static function tableName() {
        return "filter";
    }

    public function rules() {
        return [
            [['name','category_id'], 'required'],
            [['category_id','search_published'], 'integer'],
            [['name'], 'isUnique'],
            [['name', 'name_lat'], 'string', 'max' => 50],
            ['name', 'match', 'pattern' => '/^[A-Za-zА-Яа-яёЁ0-9 \-\_\.]+$/u','message' => 'В поле название фильтра допускаются только латинские буквы и цифры знаки "-_.".'],
            ['name_lat', 'match', 'pattern' => '/^[A-Za-z0-9 \-\_\.]+$/u','message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-_.".'],
        ];
    }
    
    public function isUnique(){
        if($this->isNewRecord){
            $search = self::findOne(['name'=>$this->name, 'name_lat'=>$this->name_lat, 'category_id'=>$this->category_id]);
            if($search){
                $this->addError('name', 'Название модели, имя для ссылки и выбранная категория должны быть уникальными');
                return false;
            }else
                return TRUE;
        }else{
            $search = self::find()
                    ->where(['name'=>$this->name, 'name_lat'=>$this->name_lat, 'category_id'=>$this->category_id])
                    ->select('id')
                    ->one();
            if($search && $search->id != $this->id){
                $this->addError('name', 'Название модели, имя для ссылки и выбранная категория должны быть уникальными');
                return false;
            }else
                return TRUE;
        }
    }

    public function attributeLabels() {
        return array(
            'name' => "Название фильтра",
            'name_lat' => "Имя для ссылки",
            'category_id' => "Категория",
            'search_published' => "Показывать в фильтрах",
        );
    }
    
    public function scopes() {
        return array(
            'list' => array('select' => 'id, name'),
        );
    }
    
    public function relations() {
        return array(
//            'filterValue' => array(self::HAS_MANY, 'FilterValue', array('filter_id' => 'id')),
//            'value_all' => array(self::HAS_MANY, 'FilterValue',array('filter_id' => 'id')),
        );
    }
    
    public static function itemAlias($type, $code = NULL) {
        $_items = [
            'NoYes' => [
                1 => 'Да',
                0 => 'Нет'
            ]];
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
    
    /**
     * перед тем как удалить фильтр удаляем те значения в таблицах где они существовали
     */
    public function beforeDelete(){
        $model = ProductFilterValue::find()
                ->where(['filter_id'=>$this->id])
                ->select('filter_value_id')
                ->asArray()
                ->distinct()
                ->all();
        
        $productFilterValue = [];
        foreach ($model as $value) {
            $productFilterValue[] =  $value['filter_value_id'];
        };
        ProductFilterValue::deleteAll(['filter_id'=>$this->id]);
        ProductFilterCount::deleteAll(['filter_value_id'=>$productFilterValue]);
        FilterValue::deleteAll(['filter_id'=>$this->id]);
        return parent::beforeDelete();
    }
    
    public function getCategoryFilters(){
        return $this->hasMany(CategoryFilter::className(), ['filter_id' => 'id']);
    }
    
    public function getProductFilterCount(){
        return $this->hasOne(ProductFilterCount::className(), ['filter_value_id' => 'id', 'category_id'=>'category_id']);
    }

    public function getCategory(){
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getFilterValue(){
        return $this->hasMany(FilterValue::className(), ['filter_id' => 'id']);
    }
    
    public function getfilterValueParam(){
        return $this->hasMany(FilterValue::className(), ['filter_id' => 'id']);
    }

    public function getProductFilterValues(){
        return $this->hasMany(ProductFilterValue::className(), ['filter_id' => 'id']);
    }
    
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('product_filter_value', ['filter_id' => 'id']);
    }
    
    
    public function getValue_all(){
        return $this->hasMany(FilterValue::className(), ['filter_id' => 'id']);
    }
    
//    public function getValue_all(){
//        return $this->hasMany(FilterValue::className(), ['filter_id' => 'id']);
//    }
    
    /*
     * Перед тек как сохранить в БД
    */
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (!$this->name_lat) {
                $this->name_lat = TranslitFilter::translitUrl($this->name, 'filter');
            }else{
                $model = self::findOne($this->id);
                if($model->name != $this->name && $model->name_lat == $this->name_lat)
                    $this->name_lat = TranslitFilter::translitUrl($this->name);
            }
            return TRUE;
        } else
            return false;
    }

    public static function getListFilter() {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
    
    /**
     * Возвращает фильтры с количеством товаров 
     */
    public static function getFilter(){
        $category = Category::findOne(['name_lat'=>$_GET['category']]);
        $producer = Producer::findOne(['name_lat'=>$_GET['producer']]);
        $serie = Serie::findOne(['name_lat'=>$_GET['serie']]);
        if($category  && $producer && $serie){
            $result = $category->filterSearch;
//            vd($result, false);
            
            
            foreach ($result as $value) {
                foreach ($value->filterValue as $item) {
//                    vd($item, false);
//                    echo "<br/>";
//                    echo "<br/>";
//                    
//                    SELECT pfv.article, pfv.serie_id, pfv.filter_id, pfv.filter_value_id, COUNT(*)  AS _count
//                    FROM `product_filter_value` pfv
//                    LEFT JOIN product p ON p.article=pfv.article
//                    WHERE p.category_id IS NOT NULL
//                    AND p.category_id IS NOT NULL
//                    AND p.model_id IS NOT NULL
//                    AND p.producer_id IS NOT NULL
//                    AND p.serie_id IS NOT NULL
//                    AND is_delete = 0
//                    AND status = 1
//                    AND is_published = 1
//
//                    GROUP BY pfv.producer_id, pfv.serie_id, pfv.filter_value_id
                    
                    $p_f_c = ProductFilterCount::find()
                            ->where([
                                "producer_id"=>$producer->id,
                                "serie_id"=>$serie->id,
                                'filter_value_id'=>$item->filter_value_id, 
                            ])
                            ->one();
                    $item->_product_count = $p_f_c ? $p_f_c->product_count : 0;
                }
            }
//            vd($result, false);
            return $result;
        }else{
            return [];
        }
    }


    public function getUrl() {
        return Yii::$app->controller->id."/view/id/".$this->id;
    }
    
    public static function getArrayValue($filter){
        $result = $item = array();
        $result[]= array('label'=>$filter->name, 'url'=>'#') ;
        if($filter_value = $filter->filterValue){
            foreach ($filter_value as $key=>$value){
                $item['items'][] = array('label'=>$value->name, 'url'=>'#');
            }
            $result[] = $item;
            return $result;
        }else
            return false;
    }
    
    /**
     * создает фильтр
     */
    public static function createFilter($category_id, $name){
        $filter_n = FilterCreate::find()
                                    ->where([
                                        'category_id'=>$category_id,
                                        'name'=>$name,
                                    ])
                                    ->one();
        if(!$filter_n){
            $filter_n = new FilterCreate();
            $filter_n->category_id = $category_id;
            $filter_n->name = $name;
            $filter_n->save();
        }
        return $filter_n;
    }
    
    
//    public static function getArraySort($category_id){
//        vd($category_id, false);
//        return [];
//    }
    
//    public function my_sort($x, $y){
//        $compare = [
//            0 =>'Входное напряжение',
//            1 =>'Выходное напряжение',
//            2 =>'Максимальная сила тока',
//            3 =>'Тип разъёма',
//            4 =>'Комплектность',
//            5 =>'Мощность'
//        ];
//        if($x == $compare[0]) return 0;
//        
//        
//        
//        vd($x, false);
//        echo $y;
//        echo "<br/>";
//        if ($x == $y) return 0;  
//        return ($x > $y) ? -1 : 1;  
//    }
//        
//    public static function sortProperties($people){
//        vd($people, false); 
//        $model = new self;
//        echo "<br/>";
//        echo "<br/>";
//        uksort($people,  array($model, 'my_sort'));  
//        echo "<br/>";
//        vd($people, false); 
//        
//    }
    
}
