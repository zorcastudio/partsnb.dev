<?php
namespace app\models;

use Yii;
/**
 * UserRecoveryForm class.
 * UserRecoveryForm is the data structure for keeping
 * user recovery form data. It is used by the 'recovery' action of 'UserController'.
 */
class UserRecoveryForm extends Model {
	public $login_or_email, $user_id;
	
	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('login_or_email', 'required'),
			array('login_or_email', 'match', 'pattern' => '/^[A-Za-z0-9@.-\s,]+$/u','message' => "В имени пользователя допускаются только латинские буквы и цифры."),
			// password needs to be authenticated
			array('login_or_email', 'checkexists'),
		);
	}
	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'login_or_email'=>'Email',
		);
	}
	
	public function checkexists($attribute,$params) {
		if(!$this->hasErrors())  // we only want to authenticate when no input errors
		{
			if (strpos($this->login_or_email,"@")) {
				$user=User::find()->where(['email'=>$this->login_or_email])->one();
                               if ($user)
					$this->user_id = $user->id;
			} else {
				$user=User::find()->where(['username'=>$this->login_or_email])->one();
                                if ($user)
					$this->user_id=$user->id;
			}
			
			if($user===null)
				if (strpos($this->login_or_email,"@")) {
					$this->addError("login_or_email",'Пользователь с таким электроным адресом не зарегистрирован.');
				} else {
					$this->addError("login_or_email",'Пользователь с таким именем не зарегистрирован.');
				}
		}
	}
	
}