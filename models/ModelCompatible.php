<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;


class ModelCompatible extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "model_compatible";
    }
    
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [ 
                [['model_id', 'article'], 'required']
            ];
    }
    
    public function getModel() {
        return $this->hasOne(Model::className(), ['id' => 'model_id']);
    }
    
    public function getProduct() {
        return $this->hasOne(Product::className(), ['article'=>'article', 'model_id' => 'model_id']);
    }
    
    public function getProductParent() {
        return $this->hasOne(Product::className(), ['id' => '_id']);
    }
   
    public function getProductFilterValue() {
        return $this->hasMany(ProductFilterValue::className(), ['product_id' => 'id'])->viaTable(Product::tableName() . ' p', ['id' => '_id']);
    }
    
    public function getPossibleSeries() {
        return $this->hasMany(Serie::className(), ['producer_id' => 'producer_id'])->viaTable(Model::tableName() . ' m', ['id' => 'model_id']);
    }
    
    public function getProductParentImages() {
        return $this->hasMany(Image::className(), ['product_id' => '_id']);
    }
    
    
    
    public function getProducts() {
        return $this->hasMany(Product::className(), ['article'=>'article'])->select('id, article, model_id, name');
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [];
    }
    
    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            //если была изменена серия - 
            if ($this->isNewRecord) {
                $this->createtime = time();
                return TRUE;
            }
        };
        return TRUE;
    }
    
    public static function getModelCompatible($id){
        $model_c = self::find()
                ->select('model_id, article')
                ->where(['model_id'=>$id])
                ->all();
        $result = [];
        foreach ($model_c as $value) {
            $result[] = $value->article;
        }
        return implode(', ', $result);
    }

    public static function setModelCompatible($model_id, $article, $_id) {
        $model = self::find()
                ->where(['model_id' => $model_id, 'article' => $article])
                ->one();
        if (!$model) {
            $model = new self;
            $model->model_id = $model_id;
            $model->article = $article;
            $model->_id = $_id;
            if (!$model->save()) {
                return false;
            }else
                return $model;
        }
        return $model;
    }
    
    /**
     * Создаем связи если есть товар и модель
     */
    public static function createCompatible(){
        $model = Model::find()
                ->andWhere(['between', 'id', 1222, 1222])
                ->select('id, producer_id, _id')
                ->asArray()
                ->all();
        vd($model, false);
        echo "<br/>";
        foreach ($model as $value) {
            $article = Product::find()
                        ->where(['id'=>$value['_id']])
                        ->select('article')
                        ->asArray()
                        ->one();
            vd($article['article'], false);
            echo "<br/>";
            
            $product = Product::find()
                    ->where([
                        'producer_id'=>0, 
                        'article'=>$article['article'], 
                        'model_id'=>$value['id'], 
                        'producer_id'=>$value['producer_id']
                    ])
                    ->select('name, article, model_id, producer_id, id')
                    ->asArray()
                    ->one();
            vd($product, false);
            echo "<br/>";
            
            if($product){
                $modelCompatible = self::find()
                        ->where(['article'=>'050184', 'model_id'=>$product['model_id']])
                        ->asArray()
                        ->one();
                vd($modelCompatible, false);
                echo "<br/>";
                if(!$modelCompatible){
                    vd($product['article'], false);
                    vd($product['model_id'], false);
                    vd($product['id'], false);
                    
                    $modelCompatible = new self;
                    $modelCompatible->article = $product['article'];
                    $modelCompatible->model_id = $product['model_id'];
                    $modelCompatible->_id = $product['id'];
                    vd($modelCompatible->save(), false);;
                }
                
            }
            
        }
    }
    
    
     /**
     * Досоздает связи товаров с моделями
     */
    public static function setModelCompatibleISProduct(){
        $produkt = Product::find()
                ->select('id , product.article, category_id, producer_id, product.model_id, serie_id, name')
                ->join('LEFT JOIN', 'model_compatible mc', 'product.model_id = mc.model_id')
                ->where('producer_id IS NOT NULL AND product.model_id IS NOT NULL')
                ->andWhere('mc.model_id IS NULL')
                ->limit('1000')
                ->all();
    } 
    
    public static function creatProductOnModel($article){
        $product = Product::find()
                    ->where(['article'=>$article])
                    ->andWhere(['not',['producer_id'=>0, 'model_id'=>0, 'category_id'=>0]])
                    ->one(); 
        $imageList = \app\models\Image::findAll(['product_id'=>$product->id]);

        $list= Yii::$app->db->createCommand("SELECT mc.article, m.id, m.producer_id, m.serie_id, m.name AS name_m
                                                FROM model_compatible mc
                                                LEFT JOIN model m ON m.id = mc.model_id
                                                LEFT JOIN product p ON p.model_id = m.id AND p.article = mc.article
                                                WHERE p.model_id IS NULL 
                                                AND mc.article = $article")->queryAll();
        $result =[];
        foreach ($list as $value) {
            
            $modelCompatible = ModelCompatible::find()
                    ->where(['article'=>$value['article'], 'model_id'=>$value['id']])
                    ->one();
            if(!$modelCompatible){
                $modelCompatible = new ModelCompatible();
                $modelCompatible->article = $value['article'];
                $modelCompatible->model_id = $value['id'];
            }
            
            if($modelCompatible->save()){
                $serchProduct = new ProductCreate();
                $serchProduct->in_stock1 = $product->in_stock1;
                $serchProduct->in_stock2 = $product->in_stock2;
                $serchProduct->in_res1 = $product->in_res1;
                $serchProduct->in_res2 = $product->in_res2;
                $serchProduct->trade_price = $product->trade_price;
                $serchProduct->is_timed = $product->is_timed;
                $serchProduct->category_id = $product->category_id;
                $serchProduct->price = $product->price;
                $serchProduct->producer_id = $value['producer_id'];
                $serchProduct->serie_id = $value['serie_id'];
                $serchProduct->model_id = $value['id'];
                $serchProduct->name = Product::createProductName($product->category_id, $value['producer_id'], $value['id']);
                $serchProduct->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($serchProduct->name);
                $serchProduct->logo = $product->logo;
                $serchProduct->available = $product->available;
                $serchProduct->status = $product->status;
                $serchProduct->is_delete = $product->is_delete;
                $serchProduct->createtime = time();
                $serchProduct->_is_new = 2;//создан с модели
                $serchProduct->article = $value['article'];
                if($serchProduct->save() && $imageList){
                    $result[$value['article']][]= $serchProduct->id;
                    foreach ($imageList as $img) {
                        $image = new \app\models\Image();
                        $image->attributes = $img->attributes;
                        $image->product_id = $serchProduct->id;
                        $image->save();
                    }
                }
            }
            
        };
        
        return json_encode($result);
    }
    
    /**
     * Добавляє звязок моделі з продуктами
     * @param type $product
     * @return boolean
     */
    public static function createMCforProduct($product, $id) {
        $model = self::find()->where([
                    'article' => $product->article,
                    'model_id' => $id,
                ])->one();

        if (!$model) {
            $model = new self();
            $model->article = $product->article;
            $model->model_id = $id;
            $model->createtime = time();
            if (!$model->save()) {
                return false;
            };
        }
        return true;
    }
    
    /**
     * Створює звязок товару та моделі
     * 02.12.15
     * @return boolean
     */
    public static function createCompatibleInProduct(){
        $list = Yii::$app->db->createCommand("
                SELECT p.`article`,  p.`model_id`
                FROM `product` p
                LEFT JOIN model_compatible mc ON (mc.model_id = p.model_id AND mc.article = p.article)
                WHERE   mc.model_id IS NULL AND mc.article IS NULL
                    AND p.category_id IS NOT NULL
                    AND p.producer_id IS NOT NULL
                    AND p.model_id IS NOT NULL
                    AND p.serie_id IS NOT NULL
                    AND p.is_delete = 0
                    AND p.is_published = 1
                    AND p.status = 1
                LIMIT 1000
            ")->queryAll();

        foreach ($list as $value) {
            $model = new self();
            $model->article = $value['article'];
            $model->model_id = $value['model_id'];
            if(!$model->save()){
                return false;
            };
        }
        return TRUE;
    }
    
    
}
