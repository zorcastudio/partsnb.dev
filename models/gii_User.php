<?php

namespace app\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $activkey
 * @property integer $createtime
 * @property integer $lastvisit
 * @property string $name
 * @property string $surname
 * @property string $middle_name
 * @property string $phone
 * @property string $email
 * @property integer $country_id
 * @property integer $region_id
 * @property integer $city_id
 * @property string $address
 * @property integer $zip_code
 * @property integer $currency_id
 * @property integer $status
 *
 * @property Order[] $orders
 * @property Currency $currency
 * @property Country $country
 * @property Region $region
 * @property City $city
 * @property Region $region0
 */
class gii_User extends \yii\db\ActiveRecord {

    public $verifyPassword;
    public $verifyCode;
    public $body = "Для подтверждения регистрации пройдите по ссылке";
    public static $_country_id = 1;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        $result = [
            [['username', 'password', 'activkey', 'name', 'surname', 'middle_name', 'phone', 'email', 'country_id', 'address', 'currency_id'], 'required'],
            [['createtime', 'lastvisit', 'country_id', 'region_id', 'city_id', 'zip_code', 'currency_id', 'status'], 'integer'],
            [['username'], 'string', 'max' => 20],
            [['password', 'activkey', 'email'], 'string', 'max' => 128],
            [['name', 'surname', 'middle_name'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 12],
            [['address'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['email'], 'unique']
        ];
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'verticalForm')
            return $rules;
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'activkey' => 'Activkey',
            'createtime' => 'Createtime',
            'lastvisit' => 'Lastvisit',
            'name' => 'Name',
            'surname' => 'Surname',
            'middle_name' => 'Middle Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'country_id' => 'Country ID',
            'region_id' => 'Region ID',
            'city_id' => 'City ID',
            'address' => 'Address',
            'zip_code' => 'Zip Code',
            'currency_id' => 'Currency ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders() {
        return $this->hasMany(Order::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency() {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry() {
        return $this->isNewRecord ? self::$_country_id : $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion() {
        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity() {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegion0() {
        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
    }

    public static function isAdmin() {
        if (Yii::$app->user->isGuest)
            return false;
        else {
//            if (!isset(self::$_admin)) {
//                self::$_admin = (self::user()->status == 3) ? true : false;
//            }
//            return self::$_admin;
        }
    }

    /**
     * установка cookie
     */
    public static function setCookes($name, $value) {

        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => $name,
            'value' => $value,
            'expire' => time() + (60 * 60 * 24 * 30 * 12), // (1 год)
        ]));
    }

}
