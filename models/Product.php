<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use app\models\Category;
use app\models\Filter;
use app\models\ProductFilterValue;
use app\modules\admin\components\TranslitFilter;
use app\components\FilterWorkingCount;
use app\components\ProductWorkingCount;
use app\models\SearchModel;
use app\models\ProductPnSearch;
use yz\shoppingcart\ShoppingCart;
use yii\helpers\ArrayHelper;
use app\components\WaterMark\WaterMark;


class Product extends \yii\db\ActiveRecord implements \yz\shoppingcart\CartPositionInterface {

    public $logo_temp;
    public $images_temp;
    public $images;
    public $_quantity;
    public $_compatible;
    public $_nameOne;
    public $_name;
    public $_forSearch;
    public $is_problem = 0;
    public $_count; //количество товаров в категорий
    public $_filter;
    public $discount; // Страница максимальнгой скидкой
    public $h2_compatible_model;
    public $came_from; // строрынка звыдки прийшов
    public $description_product; // описание категории в карточке
    public $_spetc_price;  // текст на кнопке спец цена и с макс скидка
    public $_request;  //пошувовий текст
    

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
//        if(in_array($_SERVER["REMOTE_ADDR"], ['193.107.106.42'])){
//            return "product2";
//        }
        return "product";
    }

    /**
     * klaviatura--sony-s8b01
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['category_id', 'name', 'model_id', 'producer_id', 'model_id', 'article'], 'required'],
            [['category_id', 'available', 'price', 'createtime', 'article', 'in_stock1', 'in_stock2', 'in_stock3', 'in_res1', 'in_res2', 'in_res3','is_published', 'ya_market', 'warranty'], 'integer'],
            [['name_lat', 'name'], 'unique', 'on' => 'update'],
            [['name_lat'], 'uniqueMy'],
//            [['name_lat'], 'unique', 'message' => 'Такая ссылка уже существует'],
            [['model_id'], 'isModel'],
            [['name_lat'], 'isUnique'],
//            ['available', 'boolean'],
//            ['name', 'match', 'pattern' => '/^[A-Za-zА-Яа-я0-9 \-]+$/u', 'message' => 'В названии категории допускаются только буквы и цифры, знак "-" и пробелы.'],
            ['_nameOne', 'match', 'pattern' => '/^[A-Za-zА-Яа-я0-9\. \-]+$/u', 'message' => 'В названии товара допускаются только буквы и цифры, знак "-" точки и пробелы.'],
            [['name_lat'], 'match', 'pattern' => '/[A-Za-z0-9\. \-\(\)]+$/u', 'message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-", точки и пробелы.'],
            [['description', 'description_short', 'logo', '_list_model', '_list_pn'], 'safe'],
            [['description', 'name_lat'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['name_lat'], 'string', 'max' => 100],
            [['logo'], 'string', 'max' => 200],
            [['article'], 'string', 'min' =>6],
            [['description_short'], 'string', 'max' => 500]
        ];
    }
    
    
    
    public function uniqueMy(){
        $model = self::findOne(['name_lat'=>$this->name_lat]);
        if ($model && $this->id != $model->id) {
            $this->addError('name_lat', 'Такая ссылка уже существует');
            return FALSE;
        }
        return TRUE;
    }
    
    public function isModel(){
        $model = self::findOne(['article'=>$this->article, 'model_id'=>$this->model_id]);
        if ($model && $this->id != $model->id && $this->model_id != $this->oldAttributes['model_id']) {
            $this->addError('model_id', 'Такой товар уже существует');
            return FALSE;
        }
        return TRUE;
    }
    
     public static function itemAlias($type, $code = NULL) {
        $_items = array(
            'Y/N' => array(
                0=>'Нет',
                1=>'Да',
            ),
        );
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
    
    /**
     * проверяет не пустой ли масив для фильтров
     * @return boolean
     */
    public function isArrayFilter(){
        //очищаем пустые значения масива
        $this->_filter = array_filter($this->_filter);
        if(empty($this->_filter)){
            $this->addError('_filter', 'Значение фильра не может быть пустым');
            return FALSE;
        }else
            return TRUE;
    }
    
    /**
     * проверяем на уникальность ссылки
     */
    public function isUnique(){
        if(isset($this->oldAttributes['name_lat']) && $this->name_lat != $this->oldAttributes['name_lat']){
            $model = self::findOne(['name_lat'=>$this->name_lat, 'article'=>$this->article]);
            
            if($model && $model->id != $this->id){
                echo $model->id;
                vd($model->name_lat, false);
                echo $this->id;
                vd($this->name_lat, false);
                $this->addError('name_lat', 'Такая ссылка уже существует');
                return FALSE;
            }
        }
    }

    public function scenarios() {
        return parent::scenarios();
    }

    public function getImages() {
//        return $this->hasMany(Image::className(), ['product_id' => 'id']);
        return $this->hasMany(Image::className(), ['product_id' => 'id'])->where("url !=''");
//        return $this->hasMany(Image::className(), ['product_id' => 'id'])->where(["is_delete"=>1]);
    }

    public function getOrderProducts() {
        return $this->hasMany(OrderProduct::className(), ['product_id' => 'id']);
    }

    public function getOrders() {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])->viaTable('order_product', ['product_id' => 'id']);
    }

    public function getCategory() {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    
    public function getCategoryName() {
        return $this->hasOne(Category::className(), ['id' => 'category_id'])->select('id, name, name_lat')->asArray();
    }
    
    public function getProducerName() {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id'])->select('id, name, name_lat')->asArray();
    }
    
    public function getSerieName() {
        return $this->hasOne(Serie::className(), ['id' => 'serie_id'])->select('id, name, name_lat')->asArray();
    }
    
    public function getModel_name() {
        return $this->hasOne(Model::className(), ['id' => 'model_id'])->select('id, name')->asArray();
    }
    
    public function getFilterValue() {
        return $this->hasOne(FilterValue::className(), ['filter_value_id' => 'filter_value_id']);
    }

    public function getProductFilterValues() {
        return $this->hasMany(ProductFilterValue::className(), ['product_id' => 'id']);
    }

    public function getFilter() {
        return $this->hasMany(ProductFilterValue::className(), ['article' => 'article']);
    }
    public function getSearchForFilter() {
        $filterValueID = FilterValue::getNormFilter();
        return $this->hasMany(ProductFilterValue::className(), [
                                'article' => 'article',
                                'producer_id' => 'producer_id',
                                'serie_id' => 'serie_id',
                            ])->where([
                                'filter_value_id'=> $filterValueID['filterValueID'] ,
                            ]);
        
    }
    
    public function getProduct_category_filter_value() {
        return $this->hasMany(FilterValue::className(), ['filter_id' => 'id'])->viaTable('filter', ['category_id' => 'category_id']);
    }
    
    public function getProduct_category_filter() {
        return $this->hasMany(Filter::className(), ['category_id' => 'category_id']);
    }
    
    public function getFilters() {
        return $this->hasMany(Filter::className(), ['id' => 'filter_id'])->viaTable('product_filter_value', ['product_id' => 'id']);
    }
    
    public function getFo_search() {
        return $this->hasMany(SerchModel::className(), ['article' => 'article']);
    }

    public function getImage() {
        return $this->hasMany(Image::className(), ['product_id' => 'id'])
                ->where(['image.is_delete'=>0]);
//                ->orderBy('image.url DESC');
    }

    public function getImageUrl() {
        return $this->hasMany(Image::className(), ['product_id' => 'id'])->select('product_id, url')->asArray();
    }
    
    public function getParam() {
        return $this->hasMany(FilterValue::className(), ['filter_value_id' => 'filter_value_id'])
                ->viaTable('product_filter_value', ['article' => 'article'])
                ->joinWith('filter')
                ->asArray();
    }

    public function getProductProperties() {
        return $this->hasMany(ProductProperties::className(), ['article' => 'article'])->where(['properties_id'=>[1,2,3,6,7,8,9,10,11,12,17,20,22,23], 'ordering'=>[0,1,2,3,4]]);
    }
    
    public function getProductPropertiesView() {
        return $this->hasMany(Properties::className(), ['id' => 'properties_id'])->viaTable('product_properties', ['product_id' => 'id']);
    }
    
    public function getProducer() {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }
    
    public function getModel() {
        return $this->hasOne(Model::className(), ['id' => 'model_id']);
    }
    
    public function getSerie(){
        return $this->hasOne(Serie::className(), ['id' => 'serie_id']);
    }
    
    public function getModelChildren() {
        return $this->hasMany(ModelChild::className(), ['model_id' => 'model_id'])->where(['model_child.is_dalete'=>0]);
//                ->where(['m.producer_id'=>[1,10,14,7]])->innerJoin('model m', 'm.id=model_child.model_id');
    }
    
//    public function getHeadProduct(){
//        return $this->hasOne(self::className(), ['article' => 'article'])->where(['not', ['description'=>null]]);
//    }
    
    
    public static function getProblem(){
        return [
            1 => "без категории",
            2 => "без производителя",
            3 => "без модели",
            4 => "без фильтров",
            5 => "Новые товары",
            6 => "без парт. номеров",
        ];
    }

    public static function getAvailableRules() {
        return [0 => '', 1 => 'можем привезти завтра после 10:00', 2 => 'можем привезти послезавтра', 3 => 'можем привезти через 1-2 дня'];
    }
    
    /**
     * Формирование SQL запроса для поиска
     * @param type $is_problem
     * @return string
     */
    public function getPropertiName($is_problem){
        if(empty($this->is_problem)){
            return "ok";
        }else{
            switch ($this->is_problem){
                case 0 : return "Отсутствует категория"; break;
                case 1 : return "Отсутствует производитель"; break;
                case 2 : $query = self::find()->andWhere(['model_id'=>null]); break;
                case 3 : $query = self::find()->andWhere(['serie_id'=>null]); break;
                case 4 : $query = self::find()->andWhere(['serie_id'=>null]); break;
                case 5 : return "Новые товары";
            }
        }
    }
    
    
    public function getAvailable(){
        return $this->in_stock1 + $this->in_stock2 + $this->in_stock3 - $this->in_res1 - $this->in_res2 - $this->in_res3;
    }

        /**
     * Обрезает длинные названия товаров
     */
    public function getSerchName(){
        if(strlen($this->name) > 80)
            return mb_substr($this->name, 0, 80, "utf-8")."...";
        else
            return $this->name;
    }
    

    

    public static function getSqlForSearch($request){
        $result = [];
//            $flag = TRUE;
            //если введены русские символы находим категорию
            $restrictions = "product.is_published = 1 AND product._is_problem = :problem AND product.status = :st AND product.is_delete = :del AND product.category_id IS NOT NULL AND product.producer_id IS NOT NULL AND product.model_id IS NOT NULL AND product.serie_id IS NOT NULL";

            $category = Category::seartchCategory($request);
            
            if($category){
                $restrictions .= $category ? " AND product.category_id IN (".implode(',', $category['key']).")" : '';
                
                //выдаляе остатки категоріїї с пошукового слова
                $request = trim(preg_replace("/{$category['name']}/iu", '', $request));
                
                $request = preg_replace("/c=\d+/iu", '', $request);
                
                //экранирую для реулярного виразу
//                $temp_request = $request;
//                $request = preg_quote($request, '/');
                
                $request = trim(preg_replace("/{$category['name']}/u", '', $request));
                if(count($category['key']) == 1){
                    $category['key'] = $category['key'][0];
                };
                $result['category'] = $category;
            }
            
            //если в введенном тексте был введен производитель включаем его в поиск
            //удаляем с запроса производителя
            $producerData = Producer::seartchProducer($request);
            $result['producer']['name'] = '';
            $result['producer']['id'] = '';
            if($producerData){
                $restrictions .= " AND product.producer_id IN ({$producerData['key']})";
                $request = $producerData['request'];
                $result['producer']['id'] =  $producerData['key'];
                $result['producer']['name'] =  $producerData['name'];
            }
            
            $sqlArticle = $sqlPn = $sqlModel = $sqlName = $full = '';
            if(preg_match("/^\d{6}$/u", $request, $matches)){
//                $request = trim($request); 
//                $article = self::find()->where(['article'=>$request])->exists();
//                if($article){
                    $restrictions  .= " AND product.article = '$request'";
//                }
                 
            }else{
//                $sqlName = "product.name LIKE '%$request%'";
                $sqlMchName = '';
                if($request && preg_match("/[\-]$/i", $request, $matches)){
                    // change RLIKE to LIKE
                    //$sqlName = "m.name RLIKE '^([a-zA-Z]* )*{$request}.*$'";
                    $sqlName = "m.name LIKE '%{$request}%'";
//                    $sqlMchName = "OR product.model_id IN (SELECT model_id FROM model_child WHERE  name RLIKE '^([a-zA-Z]* )*$request' AND is_dalete = 0)";
                }elseif($request){
                    // change RLIKE to LIKE
                    //$sqlName = "m.name RLIKE '^([a-zA-Z]* )*{$request}$' OR product.name LIKE '%$request%'";
                    $sqlName = "m.name LIKE '%{$request}' OR product.name LIKE '%$request%'";
//                    $sqlMchName = "OR product.model_id IN (SELECT model_id FROM model_child WHERE  name RLIKE '^([a-zA-Z]* )*$request$' AND is_dalete = 0)";
                    
                    if(preg_match("/[а-яё ]+/iu", $request)){
                        $sqlMchName .= " OR product.name LIKE '%{$request}%'";
                    }
                }
//                
//                    $sqlMchName = "";
                
//                $restrictions .= " $sqlName";
//                $searchInName = self::find()->where(['like', 'name', "%".$request."%", false])->exists();
//                if($searchInName){
//                    $ifPn = "OR";
//                }else
//                    $ifPn = "AND";
                
//                //поиск в совместимых моделях
//                $modelSearch = SearchModel::seartchModel($request);
//                if($modelSearch){
//                    $sqlModel = "OR article IN (".implode(",", $modelSearch['article']).")";
//                }else
                    $sqlModel = '';
                
                
                //поиск в совместимых партномерах
//                $part = ProductPnSearch::seartchPn($request)
//                if($part = Product::getPNarticle($request)){
//                    $sqlPn = "OR article IN (".implode(",", $part['article']).")";
//                }else
                    $sqlPn = '';
                    
                    
                    //поиск в таблице model_children
//                    $sqlName = "m.name RLIKE '^([a-zA-Z]* )*$request$'";


                
                if($sqlName || $sqlModel || $sqlPn){
                    $restrictions .= " AND ($sqlName $sqlModel $sqlPn $sqlMchName)";
                }
            }
            $product = Product::find()
                ->where($restrictions)
//                ->select("*, `product`.`description_short` =`$request` AS `_request` ")
                ->addParams(
                    [
                        ':problem' => 0, 
                        ':st' => 1, 
                        ':del' => 0,
                    ]
                )
                ->innerJoin('model m', 'm.id = product.model_id')
//                ->leftJoin('model_child mch', 'mch.model_id = product.model_id')
                ->orderBy('product.category_id ASC');
                
                //если ниодного ограничения не попало вывести вывести на экран пусто 
//                if($flag){
//                    $product->limit(0);
//                }
                
                
//            $model = \app\models\Model::seartchModel($request, $producer);
//            
//            if($producer){
//                $product->andWhere(['producer_id'=>$producer['key']]);
//                $model = \app\models\Model::seartchModel($request, $producer);
//                $result['producer'] = $producer;
//                if($model){
//                    $result['model'] = $model;
//                    $product->andWhere(['model_id'=>$model['key']]);
//                }
//            }else{
//                if($model){
//                    $result['model'] = $model;
//                    $product->andWhere(['model_id'=>$model['key']]);
//                };
//            }
//            //проверяем если введен артикль
//            if(preg_match("/^\d{6}$/u", $request, $matches)){
//                $product->andWhere(['article'=>$request]);
//            //если введены русские символы
//            }elseif(preg_match("/^([а-яА-Я]+).*/u", $request, $matches)){
//               
//                if($category){
//                    $result['category'] = $category;
//                    $product->andWhere(['category_id'=>$category['key']]);
//                }else
//                    $product->andWhere(['between', 'product.id', 0, 0]);
//            //проверяем если был введен парт номер
//            }else{
//                if(!$producer && !$model){
//                    $product->andWhere(['between', 'product.id', 0, 0]);
//                }
//            }
            
//            vd($product->all(), false);
//            die("<br/>end");
            $result[0]['sql'] = $product;
            $result['request'] = "{$result['producer']['name']} {$request}";
            return $result;
    }

    /**
     * возвращает название модели 
     * после распарсивания названия продукта
     */
    public function getModelName(){
        if($this->producer){
            preg_match("/".$this->producer->name."\s+(.*)/i", $this->name, $productModel);
            return $productModel[1];
        }else{
            return false;
        }
    }

    /**
     * вернет весь список свойст данного товара
     */
    public function getPropertiesList() {
        $properties = Yii::$app->db->createCommand("
                SELECT f.name AS filter, fv.name AS value 
                FROM product_filter_value pfv
                INNER JOIN filter f ON filter_id = f.id 
                INNER JOIN filter_value fv ON pfv.filter_value_id = fv.filter_value_id
                WHERE article ='{$this->article}' AND serie_id='{$this->serie_id}' AND producer_id = '{$this->producer_id}'
                ORDER BY f.sort
            ")->queryAll();
        $result = [];
        foreach($properties as $value){
            $result[$value['filter']] = $value['value'];
        }
        return $result;
    }
    
    public static function getPNarticle($request){
        $model = self::find()
                ->where(['LIKE', 'name', "$request"])
                ->all();
        $result = [];
        foreach ($model as $value) {
            $result['article'][]= $value->article;
        }
        return $result;
    }

    public function defaultScope() {
        return array('condition' => 'product.is_delete = 0');
    }

    public static function find() {
        return parent::find()->andWhere(['product.is_delete' => 0]);
    }

    public static function draft() {
        return self::find()
                        ->where(['status' => 0])
                        ->all();
    }

    public static function random() {
        return self::find()
                        ->select([
                            'product.*',
                            "CASE 
                                WHEN LOCATE('для ноутбука', product.name)  
                                THEN product.name
                                ELSE  INSERT (product.name, LOCATE(LCASE(pr.name), LCASE(product.name)), 0, ' для ноутбука ')
                            END AS name",
                            'page.text AS discount'
                        ])->where([
                            'status' => 1, 
                            'product.is_delete' => 0, 
                            '_is_problem' => 0, 
                            'is_published'=>1,
                        ])
                        ->leftJoin('producer pr', 'product.producer_id= pr.id')
                        ->leftJoin('page', 'page.id = 16')
                        ->limit(16)
                        ->orderBy('RAND()')
                        ->all();
    }

    public static function popular() {
        return self::find()
                        ->where(['status' => 1, 'is_published'=>1, 'is_delete' => 0])
                        ->limit(4)
                        ->orderBy('RAND()')
                        ->all();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'category_id' => "Категория товара",
            'producer_id' => "Производитель товара",
            'price' => "Цена",
            'name' => "Наименование",
            '_nameOne' => "Коректировочное название товаров",
            'logo' => "Логотип товара",
            'logo_temp' => "Логотип товара",
            'description' => "Описание",
            'description_short' => "Краткое описание",
            'images' => "Изображения",
            'name_lat' => "Имя для ссылки",
            'available' => "Есть в наличии",
            'model_id' => "Модель",
            'serie_id' => "Серия",
            'article' => "Артикль",
            '_isDeskription' => "Описание",
            '_count' => "Количество",
            '_forSearchModel' => "Список совместимых моделей для поиска",
            '_forSearchPN' => "Список совместимых парт-номеров для поиска",
            'in_stock1' => "Количество товаров на складе 1",
            'in_stock2' => "Количество товаров на складе 2",
            'in_stock3' => "Количество товаров на складе 3",
            'is_problem' => "Проблема",
            'is_published' => "Опубликован",
            'ya_marcet' => "yandex market",
            'warranty' => "Гарантия",
            'ya_market' => "yandex маркет",
            'googl_market' => "googl маркет",
            'is_published' => "Публикация",
            'is_delete' => "Удален",
            'createtime' => "Создан",
        );
    }

    function getId() {
        return $this->id;
    }
    
    /**
     * Список совместимых моделей для поиска в виде админки
     */
    public static function getFoSearchModel($article){
        return implode(", ", SearchModel::getModelArray($article));
    }
    
    /**
     * Список совместимых моделей для поиска в виде алминке 
     */
    public static function getFoSearchPN($article){
        return implode(", ", ProductPnSearch::getPNArray($article));
    }

     /**
     * переименовую url если найдет дубликат
     */
    public static function createAbsoluteUrl($model, $url, $i=1){
        $serch = self::find();
        
        if($model->isNewRecord)
            $serch->where("`name_lat` = '$url'");
        else
            $serch->where("id <> $model->id AND `name_lat` = '$url'");
        
        $serch->select('name_lat')->asArray();
        if($serch->one()){
            $url = preg_replace('/_\d*$/u', '', $url);
            return self::createAbsoluteUrl($model, $url."_".$i, ++$i);
        }else{
            return $url;
        }
    }
    
    /**
     * Перед тек как сохранить в БД
     */
    public function beforeSave($insert) {
        $this->updatetime = time();
        $this->available = ($this->in_stock1 + $this->in_stock2 + $this->in_stock3)-($this->in_res1 + $this->in_res2 + $this->in_res3);
        if (parent::beforeSave($insert)) {
            if (!$this->isNewRecord) {
//                if (!$this->name_lat) {
//                    $this->name_lat = TranslitFilter::translitUrl($this->name);
//                } else {
//                    $model = self::findOne($this->id);
//                    if ($model && $model->name != $this->name && $model->name_lat == $this->name_lat)
//                        $this->name_lat = TranslitFilter::translitUrl($this->name);
//                }
                return TRUE;
            } else{
                $this->updatetime = time();
                //проверяем есть ли такой товар с ссылкой
//                $this->name_lat = ($this->status == 1) ? self::createAbsoluteUrl($this, $this->name_lat) : null;
                return TRUE;
            }
        } else
            return false;
    }
    
    public function afterSave($insert, $changedAttributes) {
        if ($this->is_delete == 1) {
            ProductFilterValue::deleteProduct($this->article);
        }
        if (!$this->isNewRecord && $this->status == 1) {
            //перещитваем товары в фильтрах
//            FilterWorkingCount::setCountProductFilter();
//            
////            перещитаем количество товаров в категориях
////            ProductWorkingCount::setCountProductCategory();
        }
        return parent::afterSave($insert, $changedAttributes);
    }
    
    /**
     * перед тем как удалить товар удаляем те значения в таблицах где они были использованы
     */
    public function beforeDelete(){
        //удаляем изображения для товаров
        Image::deleteAll(['product_id'=>$this->id]);
        //удаляем связи
        ModelCompatible::deleteAll(['model_id'=>$this->model_id, 'article'=>  $this->article]);
        OrderProduct::deleteAll(['product_id'=>$this->id]);
        ProductProperties::deleteAll(['article'=>$this->article]);
//      FilterWorkingCount::setCountProductFilter();
        return parent::beforeDelete();
    }
    
    /**
     * Возвращает разную цено в зависимости от price_type пользователя
     * @return type
     */
    public function getTradePrice(){
        if(Yii::$app->user->isGuest){
            return $this->price;
        }else{
            $priceType = Yii::$app->user->identity->price_type;
            switch ($priceType){
                case 0  : return $this->price; break;
                case 1  : return $this->trade_price1; break;
                case 2  : return $this->trade_price2; break;
                case 3  : return $this->trade_price3; break;
                default : return $this->price;
            }
        }
    }

    
    /**
     * Список товаров для siteMap
     */
    public static function getProduktListUrl($category, $producer) {
        $model = self::find()
                ->select('name_lat')
                ->where(['not', ['name_lat' => '']])
                ->andWhere([
                    'category_id' => $category, 
                    'producer_id' => $producer, 
                    '_is_problem' => 0, 
                    'is_delete' => 0,
                    'is_published'=>1,
                    'status'=>1,
                ])
                ->where(['not', ['model_id' => null]])
                ->where(['not', ['serie_id' => null]])
                ->asArray()
//                ->limit(50)
                ->all();
        $result = [];
        if(!empty($model)){
            foreach ($model as $value) {
                $result[] = $value['name_lat'];
            }
        }
        return $result;
    }
    /**
     * Список товаров для siteMap 
     */
    public static function getProduktListUrlForCategory($category) {
        return self::find()
                ->select('name_lat, update, createtime, category_id, model_id, serie_id, producer_id,  id')
                ->where(['not', ['name_lat' => '']])
                ->where(['not', ['category_id' => null]])
                ->where(['not', ['producer_id' => null]])
                ->where(['not', ['model_id' => null]])
                ->where(['not', ['serie_id' => null]])
                ->andWhere([
                    'category_id' => $category, 
                    'is_delete' => 0,
                    'is_published'=>1,
                    'status'=>1,
                    '_is_problem' => 0, 
                ])
//                ->limit(25)
                ->all();
    }
    
    

    public function getUpdateProduct() {
        if($this->update != 0)
            return date('Y-m-d', $this->update)."T".date('H:i:s', $this->update)."Z";
        if($this->createtime != 0)
            return date('Y-m-d', $this->createtime)."T".date('H:i:s', $this->createtime)."Z";
        else
            return date('Y-m-d')."T".date('H:i:s')."Z";
    }
    
    public static function getUpdateDate($date) {
        return date('Y-m-d', $date)."T".date('H:i:s', $date)."Z";
    }
    
    
    public function getUrl() {
        $url = $this->category ? '/' . $this->category->getUrl() : '/null';
        $url .= $this->producer ? "/".$this->producer->name_lat : '/null';
        $url .= $this->serie ? "/".$this->serie->name_lat : '/null';
        $url .= $this->name_lat ? '/'.$this->name_lat : '/null';
        return $url;
    }

    /**
     * для дополнительной проверки 
     * на отображение фильтров
     */
    public static function getSkipFilter() {
        $url = Yii::$app->request->url;
        if (substr_count($url, '/') == 3)
            return TRUE;
        elseif (strripos($url, '?'))
            return TRUE;
        else
            return FALSE;
    }

    public static function getActiveFilter() {
        $filter = self::getFilterArray();
        vd($filter, false);

        
        
        $result = array();
        foreach ($filter as $key => $value) {
            foreach ($value as $item) {
//                 WHERE (f.name_lat = 'cvet' AND  fv.name_lat = 'chernyy') OR (f.name_lat = 'cvet' AND  fv.name_lat = 'seryy') OR (f.name_lat = 'podsvetka_klavish' AND fv.name_lat = 'est')
                $model = FilterValue::find()
                        ->where(['f.name_lat' => $key, 'filter_value.name_lat' => $item])
                        ->innerJoin('filter f', 'filter_id=f.id')
                        ->one();
                if ($model)
                    $result[$key][$item] = $model->name;
            }
        }
//        vd($result, false);
//        die("<br/>end");
        return $filter;
    }

    public static function getActivFilterOneDimensional() {
        $filter = self::getFilterArray();
        $filter = $filter ? $filter : [];

        $result = [];
        foreach ($filter as $key => $value) {
            $result = array_merge($result, $value);
        }
        return $result;
    }

    public static function getFilterArray() {
        $result = [];
        if (isset($_GET['filter'])) {
            $request = Yii::$app->request->get('filter');
            $category = Yii::$app->request->get('category', 1);
            
            $result = [];
            $filter = explode('/', $request);
            foreach ($filter as $key => $value) {
                if($key%2 == 0 && isset($filter[$key+1])){
                    $result[$filter[$key]] = explode('-', $filter[$key+1]);
                }
            }
            
            $sql = $limit = '';
            $i = 0;
            foreach ($result as $key => $value) {
                foreach ($value as $item) {
                    if($i++ == 0)
                        $sql .="(f.name_lat = '{$key}' AND  fv.name_lat = '{$item}')";
                    else
                        $sql .=" OR  (f.name_la"
                            . "t = '{$key}' AND  fv.name_lat = '{$item}')";
                }
            }
            if(!empty($sql)){
                $sql = "AND ({$sql})";
            }else
                $limit = "LIMIT 0";
            
            $list = Yii::$app->db->createCommand("
                    SELECT fv.filter_id,
                        GROUP_CONCAT(CONCAT(fv.filter_value_id) SEPARATOR ',') AS filter,
                        f.name,
                        CONCAT(GROUP_CONCAT(CONCAT(fv.name) SEPARATOR ', '), ';') AS filter_name
                    FROM `filter_value` fv
                    INNER JOIN filter f ON f.id = fv.filter_id
                    INNER JOIN category c ON c.id = f.category_id
                    WHERE c.name_lat = '{$category}' 
                    {$sql}
                    GROUP BY fv.filter_id
                    {$limit}
            ")->queryAll();
            $result = [];     
            foreach ($list as $value){
                $result[$value['filter_id']]['id'] = $value['filter'];
                $result[$value['filter_id']][$value['name']] = $value['filter_name'];
            }
        }
        return $result;
    }

    public static function getFilterView() {
        //если мы находимся внутри товара то вернуть FALSE
        //иначе проверяем есть ли дети в данной категории
//        $category = Category::isChildren(Category::getLastCat());
        if (self::getSkipFilter() && isset($_GET['serie'])) {
            $result = Filter::getFilter($_GET['category']);
            $filter = self::getFilterArray();
          
            //устанавливает checked при выборе в фильтрах
            if (!empty($filter)) {
                foreach ($result as $key => $value) {
                    foreach ($filter as $key => $item) {
                        if ($value->id == $key) {
                            foreach ($value->filterValue as $val) {
                                foreach ($item as $tem) {
                                    if ($val->filter_value_id == $tem) {
                                        $val->checked = 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $result;
        } else
            return FALSE;
    }

    /**
     * Проверяет существует ли в
     */
    public function isActiveFilterProduct($active_f) {
        $result = [];
        foreach ($this->filter as $value) {
            if (isset($active_f[$value->filter_id])) {
                $result[] = in_array($value->filter_value_id, array_keys($active_f[$value->filter_id]));
            }
        }
        return in_array(false, $result) ? false : $this;
    }

    public function getPrice($quantity = null) {
        $result = $quantity ? ($this->tradePrice * $quantity) : ($this->tradePrice * $this->quantity);
        return $result;
    }

    public function getCost($withDiscount = true) {
        return TRUE;
    }

    public function setQuantity($withDiscount = true) {
        return $this->_quantity = $withDiscount;
    }

    public function getQuantity($withDiscount = true) {
        return $this->_quantity;
    }

    public function getStock($number) {
        $in_stock = 'in_stock'.$number;
        $in_res = 'in_res'.$number;
        return ($this->$in_stock - $this->$in_res);
    }

    public static function getAvailableStock($products) {
        $stock1 = $stock3 = 1;
        $stock2 = 0;
        $only_second = false;
        foreach ($products as $value) {
            $st1 = ($value->getStock(1));
            $st2 = ($value->getStock(2));
            $st3 = ($value->getStock(3));
            if(!$only_second && $st2 > 0 && $st1 < 1 && $st3 < 1) {
                $only_second = true;
            }
            $stock1 *= $st1;
            $stock2 += $st2;
            $stock3 *= $st3;
        }
        $message = 0;
        $stock = ['stock1' => $message, 'stock3' => $message];
        $date = date('H', time());
        if($only_second) {
            $message = 3;
            $stock['stock1'] = $message;
            $stock['stock3'] = $message;
        }
        else {
            if($date > 17) {
                $message = 2;
            }
            else {
                $message = 1;
            }
            if($stock1 == 0){
                $stock['stock1'] = $message;
            }
            if($stock3 == 0){
                $stock['stock3'] = $message;
            }
        }
        return $stock;
    }

    /**
     * сохраняет новые или обновляет старые 
     * значения фильтров
     * @param type $filters
     */
    public function setProductFilterValue($filters) {
        if(isset($filters) && $this->serie_id){
            foreach ($filters as $key => $item) {
                $p_f_value = ProductFilterValue::createProductFilterValue(
                    $this->article, $this->category_id, $this->producer_id, $this->serie_id, $key, $item
                );
            }
        }
    }

    /**
     * проверяет есть ли товар в корзине
     */
    public function getBasket() {
        $basket = new ShoppingCart();
        return isset($basket->positions[$this->id]) ? $basket->positions[$this->id] : FALSE;
    }

    /*
     * Получаем общую сумму покупок
     */

    public static function getSummPrise() {
        $cart = new \yz\shoppingcart\ShoppingCart();
        $result['summ'] = $cart->getSummPrise()['summ'];
        $result['count'] = $cart->count;
        $result['count_one'] = count($cart->positions);
        return $result;
    }
    
    /**
     * Создает подкатегории по производителям
     */
    public static function parsName() {
        $product = self::find()->andWhere(['between','id',35000, 40000])->all();
        foreach ($product as $value) {
            
            $array = explode(" ", $value->name);
            if(isset($array[1])){
                $producer = $array[1];
                if(strripos($value->name, 'питания')){
                    $producer = $array[2];
                };
                
                if($array[0] == 'Петли'){
                    $producer = $array[3];
                }; 
                
                if(strripos($array[0], 'лейф')){
                    $producer = $array[3]; 
                };
                if($array[0] == 'Разъем'){
                    $producer = $array[2]; 
                };
                
                $parrent = substr_replace($array[0], '', -6);
                self::setProducer($parrent, $producer);
//            (count($array) > 3) ? self::setCategory($array[1], $array[2]) : null;
            }
        }
    }

    public static function setProducer($parrent, $name) {
//            $category = Category::find()
//                            ->andWhere(['like', 'name', $parrent.'%', false])
//                            ->one();
//            if($category){
                $nameSerch = Producer::find()
                            ->andWhere(['like', 'name', '%'.$name.'%', false])
                            ->one();
                if(!$nameSerch){
                    $model = new Producer();
                    $model->name =  $name;
                    if(!$model->save()){
                        return false;
                    };
                }
//            }
        return TRUE;
    }
    
    
    
    
    
     /**
     * Обновляет товара соответсвенно категориям и производителям прописаным в названии
     */
    public static function setProductCategory(){
        $product = self::find()
//                ->where(['producer_id'=>0])
                ->andWhere(['between', 'id', 35000, 40000])
                ->all();
        $array_producer = ArrayHelper::map(Producer::find()->all(),'id', 'name');

        foreach ($product as $value) {
            $array = explode(" ", $value->name);
            $category = Category::find()
                        ->andWhere(['like', 'name', substr_replace($array[0], '', -6).'%', false])
                        ->one();
            $category ? $value->category_id = $category->id : null;

            foreach ($array_producer as $key=>$item) {
                if(strripos($value->name, $item)){
                    $value->producer_id = $key; 
                };
            }
            if(!$value->save()){
                return false;
            }
        }
            
            
        
        return TRUE;
    } 
    
    
//    public static function setProductCategory(){
//        $product = self::find()->andWhere(['between','id',32000, 34000])->all();
//        foreach ($product as $value) {
////            $array = explode(" ", $value->name);
////            $categoryName  = (count($array) > 3) ? $array[2] : $array[1] ;
//            
//            $array = explode(" ", $value->name);
//            if(isset($array[1])){
//                $producerName  = $array[1] ;
//                if(strripos($value->name, 'питания')){
//                    $producerName = $array[2];
//                };
//                
//                if($array[0] == 'Петли'){
//                    $producerName = $array[3];
//                }; 
//                
//                if(strripos($array[0], 'лейф')){
//                    $producerName = $array[3]; 
//                };
//                if($array[0] == 'Разъем'){
//                    $producerName = $array[2]; 
//                };
//                
//                
//                $category = Category::find()
//                            ->andWhere(['like', 'name', substr_replace($array[0], '', -6).'%', false])
//                            ->one();
//                $category ? $value->category_id = $category->id : null;
//                
//                $producer = \app\models\Producer::find()->where(['name'=>$producerName])->one();
//                $producer ? $value->producer_id = $producer->id : null; 
//                
//                if(!$value->save()){
//                    return false;
//                }
//            }
//            
//            
//        }
//        return TRUE;
//    } 
    public function getH2forModelSearch(){
        $h2 = Category::getNameCategory($this->category_id);
        $h2 .= ' подходит к моделям ноутбуков';
        $h2 .= $this->producer ? " ".$this->producer->name : null;
        return $h2;
    }
    

    /*
     * сохраняем логотип
     * возвращаем вид сохранненых изображений
     */
    public function saveLogo($logo, $nameModel) {
        if (empty($logo))
            return '{}';
        $this->logo_temp = \yii\web\UploadedFile::getInstanceByName("{$nameModel}[logo_temp]");

        if ($this->logo_temp) {
            $serch = preg_match("/\.\w{3}$/u", $this->logo_temp->name, $extension);
            $name = time() . mb_strtolower($extension[0]);
            $public_path = Yii::$app->params['public_path'];
            
            $path = Yii::$app->basePath . "/$public_path/uploadfile/images/product/resized/" . $name;
            $this->logo_temp->saveAs($path);
            $this->logo = $name;
 
            $this->save();
            return json_encode(array(
                'initialPreview' => "<img src='/uploadfile/images/product/resized/$name' class='file-preview-image'>",
                'initialPreviewConfig' => array('caption' => $name, 'width' => '120px'),
                'append' => false
            ));
        } else {
            return "{}";
        }
    }
    
    public static function setWaterMark($path, $name){
        $public_path = Yii::$app->params['public_path'];
        $waterMark = new WaterMark(Yii::$app->basePath . "/$public_path/images/view/watermark.png");
        $waterMark->setPosition(WaterMark::POS_LEFT_UP);
        $filename = $waterMark->setStamp($path, false, $name);
        return $filename;
    }

    /*
     * сохраняем выбранные изображения 
     * возвращаем вид сохранненых изображений
     */
    public function saveImages($images, $article=null) {
        $p1 = $p2 = array();
        if (empty($images))
            return '{}';

        for ($i = 0; $i < count($images['name']); $i++) {
            $j = $i + 1;

            $image = new \app\models\Image();
            $name = $image->saveImage($this, "images", $i, $article);
            $p1[$i] = "<img src='/uploadfile/images/product/$name' class='file-preview-image'>";
            $p2[$i] = array('caption' => $name, 'width' => '120px');
        }
        return json_encode(array(
            'initialPreview' => $p1,
            'initialPreviewConfig' => $p2,
            'append' => false
        ));
    }

    public function getBreadCrumbs() {
//        $url = Yii::$app->request->pathInfo;
//
//        $cat = explode("/", $url);
//        $category_bc = Category::getBreadCrumbs($this->category, 'product');
//        $category_bc[] = $this->name;
        return [
            ['label' => $this->category->name, 'url' => "/".$this->category->name_lat],
            $this->producer ? ['label' => $this->producer->name, 'url' => "/".$this->category->name_lat."/".$this->producer->name_lat] : null,
            $this->serie ? ['label' => $this->serie->name, 'url' => "/".$this->category->name_lat."/".$this->producer->name_lat."/".$this->serie->name_lat] : null,
            $this->name
        ];
    }
    
//    public function getBreadcrumbs(){
//        return;
//    }
    
    /**
     * создает новое название товара
     */
    public static function createProductName($category_id, $produser_id, $model_id){
        $produser   = Producer::find()->where(['id'=>$produser_id])->one();
        $model      = \app\models\Model::find()->where(['id'=>$model_id])->one();
            switch ($category_id){
                case 1: $categoryName = 'Клавиатура'; break;
                case 2: $categoryName = 'Вентилятор'; break;
                case 3: $categoryName = 'Аккумулятор'; break;
                case 4: $categoryName = 'Блок питания'; break;
                case 5: $categoryName = 'Петля для матрицы'; break;
                case 6: $categoryName = 'Шлейф для матрицы'; break;
                case 7: $categoryName = 'Товар со скидкой'; break;
                case 8: $categoryName = 'Сетевой шнур'; break;
                case 9: $categoryName = 'Разъём питания'; break;
                case 10: $categoryName = 'Матрица'; break;
                case 11: $categoryName = 'Уценённый товар'; break;
                case 12: $categoryName = 'Расходный материал'; break;
                default : $categoryName = null;  
            }
        
        $productName = $categoryName;
        $productName .= $produser ? " ".$produser->name : null;
        $productName .= $model ? " ".$model->name : null;
        return $productName;
    }
    
    /**
     * вернет массив уникальных товаров 
     */
    public static function getArticleCategory($category_id){
        $articles = Product::find()
                ->where(['not', ['article' => 0]])
                ->andWhere(['category_id'=>$category_id])
                ->select('article')
                ->asArray()
                ->distinct()
                ->all();
        $result = [];
        foreach ($articles as $value) {
            $result[]=$value['article'];
        }
        return $result;
    }

    /**
     * Поиск товаров по модели, бренду, серии и категории
     */
    public function searchProductsByParams($category, $brand, $series, $model) {
        $query = self::find()
            ->leftJoin(Model::tableName().' m','m.id = product.model_id')
            ->leftJoin(Category::tableName().' c','c.id = product.category_id')
            ->leftJoin(Serie::tableName().' s','s.id = m.serie_id')
            ->leftJoin(Producer::tableName().' p','p.id = m.producer_id')
            ->where([
                'product.is_delete'=>0,
                'product.status'=>1,
                'product.is_published'=>1,
                'product._is_problem'=>0,
            ])
            ->andWhere(['like', 'm.name', $model, false])
            ->andWhere('p.name_lat=:pname_lat', [':pname_lat'=>$brand])
            ->andWhere('c.name_lat=:cname_lat', [':cname_lat'=>$category])
            ->andWhere('s.name_lat=:sname_lat', [':sname_lat'=>$series]);
    }
    
    /**
     * Вернет товары по уникальному артикулу
     */
    public function search($category) {
        $query = self::find()
                ->select(['product.*', 'page.text AS discount', '(`in_stock1` + `in_stock2` + `in_stock3`) > 0 AS in_stock'])
                ->leftJoin('page', 'page.id = 16')
                ->where([
                        'category_id' =>$category->id,
                        '_is_problem' =>0,
                        'status' =>1,
                        'product.is_delete' =>0,
                        'is_published'=>1,
                    ])
                ->andWhere(['not', ['producer_id'=>NULL]])
                ->andWhere(['not', ['category_id'=>NULL]]) 
                ->andWhere(['not', ['model_id'=>NULL]]) 
                ->andWhere(['not', ['serie_id'=>NULL]])
                ->orderBy('in_stock DESC, sort, article ASC');
        if(isset($_GET['filter'])){
            $productIdArray = ProductFilterValue::getProductId($_GET['filter'], $category->id);
            $query->andWhere(['or',['id'=>$productIdArray]]);
        }
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 8,
            ],
        ]);
        return $dataProvider;
    }
    
    public static function dellProduct($d){
        $product = self::find()
                ->where(['_is_new'=>$d])
                ->select('id, name, _is_new')
                ->all();
        foreach ($product as $value) {
            vd($value->id, false);
            vd($value->delete(), false);
        }
        return TRUE;
    }
    
    
       
    public function getOtherProducer(){
        return Yii::$app->db->createCommand("
                    SELECT mch.name
                    FROM  `model_compatible` mc
                    INNER JOIN model m ON m.id = mc.model_id
                    INNER JOIN model_child mch ON mc.model_id = mch.model_id
                    WHERE mc.article = '{$this->article}'
                    AND m.producer_id != '{$this->producer_id}'
                    ")->queryAll();
    }
    
    
    public static function CUDmodelProduct($POST, $model){
        $postModel = isset($POST['Compatible']) ? $POST['Compatible'] : [];
        $modelArray = GenereteProduct::getPNnameArray($postModel, 0);
        
//        vd($postModel, false);
//        vd($modelArray, false);
        //створюємо нові товари з парт номеру
        foreach ($postModel as $key => $mod) {
        
            $modelSET = isset($model->children[$key]['model']['id']) ? array_keys($model->children[$key]['model']['id']) : [];
//            vd($modelSET, false);
//            vd($postModel, false);
//            vd(array_diff($mod, $modelSET), false);
//            die_my();
            foreach (array_diff($mod, $modelSET) as $id) {
                $result = GenereteProduct::createProduct($model, $modelArray[$id], $id);
//                if(!$result)
//                    return FALSE;
            }
        }
        
        
        
        $postModel = GenereteProduct::getArrayOne($postModel);
        $delete = GenereteProduct::getArrayOneInModel($model->children, 'model');
        //Видалення товарів парт номеру
        $product = Product::find()->where(['article'=>$model->article,'model_id'=>array_diff($delete, $postModel)])->all();
        foreach ($product as $value) {
            $value->delete();
        }
        //обновляємо товари
        GenereteProduct::updateProduct($model);
        return TRUE;
    }
    
    /**
     * Создание товаров из моделей
     */
    public static function createForProductModels($POST, $product){
//        $modelList = \app\models\Model::getModelArray($product->producer_id);
        //получаем список моделей в таблице связей
        $modelCheket =  \app\models\Model::getModelCheked($product->article);
        $imageList = \app\models\Image::findAll(['product_id'=>$product->id]);
//        vd($imageList, false);
        
        //главнному товару прикрепляем фильтры
        //удаляем связи предыдущих фильтров с товарами
        //создаем новые связи с фильтрами
//        ProductFilterValue::deleteAll(['article' => $product->article]);
        
        foreach ($product->_compatible as $key => $item) {
            //если связи модели с товаром нет то создаем связь и новый товар
            
            if(!in_array($key, $modelCheket)){
                $modelCompatible = ModelCompatible::find()->where(['model_id'=>$key , 'article'=>$product->article])->one();
                if(!$modelCompatible){
                    $modelCompatible = new ModelCompatible();
                    $modelCompatible->model_id = $key;
                    $modelCompatible->article = $product->article;
                    $modelCompatible->save();
                }
            }  
                //обновляем значение товаров соответственно изменениям ключевого товара
                $productNew = self::find()
                        ->where([
                            'model_id'=>$key, 
                            'article'=>$product->article,
                            'is_delete'=>0,
                        ])
                        ->one();
                if(!$productNew)
                    $productNew = new self;
                
                $model = Model::findOne([$key]);
               
                $serch = preg_match('/^(.*) '.$product->producer->name.'/i', $product->_nameOne, $sought);
                $categoryName = $serch ? $sought[1] : $product->_nameOne;
                $productName = $categoryName." ".$model->producer->name." ".$model->name;
                
                $name_lat = $productNew->name_lat;
                $productNew->attributes = $product->attributes;
                $productNew->name_lat = $name_lat;
                
                $productNew->model_id = $key;
                $productNew->producer_id = $model->producer->id;
                $productNew->serie_id = $item;
                $productNew->name = $productName;
                $productNew->description_short = '';
                $productNew->description = '';
                $productNew->status = 1;
                $productNew->is_published = 1;
                $productNew->ya_market = 1;
                if($productNew->isNewRecord){
                    $productNew->createtime = time();
                    $productNew->name_lat = TranslitFilter::translitUrl($productNew->name);
                }else
                    $productNew->update = time();
               
                if($productNew->validate()){
                    if($productNew->save() && $imageList){
                        foreach ($imageList as $value) {
                            $image = \app\models\Image::findOne(['product_id'=>$productNew->id, 'url'=>$value->url]);
//                            vd($image, false);
                            if(!$image){
                                $image = new \app\models\Image();
                                $image->attributes = $value->attributes;
                                $image->product_id = $productNew->id;
                                $image->save();
                            }
                        }
                    }
                    if (!empty($product->_filter)) {
                        $productNew->setProductFilterValue($product->_filter);
                    }
                }else{
                    foreach ($productNew->getErrors() as $error) {
                        $product->addError ('_compatible', "В моделях, ".$error[0]); 
                    }
                    return FALSE;
                }
        }
        foreach (array_diff_key(array_flip($modelCheket), $product->_compatible) as $key => $value) {
            $delProduct = self::findOne(['article'=>$product->article, 'model_id'=>$key, 'is_delete'=>0]);
            if($delProduct){
                if($delProduct->delete()){
                    $modelCompatible = ModelCompatible::deleteAll(['model_id'=>$key, 'article'=>$product->article]);
                }
                $delProduct->update();
            } 
        }
        return true;
    }
    
    
    /**
     * Создаем товар по связям
     */
    public static function createProductCompatible(){
//        SELECT model_id FROM model_compatible WHERE model_id NOT IN(( SELECT model_id FROM product))
        $model = \app\models\Model::find()
                ->select('id, producer_id, name')
                ->where(['between', 'id', 8513, 10000])
                ->asArray()
                ->all();
        foreach ($model as $item) {
            vd($item, false);
            echo "<br/>";
            $modelCompatible = ModelCompatible::find()
                    ->select('model_id, article, _id')
                    ->where(['model_id'=>$item['id']])
                    ->asArray()
                    ->all();
            foreach ($modelCompatible as $value) {
                $product = Product::find()
                        ->where([
                            'model_id'=>$item['id'], 
                            'article'=>$value['article'], 
                            'producer_id'=>$item['producer_id']
                        ])
                        ->andWhere(['_is_problem'=>0, 'is_delete'=>0, 'status'=>1])
                        ->select('model_id, article, name, producer_id')
                        ->asArray()
                        ->one();
                if(!$product){
                    // находим товар для получения с него недостающей информации
                    $product = self::find()
                            ->where(['id'=>$value['_id']])
//                            ->andWhere(['article'=>$value['article'], '_is_problem'=>0, 'is_delete'=>0, 'status'=>1])
                            ->andWhere(['not',['producer_id'=>0, 'model_id'=>0, 'category_id'=>0]])
                            ->one(); 
//                    vd($product, false);
//                    echo "<br/>";
//                    echo "<br/>";
//                    echo "<br/>";
                    $serchProduct = new self;
                    $serchProduct->in_stock1 = $product->in_stock1;
                    $serchProduct->in_stock2 = $product->in_stock2;
                    $serchProduct->in_res1 = $product->in_res1;
                    $serchProduct->in_res2 = $product->in_res2;
                    $serchProduct->trade_price = $product->trade_price;
                    $serchProduct->is_timed = $product->is_timed;
                    $serchProduct->category_id = $product->category_id;
                    $serchProduct->price = $product->price;
                    $serchProduct->producer_id = $item['producer_id'];
                    $serchProduct->model_id = $item['id'];
                    $serchProduct->name = self::createProductName($product->category_id, $item['producer_id'], $item['id']);
                    $serchProduct->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($serchProduct->name);
                    $serchProduct->logo = $product->logo;
                    $serchProduct->available = $product->available;
                    $serchProduct->status = $product->status;
                    $serchProduct->_original_id = $product->id;
                    $serchProduct->createtime = time();
                    $serchProduct->_is_new = 2;//создан с модели
                    $serchProduct->article = $value['article'];
                    
                    $imageList = \app\models\Image::findAll(['product_id'=>$product->id]);
                    if($serchProduct->save() && $imageList){
                        foreach ($imageList as $img) {
                            $image = new \app\models\Image();
                            $image->attributes = $img->attributes;
                            $image->product_id = $serchProduct->id;
                            $image->save();
                        }
                    }
                    vd($serchProduct, false);
                    echo "<br/>";
                }
            }
            
        }
//        die("<br/>end");
    }

    /**
     * обновляет товары вставляя в него id модели
     */
    public static function updateProductModel(){
        ini_set("memory_limit","250M");
        $produser = \app\models\Producer::getProducerArray();
        $product = self::find()
                ->select('producer_id, id, name, category_id')
                ->where(['not', ['producer_id'=>0]])
                ->andWhere(['not', ['category_id' =>[4,12]]])
                ->andWhere(['_is_problem' =>0])
                ->andWhere(['between', 'id', 30000, 40000])
                ->all();
        
        foreach ($product as $value) {
            vd($value->name, false);
            vd($value->producer_id, false);
            echo "<br/>";
            
            $modelName = settingValues::getNameModel($produser[$value->producer_id], $value->name);
            $modelName = preg_replace('/ *?[а-я]+.*/i', '', $modelName);
            if($modelName){
                vd($value->id, false);
                vd($produser[$value->producer_id], false);
                echo "<br/>";
                $modelName = preg_replace('/\-(\w+|\d+).*/i', '',$modelName);
                $modelName = preg_replace('/\s*$/i', '',$modelName);
                vd($modelName, false);
                $model = \app\models\Model::findOne(['producer_id'=>$value->producer_id, 'name'=>$modelName]);
                if($model){
                    $value->model_id = $model->id;
                    vd($value->update(), false);;
                }
                echo "<br/>";
                echo "<br/>";
            }else{
                echo $value->name;
                if($serchPartNsme){
                    $value->_is_problem = 2; //парт номер в имени
                    $value->update();
                    vd($serchPartNsme, false);
                }
                echo "<br/>";
                echo "<br/>";
            }
        }
    }
    
    
    /**
     * Одиночный парсинг
     */
    public static function getOneParsModel($product_id) {
        $deskription =  '';
        $product = self::find()
                ->where(['id' => $product_id])
                ->select('id, producer_id, model_id, name, description')
//            ->where(['between', 'id', 1163, 1183])
                ->all();
        
        foreach ($product as $value) {
            $deskription = $value->description;
            $producer = $value->producer->name;
            $value->_list_model = self::pars($deskription, $producer);
//        vd(json_decode($product->_list_model), true);
            $value->save();
        }
    }
    
    public static function pars($str, $producer){
        vd($str, false);
        $str = preg_replace('/(<div(.*)>|<\/div>)/u', '', $str);
        $str = preg_replace('/\n/', '', $str);
        $str = preg_replace('/^\s*/u', '', $str);
        $str = preg_replace('/<h\d(.*?)>/u', '<h2>', $str);
        
        if(preg_match("/^<p(.*?)>/u", $str)){
            
//            if(preg_match("/^<p(.*?)>.*<br \/>/u", $str)){
//                $str = preg_replace('/<p(.*?)>/u', '<h2>', $str);
//                $str = preg_replace('/:<br \/>/u', '</h2><ul><li>', $str);
//                $str = preg_replace('/<br \/>/u', '</li><li>', $str);
//                echo "<br/>";
//                echo "<br/>";
//                vd($str, false);
//                echo "<br/>";
//                $str = preg_replace("/<li>.*[^\/]*.*".$producer.".*p\/n.*<\/h2>/u", '<h2>'.$producer.' p/n</h2>', $str);
////                $str = preg_replace('/<\/p>/u', '</li><ul>', $str);
//                
//                echo "<br/>";
//                vd($str, false);
//                die("<br/>end");
//            }
            
//            $str = preg_replace('/<\/p>/u', '</h2>', $str);
        }
        
        $str = preg_replace('/<ul(.*?)>/u', '<ul>', $str);
        $str = preg_replace('/<li(.*?)>/u', '<li>', $str);
        $str = preg_replace('/(<span(.*?)>|<\/span>)/u', '', $str);
        $str = preg_replace('/(\s*)?<ul>(\s*)?/u', '<ul>', $str);
        $str = preg_replace('/(\s*)?<\/ul>(\s*)?/u', '</ul>', $str);
        $str = preg_replace('/(\s*)?<li>(\s*)?/u', '<li>', $str);
        echo "<br/>";
        echo "<br/>";
        vd($str, false);
        
        if(preg_match("/p\/n/u", $str)){
            
            $serch = preg_match("/^<h\d>.*<\/h\d>(.*)<h\d.*p\/n/i", $str, $matches);// делим на две части описание
//            vd($matches, false);
//            die("<br/>end");
            $str = $matches[1];
        };
        $producer = substr_replace($producer, '', 0, 1);

        $str = preg_replace("/(\s*)?(\w\-)?(\w)?$producer/i", '', $str); // удаляем производителя с моделей
        $str = preg_replace('/&nbsp;/', '', $str);
        $str = preg_replace('/([а-яА-Я])?/u', '', $str);
        $str = preg_replace('/<h\d>.*?<\/h\d>/u', '', $str);
        $str = preg_replace('/<p(.*)?>.*?<\/p>/u', '', $str);
        $str = preg_replace('/(<ul>|<\/ul>)/u', '', $str);
        $str = preg_replace('/\s*?<li>\s*?/u', '', $str);
        $str = preg_replace('/\s*<\/li>\s*/u', '</li>', $str);
        $str = preg_replace('/^\s*/u', '', $str);
        
//        vd($str, false);
//        echo "<br/>";
//        $str = preg_replace('/\n/', '', $str);
//        $str = preg_replace('/(\s*)?<ul>.*?<\/ul>/u', '', $str);
//        $str = preg_replace('/(\s*)?<ul>.*/u', '', $str);
//        $str = preg_replace('/(\s*)?<h\d>(\s*)?/u', '', $str);
//        $str = preg_replace('/ Series/u', '', $str);
//        $str = preg_replace('/:?/u', '', $str);
//        $str = preg_replace('/\-\w+:?<\/h2>/u', '</h2>', $str);
        $serch = preg_match("/([а-яА-Я]*)/u", $str, $matches);// делим на две части описание
        if($serch){
            $str = preg_replace("/$matches[1].*?<\/h2>/u", '', $str);
        }
        $str = preg_replace('/Sony /u', '', $str);
        $array = explode("</li>", $str);
//        $array = explode("</h2>", $str);
        
        $array = array_filter($array);
        
        vd($array, false);
        return  json_encode($array);;
    }

    public static function setProblemProductt(){
        $product = self::find()
                ->where(['category_id'=>1]) //4
                ->andWhere(['not', ['description'=>""]])
//                ->andWhere(['not', ['producer_id'=>14]])
                ->select('id, _is_problem, is_delete, status, category_id, name, description, _list_model, _list_pn')
                ->asArray()
                ->all();
        foreach ($product as $value) {
            echo $value['id'];
            vd($value['name'], false);
            echo "<br/>";
            vd(json_decode($value['_list_model'], TRUE), false);
            echo "<br/>";
            echo "<br/>";
//            vd(json_decode($value['_list_pn'], TRUE), false);
//            echo "<br/>";
        }
    }
    
    
    public static function deleteProducer(){
        $product = self::find()
//                ->where(['category_id'=>1]) //4
                ->where(['not', ['description'=>""]])
//                ->andWhere(['not', ['producer_id'=>14]])
                ->andWhere(['not', ['producer_id'=>0]])
                ->andWhere(['between', 'id', 0, 40000])
                ->select('id, producer_id, name, description, _list_model, _list_pn')
                ->all();
        
        foreach ($product as $value) {
            $model = json_decode($value->_list_model, TRUE);
            if(is_array($model)){
//                $result = [];
//                foreach ($model as $item) {
//                    $result[] = preg_replace("/^".$value->producer->name." /u", '', $item);
//                }
//                $value->_list_model = json_encode($result);
//                $value->update();
//                
//                vd($result, false);
            }else{
                echo $value->id;
                vd($value->_list_model, false);
                echo "<br/>";
                echo "<br/>";
            }
        }
    }
    
    /**
     * Определяет производителя с названия товара
     */
    public static function updateProblemProductModel(){
        $producer = Producer::find()->asArray()->all();
        $result = [];
        foreach ($producer as $value) {
            $result[$value['id']]= $value['name'];
        }
        $product = self::find()
                ->where(['not', ['_is_problem'=>0]])
                ->select('id, producer_id, model_id, _is_problem, name')
                ->all();
        foreach ($product as $value) {
            foreach ($result as $key=>$item) {
                if(preg_match("/($item)/u", $value->name, $matches)){
                    $value->producer_id = $key;
                    vd($value->update(), false);
                    break;
                }
            }
        }
    }
    
    /**
     * Удаляем с названия лишние символы
     */
    public static function cutNameProduct(){
        $producer = Producer::find()->asArray()->all();
        $result = [];
        foreach ($producer as $value) {
            $result[$value['id']]= $value['name'];
        }
        $product = self::find()
//                ->where(['not', ['_is_problem'=>0]])
                ->where(['_is_problem'=>0])
                ->select('id, producer_id, model_id, _is_problem, name')
                ->all();
        
        foreach ($product as $value) {
            $mame = preg_replace('/( [а-яА-Я]* )?[а-яА-Я]*$/u', '', $value->name);
//            $mame = preg_replace('/ \d.{11}$/u', '', $mame);
//            $mame = preg_replace('/ \d.{9}$/u', '', $mame);
            $mame = preg_replace('/ .{2}\..*$/u', '', $mame);
            $mame = preg_replace('/ .{1}\..*$/u', '', $mame);
            $mame = preg_replace('/\-.*$/u', '', $mame);
            $mame = preg_replace('/\s*$/u', '', $mame);
            $mame = preg_replace('/ [а-яА-Я]*$/u', '', $mame);
            
//            vd($value->name, false);
//            echo "<br/>";
            
            if($value->name != $mame){
                $value->name =  $mame;
//                $value->_is_problem = 0; //очистил имя товара от парт номеров
                vd($value->save(), false);
                
                vd($value->name, false);
                echo "<br/>";
                vd($mame, false);
                echo "<br/>";
                echo "<br/>";
            }
        }
    }
    
    /**
     * позначаєм продукти що дублюються
     */
    public static function setDableProduct(){
        $produkt = self::find()
                ->select('`id`,`name`,article, Count(name) as Count')
                ->groupBy(['name'])
                ->having('count(`name`)>1')
                ->all();
        vd($produkt, false);
        foreach ($produkt as $value) {
            $model = self::find()
                ->where(['article'=>$value->article, 'name'=>$value->name])
                ->select('id, name, article')
                ->all();
            
            if(count($model)>1){
                foreach ($model as $key=>$item) {
                    if($key>0){
                        $item->_is_problem = 5; //Дублируется товар
                        vd($item->save(), false);;   
                    }
                }
            }
        }
//        vd($produkt, false);
    }
    
    /**
     * Обновляем названия товаров определяем модель их названия
     */
    public static function updateProduct_Name(){
        $product = self::find()
                ->select('id, article, category_id, producer_id, model_id, name')
                ->where(['_is_problem'=>0])
                ->andWhere(['not', ['producer_id'=>0]])
                ->andWhere(['between', 'id', 30000, 40000])
                ->all();
        foreach ($product as $value) {
            if($value->producer){
               preg_match("/ ".$value->producer->name." (.*)$/i", $value->name, $serch);
                if(isset($serch[1])){
                    $nameModel = preg_replace('/ё*/u', '', $serch[1]);
                    $nameModel = preg_replace('/\s[а-яА-Я]*\s(\d{1})?/u', '',$nameModel);
                    $nameModel = preg_replace('/[а-яА-Я]*/u', '', $nameModel);

                    $model = \app\models\Model::find()
                            ->where(['name'=>$nameModel, 'producer_id'=>$value['producer_id']])
                            ->select('id, name, producer_id')
                            ->asArray()
                            ->one();
                    $value->model_id = $model['id'];
                    vd($value->update(), false);
                }else{
                    vd($value->name, false);
                    echo "<br/>";
                    vd($serch, false);
                    echo "<br/>";
                    $value->_is_problem = 1;
                    $value->update();
                }
            }
            
//            vd($value->name, false);
//            echo "<br/>";
//            vd($nameModel, false);
//            echo "<br/>";
//            vd($model, false);
//            echo "<br/>";
//            echo "<br/>";
        }
//        vd($product, false);
    }
    
    /**
     * Убирает с названия "/"
     */
    public static function overwriteNameLat(){
        $model = self::find()
                ->where("name_lat LIKE '%/%'")
                ->all();
        foreach ($model as $value) {
            $value->name_lat = preg_replace('/[\/\_]/','-', $value->name_lat );
            vd($value->update(), false);
            echo "<br/>";
        }
    }
    
    
    /**
     * Возобновить товары которые случайно были удалены
     */
    public static function updateDeletProduct(){
        $model = self::find()
                ->where('article IN ( 030075, 020340, 060146, 060223, 010006, 060092, 020318, 080004, 080001, 080003, 010492, 090002, 090001)')
                ->andWhere(['is_delete'=>1])
                ->all();
        foreach ($model as $value) {
            $value->is_delete = 0;
//            $value->_is_problem = 7; //возо
            vd($value->update(), false);
        }
//        vd($model, false);
    }
    
    /**
     * Получить все товары у которых нет изображений
     */
    public static function getProductEmptyImage(){
//             "SELECT * FROM j251_virtuemart_product_medias pm "
//            . " JOIN j251_virtuemart_medias m ON m.virtuemart_media_id = pm.virtuemart_media_id WHERE pm.virtuemart_product_id = $relationship"
//        $product = self::find()
//                ->select('product.id, product.`article` , product.name, im.url')
//                ->join('LEFT JOIN', 'image im', 'product.id = im.product_id')
//                ->where('im.product_id IS NULL')
//                ->andWhere('_original_id IS NOT NULL')
//                ->groupBy('article')
//                ->all();
        
        $product = self::find()
                ->select('product.id, product.`article`, im.url')
                ->join('LEFT JOIN', 'image im', 'product.id = im.product_id')
                ->where('im.product_id IS NULL')
                ->andWhere('_original_id IS NOT NULL')
                ->groupBy('article')
                ->asArray()
                ->all();
      
        foreach ($product as $key => $value) {
            $model = self::find()
                    ->select('product.id, product.article')
                    ->andWhere(['article'=>$value['article']])
                    ->joinWith('image')
                    ->one();
            if($model){
                $nonImage = self::find()
                        ->select('product.id, product.article')
                        ->join('LEFT JOIN', 'image im', 'product.id = im.product_id')
                        ->where('im.product_id IS NULL')
                        ->andWhere(['article'=>$value->article])
                        ->all();
                foreach ($nonImage as $item) {
                    foreach ($model->image as $val) {
                        $image = new Image();
                        $image->product_id = $item->id;
                        $image->meta =  $val->meta;
                        $image->url =  $val->url;
                        $image->thumb =  $val->thumb;
                        $image->title =  $val->title;
                        $image->description = $val->description;
                        vd($image->save(), false);  
                    }
                }
            }
//                    ->join('INNER JOIN', 'image im', 'product.id = im.product_id')
//                    ->select('product.id, product.`article` , product.name, im.url')
//                    ->where('im.product_id IS NULL')
//                    ->andWhere('_original_id IS NOT NULL')
//                    ->where()
//                    ->one();
//            vd($value, false);
        }
//        vd($product, false);
    }
    
    public static function Connection() {
        $connection = new \yii\db\Connection([
            'dsn' => 'mysql:host=localhost;dbname=partsnb',
            'username' => 'partsnb',
            'password' => '9noXaeo9nEXoEaeo',
            'charset' => 'utf8',
        ]);
        $connection->open();
        return $connection;
    }
    
    /**
     * Поиск изображений в joomla таблиц
     */
    public static function setImageIsJoomla (){
        $product = self::find()
                ->select('product.id, product.article, product._original_id, im.url AS images')
                ->join('LEFT JOIN', 'image im', 'product.id = im.product_id')
                ->where('im.product_id IS NULL')
                ->andWhere('_original_id IS NOT NULL')
                ->groupBy('article')
                ->asArray()
                ->all();
        $connect = self::Connection();
        foreach ($product as $value) {
            $joomla = $connect->createCommand(
                            "SELECT pm.virtuemart_product_id AS product_id, m.file_meta AS meta, m.file_url AS url, m.file_url_thumb AS thumb, m.file_title AS title, m.file_description AS description
                                FROM j251_virtuemart_product_medias pm
                                JOIN j251_virtuemart_medias m ON m.virtuemart_media_id = pm.virtuemart_media_id
                                WHERE pm.virtuemart_product_id = $value[_original_id]"
            )->queryAll();
            if($joomla){
                $nonImage = self::find()
                        ->select('product.id, product.article')
                        ->join('LEFT JOIN', 'image im', 'product.id = im.product_id')
                        ->where('im.product_id IS NULL')
                        ->andWhere(['article'=>$value['article']])
                        ->all();
                foreach ($nonImage as $item) {
                    foreach ($joomla as $val) {
                        preg_match("/\/([a-z0-9]*\..*)/u", $val['url'],$url);
                        
                        $image = new Image();
                        $image->product_id = $item->id;
                        $image->meta =  $val['meta'];
                        $image->url =  $url[1];
                        $image->thumb =  $val['thumb'];
                        $image->title =  $val['title'];
                        $image->description = $val['description'];
                        vd($image->save(), false);  
                    }
                }
            }
        }
        
    }
    
    public static function getDubleProduct() {
        $list = Yii::$app->db->createCommand("
                SELECT 
                GROUP_CONCAT(p.id  SEPARATOR '# ') AS id,
                GROUP_CONCAT(p.name  SEPARATOR '# ') AS name,
                GROUP_CONCAT(pr.name SEPARATOR '# ') AS producer,
                GROUP_CONCAT(m.name  SEPARATOR '# ') AS model,
                GROUP_CONCAT(s.name  SEPARATOR '# ') AS serie,
                GROUP_CONCAT(p.name_lat   SEPARATOR '# ') AS name_lat,
                GROUP_CONCAT(p.original_name_lat   SEPARATOR '# ') AS original_name_lat,
                GROUP_CONCAT(p._original_id   SEPARATOR '# ') AS original_id,
                COUNT( p.id) AS _count
                FROM `product` p
                LEFT JOIN model m ON m.id = p.model_id
                LEFT JOIN serie s ON s.id = p.serie_id
                LEFT JOIN producer pr ON pr.id = p.producer_id
                WHERE p.is_published = 1
                AND  p.is_delete = 0
                AND  p.status = 1
                AND  p.category_id IS NOT NULL
                AND p.producer_id  IS NOT NULL
                AND p.model_id IS NOT NULL
                AND p.serie_id IS NOT NULL
                GROUP BY  p.`article`, p.`model_id`
                HAVING _count >1
                    ")->queryAll();
        foreach ($list as $key => $value) {
            foreach ($value as $k => $item) {
                $list[$key][$k] = explode('# ', $item);
            }
        }
        vd($list, false);
    }
    
    
    public static function getH1($filter, $category, $producer, $serie = null){
        $result = '';
        $request = Yii::$app->request->get('serie', false);
        if($filter){
            foreach ($filter as $value) {
                foreach ($value as $key => $val) {
                    if($key != 'id'){
                        $result .= " {$key}: {$value[$key]}"; 
                    }
                }
            }
            if($request)
                $result = "{$result} {$category->name} для ноутбуков и нетбуков {$producer->name} {$serie->name} в магазине partsnb" ;
            else
                $result = "{$result} {$category->name} для ноутбуков и нетбуков {$producer->name} {$result} в магазине partsnb" ;
        }else{
            if($request){// на страници производителя
//                        $category->name.' для ноутбуков и нетбуков '. $producer->name;
                $result = "{$category->name} для ноутбуков и нетбуков {$producer->name}";
            }else{
//                        $category->name." для ноутбуков и нетбуков"
                $result = "{$category->name} для ноутбуков и нетбуков ";
            }
        }
        return $result;
    }
    
    public static function getTitle($filter, $category, $producer, $serie = null){
        $result = '';
        $request = Yii::$app->request->get('serie', false);
        if($filter){
            foreach ($filter as $value) {
                foreach ($value as $key => $val) {
                    if($key != 'id'){
                        $result .= " {$key}: {$value[$key]} "; 
                    }
                }
            }
            if($request){
                $result = "{$result} {$category->name} для ноутбуков и нетбуков {$producer->name} {$serie->name} - partsnb.ru" ;
            }else{
                $result = "{$result} {$category->name} для ноутбуков и нетбуков {$producer->name} - partsnb.ru" ;
            }
        }else{
            if($request){ // на страници производителя
//                          $category->name." для ноутбуков и нетбуков в Санкт-Петербурге";
                $result = "{$category->name} для ноутбуков и нетбуков {$producer->name} {$serie->name} - partsnb.ru";
            }else{
//                          $category->name.'для ноутбуков и нетбуков '.$producer->name.' в Санкт-Петербурге ';
                $result = "{$category->name} для ноутбуков и нетбуков {$producer->name} - partsnb.ru";
            }
        }
        return $result;
    }
    
    
    public static function getDescripription($filter, $category, $producer, $serie = null){
        $result = '';
        $request = Yii::$app->request->get('serie', 1);
        if($filter){
            foreach ($filter as $value) {
                foreach ($value as $key => $val) {
                    if($key != 'id'){
                        $result .= "{$key}: {$value[$key]} "; 
                    }
                }
            }
            if($request == 1){
                $result = "Купить новый  {$category->name} для ноутбуков и нетбуков {$producer->name} по самой лучшей цене. (особые условия для мастеров) В наличии на складе, звоните!";
            }else{
                $result = "Купить новый  {$category->name} для ноутбуков и нетбуков {$producer->name} {$serie->name} по самой лучшей цене. (особые условия для мастеров) В наличии на складе, звоните!" ;
            }
        }else{
            if($request == 1){ // на страници производителя
                $result = "У нас в интернет-магазине partsnb.ru вы можете купить любые {$category->name} для ноутбуков {$producer->name} в Санкт-Петербурге. Звоните +7 (812) 920-05-75";
            }else{
                $result = "У нас в интернет-магазине partsnb.ru вы можете купить любые {$category->name} для ноутбуков в Санкт-Петербурге. Звоните +7 (812) 920-05-75";
            }
        }
        return $result;
    }
    
    /**
     * Список всіх сумісних моделей
     * @param type $article
     * @return type
     */
     public static function getModelSortArray($article){
        $list = Yii::$app->db->createCommand("
                    SELECT m.id, m.name
                    FROM `product` p
                    INNER JOIN model m ON m.id = p.model_id
                    WHERE `article` LIKE '{$article}'
                    AND p.`category_id` IS NOT NULL
                    AND p.`producer_id`  IS NOT NULL
                    AND p.`model_id` IS NOT NULL
                    AND p.`serie_id` IS NOT NULL
                    AND p.`is_delete` = 0
                    AND `status` = 1
                    AND `is_published` = 1
                    AND is_pn = 0
            ")->queryAll();
        $result = [];
        foreach ($list as $value) {
            $result[] = $value['name'];
        }
        return $result;
    }    

    // brand path
    public function getSelectParams($return = ['producer'], $options=[]) {
        $select = $return[0];
        $fields = [];
        $query = self::find();
        $params = [];
        if(count($return)) {
            foreach($return AS $field) {
                $fields[] = 'product.'.$field.'_id';
                $query->leftJoin($field, $field.'.id = product.'.$field.'_id');
                $params[$field] = [];
            }
        }

        if($select != 'category') {
            $query->leftJoin('category', 'category.id = product.category_id');
        }

        $query->select(implode(',', $fields))->with($return)->distinct()
            ->where([
                'product.is_delete'=>0,
                'product.is_published'=>1,
                'product._is_problem'=>0,
                'product.status' => 1,
                'category.is_published' => 1
            ]);

        foreach ($options AS $table => $value) {
            $query->andWhere("{$table}.{$value['key']}=:{$table}{$value['key']}", [":{$table}{$value['key']}"=>$value['value']]);
        }
        $query->orderBy($select.'.name');

        // Костыль, убираем серии партномеров
        $table = Producer::tableName();
        $value = null;
        if(isset($options[$table])) {
            $value = $options[$table];
        }
        $series = Model::find()->select('model.serie_id')->distinct()
            ->leftJoin($table, $table.'.id = model.producer_id')
            ->where('model.is_pn = 0');
        if($value) {
            $series = $series->andWhere("{$table}.{$value['key']}=:{$table}{$value['key']}", [":{$table}{$value['key']}"=>$value['value']]);
        }
        $series = $series->indexBy('model.serie_id')->column();
        $query->andWhere(['product.serie_id'=>$series]);

        $products = $query->all();

        $params['count'] = count($products);

        if($params['count'] > 0) {
            foreach($products AS $product) {
                foreach($return AS $param) {
                    if($product->$param) {
                        if(!isset($product->$param->name_lat)) {
                            $product->$param->name_lat = $product->$param->name;
                        }
                        $params[$param][] = $product->$param;
                    }
                }
            }
        }

        return $params;
    }
    
    
}
