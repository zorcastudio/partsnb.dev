<?php

namespace app\models;

use Yii;
use app\modules\admin\components\TranslitFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class Page extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name', 'is_menu'], 'required'],
            [['name_lat', 'name'], 'unique'],
            [['parent_id', 'is_menu','position_menu'], 'integer'],
            [['name'], 'match', 'pattern' => '/^[A-Za-zА-Яа-я0-9 \-\"\'\.]+$/u', 'message' => 'В названии категории допускаются только буквы и цифры, знак "-" и пробелы.'],
            [['name_lat'], 'match', 'pattern' => '/^[A-Za-z0-9 \-]+$/u', 'message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-" и пробелы.'],
            [['text', 'description', 'keywords'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'name' => "Наименование страницы",
            'text' => "Текст страницы",
            'name_lat' => "Имя для ссылки",
            'parent_id' => "Родительская категория",
            'is_menu' => "Для меню",
            'description' => "Мета тег Description",
            'keywords' => "Мета тег Keywords",
            'position_menu' => "Позиция меню",
        ];
    }
    
     public static function itemAlias($type, $code = NULL) {
        $_items = [
            'NoYes' => [
                1 => 'Да',
                0 => 'Нет'
            ],
            'POSITION' => [
                0 => 'Во всех',
                1 => 'Верхнее',
                2 => 'Нижнее',
            ]
            ];
        if (isset($code))
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
    
    public function getParentPage() {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    /*
     * Перед тек как сохранить в БД
     */

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if (!$this->name_lat) {
                $this->name_lat = TranslitFilter::translitUrl($this->name);
            } else {
                $model = self::findOne($this->id);
                if ($model && $model->name != $this->name && $model->name_lat == $this->name_lat)
                    $this->name_lat = TranslitFilter::translitUrl($this->name);
            }
            return TRUE;
        } else
            return false;
    }

    public function getUrl() {
        return "/".$this->name_lat;
    }

    public static function getMenuLeft() {
        return self::find()->where(["is_delete" => 0])->all();
    }

    public static function getMenu() {
        return self::find()->where(["is_delete" => 0, 'is_menu'=>1, 'position_menu'=>[2,0]])->all();
    }
    
    public static function getMenuFuter() {
        $result = [];
        foreach (self::getMenuTop([2,0]) as $value) {
            $result[] = [
                'name' => $value['label'],
                'url' => $value['url'],
            ];
            if(isset($value['items'])){
                foreach ($value['items'] as $item) {
                    $result[] = [
                        'name' => $item['label'],
                        'url' => $item['url'],
                    ];
                }
                
            }
        }
        return $result;

//        return self::find()->where(["is_delete" => 0, 'is_menu'=>1, 'position_menu'=>[2,0]])->all();
    }

    public static function getPages() {
        $pages = self::find()->where(['is_delete' => 0])->all();
        return $pages ? ArrayHelper::map($pages, 'id', 'name') : [];
    }

    public static function getPagesObj() {
        return self::find()->where(['is_delete' => 0])->all();
    }

   
    public static function getMenuTop($position, $page = null, $parent = null, $result = []) {
        if (empty($page))
            $page = self::find()
                    ->where(['parent_id' => null, "is_delete" => 0, 'is_menu'=>1 , 'position_menu'=>$position])
                    ->all();

        foreach ($page as $key => $value) {
            $list = self::find()
                    ->where(['is_delete' => 0, 'parent_id' => $value->id, 'is_menu'=>1, 'position_menu'=>$position])
                    ->all();
            $result[$key]['label'] = $value->name;
            
            if($parent)
                $result[$key]['url'] = Url::to(['/'.$parent.'/'.$value->name_lat]);
            else
                $result[$key]['url'] = Url::to(['/'.$value->name_lat]);
//                'active'=>$value->isActive()

           
            if ($list) {
                $result[$key] += ['items' => self::getMenuTop($position, $list, $value['url'])];
            }
        }
        return $result;
    }

    public static function getMenuTopHtml() {
        return self::getUlHtml(self::getMenuTop([0,1]));
    }

    public static function getUlHtml($menu) {
        $result = '<ul>';
        foreach ($menu as $value) {
            if (isset($value['items'])) {
                $result .= self::getLiHtml($value, $value['items']);
            } else {
                $result .= self::getLiHtml($value);
            }
        }
        $result .= '</ul>';
        return $result;
    }

    public static function getLiHtml($value, $items = false) {
        $result = '';
        if ($items) {
            $result .= '<li><a href="' . $value['url'] . '">' . $value['label'] . '</a>';
            $result .= self::getUlHtml($items);
            $result .= '</li>';
        } else {
            $result .= '<li><a href="' . $value['url'] . '">' . $value['label'] . '</a></li>';
            ;
        }
        return $result;
    }
    
    /**
     * Список страниц для siteMap
     */
    public static function getUrlList(){
        $model = self::find()
                ->where(['is_delete'=>0])
                ->select('name_lat')
                ->andWhere(['not', ['name_lat'=>'']])
                ->asArray()
                ->all();
        $result = [];
        foreach ($model as $value) {
            $result[]=$value['name_lat'];
        }
        return $result;
    }

    /**
     * хлбные крошки
     */
    public function getBreadCrumbs() {
        $result[] = $this->name;
        $model = self::findOne($this->parent_id);
        if ($model) {
            $result[] = ['label' => $model->name, 'url' => ["/$model->name_lat"]];
            if ($model->parent_id != null) {
                $result += $model->breadCrumbs;
            } else {
                return array_reverse($result);
            }
        } else {
            return array_reverse($result);
        }
    }
    
    /**
     * возвращает активное количество показов строк для greadView
     */
    public static function getActiveList($list){
        $params = Yii::$app->request->queryParams;
        return (isset($params['list']) && ($params['list']== $list));
    }
    
     /**
     * возвращает активное количество показов строк для greadView
     */
    public static function getUrlListGV($list){
        $url = Yii::$app->request->queryParams;
        $pvs_linck = '';
        if(isset($url['ProductViewSearch'])){
           $url['ProductViewSearch'] = array_filter($url['ProductViewSearch']);
           $pvs = $url['ProductViewSearch'];
           unset($url['ProductViewSearch']);
           
           foreach ($pvs as $key => $value) {
               $pvs_linck .= '&'.$key.'='.$value;
           }
        }
        $result  = '?';
        if(isset($_GET['article'])){
            $result .= "article={$_GET['article']}&list={$list}";
        }

        return '/'.Yii::$app->request->pathInfo."{$result}{$pvs_linck}";
    }
    
     /**
     * возвращает активное количество показов строк для greadView
     */
    public static function getUrlListALL($list){
        $url = Yii::$app->request->queryParams;
        $pvs_linck = '';
        if(isset($url['AllProductSearch'])){
           $url['AllProductSearch'] = array_filter($url['AllProductSearch']);
           $pvs = $url['AllProductSearch'];
           unset($url['AllProductSearch']);
           
           foreach ($pvs as $key => $value) {
               $pvs_linck .= '&'.$key.'='.$value;
           }
        }
        $result  = '?';
//        if(isset($_GET['article'])){
            $result .= "list={$list}";
//        }

        return '/'.Yii::$app->request->pathInfo."{$result}{$pvs_linck}";
    }
    
    
    public static function isLookFilter(){
        $request = Yii::$app->request->get();
        if(isset($request['model']) || isset($_GET['name'])){
            return false;
        }
       
        if(isset($request['producer']) && isset($request['category']) && in_array($request['category'], ['bloki', 'matritsy'])){
            return TRUE;
        }

        if (isset($_GET['serie'])){
            return true;
        }
        return false;
    }

}
