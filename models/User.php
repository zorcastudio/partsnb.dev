<?php
namespace app\models;
use yii\helpers\VarDumper;

use Yii;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {

    const STATUS_BANED    = -1;
    const STATUS_NOACTIVE = 0;
    const STATUS_ACTIVE   = 1;
    const STATUS_MODERATOR= 2;
    const STATUS_ADMIN    = 3;
    private $_user = false;
    
    public $authKey;
    //разрешение на обновление даты синхронизации
    public $permit = true;
    

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "user";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['username', 'password', 'email', 'country_id'], 'required'],
            [['zip_code', 'id', 'phone', 'country_id', 'region_id', 'city_id', 'price_type', 'lastvisit'], 'integer'],
            [['username'], 'unique', 'message' => 'Пользователь с таким логином уже существует.'],
            [['email'], 'unique', 'message' => 'Пользователь с таким электронным адресом уже существует.'],
            [['email'], 'email'],
            [['username'], 'string', 'max' => 40, 'min' => 3, 'message' => 'Длина имени пользователя от 3 до 20 символов.'],
            [['password'], 'string', 'max' => 128, 'min' => 4, 'message' => 'Минимальная длина пароля 4 символа.'],
            [['username'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,\-\.\_ \(\)]+$/u', 'message' => 'В имени пользователя допускаются только латинские буквы и цифры.'],
            

            [['name'], 'match', 'pattern' => '/^[A-Za-z0-9ёЁА-Яа-я\s,_, \-\.\(\)]+$/u', 'message' => 'В имени пользователя допускаются только буквы и цифры.'],
            [['middle_name'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В отчестве пользователя допускаются только буквы и цифры.'],
            [['surname'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В фамилия пользователя допускаются только буквы и цифры.'],
            [['address'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,\.]+$/u', 'message' => 'В адресе допускаются только латинские буквы и цифры.'],
            [['status'], 'in', 'range' => [self::STATUS_NOACTIVE, self::STATUS_ACTIVE, self::STATUS_BANED, self::STATUS_MODERATOR, self::STATUS_ADMIN]],
            [['id', 'name', 'username', 'surname', 'middle_name', 'phone, email', 'country_id', 'region_id', 'city_id', 'address', 'status'], 'safe', 'on' => 'search'],
            [['name', 'surname', 'middle_name'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 12],
            [['address'], 'string', 'max' => 255],
        ];
    }
    
    public function scenarios(){
        $scenarios = parent::scenarios();
//        $scenarios['update'] = [];
        return $scenarios;
    }
        
    public function getOrder(){
        return $this->hasMany(Order::className(), ['user_id' => 'id']);
    }

    public function getCountry(){
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }
    
    public function getCity(){
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }
    
    public function getRegion(){
        return $this->hasOne(Region::className(), ['region_id' => 'region_id']);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'username' => "Логин",
            'password' => "Пароль",
            'verifyPassword' => "Подтвердите пароль",
            'checkbox' => "Запомнить",
            'activkey' => 'Ключ активации',
            'createtime' => 'Дата регистрации',
            'lastvisit' => 'Последний визит',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'middle_name' => 'Отчество',
            'phone' => 'Телефон',
            'email' => 'Электронная почта',
            'country_id' => 'Cтрана',
            'region_id' => 'Область',
            'city_id' => 'Город',
            'address' => 'Адрес',
            'zip_code' => 'Почтовый индекс',
            'status' => 'Статус',
            'verifyCode'=>'Код Подтверждения',
            'price_type'=>'Тип цены для пользователя',
        ];
    }

    public function scopes() {
        return array(
            'active'    =>  ['condition' => 'status >=' . self::STATUS_ACTIVE],
            'notactvie' =>  ['condition' => 'status=' . self::STATUS_NOACTIVE],
            'banned'    =>  ['condition' => 'status=' . self::STATUS_BANED],
            'superuser' =>  ['condition' => 'status=' . self::STATUS_ADMIN],
            'all_but'   =>  ['condition' => 'status >=0'],
            'notsafe'   =>  ['select' => 'id, username, password, status'],
        );
    }
    
    public static function findIdentity($id){
        return self::findOne($id);
    }
    
    public static function findIdentityByAccessToken($token, $type = null){
        return static::findOne(['access_token' => $token]);
    }

    
    public function getAuthKey(){
        return $this->authKey;
    }
    
    
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
    
    public function getId(){
        return $this->id;
     }
     
    public function validatePassword($password)
    {
        return $this->password === md5($password);
    }
    
    
    public static function isAdmin() {
        if (Yii::$app->user->isGuest)
            return false;
        else {
            return Yii::$app->user->identity->status == 3 ? true : false;
        }
    }
    
    public static function setLastVisit($model){
        $user = self::findOne($model->user->id); 
        $user->lastvisit = time();
        $user->permit = false;
        return $user->save();
    }
    
    public function beforeSave($insert) {
        if($this->permit){
            $this->updatetime = time();
        }
        return parent::beforeSave($insert);
    }
   
    /**
     * установка cookie
     */
    public static function setCookes($name, $value) {
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => $name,
            'value' => $value,
            'expire' => time() + (60 * 60 * 24 * 30 * 12), // (1 год)
        ]));
    }

    public static function itemAlias($type, $code = NULL) {
        $_items = [
            'status' => [
                self::STATUS_NOACTIVE => 'Не активирован',
                self::STATUS_ACTIVE => 'Активирован',
                self::STATUS_BANED => 'Заблокирован',
                self::STATUS_MODERATOR => 'Модератор',
                self::STATUS_ADMIN => 'Администратор',
            ],
            'priceType' => [
                0 => 'розничная цена',
                1 => 'оптовая цена первой категории',
                2 => 'оптовая цена второй категории',
                3 => 'оптовая цена третьей категории',
            ],
        ];
        if (isset($code)) {
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        } else
            return isset($_items[$type]) ? $_items[$type] : false;
    }

    public function login() {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->username, $this->password);
            $this->_identity->authenticate();
        }
        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
            Yii::$app->user->login($this->_identity, $duration);
            return true;
        } else
            return false;
    }
    
    public static function findByUsername($username)
    {
        $model = self::find()
                ->where(['username'=>$username])
                ->one();
        
        return $model ? $model : null;
    }
    
    public static function updateUserTheXml($data, $time){
        $model = self::find()
                ->where(['article'=>$data['article']])
                ->andWhere("updatetime > $time")
                ->one();
        if(!$model){
            $model = new self();
        }
        $model->article = $data['article'];
        $model->username = $data['username'];
        $model->password = $data['password'];
        $model->name = $data['name'];
        $model->phone = $data['phone'];
        $model->middle_name = $data['lastname'];
        $model->zip_code = $data['zipcode'];
        $model->email = $data['email'];
        $model->address = $data['adress'];
        $model->price_type = $data['price_type'];
        $model->price_type = $data['price_type'];
        $model->updatetime = time();
        return $model->save() ? TRUE : $model->getErrors();
    }
}
