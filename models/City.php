<?php

namespace app\models;

use Yii;
use app\models\Region;
use yii\helpers\ArrayHelper;


class City extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "city";
    }

    public static function getCitys($region_id){
        $country_id = Region::getCountry($region_id);

        if($region_id && $country_id){    
            $model = self::find()
                ->where(['country_id'=>$country_id, 'region_id' => $region_id])
                ->select("city_id, name")
                ->all();
            
            return ArrayHelper::map($model,'city_id', 'name');
        }else
            return [];
    }
    
    public static function getCitysForOrder($country_id = false){
        if($country_id){    
            $model = self::find()
                ->where(['country_id'=>$country_id])
                ->select("city_id, name")
                ->all();
            
            return ArrayHelper::map($model,'city_id', 'name');
        }else
            return [];
    }
    
    public static function Name($id){
        $city = self::findOne($id);
        return $city ? $city->name : null;
    }
}
