<?php
namespace app\models;

use yii\helpers\VarDumper;

class MyActiveQuery extends \yii\db\ActiveQuery
{
    public function init()
    {
        $modelClass = $this->modelClass;
        $tableName = $modelClass::tableName();
        $this->andWhere([$tableName.'.is_delete' => 0]);
        parent::init();
    }

}