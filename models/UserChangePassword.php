<?php
namespace app\models;

use Yii;

/**
 * UserChangePassword class.
 * UserChangePassword is the data structure for keeping
 * user change password form data. It is used by the 'changepassword' action of 'UserController'.
 */
class UserChangePassword extends \yii\base\Model {
	public $password;
	public $verifyPassword;
	
	public function rules() {
            return [
                    [['password', 'verifyPassword'], 'required'],
                    ['password', 'string', 'length'=>[4, 128], 'message' => 'Минимальная длина пароля 4 символа.'],
                    ['verifyPassword', 'compare', 'compareAttribute'=>'password', 'operator' => '==', 'message' => 'Пароли не совпадают.'],
            ];
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
            return array(
                'password'=>'Пароль',
                'verifyPassword'=>'Повторите пароль',
            );
	}
} 