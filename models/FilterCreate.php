<?php
namespace app\models;
use Yii;
use yii\helpers\ArrayHelper;
use app\modules\admin\components\TranslitFilter;

class FilterCreate extends Filter {

    
  

    public function rules() {
        return [];
    }
    
    
    /**
     * Создает фильтры и подцепляет эти фильтры к товару соответственно свойствам!!!!!
     */
    public static function createFilterIsProperties($s, $f){
        $properties = ArrayHelper::map(Properties::find()->all(),'id', 'name');

        $product = Product::find()
            ->asArray()
            ->select('product.id, product.category_id, product.producer_id, product.serie_id. product.article')
            ->where(['between', 'product.id', $s, $f])
            ->joinWith('productProperties')
            ->all();
        $result = [];
        foreach ($product as $value) {
            foreach ($value['productProperties'] as $propertiesValue) {
                $filter = self::createFilter($value['category_id'], $properties[$propertiesValue['properties_id']]);
                $filterValue = FilterValue::createFilterValue($propertiesValue['value'], $filter);
                $productFilterValue = ProductFilterValue::createProductFilterValue($value['article'], $value['category_id'], $value['producer_id'], $value['serie_id'], $filter->id, $filterValue->filter_value_id);
                $result[] = $productFilterValue->product_id;
            }
        }
        return $result;
    }

    
    
    /**
     * создает связь фильтра с товаром (используя свойство)
     */
    public static function createProductFilterIsProperties($s, $f){
        $filter = FilterValue::find()
                ->asArray()
                ->all();
   
        $product = Product::find()
                ->asArray()
                ->where(['between', 'product.id', $s, $f])
                ->joinWith('productProperties')
                ->all();
        $r = [];
        foreach ($product as $value) {
            foreach ($value['productProperties'] as $item) {
                $v = false;
                foreach ($filter as $v) {
                    if(preg_match("/$v[name]/i", $item['value'])){
                        $result [] = $v;
                    };
                }
                if(!isset($result)){
                    //создает фильтры с свойств
                
                    $properties = Properties::find()->where(['id'=>$item['properties_id']])->asArray()->one();
                    $filter_n = self::createFilter($value['category_id'], $properties['name']);
                    $v = FilterValue::createFilterValue($item['value'], $filter_n);
                }
                
                if(!empty($result)){
                    if(count($result) > 1){
                        foreach ($result as $res) {
                            $filter_n = Filter::find()->where(['id'=>$res['filter_id'], 'category_id'=>$value['category_id']])->one();
                        }
                        
                        //создает фильтры с свойств
                        if(!$filter_n){
                            $properties = Properties::find()->where(['id'=>$item['properties_id']])->asArray()->one();
                            $filter_n = self::createFilter($value['category_id'], $properties['name']);
                        }
                        $v = FilterValue::createFilterValue($item['value'], $filter_n);
                    }
                    
                    $productFilterValue = ProductFilterValue::find()
                            ->where([
                                'product_id'=>$value['id'],
                                'category_id'=>$value['category_id'],
                                'producer_id'=>$value['producer_id'],
                                'filter_id'=>$v['filter_id'],
                                'serie_id'=>$value['serie_id'],
                                'filter_value_id'=>$v['filter_value_id'],
                            ])->one();
                    if(!$productFilterValue){
                        $productFilterValue = new ProductFilterValue();
                    }
                    $productFilterValue->product_id = $value['id'];
                    $productFilterValue->category_id = $value['category_id'];
                    $productFilterValue->producer_id = $value['producer_id'];
                    $productFilterValue->filter_id = $v['filter_id'];
                    $productFilterValue->serie_id = $value['serie_id'];
                    $productFilterValue->filter_value_id = $v['filter_value_id'];
                    $r[] = $productFilterValue->save();
                }
            }
        }
        return $r;
    }
    
    
    
    /**
     * копирует свойства тоавара в фильтры
     */
    public static function createFilterIsProp(){
        $filterValue = \app\models\FilterValue::find()
                ->asArray()
                ->joinWith(['filter'])
                ->all();
        
        $productProperties = \app\models\ProductProperties::find()
                ->asArray()
                ->joinWith('serieInProduct')
                ->all();
        
        $result = [];
        foreach ($productProperties as $value) {
            $v = false;
            foreach ($filterValue as $v) {
                if(preg_match("/$v[name]/i", $value['value'])){
                    $category_id = Category::getIdCstegory($value['article']);
                    if($category_id == $v['filter']['category_id'])
                        foreach ($value['serieInProduct'] as $serie) {
                            ProductFilterValue::createProductFilterValue ($value['article'], $v['filter']['category_id'], $value['properties_id'], $serie['serie_id'], $v['filter_id'], $v['filter_value_id']);
                            $result[$value['article']][] = $v;
                        }
                };
            }
        }
        return $result;
    }
    
    
    
    
}
