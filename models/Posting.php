<?php

namespace app\models;

use Yii;
use yii\base\Model;


class Posting extends Model {
    
    public $sety;
    public $theme;
    public $message;
    public $file;

    public function rules() {
        $rules = [
            [['sety', 'theme','message','file'], 'safe']
        ];

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'verticalForm')
            return $rules;
        return $rules;
    }
    
     public function attributeLabels()
    {
        return [
            'sety' => 'Соцсети',
            'theme' => 'Тема',
            'message' => 'Сообщение',
            'file' => 'Файл',
        ];
    }
}
