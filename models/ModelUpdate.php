<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;


class ModelUpdate extends Model {

     public function rules() {
        return [];
    }
    
    /**
     * ищем все модели которые соответствеют описанию
     */
    public static function updateModelInDeskription($s, $f){
        $producer = ArrayHelper::map(Producer::find()->all(),'id', 'name');
        $product = Product::find()
                ->where(['not', ['_list_model'=>null]])
                ->select('id, article, _list_model, producer_id, name')
                ->asArray()
                ->andWhere(['between', 'id', $s, $f])
                ->all();
        $result = [];
        foreach ($product as $value) {
            $name = '';
            foreach (json_decode($value['_list_model']) as $item) {
                foreach ($producer as $key => $nameProducer) {
                    $producer_id = null;
                    if(preg_match("/".$nameProducer."/i", $item, $matches)){
                        $name = preg_replace("/$nameProducer/i", '', $item);
                        $name = preg_replace("/^\s*/i", '', $name);
                        $producer_id = $key;
                        break;
                    }
                }
                if(!isset($producer_id)){
                    $producer_id = $value['producer_id'] ? $value['producer_id'] : 28;
                }
                
                $name = $name ? $name : $item;
                
                $model = self::find()->where(['name'=>$name, 'producer_id'=>$producer_id])->one();
                if(!$model){
                    $model = new self();
                    $model->name = $name;
                    $model->producer_id = $producer_id;
                    $result['model'][] = $model->save();
                }
                
                $modelCompatible = ModelCompatible::find()->where(['article'=>$value['article'], 'model_id'=>$model->id])->one();
                if(!$modelCompatible){
                    $modelCompatible = new ModelCompatible();
                    $modelCompatible->model_id = $model->id;
                    $modelCompatible->article = $value['article'];
                    $modelCompatible->_id = $value['id'];
                    $result['compotible'][] = $modelCompatible->save();
                }else{
                    $modelCompatible->_id = $value['id'];
                    $result['compotible'][] = $modelCompatible->update();
                }
            $name = '';    
            }
        }
        return $result;
    }
    
    /**
     * Удаляем модели
     */
    public static function deleteModel($s, $f) {
//        $modelCompatible = ModelCompatible::find()
//                ->where(['not', ['model_compatible._id'=>null]])
//                ->limit(1000)
//                ->joinWith('model')
//                ->all();
        $model = Model::find()
                ->where(['not', ['model._id'=>null]])
                ->joinWith('product')
                ->limit(1000)
                ->all();
        foreach ($model as $value) {
            vd($value->delete(), false);
        }
    }
    
    /**
     * eудаляем модели ку которых в названии есть парт номер
     */
    public static function deleteModelIsPN($pn){
        $result = [];
        foreach (json_decode($pn) as $item) {
            $model = Model::find()
                    ->andWhere(['like','name', $item, FALSE])
                    ->one();
            $model ? $result[]=$model->delete() : null;
        }
        return $result;
    }
    
    public static function get_Listmodel(){
        $product = Product::find()
                ->where(['not', ['_list_pn'=>null]])
                ->anDwhere(['not', ['_list_pn'=>'']])
                ->select('_list_pn')
                ->andWhere(['between', 'id', 217, 50000])
                ->asArray()
                ->all();
        $result = [];
        foreach ($product as $value) {
            $result[] = $value['_list_pn'];
        }
        return $result;
    }
    
    /**
     * Удаляю модели которые не существуют в связях
     * и в товарах
     */
    public static function deleteModelIsNotCompatible(){
        
        $model = self::find()
                ->join(	'LEFT JOIN', 
                        'model_compatible',
                        'model.id = model_compatible.model_id'
			) 
                ->join(	'LEFT JOIN', 
                        'product',
                        'product.model_id = model.id'
			) 
                ->where('model_compatible.model_id IS NULL AND product.model_id IS NULL')
                ->limit(1000)
                ->all();
        foreach ($model as $value) {
            vd($value->delete(), false);
        }
    }
    
    /**
     * Гененрирует товари с модели если сужществует связь !!!
     */
    public static function createProductInCompatible(){
        $model = self::find()
                ->limit(500)
                ->joinWith('compotible')
                ->join('LEFT JOIN', 'product', 'product.model_id = model.id')
                ->andWhere('product.model_id IS NULL')
                ->all();
        foreach ($model as $value) {
            foreach ($value['compotible'] as $item) {
                $product = Product::find()
                        ->where([
                            'article'=>$item->article,
                            '_is_problem'=>0,
                            'status'=>1,
                            'product.is_delete'=>0,
                        ])
                        ->joinWith('image')
                        ->one();
                $productNew = new ProductCreate([
                    'article'=>$product['article'],
                    'in_stock1'=>$product['in_stock1'],
                    'in_stock2'=>$product['in_stock2'],
                    'in_stock3'=>$product['in_stock3'],
                    'in_res1'=>$product['in_res1'],
                    'in_res2'=>$product['in_res2'],
                    'in_res3'=>$product['in_res3'],
                    'trade_price1'=>$product['trade_price1'],
                    'trade_price2'=>$product['trade_price2'],
                    'trade_price3'=>$product['trade_price3'],
                    'price'=>$product['price'],
                    'is_timed'=>$product['is_timed'],
                    'delivery_date'=>$product['delivery_date'],
                    'updatetime'=>time(),
                    'createtime'=>time(),
                    '_is_new'=>1,
                    'category_id'=>$product['category_id'],
                    'producer_id'=>$value['producer_id'],
                    'model_id'=>$value['id'],
                    'serie_id'=>$value['serie_id'],
                    'name'=>Product::createProductName($product['category_id'], $value['producer_id'], $value['id']),
                    'logo'=>$product['logo'],
                    'available'=>$product['available'],
                    'status'=>$product['status'],
                ]);
                $productNew->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($productNew['name']);
                
                if($productNew->save()){
                    foreach ($product['image'] as $val) {
                        $image = new Image();
                        $image->product_id = $productNew->id;
                        $image->meta = $val->meta;
                        $image->url = $val->url;
                        $image->thumb = $val->thumb;
                        $image->title = $val->title;
                        $image->description = $val->description;
                        vd($image->save(), false);
                    }
                };
            }
        }
    }
    
     
    /**
     * Видаляем дублюючі model 
     */
    public static function getModelDuble() {
        $model = self::find()
                ->select('model.id, model.producer_id, model.serie_id, model.name')
                ->where("p.model_id IS NULL")
                ->join("LEFT JOIN", 'product p', 'p.model_id  = model.id')
                ->all();
        vd($model, false);
        foreach ($model as $value) {
            vd($value->delete(), false);
        }
        die("<br/>end");
        
        
        $list = Yii::$app->db->createCommand("SELECT   id , producer_id , name, COUNT(m.id) AS _count
                                                FROM  `model` m
                                                GROUP BY producer_id, name
                                                HAVING _count >1"
                                            )->queryAll();
//        vd($list, false);
//        die("<br/>end");
        foreach ($list as $value) {
            $model = Yii::$app->db->createCommand("SELECT m.id, m.producer_id,  m.name
                                                FROM  `model` m
                                                WHERE m.producer_id = $value[producer_id] AND m.name = '$value[name]'"
                                            )->queryAll();
//            vd($model, false);
//                                                HAVING _count > 1 
//                                                GROUP BY m.producer_id, m.name, article
//                                                LEFT JOIN product p ON p.model_id = m.id
            if(!empty($model)){
                foreach ($model as $key => $item) {
                    if($key != 0){
                        $prod = Product::find()
                                    ->where([
                                        'producer_id'=>$item['producer_id'],
                                        'model_id'=>$item['id']
                                    ])->one();
                        if($prod){
                            $prod->model_id = $value['id'];
                            $prod->save();
                            if($prod->getErrors()){
                                $error = $prod->getErrors();
                                if(isset($error['model_id']))
                                    $prod->delete();
                            }
                        }
                    }
                }
                
                
                
//                    $product = Yii::$app->db->createCommand("SELECT m.id , m.producer_id, m.name, article, _original_id, p.id AS product_id
//                                                FROM  `model` m
//                                                LEFT JOIN product p ON p.model_id = m.id
//                                                WHERE m.producer_id = $model[producer_id] AND m.name = '$model[name]' AND p.article = $model[article] AND _original_id IS NULL"
//                                            )->queryOne();
//                                    vd($product, false);
//                    if(!empty($product)){
//                        vd($product, false);
//                            $prod = Product::find()
//                                    ->where(['id'=>$product['product_id']])
//                                    ->one();
////                            vd($prod, false);
//                            if($prod)
//                                vd($prod->delete(), false);
//                        
//                    }
            }
            
        }
    }
    
}
