<?php
namespace app\models;
use yii\helpers\ArrayHelper;
use Yii;


class ProductUpdate extends \app\models\Product {
    public $_name;
    public $children = [];
    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['category_id', 'producer_id', 'article', 'name', 'model_id'], 'safe'],
            [['_filter'], 'isArrayFilter'],
        ];
    }
    
    public function beforeDelete(){
        //удаляем изображения для товаров
        Image::deleteAll(['product_id'=>$this->id]);
      
        //удаляем связь
//        $modelCompatible = ModelCompatible::deleteAll(['model_id'=>$this->model_id, 'article'=>$this->article]);
//        $modelCompatible ? $modelCompatible->delete() : null;
        OrderProduct::deleteAll(['product_id'=>$this->id]);
//        ProductProperties::deleteAll(['article'=>$this->article]);
//      FilterWorkingCount::setCountProductFilter();
        return self::deleteAll(['id'=>$this->id]); 
    }
    
    
    public static function getProductProducerNull(){
        $product = Product::find()
            ->where(['not', ['_list_model'=>null]])
            ->andWhere(['producer_id'=>null])
            ->select('id, article, _list_model, producer_id_, name')
            ->asArray()
//            ->andWhere(['between', 'id',$s, $f])
            ->all();
        vd($product, false);
    }


    
    
    public static function setProductModelNull(){
        $product = self::find()
                ->where(['not', ['model_id'=>null]])
                ->andWhere(['between', 'id', 40000, 50000])
                ->select('id, producer_id, name, _is_problem')
                ->all();
    }

    /**
     * Ищет в товаре производителя
     */
    public static function updateProducer(){
        $producer = Producer::find()
                ->asArray()
                ->select('id, name')
                ->all();
        $result = [];
        foreach ($producer as $value) {
            $result[$value['id']] = $value['name'];
        }
        
        $product = self::find()
                ->where(['producer_id'=>null])
                ->andWhere(['between', 'id', 40000, 50000])
                ->select('id, producer_id, name, _is_problem')
                ->all();
        
        foreach ($product as $value) {
            foreach ($result as $key => $item) {
                $serch = preg_match("/$item/i", $value->name);// делим на две части описание
                if($serch){
                    $value->producer_id = $key;
                    vd($value->update(), false);
                    break;
                }
            }
        }
    }
    
    /**
     * сверяет и обновляет названия товаров с оригиналом
     */
    public static function updateProductName($s, $f){
        $product = self::find()
                ->where(['not',['_original_id'=>null]])
                ->andWhere(['between', 'id', $s, $f])
                ->select('id, name, original_name_lat, name_lat, _original_id')
                ->all();
        $result = [];
        foreach ($product as $value) {
            $connect = settingValues::Connection();
            $joomla = $connect->createCommand("SELECT product_name, slug  FROM  j251_virtuemart_products_ru_ru WHERE virtuemart_product_id = $value[_original_id]")->queryOne();
            if($value->name != $joomla['product_name'] || $value->original_name_lat != $joomla['slug']){
                $value->name = $joomla['product_name'];
                $value->original_name_lat = $joomla['slug'];
                $value->name_lat = $joomla['slug'];
                $result[$value->id] = $value->save();
            }
        }
        return $result;
    }
    
    /**
     * удаляем все товары у которіх нет _original_id
     */
    public static function deleteProductNotOriginalId(){
         $product = self::find()
                ->where(['_is_new'=>2])
                ->select('product.id, product._original_id, product._is_new')
                ->joinWith('image')
                ->joinWith('productFilterValues')
                 ->limit(1000)
                ->all();
        $result = [];
        foreach ($product as $value) {
            foreach ($value['productFilterValues'] as $item) {
                $item->delete();
            }
            foreach ($value['image'] as $item) {
                $item->delete();
            }
            $result[$value->id] = $value->delete();
        }
        return $result;
    }
    
    /**
     * создаем модели с названия товара
     */
    public static function createModelIsNameProduct($s, $f) {
        $producer = ArrayHelper::map(Producer::find()->all(), 'id', 'name');
        $product = self::find()
                ->where(['not',['_is_problem'=>0]])
                ->andWhere(['between', 'id', $s, $f])
                ->select('id, name, model_id, producer_id, article, _is_problem')
                ->all();
       
        $result = [];
        foreach ($product as $value) {
            $name = false;
            $producer_id = false;
//            echo $value->name;
            foreach ($producer as $key => $nameProducer) {
                if (preg_match("/$nameProducer/i", $value->name)) {
                    $name = preg_replace("/$nameProducer/i", '', $value->name);
                    $name = preg_replace("/[а-яА-ЯёЁ]/u", '', $name);
                    $name = preg_replace("/^\s*/i", '', $name);
                    $name = preg_replace("/\s*$/i", '', $name);
                    $producer_id = $key;
//                    vd($producer[$key], false);
//                    echo $name;
                    break;
                }
            }
//            echo "<br/>";
            if(!$name){
                $value->_is_problem = 3;
                $result[$value->id] =[3=>$value->save()];
            }{
                if(!$producer_id){
                    $value->_is_problem = 6;
                    $result[$value->id] =[6=>$value->save()];
                }else{
                    $model = ModelUpdate::find()->where(['name'=>$name, 'producer_id'=>$producer_id])->one();
                    if(!$model){
                        $model = new ModelUpdate();
                        $model->name = $name;
                        $model->producer_id = $producer_id;
                        $result[] = $model->save();
                    }

                    if(!ModelCompatible::find()->where(['article'=>$value->article, 'model_id'=>$model->id])->exists()){
                        $modelCompatible = new ModelCompatible();
                        $modelCompatible->model_id = $model->id;
                        $modelCompatible->article = $value->article;
                        $modelCompatible->save();
                    }
                    
                    $value->model_id = $model->id;
                    $value->producer_id = $producer_id;
                    $result[$value->id] =$value->save();
                }
            }
        }
        return $result;
    }

    /**
     * пересмтривает is_problem если по предудущей функцыей определился производитель и модель
     */
    public static function updateIsProblem(){
        $product = self::find()
                ->where(['not', ['producer_id'=>null]])
                ->andWhere(['not', ['model_id'=>null]])
                ->andWhere(['not', ['_is_problem'=>0]])
                ->andWhere(['between', 'id', 30000, 50000])
                ->select('id, producer_id, model_id, _is_problem, name')
                ->all();
        foreach ($product as $value) {
            $value->_is_problem = 0;
            vd($value->update(), false);
        }
    }
    
    /**
     * находит в названии обновляет model_id!!! товара 
     */
    public static function updateModelProduct(){
        $producer = ArrayHelper::map(Producer::find()->all(),'id', 'name');
        $serie = ArrayHelper::map(Serie::find()->all(),'id', 'name');
        
        $product = self::find()
                ->where(['not', ['producer_id'=>null]])
                ->andWhere(['model_id'=>null])
//                ->andWhere(['not', ['_is_problem'=>0]])
                ->andWhere(['between', 'id', 40000, 50000])
                ->select('id, producer_id, category_id,  model_id, article, name, _is_problem')
                ->all();
        foreach ($product as $value) {
            $modelName = preg_replace('/[а-яА-ЯёЁ]/u', '', $value->name);
            $modelName = preg_replace('/'.$producer[$value->producer_id].'(\-[a-zA-Z]*)? ?/i', '', $modelName);
            
            //обрезаем значение после дефиса
            //удаляем модель если это п/н
            $modelName = preg_replace('/\-.*$/i', '', $modelName);
            $modelName = preg_replace('/ .*\..*$/i', '', $modelName);
            $modelName = preg_replace('/^\s*/i', '', $modelName);
            $modelName = preg_replace('/\s*$/i', '', $modelName);
            if($modelName){
                $model = Model::find()
                      ->where(['name'=>$modelName])
                      ->andWhere(['producer_id'=>$value->producer_id])
                      ->select('id')
                      ->asArray()
                      ->one();
                if($model){
                    $value->model_id = $model['id'];
                }else{
                    //создаем новую модель
//                    $model = new Model();
//                    $model->name = $modelName;
//                    $model->producer_id = $value->producer_id;
//                    $model->_id = $value->id;
//                    if($model->save()){
//                        //создаем связи
//                        \app\models\ModelCompatible::setModelCompatible($model->id, $value->article, $value->id);
//                        $value->model_id = $model->id;
//                    }
                }
                vd($value->update(), false);
            }
        }
    }
    
    /**
     * генерируем товар из связей
     */
    public static function cresteProductCompotible($article){
        $compotible  = ModelCompatible::find()
                ->where(['like','model_compatible.article',$article, FALSE])
                ->andWhere(['not', ['model_compatible.article'=>null]])
                ->andWhere(['not', ['model_compatible._id'=>null]])
                ->joinWith('model')
                ->joinWith('productParentImages')
//                ->joinWith('productFilterValue')
                ->joinWith('possibleSeries')
                ->joinWith('productParent')
                ->asArray()
                ->all();
      
        $result = [];
        foreach ($compotible as $value) {
            $product = Product::find()->where(['article'=>$value['article'], 'model_id'=>$value['model_id']])->one();
              
            $serie_id = null;
            foreach ($value['possibleSeries'] as $serie) {
                if(preg_match("/^".$serie['name']."$/i", $value['model']['name'])){
                   $serie_id = $serie['id'];
                }

            }
                      
            if(!$product){
                $product = new self();
                $product->article = $value['article'];
                $product->in_stock1 = $value['productParent']['in_stock1'];
                $product->in_stock2 = $value['productParent']['in_stock2'];
                $product->in_stock3 = $value['productParent']['in_stock3'];
                $product->in_res1 = $value['productParent']['in_res1'];
                $product->in_res2 = $value['productParent']['in_res2'];
                $product->in_res3 = $value['productParent']['in_res3'];
                $product->trade_price = $value['productParent']['trade_price'];
                $product->is_timed = $value['productParent']['is_timed'];
                $product->category_id = $value['productParent']['category_id'];
                $product->producer_id = $value['model']['producer_id'];
                $product->model_id = $value['model']['id'];
                $product->serie_id = $serie_id;
                $product->price = $value['productParent']['price'];
                $product->name = Product::createProductName($value['productParent']['category_id'], $value['model']['producer_id'], $value['model']['id']);
                $product->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($product->name);
                $product->logo = $value['productParent']['logo'];
                $product->available = $value['productParent']['available'];
                $product->status = $value['productParent']['status'];
                $product->createtime = time();
                $product->_is_new = 2;//создан с связи
                $result[$product->id]['product'] = $product->save();
            }
                foreach ($value['productParentImages'] as $item) {
                    $image = \app\models\Image::findOne(['product_id' => $product->id]);//, 'url' => $item['url']
                    if (!$image) {
                        $image = new \app\models\Image();
                        $image->product_id = $product->id;
                        $image->meta = $item['meta'];
                        $image->url = $item['url'];
                        $image->thumb = $item['thumb'];
                        $image->title = $item['title'];
                        $image->description = $item['description'];
                        $image->is_delete = $item['is_delete'];
                        $result[$product->id]['Images'][] = [$product->id =>$image->save()];
                    }
                }
                
//                foreach ($value['productFilterValue'] as $item) {
//                    $productFilterValue = ProductFilterValue::createProductFilterValue($product->id, $product->category_id, $product->producer_id, $serie_id,  $item['filter_id'], $item['filter_value_id']);
//                    $result[$product->id]['productFilterValue'][] = $productFilterValue->id;
//                }
        }
        return $result;
    }
    
    /**
     * ищем несоответствие товару относительно ариткула
     */
    public static function deleteProductNotIsArticle($article){
        $result = [];
        $nameCategory = Category::getNameInCategory($article);
            
        $product = self::find()
            ->where(['article'=>$article])
            ->andWhere(['not',['category_id'=> Category::getIdCstegory($article)]])
            ->andWhere(['_is_new'=>2])
            ->select('id')
            ->all();
        if(!empty($product)){
            foreach ($product as $value) {
                $result[$nameCategory][$value->id] = $value->delete();
            }
        }
        return $result;
    }
    
    /**
     * По id_model устанавливаем серию в товаре 
     */
    public static function setSerieForModel(){
        $product = self::find()
                ->where('category_id IS NOT NULL AND product.producer_id IS NOT NULL AND model_id IS NOT NULL AND product.serie_id IS NULL AND model.serie_id IS NOT NULL')
                ->select('product.name, product.id, category_id, product.producer_id, model_id, product.serie_id')
                ->joinWith('model')
                ->limit(1000)
                ->all();
        foreach ($product as $value) {
            $value->serie_id = $value['model']['serie_id'];
            vd($value->update(), false);
        }
    }
    
    
    /**
     * Снимаем с товаров статус не публиковать в yandexMarket
     */
    public static function setNotYandexPublished(){
        $list = Yii::$app->db->createCommand("
                    SELECT  p.article,  `is_published`, `ya_market` 
                    FROM product p
                    LEFT JOIN product_filter_value pfv USING ( article ) 
                    WHERE pfv.article IS NULL AND is_published = 1 AND ya_market = 1
                    GROUP BY p.article")->queryAll();
        
        foreach ($list as $value) {
           self::updateAll(['ya_market'=>0],['article'=>$value['article']]);
        }
        die("<br/>end");
    }
    
    /**
     * Обновляємо в товарах моделі по найближче схожим
     */
    public static function updateModelInName(){
        $model = self::find()
                ->select(["product.*,  GROUP_CONCAT(CONCAT(m2.id,':',m2.name) SEPARATOR '; ') AS _name"])
                ->leftJoin('model m', 'product.model_id=m.id')
                ->leftJoin('model m2', "product.name LIKE CONCAT('%',TRIM(m2.name),'%')")
                ->where("product.name NOT LIKE CONCAT('%',TRIM(m.name),'%')")
                ->andWhere(['IS NOT', 'm2.name', null])
                ->groupBy('m.name')
                ->limit(100)
                ->all();
        
        foreach ($model as $value){
            $result = [];
            foreach (explode('; ', $value->_name) as $ietm) {
                $mod = explode(':', $ietm);
                if(empty($result)){
                    $result = $mod;
                }  elseif(strlen($mod[1]) > $result[1]){
                    $result[] = $mod;
                }
            }
            $value->model_id = $result[0];
            vd($value->_name, false);
            vd($value->save(), false);;
        }
    }
    
    /**
     * Обновляє в товарах моделі або створює нові найближче схожі товари
     * @return boolean
     */
    public static function updateProducerInName(){
            $list = Yii::$app->db->createCommand("
                SELECT p.id,p.serie_id,p.producer_id, m.name, 
                m2.id AS model_id,
                m2.name AS _model_name
                FROM `product` p
                LEFT JOIN model m ON m.id = p.model_id
                LEFT JOIN model m2 ON m.name = m2.name AND p.producer_id = m2.producer_id
                WHERE m.producer_id != p.producer_id
            ")->queryAll();
            
            foreach($list as $value){
                $product = self::findOne($value['id']);
                if($value['model_id'] != NULL){
                    $product->model_id = $value['model_id'];
                }else{
                    $model = new Model();
                    $model->producer_id = $value['producer_id'];
                    $model->serie_id = $value['serie_id'];
                    $model->name = $value['name'];
                    if($model->save()){
                        $product->model_id = $model->id;
                    }  else {
                        vd($value, false);
                        echo "<br/>";
                        vd($model->getErrors(), false);
//                        return;
                    }
                }
                if(!$product->save()){
                    vd($product->getErrors(), false);
                }
            }
            return TRUE;
     }
     
     /**
      * Видаляе дублюючы товари
      */
    public static function deleteDubleProduct(){
        $list = Yii::$app->db->createCommand("
                SELECT
                    GROUP_CONCAT(id SEPARATOR ',') AS id,
                    article,
                    GROUP_CONCAT(name_lat SEPARATOR ',')  AS name_lat,
                    GROUP_CONCAT(original_name_lat SEPARATOR ',')  AS original_name_lat,
                    COUNT(id) AS _count 
                FROM `product`
                WHERE model_id IS NOT NULL
                GROUP BY article, model_id, name, serie_id, producer_id, name_lat
                HAVING _count > 1
          ")->queryAll();
        vd($list, false);
        foreach ($list as $value){
            $_id = array_filter(explode(',', $value['id']));
            $name_lat = array_filter(explode(',', $value['name_lat']));
            $original_name_lat = array_filter(explode(',', $value['original_name_lat']));
            
            $model_1 = self::findOne($_id[0]);
            unset($_id[0]);
            foreach ($_id as  $item) {
                $product = self::findOne($item);
                if($product){
                    vd($product, false);
                    $product->delete();
                }
            }
            if($original_name_lat){
                $original_name_lat = array_diff(array_unique($original_name_lat), array_unique($name_lat));
                $model_1->original_name_lat = implode(',', $original_name_lat);
                vd($model_1->save(), false);;
            }
        }
     }


    
    
    
}
