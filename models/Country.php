<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\Geo\SxGeo;

class Country extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "country";
    }

    public static function getCountrys(){
        $country_id = self::find()
                ->select('country_id, name')
                ->all();

        if($country_id){    
            return ArrayHelper::map($country_id,'country_id', 'name');
        }else
            return [];
    }
    
    public static function Name($id){
        $country = self::findOne($id);
        return $country ? $country->name : null;
    }
    
    public static function getIP(){
        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $ip = isset($_SERVER['HTTP_X_REAL_IP']) ? $_SERVER['HTTP_X_REAL_IP'] : $ip;
        return $ip;
    }
    
    public static function getGeo() {
        return Yii::$app->sypexGeo->getCityFull(self::getIP());
    }
}
