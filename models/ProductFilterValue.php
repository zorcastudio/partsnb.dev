<?php
namespace app\models;

use Yii;
use \yii\db\ActiveRecord;

class ProductFilterValue extends ActiveRecord {
    
    public $sort;
    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "product_filter_value";
    }

    public function rules() {
        return [
            [['filter_id', 'filter_value_id'], 'integer'],
            [['article', 'filter_value_id', 'serie_id'], 'unique', 'targetAttribute' => ['article', 'filter_value_id', 'serie_id'], 'message' => 'Поєднання данного товару та фільтру не повинні повторюватись.']
        ];
    }
    /*
     * удаляем связи продукта и фильтра
     */
    public static function deleteProduct($article){
        self::deleteAll(['article' => $article]);
    }

    public function attributeLabels() {
        return array(
            'filter_id' => "Фильтр",
            'category_id' => "Категория",
        );
    }
    
    public function getValue_all(){
        return $this->hasMany(FilterValue::className(), ['filter_id' => 'filter_id']);
    }
    
    public function getValue(){
        return $this->hasOne(FilterValue::className(), ['filter_value_id' => 'filter_value_id']);
    }

    public function getProductCount(){
        return $this->hasMany(Product::className(), [
                        'article' => 'article', 
                        'category_id'=>'category_id',
                        'producer_id'=>'producer_id',
                        'serie_id'=>'serie_id',
                ])
                ->where(['is_delete'=>0, '_is_problem'=>0, 'is_published'=>1, 'status'=>1])
                ->andWhere(['not', ['product.serie_id'=>null]])
                ->count();
    }
    
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['article' => 'article'])
                ->select('id, article, category_id, producer_id, model_id, serie_id, name, name_lat, is_delete, status, _is_problem');
    }

    public function getProduct_Id()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id'])
                ->select('id, article, category_id, producer_id, model_id, serie_id, name');
    }
    

    public function getFilter()
    {
        return $this->hasOne(Filter::className(), ['id' => 'filter_id']);
    }
    
    public function getFilterValue()
    {
        return $this->hasOne(FilterValue::className(), ['filter_value_id' => 'filter_value_id']);
    }
    
    
    public static function isActive($model){
        $result = [];
        if($model->filter){
            foreach ($model->filter as $value) 
                $result [$value->filter_id] = $value->filter_value_id;
        }else
            $result = [];
        
        return $result;
    }
    
    
    
    public function setProductFilterValue($model) {
        $models = [];
        if(isset($_POST['Compatible']) && isset($_POST['PartNamber'])){
            foreach (array_merge($_POST['Compatible'], $_POST['PartNamber']) as $value) {
                foreach ($value as $item) {
                    $models[] = $item;
                }
            }
        }else{
            if(isset($_POST['PartNamber'])){
                foreach ($_POST['PartNamber'] as $value) {
                    foreach ($value as $item) {
                        $models[] = $item;
                    }
                }
            }
            if(isset($_POST['Compatible'])){
                foreach ($_POST['Compatible'] as $value) {
                    foreach ($value as $item) {
                        $models[] = $item;
                    }
                }
            }
        }

        $result = implode(', ', $models);
        $filter = isset($_POST['Product']['filter']) ? $_POST['Product']['filter'] : NULL;
        
        
        if($result && $filter){
            $filter_key = [];
            //відфільтруємо пусті фільтри
            foreach ($filter as $key => $value) {
                if($value == '')
                    $filter_key[] = $key;
            }
            
            $filter_key = implode(', ', array_values($filter_key));
            
            $or_filter =  NULL;
            if($filter_key != ''){
                $or_filter = "OR (IF(pfv.filter_id IN ({$filter_key}), 1, 0) = 1)";
            }
            
            // видаляємо фільтри моделей які видалил з артикула або удалили значення фільтрів
            Yii::$app->db->createCommand("
                DELETE pfv FROM product_filter_value pfv
                LEFT JOIN model m ON m.id IN ({$result}) AND pfv.producer_id = m.producer_id AND pfv.serie_id = m.serie_id
                WHERE article = '{$model->article}'
                -- AND (m.id IS NULL {$or_filter})
            ")->execute();
               
            $filter = array_filter($filter);
            
            if(!empty($filter)){
                //Створюємо для нових серії фільтри
                $add_filter = Yii::$app->db->createCommand("
                    SELECT p.producer_id, p.serie_id
                    FROM product p
                    LEFT JOIN product_filter_value pfv ON pfv.article = '{$model->article}' AND pfv.producer_id = p.producer_id AND pfv.serie_id = p.serie_id
                    WHERE p.article = '{$model->article}'
                    AND p.is_published = 1
                    AND pfv.article IS NULL
                    GROUP BY p.producer_id, p.serie_id
                ")->queryAll();
                    
                $filter = array_filter($filter);
                foreach ($add_filter as $value) {
                    foreach ($filter as $key => $item) {
                        $filterValue = new self();
                        $filterValue->article = $model->article;
                        $filterValue->category_id = (int) $model->category_id;
                        $filterValue->producer_id = (int) $value['producer_id'];
                        $filterValue->serie_id = (int) $value['serie_id'];
                        $filterValue->filter_id = (int) $key;
                        $filterValue->filter_value_id = (int) $item;
                        $filterValue->save();
                    }
                }
                
                // створює недостаючі фальтри для серій
                foreach ($filter as $key => $value) {
                    $model_info = Yii::$app->db->createCommand("
                        SELECT m.producer_id, m.serie_id
                        FROM model m
                        WHERE m.id IN ({$result})
                        GROUP BY producer_id, serie_id
                    ")->queryAll();
                    foreach ($model_info as $item) {
                        if(!ProductFilterValue::find()->where(['article'=>$model->article, 'producer_id'=>$item['producer_id'], 'serie_id'=>$item['serie_id'], 'filter_id'=>$key])->exists()){
                            $filterValue = new self();
                            $filterValue->article = $model->article;
                            $filterValue->category_id = (int) $model->category_id;
                            $filterValue->producer_id = (int) $item['producer_id'];
                            $filterValue->serie_id = (int) $item['serie_id'];
                            $filterValue->filter_id = (int) $key;
                            $filterValue->filter_value_id = (int) $value;
                            $filterValue->save();
                        }
                    }
                }
                 
                // Обновляємо фільтри
                $update_filter = Yii::$app->db->createCommand("
                    SELECT pfv.article, m.producer_id, m.serie_id, pfv.filter_id
                    FROM model m
                    LEFT JOIN product_filter_value pfv ON pfv.article = '{$model->article}' AND pfv.producer_id = m.producer_id AND pfv.serie_id = m.serie_id AND pfv.filter_id IN (".implode(', ', array_keys($filter)).")
                    WHERE m.id IN ({$result})
                    AND pfv.filter_value_id NOT IN (".implode(', ', array_values($filter)).")
                ")->queryAll();
                foreach ($update_filter as $value) {
                    $filterUpdate = ProductFilterValue::find()
                            ->where([
                                'article'=>$value['article'],
                                'producer_id'=>$value['producer_id'],
                                'serie_id'=>$value['serie_id'],
                                'filter_id'=>$value['filter_id'],
                            ])->one();
                    
                    $filterUpdate->filter_value_id = (int)$filter[$value['filter_id']];
                    $filterUpdate->update();
                }
            }
        }
        
            
//        vd($_POST['Product']['filter'], false);
//            
////        vd($add_filter, false);
//        die("<br/>end");
        
        
//        vd($result);
//        die("<br/>end");
//        
//        if($model->_filter)
//            $_filter = array_filter($model->_filter);
//        
//        
//        if(!empty($_filter)){
//            $active = Yii::$app->db->createCommand("
//                        SELECT GROUP_CONCAT(CONCAT(producer_id,':',serie_id,':',filter_id,':',filter_value_id) SEPARATOR '; ') AS list
//                        FROM  product_filter_value
//                        WHERE article = '{$model->article}'
//                        GROUP BY article
//                    ")->queryAll();
//            $active = $active ? explode('; ', $active[0]['list']) : [];
//            $products =  Yii::$app->db->createCommand("
//                        SELECT `producer_id`, `serie_id`  
//                        FROM `product` 
//                        WHERE `article` = '{$model->article}'
//                            AND serie_id IS NOT NULL
//                            AND producer_id IS NOT NULL
//                        GROUP BY  `article`, `producer_id`, `serie_id`  
//                    ")->queryAll();
//                        
//            $filter = [];
//            foreach ($products as $value) {
//                foreach ($_filter as $key => $val){
//                    $filter[] = $value['producer_id'].':'.$value['serie_id'].':'. $key .':'. $val;
//                }
//            }
//            
//            foreach (array_diff($filter, $active) as $_f_) {
//                $f = explode(':', $_f_);
//                $filterValue = new self();
//                $filterValue->article = $model->article;
//                $filterValue->category_id = (int) $model->category_id;
//                $filterValue->producer_id = (int) $f[0];
//                $filterValue->serie_id = (int) $f[1];
//                $filterValue->filter_id = (int) $f[2];
//                $filterValue->filter_value_id = (int) $f[3];
//                $filterValue->save();
//            }
//            
//            foreach (array_diff($active, $filter) as $_f_) {
//                $f = explode(':', $_f_);
//                $temp = ProductFilterValue::deleteAll([
//                    'article'=>$model->article,
//                    'filter_id'=>$f[2],
//                    'filter_value_id'=>$f[3],
//                ]);
//            }
//        }
    }
    
    //Видалити якщо з фыльтрами буде все гаразд
    public function _setProductFilterValue($model) {
        if($model->_filter)
            $_filter = array_filter($model->_filter);
        
        
        if(!empty($_filter)){
            $active = Yii::$app->db->createCommand("
                        SELECT GROUP_CONCAT(CONCAT(producer_id,':',serie_id,':',filter_id,':',filter_value_id) SEPARATOR '; ') AS list
                        FROM  product_filter_value
                        WHERE article = '{$model->article}'
                        GROUP BY article
                    ")->queryAll();
            $active = $active ? explode('; ', $active[0]['list']) : [];
            $products =  Yii::$app->db->createCommand("
                        SELECT `producer_id`, `serie_id`  
                        FROM `product` 
                        WHERE `article` = '{$model->article}'
                            AND serie_id IS NOT NULL
                            AND producer_id IS NOT NULL
                        GROUP BY  `article`, `producer_id`, `serie_id`  
                    ")->queryAll();
                        
            $filter = [];
            foreach ($products as $value) {
                foreach ($_filter as $key => $val){
                    $filter[] = $value['producer_id'].':'.$value['serie_id'].':'. $key .':'. $val;
                }
            }
            
            foreach (array_diff($filter, $active) as $_f_) {
                $f = explode(':', $_f_);
                $filterValue = new self();
                $filterValue->article = $model->article;
                $filterValue->category_id = (int) $model->category_id;
                $filterValue->producer_id = (int) $f[0];
                $filterValue->serie_id = (int) $f[1];
                $filterValue->filter_id = (int) $f[2];
                $filterValue->filter_value_id = (int) $f[3];
                $filterValue->save();
            }
            
            foreach (array_diff($active, $filter) as $_f_) {
                $f = explode(':', $_f_);
                $temp = ProductFilterValue::deleteAll([
                    'article'=>$model->article,
                    'filter_id'=>$f[2],
                    'filter_value_id'=>$f[3],
                ]);
            }
        }
    }
    
    /**
     * Вернет список уникальных фильтров
     * @param type $get
     * @return int
     */
    public static function getFilterOut($get){
        $serie = '';
        $joinSerie = '';
        if(isset($get['serie'])){
            $serie = "AND s.name_lat = '{$get['serie']}'";
            $joinSerie = "INNER JOIN serie s ON  s.id = pfv.serie_id";
        }
        if(!isset($get['category'])) {
            return false;
        }
        $active = Yii::$app->db->createCommand("
                    SELECT  pfv.filter_id, pfv.filter_value_id,
                        COUNT( p.article ) AS p_count,
                        GROUP_CONCAT(DISTINCT CONCAT(f.name,':',f.name_lat,';',fv.name,':',fv.name_lat) SEPARATOR '; ') AS filter
                    FROM product_filter_value pfv
                    LEFT JOIN filter f ON f.id = pfv.filter_id
                    LEFT JOIN filter_value fv ON fv.filter_value_id = pfv.filter_value_id
                    LEFT JOIN product p ON p.article = pfv.article AND p.producer_id = pfv.producer_id AND p.serie_id = pfv.serie_id
                    INNER JOIN category c ON  c.id = pfv.category_id
                    INNER JOIN producer pr ON  pr.id = pfv.producer_id
                    {$joinSerie}
                    WHERE   c.name_lat = '{$get['category']}'
                        AND pr.name_lat = '{$get['producer']}'
                        {$serie}
                        AND p.is_delete = 0
                        AND p.status = 1
                        AND p.is_published = 1
                        AND p._is_problem = 0
                    GROUP BY  pfv.filter_value_id
                    ORDER BY  `filter_id` ASC             
                ")->queryAll();
        $result = [];
        foreach($active as $value){
            $filter = explode(';', $value['filter']);
            $name = explode(':', $filter[1]);
            $filter_name = explode(':', $filter[0]);
            $result[$value['filter_id']]['filter']['name'] = $filter_name[0];
            $result[$value['filter_id']]['filter']['name_lat'] = $filter_name[1];
            $result[$value['filter_id']]['value'][$value['filter_value_id']]['name'] = $name[0];      
            $result[$value['filter_id']]['value'][$value['filter_value_id']]['name_lat'] = $name[1];      
            $result[$value['filter_id']]['value'][$value['filter_value_id']]['count'] = (int) $value['p_count'];      
        }

        if (isset($get['filter'])) {
            $filter = Product::getFilterArray();
            foreach ($filter as $key => $value) {
                if(isset($value['id'])){
                    foreach (explode(',', $value['id']) as $val) {
                        $result[$key]['value'][$val]['checked'] = 1;
                    }
                }
            }
        }
        // видалемо одиночні фільтри
        foreach ($result as $key => $value) {
            if(count($value['value'])==1)
                unset($result[$key]);
        }
            
        return $result;
    }


    public static function updateProductFilterValue($s, $f){
        $product = Product::find()
                ->where([
                    'product._is_problem'=>0,
                    'product.status'=>1,
                    'product.is_delete'=>0,
                    'is_published'=>1,
                ])
                ->andWhere(['not',['product.category_id'=>null]])
                ->andWhere(['not',['product.producer_id'=>null]])
                ->andWhere(['not',['product.serie_id'=>null]])
                
                ->andWhere(['between', 'product.id',  $s, $f])
                ->select('product.id, product.category_id, product.serie_id, product.producer_id')
                ->joinWith('productPropertiesView', true, 'INNER JOIN')
                ->joinWith('filters', true, 'INNER JOIN')
                ->asArray()
                ->all();
        $result = [];
        foreach ($product as $value) {
            $properties = [];
            foreach ($value['productPropertiesView'] as $view) {
                $properties[$view['id']] = $view['name'];
            }
            $filter = [];
            foreach ($value['filters'] as $filt) {
                $filter[$filt['id']] = $filt['name'];
            }
            foreach (array_intersect($filter, $properties) as $key => $item) {
                $keyProperties = array_search($item, $properties);
                if($keyProperties){
                    $productProperties = ProductProperties::find()
                            ->where([
                                'product_id'=>$value['id'],
                                'properties_id'=>$keyProperties,
                            ])
                            ->select('value')
                            ->asArray()
                            ->one();
                    if($productProperties){
                        $filterValue = FilterValue::find()
                                ->where([
                                    'filter_value.name'=>$productProperties['value'],
                                    'filter_value.filter_id'=>$key,
                                ])
                                ->select('filter_value.filter_value_id, filter_value.filter_id, filter_value.name')
                                ->asArray()
                                ->one();
                        //если найдем в фильтрах соответственное свойство 
                        //перезаписываем фильтр товара
                        if($filterValue){
                            $productFilterValue = ProductFilterValue::find()
                                    ->where([
                                        'product_id'=>$value['id'],
                                        'filter_id'=>$key
                                    ])->one();
                    
//                            vd($productFilterValue, false);
                            $productFilterValue->filter_value_id = $filterValue['filter_value_id'];
//                            vd($productFilterValue->update(), false);;
                            $result [$value['id']] = $productFilterValue->update();
                        }
                    }
                }
            }
        }
        return $result;
    }

    
    public static function getProductId($filter, $category_id){
        $query = self::find();
        $_filter = base64_decode($filter);
        $_filter = json_decode($_filter, true);
        $filter = [];
        $match = [];
        $k = [];
        foreach ($_filter as $key => $value) {
            $k[] = $key;
            foreach ($value as $item) {
                $filter[] = $item;
            }
            $product = $query
                    ->where(['filter_value_id' => $filter])
                    ->select('article')
                    ->distinct()
                    ->asArray()
                    ->all();
            $result = [];
            foreach ($product as $item) {
                $result[] = $item ['article'];
            }
            //возвращает только те значеиния масива которые есть
            $match = (empty($match)) ? $result : array_intersect($result, $match);
            $filter = [];
            //                array_diff
            //                vd($result, false);
            //                echo "<br/>";
            //                vd($match, false);
            //                echo "<br/>";
        }
        return $match;
        //            vd($_filter, false);
        //            echo "<br/>";
        //            vd($filter, false);
        die("<br/>end");

        $query->where(['filter_value_id' => $filter]);

        $filter = [];
        if (count($_filter) > 1) {
            unset($_filter[$k[0]]);
            $k = [];
            foreach ($_filter as $key => $value) {
                $k[] = $key;
                foreach ($value as $item)
                    $filter[] = $item;
                //возвращает только те значеиния масива которые есть 
                array_intersect($array1, $array2);

                $query->andWhere(['filter_value_id' => $filter]);
            }
        }
        $model = $query->select('product_id')->distinct()->asArray()->all();
        $result = [];
        foreach ($model as $value) {
            $result[] = $value['product_id'];
        }
        return $result;


        die("<br/>end");
        $result = [];


        $filter = [];
        $f_value = [];
        //формируем масив всех допустимых товаров которые отмечены в фильрах


        $query->where(['id' => $result]);



        //если отмечены товары в перекрестных фильтрах
        if (count($_filter) > 1) {
            unset($_filter[$filter[0]]);
            foreach ($_filter as $key => $value) {
                $filter[] = $key;
                foreach ($value as $item)
                    $f_value[] = $item;
            }
            //формируем масив всех допустимых товаров которые отмечены в фильрах
            $p_filter = ProductFilterValue::find()
                    ->where([
                        'category_id_' => $category->id,
                        'filter_id' => $filter,
                        'filter_value_id' => $f_value
                    ])
                    ->asArray()
                    ->select('product_id')
                    ->distinct()
                    ->all();
            foreach ($p_filter as $item) {
                $result[] = $item['product_id'];
            }
            

            //                vd($_filter, false);
            //                echo "<br/>";
            die("<br/>end");
            //                $query->andWhere(['id'=>$result]);
        }
        echo $key;
       
        return;
    }
    
    
    /**
     * 
     * @param type $category_id - категория фильтра
     * @param type $product_id - id товара
     * @param type $producer_id - id производителыя
     * @param type $serie_id - id серии товара
     * @param type $filter_id - id фильтра
     * @param type $filter_value_id - id значения фильтра
     * @return модель связи фильтра с товаром
     */
    public static function createProductFilterValue($article, $category_id, $producer_id, $serie_id, $filter_id, $filter_value_id){
        $model = self::find()
                    ->where([
                        'article'=>$article,
                        'category_id'=>$category_id,
                        'producer_id'=>$producer_id,
                        'serie_id'=>$serie_id,
                        'filter_id'=>$filter_id,
                        'filter_value_id'=>$filter_value_id,
                    ])
                    ->one();
        $model ?: $model = new self();
        
        $model->article = $article;
        $model->category_id = $category_id;
        $model->producer_id = $producer_id;
        $model->serie_id = $serie_id;
        $model->filter_id = $filter_id;
        $model->filter_value_id = $filter_value_id;
        $model->save();
        return $model;
    }
    
     /**
     * добавляет к таблице артикуль вместо id_product
     */
    public static function setArticle(){
        $productFilterValue = self::find()
                ->andWhere(['product_filter_value.article'=>''])
                ->limit(1000)
//                ->joinWith('product_Id')
                ->all();
        foreach ($productFilterValue as $value) {
            $model = self::find()
                    ->where([
                        'article'=>$value['product_Id']->article,
                        'filter_value_id'=>$value['filter_value_id'],
                    ])
                    ->all();
            if(!$model){
                $value->article = $value['product_Id']->article;
                $value->update();
                
            }else{
                $value->delete();
            }
        }
    }
    
}
