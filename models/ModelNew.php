<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;


class ModelNew extends \yii\db\ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "model_new";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [];
    }
    
    public function getProducer() {
        return $this->hasOne(Producer::className(), ['id' => 'producer_id']);
    }
    
    public function getProduct() {
        return $this->hasOne(\app\modules\admin\models\ProductSet::className(), ['id' => '_id']);
    }
    
    public function getCompotible() {
        return $this->hasMany(ModelCompatible::className(), ['model_id' => 'id']);
    }

   
    
    /**
     * Существует ли данная модель
     */
    public function isExists(){
        return self::find()
                ->where(['producer_id'=>$this->producer_id, 'name'=> $this->name])
                ->exists();
    }

   
}
