FROM_UNIXTIME(createtime) AS createtime 

-- Функция поиска и замены в слове по регулярному выражению
DELIMITER $$
CREATE FUNCTION  `regex_replace`(pattern VARCHAR(1000),replacement VARCHAR(1000),original VARCHAR(1000))

RETURNS VARCHAR(1000)
DETERMINISTIC
BEGIN 
    DECLARE temp VARCHAR(1000); 
    DECLARE ch VARCHAR(1); 
    DECLARE i INT;
    SET i = 1;
    SET temp = '';
    IF original REGEXP pattern THEN 
        loop_label: LOOP 
        IF i>CHAR_LENGTH(original) THEN
            LEAVE loop_label;  
        END IF;
        SET ch = SUBSTRING(original,i,1);
        IF NOT ch REGEXP pattern THEN
            SET temp = CONCAT(temp,ch);
        ELSE
        SET temp = CONCAT(temp,replacement);
        END IF;
            SET i=i+1;
        END LOOP;
    ELSE
        SET temp = original;
    END IF;
        RETURN temp;
END$$
DELIMITER;



SELECT *, COUNT( * ) AS _count 
FROM `product` 
LEFT JOIN `category` 
    ON `product`.`category_id` = `category`.`id` 
    WHERE (
            product._is_problem = 0 
            AND product.status = 1 
            AND product.is_delete = 0 
            AND product.category_id IS NOT NULL 
            AND producer_id IS NOT NULL 
            AND model_id IS NOT NULL 
            AND serie_id IS NOT NULL 
            AND `product`.`name` LIKE '%PK1314D1A100%'
            AND article IN (010432,010414) 
        ) 
    AND (`category`.`is_delete`=0) 

    GROUP BY `category_id` 
    ORDER BY `product`.`category_id`

-- отображает толкько те товары у которых одна фотография
SELECT  
    `product`.`id`,  
    `product`.`name`,  
    `image`.`product_id`,  
    `image`.`id`,  
    `image`.`url`, 
    COUNT( image.id ) AS _count
FROM  `product` 
INNER JOIN  `image` 
    ON product.id = image.product_id
    WHERE  `product`.`is_delete` = 0
GROUP BY  `image`.`product_id` 
HAVING _count < 2;


SELECT 
    model.id, 
    model.name, 
    product.id AS Product_ID, 
    product.name AS product_Name
FROM `model` 
INNER JOIN `product` 
WHERE   `model`.`producer_id`=6 
    AND `model`.`serie_id`=111 
    AND model.id = product.model_id 
    AND product.category_id = 2


SELECT 
    model.id, 
    model.name, 
    product.id AS Product_ID, 
    product.name AS product_Name
FROM `model` 
INNER JOIN `product` ON model.id = product.model_id
WHERE   `model`.`producer_id`=6 
    AND `model`.`serie_id`=111 
    AND product.category_id = 2



SELECT  product.id, article, category_id, producer_id, model_id, serie_id, product.name, product.is_delete, _is_problem, status, COUNT(*) AS _count
FROM `product` 
LEFT JOIN `category` ON `product`.`category_id` = `category`.`id` 
WHERE 
    (
        product._is_problem = 0 
        AND product.status = 1 
        AND product.is_delete = 0 
        AND product.category_id IS NOT NULL 
        AND producer_id IS NOT NULL 
        AND model_id IS NOT NULL 
        AND serie_id IS NOT NULL 
        AND `product`.`name` LIKE '%GC054509VH-A%' 
        OR/AND article IN (020005,020070,020258,020271) 
    ) 
    AND (`category`.`is_delete`=0) 
GROUP BY `category_id`

SELECT * FROM `product_filter_value` 
WHERE 
    (
            `product_filter_value`.`article`='030030' 
        AND `product_filter_value`.`category_id`=3 
        AND `product_filter_value`.`producer_id`=15 
        AND `product_filter_value`.`filter_id`=8 
        AND `product_filter_value`.`serie_id`=352 
        AND `filter_id`=8
    ) 
AND (NOT (`filter_value_id`=19))

SELECT model.id AS ID, producer.name AS Производитель, model.name AS Модель 
FROM `model`
INNER JOIN producer ON producer.id = model.producer_id


"acer 5810"
SELECT * FROM `product` 
WHERE 
        product._is_problem = 0 
    AND product.status = 1 
    AND product.is_delete = 0 
    AND product.category_id IS NOT NULL 
    AND producer_id IS NOT NULL 
    AND model_id IS NOT NULL 
    AND serie_id IS NOT NULL 
    AND product.category_id = 1 
    AND producer_id = 1 
    AND (
        article IN (010006,020023,040004,050008,060040,080003) 
        OR product.name LIKE '%5810%' 
        OR article IN (010121,010121,010121,020137)
    )


SELECT *, COUNT( * ) AS _count 
FROM `product` 
LEFT JOIN `category` ON `product`.`category_id` = `category`.`id` 
WHERE (
        article = '010002'
    ) 
    AND (`category`.`is_delete` = 0) 
GROUP BY `category_id` 
ORDER BY `product`.`category_id`


SELECT *, COUNT( * ) AS _count 
FROM `product` 
LEFT JOIN `category` ON `product`.`category_id` = `category`.`id` 
WHERE (
        product._is_problem = 0 
    AND product.status = 1 
    AND product.is_delete = 0 
    AND product.category_id IS NOT NULL 
    AND producer_id IS NOT NULL 
    AND model_id IS NOT NULL 
    AND serie_id IS NOT NULL 
    AND article = '010002'
) 
AND (`category`.`is_delete`=0) 
GROUP BY `category_id` 
ORDER BY `error`



SELECT `id`, p.article, `category_id`, `producer_id`, p.model_id, `serie_id`, `name`, ' - ', mc.model_id AS  `Модели` 
FROM `product` p
LEFT JOIN model_compatible mc USING(model_id)
WHERE p.article LIKE '010001'
AND mc.model_id IS NULL
-- AND p.model_id IS NOT NULL
-- AND p.producer_id IS NOT NULL


SELECT `id`, `product`.`article`, `category_id`, `producer_id`, `product`.`model_id`, `serie_id`, `name` 
FROM `product` LEFT JOIN `model_compatible` `mc` USING(model_id)
WHERE 
        producer_id IS NOT NULL 
    AND product.model_id IS NOT NULL 
    AND mc.model_id IS NULL 



SELECT mc.model_id, mc.article, '', p.`model_id`, p.name 
FROM `model_compatible` mc
LEFT JOIN product p USING(model_id)
WHERE p.model_id IS NULL
--         producer_id IS NOT NULL 
--     AND product.model_id IS NOT NULL 
--     AND mc.model_id IS NULL 

INSERT INTO `page` (`is_menu`, `name`, `name_lat`, `keywords`, `description`, `text`, `parent_id`) 
VALUES (0, 'Схема проезда к магазину \"м. Садовая\"', 'shema-proezda-k-magazinu-m-sadovaya', '', '', '<p>м. &quot;Садовая&quot; БЦ &quot;МИР&quot; (ул. Ефимова, д.4а, лит А), 4 этаж, офис 425<br />\r\nТел. (812) 922-05-70, +7 911 922-05-70<br />\r\nРежим работы: с 10:00 до 20:00 пн-пт и с 11:00 до 19:00 сб-вс</p>\r\n', 0)


--Фильтры
SELECT p.article, c.name AS `категория`,  pr.name AS `производитель` ,  s.name AS `серия`, f.name AS `Фильтр`, fv.name AS `значение`
FROM `product_filter_value`p
LEFT JOIN category c ON c.id = p.category_id 
LEFT JOIN producer pr ON pr.id = p.producer_id
LEFT JOIN serie s ON s.id = p.serie_id  
LEFT JOIN filter_value fv ON fv.filter_value_id = p.`filter_value_id`  
LEFT JOIN filter f ON f.id = p.`filter_id`  
WHERE p.category_id = 1 AND p.producer_id = 9 AND p.serie_id = 362

-- вернет все продукты в разрезе уникальных артикулов которых разные категории
SELECT  `id` ,  `article` ,  `category_id` ,  `producer_id` ,  `model_id` ,  `serie_id` ,  `name` ,  `is_delete` ,  `is_published` ,  '*', COUNT( DISTINCT article, category_id ) AS _count
FROM  `product` 
GROUP BY article
HAVING _count >1

-- Связывает товары с оригинальными изображениями
SELECT p.id, p.article, p.name, `is_published`, `_original_id`, '#', jpm.virtuemart_media_id, '#', jm.file_url 
FROM `product` p
LEFT JOIN  j251_virtuemart_product_medias jpm ON jpm.virtuemart_product_id = p._original_id
LEFT JOIN  j251_virtuemart_medias jm USING(virtuemart_media_id)
WHERE is_published =1
GROUP BY article


-- Удаляем псевдо пробел &nbsp; с названия моделей
UPDATE model  SET name = replace (name, '>\n\t*', ' ');


SELECT  `id` ,  `article` ,  `category_id` ,  `producer_id` ,  `model_id` ,  `serie_id` ,  `name` ,  `_original_id` ,  "*", COUNT( id ) AS _count
FROM  `product` 
GROUP BY  `article` ,  `category_id` ,  `producer_id` ,  `model_id` ,  `serie_id` 
HAVING _count >1

-- Отсутствующие товары
SELECT  `virtuemart_product_id` ,  `product_sku` ,  `product_url` 
FROM  `j251_virtuemart_products` jp
LEFT JOIN product p ON p.article = jp.product_sku
WHERE p.article IS NULL 
GROUP BY product_sku


SELECT i.article, i.url, product_id, '#', p.name, _original_id, '#', jpm.virtuemart_media_id AS VM_id
FROM `image` i
LEFT JOIN product p ON p.id = i.product_id
LEFT JOIN j251_virtuemart_product_medias jpm ON jpm.virtuemart_product_id = i.product_id
LEFT JOIN j251_virtuemart_medias  jm ON jm.virtuemart_media_id = jpm.virtuemart_media_id
WHERE jpm.virtuemart_media_id IS NOT NULL 
GROUP BY i.article, url


SELECT p.id, p.article, `_original_id` , " # ", file_url, "#", i.url
FROM `product` p
LEFT JOIN j251_virtuemart_product_medias jpm ON jpm.virtuemart_product_id = p._original_id
LEFT JOIN j251_virtuemart_medias  jm USING(virtuemart_media_id)
LEFT JOIN image i ON i.product_id=p.id
WHERE p.article IN (010001, 010002, 010003) AND  jpm.virtuemart_media_id IS NOT NULL
GROUP BY _original_id, file_url


SELECT p.id, p.`article`, '#', i.url
FROM `product` p
LEFT JOIN image i ON i.product_id=p.id
WHERE i.url IS NULL
AND p.article = 010001

-- каких товаров не существует изображений
SELECT p.id, p.`article` ,  '#', i.url
FROM  `product` p
LEFT JOIN image i ON i.article = p.article
WHERE i.article IS NULL 
LIMIT 0 , 30

-- возвращает ссылки на изображения по ариткулу
SELECT 
    jp.virtuemart_product_id AS id, 
    jp.product_sku AS article, 
    "#",  
    TRIM(LEADING 'images/stories/virtuemart/product/' FROM file_url) AS url,   
    TRIM(LEADING 'images/stories/virtuemart/product/resized/' FROM file_url_thumb) AS thumb,
    file_meta AS title, 
    file_description AS description   
FROM `j251_virtuemart_products` jp
LEFT JOIN j251_virtuemart_product_medias jpm USING(virtuemart_product_id)
LEFT JOIN j251_virtuemart_medias jm USING(virtuemart_media_id)
WHERE `product_sku` LIKE '%010182%'
GROUP BY article, url

-- Показывает все товары у которых нет изображений
SELECT p.article 
FROM `product` p
LEFT JOIN image i ON i.article=p.article
WHERE p.article != '' AND p.article IS NULL
GROUP BY p.article

-- список унікальних товарів (по артикулу) без зображень з масивом id
SELECT p.`id` , p.`article` , p.`category_id` , p.`producer_id` , p.`model_id` , p.`serie_id` , p.`name` , p.`name_lat` , p.`is_published` , p.`_original_id`
, GROUP_CONCAT( p.id SEPARATOR ',') AS group_id
FROM  `product` p
LEFT JOIN  `image`  `i` ON i.product_id = p.id
WHERE p.article !=  ''
AND i.product_id IS NULL 
AND p.category_id IS NOT NULL 
AND p.producer_id IS NOT NULL 
AND p.model_id IS NOT NULL 
AND p.serie_id IS NOT NULL 
AND p._original_id IS NOT NULL
GROUP BY article

-- Всі ссілки які були у файлі перевірки 9 856
SELECT  `id` ,  `article` ,  `ansver` ,  `url_test` 
FROM  `_urls` 
WHERE ansver
IN ( 404, 500 ) 



SELECT  u.`id` , u.`article` ,  `ansver`,  `url_test`, "#", p.name  
FROM  `_urls` u
LEFT JOIN product p ON u.url_test = p.name_lat
WHERE p.name_lat IS NULL
AND  ansver IN (404, 500) 
OR p.category_id IS NULL 
OR p.producer_id IS  NULL 
OR p.model_id IS  NULL 
OR p.serie_id IS  NULL 


SELECT  u.`id` , u.`article` ,  `ansver`,  `url_test`, "#", p.`id`, p.`name`, `is_delete`, `status`, `is_published`  
FROM `_urls` u
LEFT JOIN product p ON u.url_test = p.name_lat
WHERE ansver = 404 
AND u.article = p.article
AND is_published != 1
AND category_id IS NOT NULL
AND producer_id IS NOT NULL
AND model_id IS NOT NULL
AND serie_id IS NOT NULL


SELECT mch.id, mch.name, GROUP_CONCAT(CONCAT(mch.id,':',mch.name)   ORDER BY mch.name   SEPARATOR '; ') AS group_model,
 m.producer_id, pr.name
FROM `model_compatible` mc
INNER JOIN model m ON m.id = mc.model_id
INNER JOIN model_child mch ON mc.model_id = mch.model_id
LEFT JOIN producer pr ON pr.id = m.producer_id
WHERE mc.article = 070063
GROUP BY m.producer_id



SELECT p.`id`,
    GROUP_CONCAT(
        CONCAT(
            p.id,':',
            (
                CASE 
                    WHEN p.update != 0 THEN  p.update
                    WHEN p.createtime != 0 THEN p.createtime
                    ELSE unix_timestamp(now()) END
            ),':',
            CONCAT('/', c.name_lat, '/', pr.name_lat, '/', s.name_lat, '/', p.name_lat)
        )   SEPARATOR '; '
    ) AS LIST
    FROM `product` p
    INNER JOIN category c ON p.category_id = c.id
    INNER JOIN producer pr ON p.producer_id = pr.id
    INNER JOIN serie s ON p.serie_id = s.id
    WHERE p.is_delete =0
    AND p.status = 1
    AND p.is_published = 1
    AND p._is_problem = 0
    AND p.article IS NOT NULL
    AND p.category_id IS NOT NULL
    AND p.producer_id IS NOT NULL
    AND p.model_id IS NOT NULL
    AND p.serie_id IS NOT NULL
    AND p.name_lat IS NOT NULL



SELECT p.id, p.parent_id, p.name_lat,
CASE 
    WHEN p.parent_id IS NOT NULL THEN 
        CONCAT('/', (SELECT name_lat FROM page WHERE id = p.parent_id),'/', p.name_lat)
    ELSE 
        CONCAT('/', p.name_lat)
    END  AS url
FROM `page` p




SELECT p.id,
CONCAT('/', c.name_lat, '/', pr.name_lat, '/', s.name_lat, '/', p.name_lat) AS url,
p.price,
p.category_id, 
CASE 
    WHEN p.category_id IN (1,2)  
        THEN CONCAT('Компьютеры/Аксессуары и расходные материалы/', c.name, ' для ноутбуков')
    ELSE "Компьютеры/Аксессуары и расходные материалы/Аксессуары для ноутбуков"
END AS market_category,
GROUP_CONCAT(CONCAT('/uploadfile/images/product/',i.url) SEPARATOR '; ') AS picture,
    p.`name` AS typePrefix,
    pr.name AS vendor,
    m.name AS model,
GROUP_CONCAT(CONCAT(f.name,', ', fv.name) SEPARATOR '; ')  AS param
FROM `product` p
INNER JOIN category c ON p.category_id = c.id
INNER JOIN producer pr ON p.producer_id = pr.id
INNER JOIN serie s ON p.serie_id = s.id
INNER JOIN model m ON p.model_id = m.id
LEFT JOIN image i ON p.id = i.product_id
LEFT JOIN product_filter_value pfv ON p.article = pfv.article
INNER JOIN filter f ON pfv.filter_id = f.id
INNER JOIN filter_value fv ON pfv.filter_value_id = fv.filter_value_id
WHERE p.is_delete =0
AND p.status = 1
AND p.is_published = 1
AND p._is_problem = 0
AND p.article IS NOT NULL
AND p.category_id IS NOT NULL
AND p.producer_id IS NOT NULL
AND p.model_id IS NOT NULL
AND p.serie_id IS NOT NULL
AND p.name_lat IS NOT NULL
AND p.ya_market=1
GROUP BY p.id


-- Обновляє стрічки по умові
UPDATE `model` m 
INNER JOIN all_p_n a ON m.name=a.name
SET m.is_pn = 1 
WHERE m.is_pn = 0

SELECT a.id, a.producer_id,
CASE a.producer_id  
    WHEN 1 THEN 471
    WHEN 2 THEN 473
    WHEN  13 THEN 472
    WHEN  22 THEN 474
    WHEN  24 THEN 475
    WHEN  16 THEN 476
    WHEN  18 THEN 477
    WHEN  17 THEN 478
    WHEN  23 THEN 479
    WHEN 30 THEN 480
    WHEN 13 THEN 472
    WHEN  3 THEN 481
    WHEN  21 THEN 482
    WHEN 20 THEN 483
    WHEN  13 THEN 472
    WHEN  14 THEN 484
    WHEN  4 THEN 485
    WHEN  27 THEN 486
    WHEN  5 THEN 487
    WHEN  6 THEN 488
    WHEN  10 THEN 489
    WHEN  8 THEN 490
    WHEN  33 THEN 491
    WHEN  34 THEN 492
    WHEN  29 THEN 493
    WHEN  9 THEN 494    
    WHEN  31 THEN 495
    WHEN  25 THEN 496
    WHEN  7 THEN 497
    WHEN  35 THEN 498
    WHEN  19 THEN 499
    WHEN  11 THEN 500
    WHEN  12 THEN 501
    WHEN  32 THEN 502
    WHEN  15 THEN 503
    WHEN  36 THEN 504
    WHEN  28 THEN 505
    WHEN  26 THEN 506
    WHEN  37 THEN 507
END AS serie_id, a .name
FROM `all_p_n` a
LEFT JOIN model m ON m.name=a.name
WHERE m.name IS null
ORDER BY id DESC
LIMIT 400

/**
* проверяем к каким артикулам относится товары
*/
SELECT p.`virtuemart_product_id` AS id, p.`product_sku` AS article, vp.product_name AS name
FROM `3_j251_virtuemart_products` p
LEFT JOIN 3_j251_virtuemart_products_ru_ru vp ON vp.virtuemart_product_id = p.virtuemart_product_id
WHERE p.`product_sku` RLIKE '^01'
ORDER BY  `vp`.`product_name` DESC 

/**
* Вернет все парт номера для которых не создано торару
*/
SELECT pn.*
FROM `product_pn_search` pn
LEFT JOIN product p ON p.article = pn.article AND p.name LIKE CONCAT('%',pn.name,'%')
WHERE  p.name IS NULL


SELECT pfv.article,  pfv.filter_id, pfv.filter_value_id,  -- COUNT( p.article ),
-- SUM (SELECT COUNT( * ) FROM  `product` WHERE  `article` LIKE  pfv.article)
GROUP_CONCAT(CONCAT(pfv.filter_id,':', f.name,'-',pfv.filter_value_id,':', fv.name) SEPARATOR '; ') AS filter
-- COUNT(p.article)
-- COUNT(pfv.filter_value_id)


FROM product_filter_value pfv
LEFT JOIN filter f ON f.id = pfv.filter_id
LEFT JOIN filter_value fv ON fv.filter_value_id = pfv.filter_value_id
LEFT JOIN product p ON p.article = pfv.article

WHERE pfv.category_id = '1'
AND pfv.producer_id = '6'
AND pfv.serie_id = '146'
GROUP BY  pfv.filter_value_id




SELECT *, 
    GROUP_CONCAT(CONCAT(m2.id,':',m2.name) SEPARATOR '; ') AS _name 
FROM `product` 
LEFT JOIN `model` `m` ON product.model_id=m.id 
LEFT JOIN `model` `m2` ON product.name 
LIKE CONCAT('%',TRIM(m2.name),'%') 
WHERE product.name NOT LIKE CONCAT('%',TRIM(m.name),'%') 
    AND (`m2`.`name` IS NOT NULL) 
GROUP BY `m`.`name` 
LIMIT 5

-- Видаляе ті строки для яких немає в товарах звязку
DELETE mc.*
FROM `model_compatible` mc
LEFT JOIN product p ON p.article = mc.article AND p.model_id = mc.model_id
WHERE p.model_id IS NULL AND p.article IS NULL

-- Створює недостаючі звязки
INSERT INTO `model_compatible` (`article`, `model_id`)
SELECT p.article, p.model_id
FROM product p
LEFT JOIN model_compatible mc ON (p.model_id  = mc.model_id AND p.article = mc.article)
WHERE p.model_id IS NOT NULL
AND  (mc.model_id IS NULL AND mc.article IS NULL)
GROUP BY p.model_id, p.article -- щоб не було дублюючих товарів


-- заповняє недістаючі зображення логотипу
UPDATE `product` p 
INNER JOIN  `j251_virtuemart_products` vp ON p.article = vp.product_sku
INNER JOIN j251_virtuemart_product_medias vpm ON vp.virtuemart_product_id = vpm.virtuemart_product_id
INNER JOIN j251_virtuemart_medias vm ON vm.virtuemart_media_id = vpm.virtuemart_media_id
SET p.logo = CONCAT(REPLACE (REPLACE (file_url, 'images/stories/virtuemart/product/', ''), '.jpg', ''),'_150x150.jpg')
WHERE ordering = 1
AND p.logo = ''

-- Заповняе недостаючі зображення в розрізі артикула
INSERT INTO image (product_id, article, meta, url, thumb, title, description, is_delete)
SELECT p.id, i2.*
FROM product p
LEFT JOIN image i ON i.product_id = p.id
LEFT JOIN (
    SELECT article, meta, url, thumb, title, description, is_delete 
    FROM image
    WHERE url != '' AND article != ''
    GROUP BY article, url
) i2 ON i2.article = p.article
WHERE i.id IS NULL 
AND i2.url IS NOT NULL


SELECT name,
 CASE
                     WHEN LOCATE('Петля для матриц ', name)  
                             THEN REPLACE (name, 'Петля для матриц', 'Петли матриц для ноутбука')
                         ELSE  REPLACE (name, 'Петли для матриц', 'Петли матриц для ноутбука')
                     END AS _name
FROM product
WHERE article RLIKE '^06' 
-- AND name NOT LIKE 'Петли матриц для ноутбука%'
ORDER BY name DESC


DROP FUNCTION IF EXISTS hello_world;
DELIMITER $$
CREATE FUNCTION hello_world(_typeId INT, _typeText TEXT)
  RETURNS TEXT
BEGIN
    DECLARE _parentId INT;
    DECLARE _typeContent TEXT;

    SELECT id, email INTO _parentId, _typeContent 
    FROM Ouser WHERE affid = _typeId;


    RETURN CONCAT(
      IF(_parentId IS NOT NULL, 
        hello_world(_parentId, _typeContent), 
        'not found'), 
      _typeContent, ' - ', _parantId);
END;
$$
DELIMITER;





DELIMITER $$
CREATE FUNCTION build_path(_typeId INT)
  RETURNS TEXT
BEGIN

    DECLARE _parentId INT;
    DECLARE _typeContent TEXT;

    set @ret = '';
    while _typeId is not null do

      SELECT id, email, affid
      INTO  _parentId,  _typeContent 
      FROM fcom_product_type 
      WHERE id = _typeId;

      IF(found_rows() != 1) THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'id not found in table fcom_product_type';
      END IF;

      set @ret := CONCAT('/', _typeContent, @ret); 
      set _typeId := _parentId;

    end while;

    RETURN @ret;
END;
$$
DELIMITER ;


-- Каскадное удаление MySQL
CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
title CHAR(40),
content TEXT,
public_date DATE,
PRIMARY KEY (id)
) TYPE = InnoDB;

CREATE TABLE comments(
id INT NOT NULL AUTO_INCREMENT,
post INT,
autor CHAR(20),
text CHAR(100),
PRIMARY KEY (id),
FOREIGN KEY (post) REFERENCES posts (id)
ON UPDATE RESTRICT
ON DELETE CASCADE
) TYPE = InnoDB;

SELECT `category`.*, COUNT( * ) AS _count 
FROM `product` 
LEFT JOIN `category` ON `product`.`category_id` = `category`.`id` 
INNER JOIN `model` `m` ON m.id = product.model_id 
WHERE ( product.is_published = 1 
    AND product._is_problem = 0 
    AND product.status = 1 
    AND product.is_delete = 0 
    AND product.category_id IS NOT NULL 
    AND product.producer_id IS NOT NULL 
    AND product.model_id IS NOT NULL 
    AND product.serie_id IS NOT NULL 
    AND (
        m.name RLIKE '^([a-zA-Z]* )*сетевой кабель$' 
        OR product.model_id IN (SELECT model_id FROM model_child WHERE name RLIKE '^([a-zA-Z]* )*сетевой кабель$' AND is_dalete = 0))
    ) 
    AND (`category`.`is_delete`=0) 
GROUP BY `category_id`


-- UPDATE  `result` r
SELECT r.id, r.model_id, r.name,
TRIM(replace(regex_replace('[а-яёА-ЯЁ\(\)]', '',r.name), pr.name, "")) AS _name,
m.name AS m_name
FROM `result` r
INNER JOIN producer pr ON pr.id=r.producer_id
LEFT JOIN model m ON r.producer_id = m.producer_id AND r.serie_id=m.serie_id AND TRIM(replace(regex_replace('[а-яёА-ЯЁ\(\)]', '',r.name), pr.name, ""))  RLIKE CONCAT('^',m.name)
-- SET r.serie_id = s.id
WHERE r.serie_id IS NOT NULL 
-- AND 
-- r.serie_id IS NULL

-- 10, 11, 1, 13, 12, 15


-- створюємо товари з таблиці результ 
INSERT INTO product (article,     in_stock1,      in_stock2,     in_stock3,      in_res1,     in_res2,     in_res3,      warranty_months, category_id,     producer_id,        model_id,    serie_id,  price,       trade_price1, trade_price2,       trade_price3,       name,     name_lat,  description_short, description, available,                                                                                                        is_delete, status, pay_count, warranty, is_published, ya_market, _is_problem)
SELECT                      r.`article`, vp.in_stock1, vp.in_stock2, vp.in_stock3, vp.in_res1, vp.in_res2, vp.in_res3, 0,                        r.`category_id`, r.`producer_id`, r.`model_id`, r.`serie_id`, vpp.price,0,                 vpp.trade_price2, vpp.trade_price3, r.`name`, r.`slug`,      vpr.s_desc,          vpr.desc,    ((vp.in_stock1+vp.in_stock2+vp.in_stock3)- (vp.in_res1+vp.in_res2+vp.in_res3)), 0,           1,         0,               6,          3,                 1,               0     
FROM `result` r
LEFT JOIN v_products_ru_ru vpr ON r.id=vpr.id
LEFT JOIN v_products vp ON r.id=vp.id
LEFT JOIN v_product_prices vpp ON r.id=vpp.id
LEFT JOIN product p ON r.slug=p.name_lat
WHERE r.model_id IS NOT NULL AND p.article IS NULL

-- заповняєм логотипами
UPDATE product p
LEFT JOIN (SELECT p2.article, p2.logo FROM product p2 WHERE p2.logo !='' GROUP BY article) p3 ON p3.article=p.article
SET p.logo = p3.logo
WHERE `is_published` = 3

-- добавляєм недостающі зображення для карточки товару
INSERT INTO image (product_id, article,    meta,    url,    thumb,    title,     description,    is_delete) 
SELECT                    p.id,          p.article, i2.meta, i2.url, i2.thumb, i2.title, i2.description,i2.is_delete
FROM `product` p
LEFT JOIN image i ON i.product_id=p.id
INNER JOIN (SELECT * FROM image GROUP BY article, url) i2 ON i2.article=p.article
WHERE i.article IS NULL AND p.`is_published` = 3



SELECT p.`id`, p.`article`, p.`name`, p.`name_lat`, '#', r.*
FROM `product` p
RIGHT JOIN result r ON 
(r.slug = p.name_lat OR p.original_name_lat RLIKE CONCAT('^',r.slug,'$|^',r.slug,',|,',r.slug,',|,',r.slug,'$'))
-- r.article=p.article AND r.slug = p.name_lat
WHERE p.id IS NULL
LIMIT 200

-- обновляет сортировку товаров
SET @i:= 1;
SET @article := '';
SET @category := 1;
UPDATE product p 
LEFT JOIN (
    SELECT `id`, `article`, `name`, @article,
    CASE 
        WHEN @article NOT LIKE article
        THEN  @i:= 1
        ELSE  @i:=@i+1
    END AS i, @article := article
    FROM product 
    WHERE category_id IS NOT NULL AND category_id = @category
    ORDER BY article
)  p2 ON p.id = p2.id
SET p.sort = p2.i
WHERE category_id IS NOT NULL AND category_id = @category


SET @i:= 1;
SET @article := '';
SELECT `id`, `article`, `name`, @article,
CASE 
    WHEN @article NOT LIKE article
    THEN  @i:= 1
    ELSE  @i:=@i+1
END AS i, @article := article AS _article
FROM product 
WHERE category_id IS NOT NULL AND category_id = 12
ORDER BY article  




SET @article := '040050';
SET @product_id := (SELECT product_id FROM image WHERE `article` LIKE @article ORDER BY  product_id ASC LIMIT 1);

-- видаляэмо зображення неіснуючих товарів
DELETE i FROM image i
LEFT JOIN product p ON p.id= i.product_id
WHERE i.article = @article AND p.id IS NULL;

-- видаляемо неактуальні зображення 
DELETE i FROM image i
LEFT JOIN product p ON i.product_id = p.id
LEFT JOIN (SELECT id, url FROM image WHERE `product_id`= @product_id ) i2 ON i2.url=i.url
WHERE  p.article = @article AND i2.url IS NULL;

-- добавляємо нові зображення
INSERT INTO image (product_id, article, meta, url, thumb, title, description, is_delete)
SELECT p.id AS product_id, i2.*
FROM product p 
LEFT JOIN (SELECT article, meta, url, thumb, title, description, is_delete  FROM image WHERE `product_id`= @product_id ) i2 ON i2.article = p.article
LEFT JOIN  `image` i ON i.product_id = p.id AND i2.url = i.url
WHERE p.article = @article AND i.id IS NULL;



SELECT p.`producer_id`, _m_name
-- SELECT p.`id`, p.`article`, p.`category_id`, p.`producer_id`, p.`model_id`, p.`serie_id`, p.`name`, _m_name, '#', m.*
FROM `product` p
LEFT JOIN model m ON p.producer_id = m.producer_id AND m.name = p._m_name
-- SET p.model_id = m.id, p.serie_id = m.serie_id
WHERE p._is_problem  = 7 
AND p.category_id IS NOT NULL 
AND p.producer_id IS NOT NULL 
AND (p.model_id IS NULL OR p.serie_id IS NULL) 
AND m.name IS NULL
GROUP BY p._m_name
-- LIMIT 2000


SELECT p.`article`, c.name, pr.name, m.name, p.`serie_id`, p.`name_lat`  
FROM `product` p
INNER JOIN category c ON c.id = p.category_id
LEFT JOIN (
    SELECT p3.article, IF( m.is_pn =0, m.name,  '' ) AS _model, IF( m.is_pn =1, m.name,  '' ) AS _pn
    FROM  `product` p2
    INNER JOIN model m ON m.id = p.model_id
    WHERE p2.article = article
    ORDER BY m.is_pn
)  p3 ON p3.article = p.article

INNER JOIN model m ON m.id= p.model_id
INNER JOIN producer pr ON pr.id= p.producer_id


-- шукає в назвах моделей партномера
UPDATE model
-- SELECT `id`, `producer_id`, `name`, `is_pn` 
-- FROM `model`
SET is_pn = 1
WHERE name RLIKE '^[A-Z0-9.-]{4,}[A-Z]+$'
AND is_pn = 0
AND id NOT IN (37437, 23415, 23416, 32877)
-- ORDER BY name ASC

SELECT `mt`.*, `s`.`id` AS `serie_id` 
FROM `model_true` `mt` 
LEFT JOIN (
    SELECT id, producer_id, `name` 
    FROM serie WHERE `name` != 'p/n' 
    ORDER BY `name` DESC) s ON `s`.`producer_id`=mt.producer_id AND mt.model RLIKE CONCAT('^',s.`name`) 
WHERE ((`mt`.`is_pn`=0) AND (s.id IS NOT NULL)) AND (mt.serie_id IS NULL) 
GROUP BY `mt`.`id` 
ORDER BY `mt`.`id` 
LIMIT 100


-- Заповнення нової таблиці з правильної таблиці моделей
INSERT INTO product2 (article, producer_id, serie_id, m_f_id, `name`)
SELECT article, producer_id, serie_id, id, model
FROM model_true
GROUP BY CONCAT(article, producer_id, serie_id, model);

-- заповнення назви та категорії
UPDATE product2 p2
LEFT JOIN producer pr ON pr.id = p2.producer_id
SET p2.category_id = 
CASE 
    WHEN p2.article RLIKE '^01[0-9]{4}' THEN 1 -- 'клавиатуры'
    WHEN p2.article RLIKE '^02[0-9]{4}' THEN 2 -- 'вентиляторы'
    WHEN p2.article RLIKE '^03[0-9]{4}' THEN 4 -- 'блоки питания'
    WHEN p2.article RLIKE '^04[0-9]{4}' THEN 3 -- 'аккумуляторы'
    WHEN p2.article RLIKE '^05[0-9]{4}' THEN 5 -- 'Петли'
    WHEN p2.article RLIKE '^06[0-9]{4}' THEN 6 -- 'Шлейф матрицы '
    WHEN p2.article RLIKE '^07[0-9]{4}' THEN 9 -- 'Разъем питания'
    WHEN p2.article RLIKE '^08[0-9]{4}' THEN 10 -- 'Матрицы'
    WHEN p2.article RLIKE '^09[0-9]{4}' THEN 12 -- 'Расходные материалы'
END,
p2.`name` =
CASE 
    WHEN p2.article RLIKE '^01[0-9]{4}' THEN CONCAT('Клавиатура для ноутбука ', pr.name,' ', p2.`name` )
    WHEN p2.article RLIKE '^02[0-9]{4}' THEN CONCAT('Вентилятор (кулер) для ноутбука ', pr.name,' ', p2.`name` )
    WHEN p2.article RLIKE '^03[0-9]{4}' THEN CONCAT('Блок питания для ноутбука ', pr.name,' ', p2.`name` )
    WHEN p2.article RLIKE '^04[0-9]{4}' THEN CONCAT('Аккумулятор для ноутбука ', pr.name,' ', p2.`name` )
    WHEN p2.article RLIKE '^05[0-9]{4}' THEN CONCAT('Петли матриц для ноутбука ', pr.name,' ', p2.`name` )
    WHEN p2.article RLIKE '^06[0-9]{4}' THEN CONCAT('Шлейф матриц для ноутбука ', pr.name,' ', p2.`name` )
    WHEN p2.article RLIKE '^07[0-9]{4}' THEN CONCAT('Разъём питания для ноутбука ', pr.name,' ', p2.`name` )
    WHEN p2.article RLIKE '^08[0-9]{4}' THEN CONCAT('Матрица для ноутбука ', pr.name,' ', p2.`name` )
    WHEN p2.article RLIKE '^09[0-9]{4}' THEN CONCAT('Расходные материалы для ноутбука ', pr.name,' ', p2.`name` )
    ELSE CONCAT(pr.name,' ', p2.`name` )
END;

-- дозаповнює таблицю з товарами
UPDATE product2 p2
LEFT JOIN (
    SELECT `article`, `in_stock1`, `in_stock2`, `in_stock3`, `in_res1`, `in_res2`, `in_res3`, `trade_price`, `is_timed`, `delivery_date`, `warranty_months`, `price`, `trade_price1`, `trade_price2`, `trade_price3`, `logo`, `available`, `is_delete`, `status`, `pay_count`, `is_published`, `ya_market`, `_is_problem`
    FROM product
    GROUP BY article
) p ON p.article = p2.article
SET p2.`in_stock1` = p.in_stock1,  
p2.`in_stock2` = p.in_stock2,
p2.`in_stock3` = p.in_stock3,
p2.`in_res1` = p.in_res1,
p2.`in_res2` = p.in_res2,
p2.`in_res3` = p.in_res3,
p2.`trade_price` = p.trade_price,
p2.`is_timed` = p.is_timed,
p2.`delivery_date` = p.delivery_date,
p2.`warranty_months` = p.warranty_months,
p2.`price` = p.price,
p2.`trade_price1` = p.trade_price1,
p2.`trade_price2` = p.trade_price2,
p2.`trade_price3` = p.trade_price3,
p2.`logo` = p.logo,
p2.`available` = p.available,
p2.`is_delete` = p.is_delete,
p2.`status` = p.status,
p2.`pay_count` = p.pay_count,
p2.`is_published` = p.is_published,
p2.`ya_market` = p.ya_market,
p2.`_is_problem` = p._is_problem

-- заповнюємо моделями
UPDATE product2 p2
LEFT JOIN model_true mt ON mt.id = p2.m_f_id
LEFT JOIN model m ON m.producer_id = p2.producer_id AND m.serie_id = p2.serie_id AND mt.model = m.name
SET p2.model_id =  m.id


-- заповнюємо ссилками
UPDATE product2 p2
LEFT JOIN
(
    SELECT article, producer_id, model_id, name_lat, original_name_lat, old_name_lat 
    FROM product 
    GROUP BY CONCAT(article, ' ', producer_id, ' ', model_id)
) p ON p.article = p2.article AND p.producer_id = p2.producer_id  AND p.model_id = p2.model_id
SET p2.name_lat = p.name_lat, p2.original_name_lat = p.original_name_lat, p2.old_name_lat = p.old_name_lat
WHERE p2.model_id IS NOT NULL
AND p.name_lat IS NOT NULL

-- Стрворення тригеру
-- при публікації товару добавляеться звязок моделі та артикулу і удаляе
DELIMITER |
DROP TRIGGER IF EXISTS DELADD_COMPATIBLE;
CREATE TRIGGER DELADD_COMPATIBLE AFTER UPDATE ON product
    FOR EACH ROW 
    BEGIN
        INSERT INTO model_compatible (article, model_id)
        SELECT p.article, p.model_id
        FROM product p
        LEFT JOIN model_compatible mc ON mc.model_id = p.model_id AND mc.article=p.article
        WHERE p.id = OLD.id 
            AND mc.model_id IS NULL 
            AND p.is_published = 1;

        DELETE mc.* FROM model_compatible mc
        LEFT JOIN  product p ON mc.model_id = p.model_id AND mc.article=p.article
        WHERE p.is_published = 0
            AND p.id = OLD.id;
    END;

-- Добавляе за замовчуванням значення фільтрів в артикул
INSERT INTO product_filter_value (article, category_id, producer_id, serie_id, filter_id, filter_value_id)
SELECT p.article, p.category_id, p.producer_id, p.serie_id, 59 AS filter_id, 315 AS filter_value_id
FROM product p
WHERE p.category_id = 6
AND p.serie_id IS NOT NULL
GROUP BY p.article
