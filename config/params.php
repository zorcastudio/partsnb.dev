<?php

return [
    'adminEmail' => 'sales@partsnb.ru',
    'dateFormat' => 'd.m.Y',
    'public_path' => 'www',
    'scheme' => 'https' // for https => 'https', for http => true
];
