<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'sourceLanguage'=>'ru',
    'language' => 'ru',
    'defaultRoute'=>"page",
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
        'debug' => [
            'class' => 'yii\debug\Module',
            //'allowedIPs' => ['176.241.148.235'] //178.94.28.182
        ],
//        'treemanager' =>  [
//            'class' => '\kartik\tree\Module',
//        ]
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'partsnb',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
//            'class' => 'yii\caching\MemCache',
//            'servers' => [
//                [
//                    'host' => '109.120.155.163:80',
//                    'port' => 11211,
//                    'weight' => 100,
//                ],
//            ],
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'sypexGeo' => [
            'class' => 'omnilight\sypexgeo\SypexGeo',
            'database' => '@app/data/SxGeoCity.dat',
        ],
        'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                'suffix'=>'',
            'rules' => [
                            'site'=>'site',
                            'site/<action>'=>'site/<action>',
                            '/sync/<action>'=>'/sync/<action>',
                            '/cron/start/synchronization'=> '/ajax/syncing2',
                            
                            '/yandexmarket/<action>'=>'/yandexmarket/<action>',
                            '/yandexmarket/file'=>'/yandexmarket/file',
                            '/yandexmarket.yml'=>'/yandexmarket.yml',
                
                            '/avito/file'=>'/avito/file',
                            '/avito/get'=>'/avito/get',
                            '/avito.yml'=>'/avito.yml',

                            '/others/file'=>'/others/file',
                            '/others/get'=>'/others/get',
                            '/others840.yml'=>'/others840.yml',
                            
                            '/googlemarket/<action>'=>'/googlemarket/<action>',
                            '/googlemarket/file'=>'/googlemarket/file',
                            '/products_GM.xml'=>'/products_GM.xml',
                
                            '/sitemap.xml'=> '/sitemap.xml',
                            '/sitemap'=> '/sitemap/index',
                            '/sitemap/<action>'=>'/sitemap/<action>',
                            '/sync'=> '/sync',
                
                            'new'=>'new',
                            'new/<action>'=>'new/<action>',
                            'import'=>'/import',
                            '/test/<action>' => '/test/<action>',
                            '/product/test' => '/product/test',
                            '/imgUpload' => '/imgUpload',
                
                            '/ajax/<action:\w+>' => '/ajax/<action>',
                            '/ajaxx/<action:\w+>' => '/ajaxx/<action>',
                            '/basket' => '/basket/index',
                            '/basket/<action>' => '/basket/<action>',
                            '/login' => '/user/login',
                            '/logout' => '/user/logout',
                            'user' => '/user/index',
                            '/registration' => '/user/registration',
                
                            'search/<search:.*>' => "/product/search",
                            'search/<temp>/<search>' => "/product/search",
                            
                            '/site'=>'site',
                            '/site/<action\w+>'=>'/site/<action>',
                            '/order'=>'/order',
                            '/order/<action:\w+>'=>'/order/<action>',
                
                            '/basket'=>'/order',
                            '/basket/<action:\w+>'=>'/order/<action>',

                            '/ymsuccess-test'=>'/order/kassaok',
                            '/ymfail-test'=>'/order/kassafail',

                            '/ymsuccess'=>'/order/kassaok',
                            '/ymfail'=>'/order/kassafail',
                
                            '/admin'=> '/admin',
                            '/admin/user/<action>'=>'/admin/user/<action>',
                            '/admin/<controller:\w+>'=>'/admin/<controller>',
                            '/admin/<controller>/<action>'=>'/admin/<controller>/<action>',
                            '/admin/logs/downloadfile/<file>'=>'/admin/logs/downloadfile',
                
                            '/model'=>'/model',
                            '/model/<action>'=>'/model/<action>',

                            [
                                'pattern' => '/product/<action>',
                                'route' => '/product/<action>',
                                'suffix' => ''
                            ],
                            //asus/ноутбук/a_series/клавиатура/
                            //'<category>/<producer>/<serie>' => '/product/serie',
                            [
                                'pattern' => '/<producer>/notebook/<serie>/<category>',
                                'route' => '/product/serie',
                                'suffix' => ''
                            ],

                            //'/product/<action>'=>'/product/<action>',
                            '/product/producer'=>'/product/producer',
                            '/product/model'=>'/product/model',
                            '/product/problem'=>'/product/problem',
                            '/problem/<action>'=>'/problem/<action>',
                            '/product/test'=>'/product/test',
                
                            '/user/basket'=>'/user/order',
                            '/user/<action>'=>'/user/<action>',
                
                            '/user/basket/<id\d+>'=>'/user/order_view',
                            '/user/order/<id\d+>'=>'/user/order_view',
//                            '/page/<url>' => '/page/view',
                    
                            '/novosti' => '/novosti/index',
                            '/novosti/<url>' => '/novosti/view',
                            //'/o-magazine' => '/page/view',
                            '<action:o-magazine|kontakty|informatsiya|dostavka-i-oplata|payment-thanx|payment-error>' => '/page/view',
                            '/kontakty/<url>' => '/page/view',
                            //'/kontakty' => '/page/view',
                            //'/informatsiya' => '/page/view',
                            //'/dostavka-i-oplata' => '/page/view',
                            '/informatsiya/<view>' =>'/page/view',
                            '/postoyannym-klientam' =>'/page/view',
                            '/posting/<view>' =>'/posting/<view>',

                            /*[
                                'pattern' => '/brands',
                                'route' => '/product/brands',
                                'suffix' => '/'
                            ],*/
                            '/brands' => '/product/params_search',
                            '/brands/<producer>' => '/product/params_search',
                            '/brands/<producer>/<serie>' => '/product/params_search',
                            '/brands/<producer>/<serie>/<model:.*>' => '/product/params_search',
                            // '/brands/<brand>' => '/product/categories-by-brand',
                            // '/brands/<brand>/<category>' => '/product/series-by-brand-category',
                            // '/brands/<brand>/<category>/<series>' => '/product/models-by-params',
                            // '/brands/<brand>/<category>/<series>/<model:.*>' => '/product/model_search',

                            '<category>' => '/product/index',
                            '<category>/<producer>' => '/product/producer',
                
                            '<category>/<producer>/filter/<filter:.+>' => '/product/producer',
                            '<category>/<producer>/<serie:\w+\/$>' => '/product/serie',
                            '<category>/<producer>/<serie>' => '/product/serie',
                            '<category>/<producer>/<serie>/filter/<filter:.+>' => '/product/serie',
                            '<category>/<producer>/<serie:\w+\/model\/$>' => '/product/model_serch',
                            '<category>/<producer>/<serie:\w+\/model$>' => '/product/model_serch',
                            '<category>/<producer>/<serie>/model/<model:.*>' => '/product/model_serch',
                
                            '<category>/<producer>/<serie>/<name>' => '/product/view',
                            [
                                'pattern' => '<category>/<producer>/<serie>/<name>',
                                'route' => '/product/view',
                                'suffix' => ''
                            ],
                            
                            '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                            '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                            
                            '<action>' => '/', '/category/index/url/<action>',
                            '<category>/<action>' => '/product/view/url/<action>',
                            /*[
                                'pattern' => '<category>/<action>',
                                'route' => '/product/view/url/<action>/',
                                'suffix' => '/'
                            ],*/
//                            
            ],
        ],
        'errorHandler' => [
            'errorAction' => '/site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    /*
        $config['bootstrap'][] = 'debug';
        $config['modules']['debug'] = 'yii\debug\Module';
        $config['bootstrap'][] = 'gii';
        $config['modules']['gii'] = [
            'class' => 'yii\gii\Module',
            'allowedIPs' => ['92.112.*.*', "178.94.*.*"],
        ];
    */
}
return $config;
