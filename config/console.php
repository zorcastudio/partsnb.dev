<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'name' => 'partsNB Console Application',
    //'preload' => ['log'],
    'bootstrap' => ['log', 'gii'],
    /*'import' => [
        'application.models.*',
        'application.components.*',
        'application.components.Api.*',
        'application.components.Geo.*',
        'application.components.Language.*',
        'application.components.Manager.*',
        'application.modules.user.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.adminny.models.*',
        'application.modules.adminny.components.*',
        'application.modules.api.models.*',
        'application.modules.ulogin.models.*',
        'application.modules.api.components.*',
        'application.modules.merchant.*',
        'application.modules.merchant.models.*',
        'application.modules.operator.*',
        'application.modules.operator.models.*',
        'application.modules.operator.components.*',
        'ext.eoauth.*',
        'ext.eoauth.lib.*',
        'ext.lightopenid.*',
        'ext.eauth.*',
        'ext.eauth.services.*',
    ],*/
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            /*'class' => 'CLogRouter',
            'routes' => [
                [
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ],
            ],*/
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'params' => $params,
];




// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
//return array(
//    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
//);
