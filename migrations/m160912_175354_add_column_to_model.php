<?php

use yii\db\Schema;
use yii\db\Migration;

class m160912_175354_add_column_to_model extends Migration
{
    public function up()
    {
        $this->addColumn('model', 'description', 'text');
    }

    public function down()
    {
        $this->dropColumn('model', 'description');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
