<?php

use yii\db\Schema;
use yii\db\Migration;

class m160921_101318_change_column_default_in_order extends Migration
{
    public function up()
    {
        $this->alterColumn('order', 'is_delete', 'integer NOT NULL DEFAULT 0');
    }

    public function down()
    {
        echo "m160921_101318_change_column_default_in_order cannot be reverted.\n";

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
