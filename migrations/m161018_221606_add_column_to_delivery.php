<?php

use yii\db\Schema;
use yii\db\Migration;

class m161018_221606_add_column_to_delivery extends Migration
{
    public function up()
    {
        $this->addColumn('delivery', 'email_info', 'text');
    }

    public function down()
    {
        $this->dropColumn('delivery', 'email_info');

        return false;
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
