<?php

use yii\db\Schema;
use yii\db\Migration;

class m160712_214823_add_column_to_model extends Migration
{
    public function up()
    {
        $this->addColumn('model', 'name_lat', 'string not null');
    }

    public function down()
    {
        $this->dropColumn('model', 'name_lat');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
