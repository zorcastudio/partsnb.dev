<?php

use yii\db\Schema;
use yii\db\Migration;

class m160912_175341_add_column_to_serie extends Migration
{
    public function up()
    {
        $this->addColumn('serie', 'description', 'text');
    }

    public function down()
    {
        $this->dropColumn('serie', 'description');
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
