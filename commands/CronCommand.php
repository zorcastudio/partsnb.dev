<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CronCommand
 *
 * @author Sergiy
 */
class CronCommand extends CConsoleCommand {

    public function actionSyncing() {
        ignore_user_abort(true);
        session_write_close();
        set_time_limit(0);
        
        $model = \app\components\sinc\Import::syncXML();
        echo $model ? json_encode($model) : -1;
    }
}
