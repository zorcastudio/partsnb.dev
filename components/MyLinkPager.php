<?php
namespace app\components;
use yii\helpers\Html;
use yii\helpers\Url;
use Yii;


class MyLinkPager extends \yii\widgets\LinkPager 
{

    public function __construct($owner = null) {
        parent::__construct($owner);
        $this->options['id'] = 'pagination-links';
    }

    protected function renderPageButton($label, $page, $class, $disabled, $active)
    {
        $options = ['class' => $class === '' ? null : $class];
        if ($active) {
            Html::addCssClass($options, $this->activePageCssClass);
        }
        if ($disabled) {
            Html::addCssClass($options, $this->disabledPageCssClass);

            return Html::tag('li', Html::tag('span', $label), $options);
        }
        $linkOptions = $this->linkOptions;
        $linkOptions['data-page'] = $page;
        $linkOptions['data-url'] = $this->getUrl(++$page);
        $linkOptions['class'] = 'hidden-link';
        return Html::tag('li', Html::tag('span', $label, $linkOptions), $options);
    }
    
    public function getUrl($page){
        $category = Yii::$app->request->get('category');
        $producer = Yii::$app->request->get('producer');
        $model = Yii::$app->request->get('model');
        $serie = Yii::$app->request->get('serie');
        $filter = Yii::$app->request->get('filter');
        
        $url = $category ? "/".$category : null;
        $url .= $producer ? "/".$producer : null;
        $url .= $serie ? "/".$serie : null;
        $url .= $model ? "/model/".$model : null;
        $url .= $filter ? "/filter/".$filter : null;
        $url .= $page ? "?page=$page" : null;
        
//        if($page){
//            $url .= $filter ? "&filter=$filter" : null;
//        }
        
        return $url;
    }
}
