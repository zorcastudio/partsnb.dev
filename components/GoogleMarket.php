<?php
namespace app\components;
use Yii;
use yii\helpers\Url;

class GoogleMarket {

    public static $exclude_categories = '10';

    public static function getXML($log) {
        $scheme = Yii::$app->params['scheme'];
        $product = Yii::$app->db->createCommand("
            SELECT p.id,
            p.name AS title,                
            GROUP_CONCAT(DISTINCT  CONCAT(f.name,': ', fv.name) SEPARATOR ', ')  AS description,
            CONCAT('/', c.name_lat, '/', pr.name_lat, '/', s.name_lat, '/', p.name_lat) AS link,
            CONCAT('/uploadfile/images/product/',i.url) AS image_link,
            'new' AS `condition`,
            'in stock' AS availability,
            p.price,
            'RU' AS country,
            'Standard' AS service,
            '200 RUB' AS s_price,
            pr.name AS brand,
            'FALSE' AS identifier_exists,
            CASE 
                WHEN p.category_id =1 THEN 4301
                WHEN p.category_id IN (2,3,4,6,9) THEN 4224
                WHEN p.category_id = 5 THEN 6416
                WHEN p.category_id = 10 THEN 4102
                WHEN p.category_id = 23 THEN 7347
            ELSE 4224 END AS google_product_category,
            CASE 
                WHEN p.category_id IN (1,5)         THEN 'Электроника &gt; Принадлежности для электроники &gt; Компьютерные компоненты &gt; Запчасти для ноутбуков &gt; Петли для ноутбуков'
                WHEN p.category_id IN (2,3,4,6,9)   THEN 'Электроника &gt; Принадлежности для электроники &gt; Компьютерные компоненты &gt; Запчасти для ноутбуков'
                WHEN p.category_id = 10             THEN 'Электроника &gt;Принадлежности для электроники &gt; Компьютерные компоненты &gt; Запчасти для ноутбуков &gt; Сменные экраны для ноутбуков'
                WHEN p.category_id = 23             THEN 'Электроника &gt; Связь &gt; Телефония &gt; Принадлежности для мобильных телефонов &gt; Запасные части для мобильных телефонов'
                ELSE 'Электроника &gt; Принадлежности для электроники &gt; Компьютерные компоненты &gt; Запчасти для ноутбуков'
            END AS product_type,
            (in_stock1+in_stock2+in_stock3) - (in_res1 + in_res2 + in_res3)  AS available

            FROM `product` p
            
            INNER JOIN category c ON p.category_id = c.id
            INNER JOIN producer pr ON p.producer_id = pr.id
            INNER JOIN serie s ON p.serie_id = s.id
            INNER JOIN model m ON p.model_id = m.id
            LEFT JOIN image i ON p.id = i.product_id
            LEFT JOIN product_filter_value pfv ON p.article = pfv.article
            LEFT JOIN filter f ON pfv.filter_id = f.id
            LEFT JOIN filter_value fv ON pfv.filter_value_id = fv.filter_value_id

            WHERE p.is_delete =0
            AND p.status = 1
            AND p.is_published = 1
            AND p._is_problem = 0
            AND p.googl_market = 1
            AND p.article IS NOT NULL
            AND p.category_id IS NOT NULL
            AND p.producer_id IS NOT NULL
            AND p.model_id IS NOT NULL
            AND p.serie_id IS NOT NULL
            AND p.name_lat IS NOT NULL
            GROUP BY p.id
            HAVING available > 0
        ")->queryAll();
        
        $xmlstr = '<?xml version="1.0" encoding="utf-8"?><rss xmlns:g="http://base.google.com/ns/1.0" version="2.0"/>';
        $dom = new \DOMDocument("1.0", "utf-8"); // Создаём XML-документ версии 1.0 с кодировкой utf-8
        
        $rss = $dom->createElement("rss");
        $rss->setAttribute('xmlns:g', "http://base.google.com/ns/1.0");
        $dom->appendChild($rss);
        $channel = $rss->appendChild($dom->createElement("channel"));
        $channel->appendChild($dom->createElement("title", 'сайт PartsNB.ru'));
        $channel->appendChild($dom->createElement("link", 'https://partsnb.ru/'));
        $channel->appendChild($dom->createElement("description", 'Feed - Запчасти для ноутбуков PARTSNB.RU'));
        
        $i = 0;
        foreach ($product as $key => $value) {
            $item = $dom->createElement("item");
            $channel->appendChild($item);
            $item->appendChild($dom->createElement("g:id", $value['id']));
            $item->appendChild($dom->createElement("g:title", $value['title']));
            $item->appendChild($dom->createElement('g:description', $value['description']));
            $item->appendChild($dom->createElement('g:link',  Url::to($value['link'], $scheme)));
            $item->appendChild($dom->createElement('g:image_link', Url::to($value['image_link'], $scheme)));
            $item->appendChild($dom->createElement('g:condition', $value['condition']));
            $item->appendChild($dom->createElement('g:availability', $value['availability']));
            $item->appendChild($dom->createElement('g:price', "{$value['price']} RUB'"));
            
            $shipping = $dom->createElement('g:shipping');
            $item->appendChild($shipping);
            
            $shipping->appendChild($dom->createElement('g:country', $value['country']));
            $shipping->appendChild($dom->createElement('g:service', $value['service']));
            $shipping->appendChild($dom->createElement('g:price', $value['s_price'])) ;
            
            $item->appendChild($dom->createElement('g:brand', $value['brand']));
            $item->appendChild($dom->createElement('g:identifier_exists', $value['identifier_exists']));
            $item->appendChild($dom->createElement('g:google_product_category', $value['google_product_category']));
            $item->appendChild($dom->createElement('g:product_type', $value['product_type']));
            
            $i++;
        }
      
        $log->count = $i;
        $log->not_error = 1;
        return $dom;
    }
    
    
    /**
     * генерація файла
     */
    public static function saveFile(){
        ini_set("memory_limit","2000M");
        ignore_user_abort(true);
        set_time_limit(0);
        $public_path = Yii::$app->params['public_path'];
        $log = new \app\modules\admin\models\LogsSearch();
        $log->createtime = time();
        
        $XML = self::getXML($log);
        
        $XML->save(Yii::$app->basePath . "/$public_path/products_GM.xml");
        $XML->save(Yii::$app->basePath . "/$public_path/history/products_GM_".date("Y_m_d_H_i_s", $log->createtime).".xml");

        $log->type = 1;
        $log->finishtime = time();
        $log->save();
    }
   
}
