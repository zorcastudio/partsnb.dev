<?php

namespace app\components;

use yii\helpers\Url;
use app\models\Page;
use app\models\Category;
use app\models\Producer;
use app\models\Product;
use Yii;

class Sitemap{

    public static function getAllUrls() {
        $urls = [];
        $scheme = Yii::$app->params['scheme'];
        // общие страницы сайта
        $urls[] = [
            'loc' => Url::to(['/'],$scheme),
            'lastmod' => date('Y-m-d'),
            'changefreq' => 'weekly',
            'priority' => '1',
        ];
        
        //ВСЕ СТРАНИЦЫ
        foreach (Page::getUrlList() as $value) {
            $urls[] = [
                'loc' => Url::to(['/page/'.$value], $scheme),
                'lastmod' => date('Y-m-d'),
                'changefreq' => 'weekly',
                'priority' => '0.8',
            ];
        }
        
        //ВСЕ категории + производители + товары
        foreach (Category::getCategoryListUrl() as $cat => $category) {
            $urls[] = [
                'loc' => Url::to(["/".$category], $scheme),
                'lastmod' => date('Y-m-d'),
                'changefreq' => 'weekly',
                'priority' => '0.7',
            ];
            
            //ВСЕ производители + товары
            foreach (Producer::getProducerListUrl() as $pr => $producer) {
                $urls[] = [
                    'loc' => Url::to(["/".$category.'/'.$producer['slug']], $scheme),
                    'lastmod' => date('Y-m-d'),
                    'changefreq' => 'weekly',
                    'priority' => '0.6',
                ];

                //товары по категориям и производителям
                foreach (Product::getProduktListUrl($cat, $pr) as $product) {
                    $urls[] = [
                        'loc' => Url::to(["/" . $category . '/' . $producer."/".$product], $scheme),
                        'lastmod' => date('Y-m-d'),
                        'changefreq' => 'weekly',
                        'priority' => '0.5',
                    ];
                }
            }
        }
        return $urls;
    }
    
    
    public static function getXML($log = false){

        $scheme = Yii::$app->params['scheme'];
        $dom = new \DOMDocument("1.0", "utf-8"); // Создаём XML-документ версии 1.0 с кодировкой utf-8
        $rss = $dom->createElement("urlset");
        $rss->setAttribute('xmlns', "http://www.sitemaps.org/schemas/sitemap/0.9");
//        $rss->setAttribute('xmlns', "xmlns:xhtml='http://www.w3.org/1999/xhtml'");

        $dom->appendChild($rss);
        
        
        $url = $dom->createElement("url");
        $rss->appendChild($url);
        $url->appendChild($dom->createElement("loc", Url::to(['/'],$scheme)));
        $url->appendChild($dom->createElement("changefreq", 'weekly'));
        $url->appendChild($dom->createElement("priority", '1'));
        
        //ВСЕ СТРАНИЦЫ
        $page = Yii::$app->db->createCommand("
                    SELECT 
                    CASE 
                        WHEN p.parent_id IS NOT NULL THEN 
                            CONCAT('/', (SELECT name_lat FROM page WHERE id = p.parent_id),'/', p.name_lat)
                        ELSE 
                            CONCAT('/', p.name_lat)
                        END  AS url
                    FROM `page` p
                    WHERE p.is_delete = 0 AND name_lat != ''
                ")->queryAll();
        foreach ($page as $value) {
            $url = $dom->createElement("url");
            $rss->appendChild($url);
            $url->appendChild($dom->createElement("loc", Url::to([$value['url']], $scheme)));
            $url->appendChild($dom->createElement("changefreq", 'weekly'));
            $url->appendChild($dom->createElement("priority", '0.5'));
        }
        
        $list = Yii::$app->db->createCommand("
                    SELECT p.`id`,
                    CASE 
                       WHEN p.update != 0 THEN  p.update
                       WHEN p.createtime != 0 THEN p.createtime
                       ELSE unix_timestamp(now()) END  AS date,
                    CONCAT('/', c.name_lat) AS category,
                    CONCAT('/', c.name_lat, '/', pr.name_lat, '/', s.name_lat, '/', p.name_lat) AS url 
                    FROM `product` p
                    INNER JOIN category c ON p.category_id = c.id
                    INNER JOIN producer pr ON p.producer_id = pr.id
                    INNER JOIN serie s ON p.serie_id = s.id
                    WHERE p.is_delete =0
                    AND p.status = 1
                    AND p.is_published = 1
                    AND p._is_problem = 0
                    AND p.article IS NOT NULL
                    AND p.category_id IS NOT NULL
                    AND p.producer_id IS NOT NULL
                    AND p.model_id IS NOT NULL
                    AND p.serie_id IS NOT NULL
                    AND p.name_lat IS NOT NULL
                    ORDER BY  `category` DESC
                    ")->queryAll();
        $category = [];
        foreach ($list as $key => $value) {
            if (!in_array($value['category'], $category)){
                $category[] = $value['category'];
                $url = $dom->createElement("url");
                $rss->appendChild($url);
                $url->appendChild($dom->createElement("loc", Url::to([$value['url']], $scheme)));
                $url->appendChild($dom->createElement("changefreq", 'weekly'));
                $url->appendChild($dom->createElement("priority", '0.5'));
            }
            
            $url = $dom->createElement("url");
            $rss->appendChild($url);
            $url->appendChild($dom->createElement("loc", Url::to([$value['url']], $scheme)));
            $url->appendChild($dom->createElement("changefreq", 'weekly'));
            $url->appendChild($dom->createElement("lastmod", Product::getUpdateDate($value['date'])));
            $url->appendChild($dom->createElement("priority", '0.5'));
        }
        $log->count = count($dom);
        return $dom;
    }
    
     /**
     * генерація файла
     */
    public static function saveFile(){
        ini_set("memory_limit","2000M");
        ignore_user_abort(true);
        set_time_limit(0);
        
        $log = new \app\modules\admin\models\LogsSearch();
        $log->createtime = time();
        
        $XML = self::getXML($log);
        $public_path = Yii::$app->params['public_path'];
        $XML->save(Yii::$app->basePath . "/$public_path/history/sitemap_".date("Y_m_d_H_i_s", $log->createtime).".xml");
        $XML->save(Yii::$app->basePath . "/$public_path/sitemap.xml");

        $log->type = 2;
        $log->not_error = 1;
        $log->finishtime = time();
        $log->save();
    }

}
