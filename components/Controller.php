<?php

namespace app\components;

use app\models\User;
use Yii;
use app\models\Product;
use app\models\Category;
use yii\helpers\Url;

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends \yii\web\Controller {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $specific_data;

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = [];

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = [];
    
    public $description = 'description';
    public $keywords = 'keywords';
    
    public function __construct($id, $module = null) {
        date_default_timezone_set('Europe/Moscow');
        $this->layout = 'colum2';
        parent::__construct($id, $module);

        if(preg_match('/(.+)\/model$/', Yii::$app->request->url, $subject)){
            $this->redirect(Url::to($subject[1]), 301)->send();
        }
        
        if(preg_match('/(.+)\/$/', Yii::$app->request->url, $subject)){
            $url = trim(preg_replace("/model/iu", '', $subject[1]));
            //$this->redirect(Url::to($url), 301)->send();
        }
        
        $this->specific_data = self::getSpecificData();

        //перенапрвляем на нужную страницу нашего сайта со старых ссыок
        $url = Url::to();
        $serch = preg_match("/^(.*)-detail/u", $url, $matches);//определяем не старая ли ссылка
        if($serch){
            $url = $matches[1];
            $serch = preg_match("/\/.*\/(.*)$/u", $url, $matches);//определяем не старая ли ссылка
            if($serch){
                $product = Product::find()
                        ->select('id, category_id, producer_id, model_id, name_lat, serie_id')
                        ->where(['name_lat'=>$matches[1]])
                        ->orWhere(['RLIKE', 'original_name_lat', "^$matches[1]$|^$matches[1],|,$matches[1],|,$matches[1]$"])
                        ->one();
                //перенапрвляем на нужную страницу нашего сайта
                if($product)
                    $this->redirect($product->getUrl(), 301)->send();
                else
                    $this->redirect('/category/product/serie/'.$matches[1], FALSE);
            }
        }
    }


    public function getLayout() {
        return null;
    }

    protected function getBlocks() {
        return ['header', 'footer', 'left_sidebar', 'head','header_admin', 'basket'];
    }

    protected static function getSpecificData() {
        return array(
            'summ_prise'=> Product::getSummPrise(),
//            'filter'=> Product::getFilterView(),
            'novosti'=> \app\models\Novosti::geShort(),
        );
    }
  
    /**
     * Send mail method
     */
    public static function sendMail($email, $subject, $message, $adminEmail = null) {
        $adminEmail = $adminEmail ?: Yii::$app->params['adminEmail'];
        
        $headers = "MIME-Version: 1.0\r\nFrom: $adminEmail\r\nReply-To: $adminEmail\r\nContent-Type: text/html; charset=utf-8";
        $message = wordwrap($message, 70);
        $message = str_replace("\n.", "\n..", $message);
        return mail($email,'=?UTF-8?B?'.base64_encode($subject).'?=',$message,$headers);
    }
    
    /**
     * Відправка листів через yandexMail
     * @param type $email
     * @param type $subject
     * @param type $message
     * @param type $adminEmail
     * @return type
     */
    public static function sentMailYandex($email, $subject, $message, $adminEmail = null){
        $adminEmail = $adminEmail ?: Yii::$app->params['adminEmail'];
          
        $mailSMTP  = new SendMailSmtpClass('sales@partsnb.ru', 'M9masd23fzf7eq', 'ssl://smtp.yandex.ru', 'Partsnb', $smtp_port = 465, $smtp_charset = "utf-8");
        $headers= "MIME-Version: 1.0\r\n";
        $headers .= "From: $adminEmail\r\nReply-To: $adminEmail\r\n"; // от кого письмо
        $headers .= "Content-type: text/html; charset=utf-8\r\n"; // кодировка письма

//        $headers .= "To: {$email}"; 
        return $mailSMTP->send($email, $subject, $message, $headers); // отправляем письмо
        // $result =  $mailSMTP->send('Кому письмо', 'Тема письма', 'Текст письма', 'Заголовки письма');
    }
}
