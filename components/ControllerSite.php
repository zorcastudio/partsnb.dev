<?php

namespace app\components;

use app\models\User;
use Yii;
use yii\helpers\VarDumper;
use app\models\Product;
use app\models\Category;

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class ControllerSite extends \yii\web\Controller {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $specific_data;

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = [];

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = [];
    
    public $description = 'description';
    public $keywords = 'keywords';
    
    public function __construct($id, $module = null) {
        $this->layout = 'partsnb.ru/colum2';

        parent::__construct($id, $module);
        $this->specific_data = self::getSpecificData();
    }


    public function getLayout() {
        return null;
    }

    protected function getBlocks() {
        return ['header', 'footer', 'left_sidebar', 'head','header_admin', 'basket'];
    }

    protected static function getSpecificData() {
        return array(
            'summ_prise'=> Product::getSummPrise(),
            'filter'=> Product::getFilterView(),
        );
    }
  
    /**
     * Send mail method
     */
    public static function sendMail($email, $subject, $message) {
        $adminEmail = Yii::$app->params['adminEmail'];
        $headers = "MIME-Version: 1.0\r\nFrom: $adminEmail\r\nReply-To: $adminEmail\r\nContent-Type: text/html; charset=utf-8";
        $message = wordwrap($message, 70);
        $message = str_replace("\n.", "\n..", $message);
        return mail($email,'=?UTF-8?B?'.base64_encode($subject).'?=',$message,$headers);
    }
}
