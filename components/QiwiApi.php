<?php
namespace app\components;

/**
 * Api
 *
 * @author Sergey Trofimenkov <sergey@trofimenkov.com>
 */
class QiwiApi {

    const API_URL = "http://78.47.135.110/api";
    const API_ID = 36695125;
    const API_PASSWORD = "YKDL0nyD0cBgk54UGdY1";

    public function __construct() {
        $this->url = self::API_URL;
        $this->api_id = self::API_ID;
        $this->api_secret = self::API_SECRET;

//        $this->client = $client;
//        $this->uphone = $uphone;
//        $this->upassword = $upassword;
//        $this->client_id = $client_id;
    }

    public static function setQiwi() {
        //API ID из вкладки "Данные магазина"
        //https://ishop.qiwi.com/options/rest.action
        $REST_ID = self::API_ID;
        //API пароль из вкладки "Данные магазина"
        //https://ishop.qiwi.com/options/rest.action
        $PWD = self::API_PASSWORD;
        //ID счета
        $BILL_ID = "99111-ABCD-1-2-1";
        $PHONE = "79191234567";
        $data = array(
            "user" => "tel:+" . $PHONE,
            "amount" => "1000.00",
            "ccy" => "RUB",
            "comment" => "Все очень хорошо",
            "lifetime" => "2015-01-30T15:35:00",
            "pay_source" => "qw",
            "prv_name" => "Хороший магазин"
        );
        
        $ch = curl_init();
        $ch = curl_init($ch, CURLOPT_URL, 'https://api.qiwi.com/api/v2/prv/' . $SHOP_ID . '/bills/' . $BILL_ID);
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $REST_ID . ":" . $PWD);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Accept: application/json"
        ));
        $results = curl_exec($ch) or die(curl_error($ch));
        echo $results;
        echo curl_error($ch);
        curl_close($ch);
        //Необязательный редирект пользователя
        $url = 'https://qiwi.com/order/external/main.action?shop=' . $SHOP_ID . '&
                transaction=' . $BILL_ID . '&successUrl=http%3A%2F%2Fieast.ru%2Findex.php%3Froute%3D
                payment%2Fqiwi%2Fsuccess&failUrl=http%3A%2F%2Fieast.ru%2Findex.php%3Froute%3D
                payment%2Fqiwi%2Ffail&qiwi_phone=' . $PHONE;
        echo '<br><br><b><a href="' . $url . '">Ссылка переадресации для оплаты счета</a></b>';
    }

    
    
    
    /**
     *  Get balance
     */
    public function getBalance($request_id, $return_url) {

        $xml = "<request>"
                . "<version>" . self::API_VERSION . "</version>"
                . "<api_id>$this->api_id</api_id>"
                . "<ip>" . $_SERVER['REMOTE_ADDR'] . "</ip>"
                . "<client_id>$this->client_id</client_id>"
                . "<phone>$this->uphone</phone>"
                . "<password>$this->upassword</password>"
                . "<request_id>$request_id</request_id>"
                . "<return_url>$return_url</return_url>"
                . "</request>";




        $params['sign'] = md5($this->api_secret . $xml . $this->api_secret);
        $params['xml'] = base64_encode($xml);

        $result = $this->client->post($this->url . "/get_balance", $params);


        $xml = simplexml_load_string($result);

        if (!in_array($this->client->getStatusCode(), array(200))) {
            throw new CException("API failed! Server returned: \n $result .");
        }
        if ((int) $xml->status != 1) {
            throw new CException("Game API failed! Server returned: \n $xml->error .");
        }
        return true;
    }

    /**
     *  send money
     */
    public function sendMoney($request_id, $return_url, $amount, $currency, $typeid, $data) {
        $xml = new SimpleXMLElement('<request/>');
        $xml->addChild('version', self::API_VERSION);
        $xml->addChild('api_id', $this->api_id);
        $xml->addChild('ip', $_SERVER['REMOTE_ADDR']);
        $xml->addChild('client_id', $this->client_id);
        $xml->addChild('phone', $this->uphone);
        $xml->addChild('password', $this->upassword);
        $xml->addChild('request_id', $request_id);
        $xml->addChild('return_url', $return_url);
        $xml->addChild('amount', $amount);
        $xml->addChild('currency', $currency);
        $xml->addChild('typeid', $typeid);

        $data_ = $xml->addChild('data');
        foreach ($data as $key => $param) {
            $data_->addChild($key, $param);
        }
        $xml = str_replace("<?xml version=\"1.0\"?>\n", '', $xml->asXML());
        $params['sign'] = md5($this->api_secret . $xml . $this->api_secret);
        $params['xml'] = base64_encode($xml);

        $result = $this->client->post($this->url . "/send_money", $params);

        $xml = simplexml_load_string($result);

        if (!in_array($this->client->getStatusCode(), array(200))) {
            throw new CException("API failed! Server returned: \n $result .");
        }
        if ((int) $xml->status != 1) {
            throw new CException("Game API failed! Server returned: \n $xml->error .");
        }

        return true;
    }

    /**
     *  delete token
     */
    public function deleteToken() {
        $xml = "<request>"
                . "<version>" . self::API_VERSION . "</version>"
                . "<api_id>$this->api_id</api_id>"
                . "<phone>$this->uphone</phone>"
                . "<password>$this->upassword</password>"
                . "</request>";

        $params['sign'] = md5($this->api_secret . $xml . $this->api_secret);
        $params['xml'] = base64_encode($xml);

        $result = $this->client->post($this->url . "/delete_token", $params);

        $xml = simplexml_load_string($result);

        if (!in_array($this->client->getStatusCode(), array(200))) {
            throw new CException("API failed! Server returned: \n $result .");
        }
        if ((int) $xml->status != 1) {
            throw new CException("Game API failed! Server returned: \n $xml->error .");
        }

        return true;
    }

//    public function getCurrency($code) {
//        if (!isset($this->code_currency[$code])) {
//            throw new ApiException("Currency not found: \n$code.");
//        }
//        return $this->code_currency[$code];
//    }
}
