<?php
namespace app\components;
use yii\helpers\Html;
use yii\helpers\Url;
use Yii;
use app\models\Category;
use app\models\Product;
use yii\helpers\ArrayHelper;


class YC {
    
    public static function getCSV() {
        ini_set("memory_limit","2000M");
        ignore_user_abort(true);
        set_time_limit(0);
        $scheme = Yii::$app->params['scheme'];
        
        $article = Yii::$app->db->createCommand("
            SELECT `article` FROM `product`
            WHERE article != ''
            GROUP BY article
        ")->queryAll();

        $public_path = Yii::$app->params['public_path'];
        $fp = fopen(Yii::$app->basePath . "/{$public_path}/productYC.csv", 'w');
        foreach ($article as $art) {
            Yii::$app->db->createCommand("
                SET group_concat_max_len = 40000; 
                SET @art := '{$art['article']}';
            ")->execute();
            $list = Yii::$app->db->createCommand("
                SELECT p.`article`, c.name AS c_name, 
                p3._model, p3._pn,
                pr.name AS pr_name,
                CONCAT('/', c.name_lat, '/', pr.name_lat, '/', s.name_lat, '/', p.name_lat) AS url,
                p.price

                FROM `product` p
                INNER JOIN category c ON c.id = p.category_id
                INNER JOIN model m ON m.id= p.model_id
                INNER JOIN producer pr ON pr.id= p.producer_id
                INNER JOIN serie s ON s.id= p.serie_id

                LEFT JOIN (
                   SELECT p2.article, 
                   GROUP_CONCAT(IF(m.is_pn =0,  m.name, '') SEPARATOR ';') AS _model,
                   GROUP_CONCAT(IF(m.is_pn =1,  m.name, '') SEPARATOR ';') AS _pn
                   FROM  `product` p2
                    INNER JOIN model m ON m.id = p2.model_id
                   WHERE p2.article = @art 
                )  p3 ON p3.article = @art 

                WHERE p.article = @art 
                GROUP BY p.article
                ")->queryAll();

           
            $result = [];
            foreach ($list as $k =>$value) {
                $pn = explode(';', $value['_pn']);
                $i = 0;
                foreach (explode(';', $value['_model']) as $key => $item) {
                    if($key == 0){
                        $result[$k][$i][]= $value['article'];
                        $result[$k][$i][]= $value['c_name'];
                        $result[$k][$i][]= $item;
                        $result[$k][$i][]= $pn[$key];
                        $result[$k][$i][]= $value['pr_name'];
                        $result[$k][$i][]= Url::to($value['url'],$scheme);
                        $result[$k][$i][]= $value['price'];
                    }else{
                        $result[$k][$i][]= '';
                        $result[$k][$i][]= '';
                        $result[$k][$i][]= $item;
                        $result[$k][$i][]= isset($pn[$key]) ? $pn[$key] : '';
                        $result[$k][$i][]= '';
                        $result[$k][$i][]= '';
                        $result[$k][$i][]= '';
                    }
                    
                    vd(implode(';', $result[$k][$i]), false);
                    echo "<br/>";
                    fputcsv($fp, $result[$k][$i], ';');
                    $i++;
                }

            }
        }
        fclose($fp);
        return true;
    }
}
   

