<?php
namespace app\components;
use yii\helpers\Html;
use yii\helpers\Url;
use Yii;
use app\models\Category;
use app\models\Product;
use yii\helpers\ArrayHelper;


class YandexMarket {

    public static $exclude_categories = '10';
    
    public static function getXML($log) {
        $scheme = Yii::$app->params['scheme'];
        $xmlstr = '<?xml version="1.0" encoding="utf-8"?><!DOCTYPE yml_catalog SYSTEM "shops.dtd"><yml_catalog date="'. date("Y-m-d H:i", time()) .'"/>';
        $xml = new \SimpleXMLElement($xmlstr);
        $shop = $xml->addChild('shop');
        $name = $shop->addChild('name', 'Запчасти для ноутбуков PARTSNB.RU');
        $company = $shop->addChild('company', 'сайт PartsNB.ru');
        $url = $shop->addChild('url', $scheme.'://partsnb.ru/');
        $platform = $shop->addChild('platform', 'Yii');
        $version = $shop->addChild('version', '2');
        $agency = $shop->addChild('agency', 'partsnb.ru');
        $email = $shop->addChild('email', 'partsnb@mail.ru');
        $currencies = $shop->addChild('currencies');
        $currency = $currencies->addChild('currency');
        $currency->addAttribute('id',"RUR");
        $currency->addAttribute('rate',1);
        $categories = $shop->addChild('categories');
        
        $category = Yii::$app->db->createCommand("
                    SELECT id, name
                    FROM category
                    WHERE   is_delete =0 
                        AND is_published = 1
                    ")->queryAll();
        
        foreach ($category as $value) {
            $categories->addChild('category', $value['name'])->addAttribute('id', $value['id']);
        }
      
        $offers = $shop->addChild('offers');
        
        $product = Yii::$app->db->createCommand("
                SELECT p.`id`,
                CONCAT('/', c.name_lat, '/', pr.name_lat, '/', s.name_lat, '/', p.name_lat) AS url,
                p.`price`,
                p.`category_id`, 
                CASE 
                    WHEN p.category_id IN (3,4)  
                        THEN CONCAT('Компьютеры/Аксессуары и расходные материалы/', c.name, ' для ноутбуков')
                    ELSE 'Компьютеры/Аксессуары и расходные материалы/Аксессуары для ноутбуков'
                END AS market_category,
                logo,
                p.name AS product_name,
                pr.name AS vendor,
                m.name AS model,
                GROUP_CONCAT(DISTINCT  CONCAT(f.name,': ', fv.name) SEPARATOR ', ') AS param,
                (in_stock1+in_stock2+in_stock3) - (in_res1 + in_res2 + in_res3)  AS available
                
                FROM `product` p
                INNER JOIN category c ON p.category_id = c.id
                INNER JOIN producer pr ON p.producer_id = pr.id
                INNER JOIN serie s ON p.serie_id = s.id
                INNER JOIN model m ON p.model_id = m.id
                -- LEFT JOIN image i ON p.id = i.product_id
                LEFT JOIN product_filter_value pfv ON p.article = pfv.article
                LEFT JOIN filter f ON pfv.filter_id = f.id
                LEFT JOIN filter_value fv ON pfv.filter_value_id = fv.filter_value_id
                WHERE p.is_delete =0
                AND p.status = 1
                AND p.is_published = 1
                AND c.is_published = 1
                AND p._is_problem = 0
                AND p.article IS NOT NULL
                AND p.category_id IS NOT NULL
                AND p.producer_id IS NOT NULL
                AND p.model_id IS NOT NULL
                AND p.serie_id IS NOT NULL
                AND p.name_lat IS NOT NULL
                AND p.name_lat != ''
                AND p.name != ''
                AND p.name IS NOT NULL
                AND p.ya_market=1
                GROUP BY p.id
                HAVING available > 0
            ")->queryAll();
        
        foreach ($product as $key => $value) {
            $offer = $offers->addChild('offer');
            $offer->addAttribute('id', $value['id']);
            $offer->addAttribute('type', 'vendor.model');
            $offer->addAttribute('available', 'true');

            $offer->addChild('url', Url::to($value['url'], $scheme));
            $offer->addChild('price', $value['price']);
            $offer->addChild('currencyId', 'RUR');
            $offer->addChild('categoryId', $value['category_id']);
            $offer->addChild('market_category', $value['market_category']);
  
                
//            foreach (explode("; ", $value['picture']) as $image) {
//            $offer->addChild('picture', Url::to($image,true));
            $offer->addChild('picture', Url::to("/uploadfile/images/product/resized/{$value['logo']}", $scheme));
//            }
            
            $offer->addChild('pickup', 'true');
            $offer->addChild('delivery', 'true');
            $position = strpos(strtolower($value['product_name']), strtolower($value['vendor']));
            $typePrefix = trim(substr($value['product_name'], 0, $position));
            $offer->addChild('typePrefix', $typePrefix);
            $offer->addChild('vendor', $value['vendor']);
            $model_position = strpos(strtolower($value['product_name']), strtolower($value['model']));
            $model = trim(substr($value['product_name'], $model_position));
            $offer->addChild('model', $model);
            //$offer->addChild('description', $value['param']);
            //$offer->addChild('sales_notes', "QIWI и YANDEX без процентов, Нал/Безнал");
            $offer->addChild('seller_warranty', "P3M");
            
            foreach (explode(', ', $value['param']) as $item) {
                $param = explode(': ', $item);
                if(isset($param[1]))
                    $offer->addChild('param', $param[1])->addAttribute('name', $param[0]);
            }
        }
        $log->count = count($product);
        $log->not_error = 1;
        return $xml;
    }
    
    /**
     * генерація файла
     */
    public static function saveFile(){
        ini_set("memory_limit","2000M");
        ignore_user_abort(true);
        set_time_limit(0);
        $public_path = Yii::$app->params['public_path'];
        $log = new \app\modules\admin\models\LogsSearch();
        $log->createtime = time();
        
        $XML = self::getXML($log);
        $XML->asXML(Yii::$app->basePath . "/{$public_path}/yandexmarket.yml");
        $XML->asXML(Yii::$app->basePath . "/{$public_path}/history/yandexmarket_".date("Y_m_d_H_i_s", $log->createtime).".yml");

        $log->type = 0;
        $log->finishtime = time();
        $log->save();
    }
   
}
