<?php
namespace app\components\sinc;
use Yii;

class Import {
    
    public static function getProductXML(){
        $product = \app\models\Product::find()
                ->where(['_is_problem'=>0, 'status'=>1, 'is_delete'=>0])
                ->select('article,  in_stock1,  in_stock2,  in_stock3, in_res1, in_res2, in_res3, price')
                ->andWhere('product.article IN (SELECT DISTINCT article FROM product ORDER BY product.update DESC)')
                ->asArray()
                ->limit(5)
                ->all();

        $xml = new \SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><root />");
        foreach ($product as $value) {
            $item = $xml->addChild('item');
            $type = $item->addChild('type', "product");
            $data = $type->addChild('Data');
            foreach ($value as $key => $it) {
                $data->addChild($key, $it);
            }
        }
        return $xml->asXML();
    }
    
    public static function syncXML($log = false){
        ignore_user_abort(true);
        set_time_limit(0);

        $urlSync = Yii::$app->basePath . "/sync/";
        $message = [];
        //проверяем существует ли файл Export.xml 1446796574
        if (!file_exists($urlSync . "updatetime.txt")) {
            file_put_contents($urlSync . "updatetime.txt", time());
            $time = time();
        } else 
            $time = file_get_contents($urlSync . "updatetime.txt") * 1;

        if(!file_exists($urlSync."Export.xml")){
            $user = \app\models\User::find()
                    ->where("updatetime > $time")
//                    ->andWhere(['not',['status'=>-1]])
                    ->all();
   
            $userArray = [];
            $list = ['article', 'username', 'name', 'password', 'surname', 'phone', 'zip_code', 'email', 'address', 'price_type'];
            foreach ($user as $i => $value) {
                foreach ($value->attributes as $key => $item) {
                    if (in_array($key, $list)) {
                        switch ($key) {
                            case 'surname' : $k = 'lastname';
                                break;
                            case 'zip_code' : $k = 'zipcode';
                                break;
                            case 'address' : $k = 'adress';
                                break;
                            default : $k = $key;
                        }
                        $userArray[$i][$k] = $item;
                    }
                }
            }
            if (!empty($user)) {
                $xml = new \SimpleXMLElement("<?xml version='1.0' encoding='utf-8'?><root/>");
                foreach ($userArray as $key => $value) {
                    $item = $xml->addChild('item');
                    $type = $item->addChild('type', "user");
                    $data = $item->addChild('Data');
                    foreach ($value as $k => $it) {
                        $data->addChild($k, $it);
                    }
                    $user[$key]->updatetime = time();
                    $user[$key]->save();
                }
            }
            if(empty($user)){
                $message['Export'][] = ["Пользователи в синхронизации не нуждаются"];
            }else{
                file_put_contents($urlSync . "Export.xml", $xml->asXML());
                foreach ($userArray as $value) {
                    $message['Export'][] = $value['article']." : ".$value['lastname']." ".$value['name'];
                }
            }
        }else{
            $message['Export'][] = ["1C еще не синхронизировалась"];
            $log->not_error = 0;
        };


        
        //если существует файл импорта Import.xml
        if(file_exists($urlSync."Import.xml")){
            $import = simplexml_load_file($urlSync."Import.xml");
            $error = [];
            
            foreach ($import as $item) {
                $data = (array)$item->Data;
                
                //перевіряю чи не пустий артикул
                if(trim($data['article'])){
                    if($item->type == 'product'){
                        //Обновляем/создаем новый товар
                        $result = \app\models\ProductCreate::updateProductTheXml($data, $time);
                        if($result !== true){
                            if($result == 1 || $result == 2 || $result == -1){
                                if($result == -1)
                                    $message['import'][] = "Товары ".$data['article']." не нуждаются в обновлении";
                                elseif($result == 2)
                                    $message['import'][] = "Товары артикула {$data['article']} новые!!! Пожалуйста заполните недостающую информацию в админке";
                                else
                                    $message['import'][] = "Товары ".$data['article']." обновлены";

                            }else
                                $error['product']['error'][][$data['article']] = $result;
                        };
                    }
                    if($item->type == 'user'){
                        $result = \app\models\UserSync::updateUserTheXml($data, $time);
                        if($result !== true){
                            $error['user'][][$data['article']] = $result;
                        }
                    }
                }
            }

            if(empty($error)){
                $oldname = $urlSync."Import.xml";
                $newname = $urlSync."history/Import_".date("Y_m_d_H_i_s", $log->createtime).".xml";
                rename ((string)$oldname, (string) $newname);
                
                $log->not_error = 1;
                $log->count = count($import);
            }else{
                $log->not_error = 0;
                $srt = '';
                foreach ($error as $key => $value) {
                    if($key == "user"){
                        foreach ($value as $item) {
                            foreach ($item as $article => $error) {
                                $srt .= "<b> $key ".$article."</b>";
                                foreach ($error as $val) {
                                    $srt .= "\n\t<li>".$val[0]."</li>";
                                }
                                $srt .="\n\n";
                            }
                        }
                    }
                    if($key == "product"){
                        foreach ($value as $item) {
                            foreach ($item as $article => $error) {
                                $srt .= " $key ".$article;
                                foreach ($error as $val) {
                                    $srt .= "\n\t".$val[0];
                                }
                                $srt .="\n\n";
                            }
                        }
                    }
                }
                $message['import']['error'][] = $srt;
                self::log($srt);
            }
        }else{
            $message['import_no_file'] = true;
            $message['import'][] = ["1C еще не синхронизировалась"];
            $log->not_error = 0;
        };
        self::log(json_encode($message));

        //сохраняем последнее время синхронизации
        file_put_contents($urlSync . "updatetime.txt", time());
        return $message;
    }
    
    
    public static function log($message){
//        $message = \app\modules\admin\components\Translit::translit($message);
        $fp = fopen(Yii::$app->basePath.'/log.txt', 'a+');
        $log = "message: " . $message."\n";
        fwrite($fp, $log);
        fclose($fp);
    }
}
