<?php
namespace app\components;

use app\models\FilterValue;
use app\models\ProductFilterValue;
use app\models\Product;
use app\models\ProductFilterCount;

class FilterWorkingCount {
    /**
     * сохраняет количество товаров в фильтрах
     */
//    public static function setCountProductFilter_1(){
//        foreach (FilterValue::find()->all() as $item) {
//            $model = ProductFilterValue::find()->where(["filter_value_id"=>$item->filter_value_id])->all();
//            
//            
//            $result= array();
//            foreach ($model as $value) {
//                $product = Product::find()->where(["id"=>$value->product_id, "is_delete"=>0, "status"=>1])->one();
//                $result[$product->category_id][] = $value->filter_value_id;
//            }
//            
//            foreach ($result as $cat => $value){
//                $count_product = ProductFilterCount::find()->where(["filter_value_id"=>$value, "category_id"=>$cat])->one();
//                if(!$count_product){
//                    $count_product = new ProductFilterCount();
//                }
//                
//                $count_product->filter_value_id = $item->filter_value_id;
//                $count_product->category_id = $cat;
//                $count_product->product_count = count($value);
//                
//                $count_product->isNewRecord ? $count_product->save() : $count_product->update();
//            }
//        }
//        return TRUE;
//    }
    
    
    public static function setCountProductFilter(){
        //возвращает список уникальных артикулов
        $productFilterValue = ProductFilterValue::find()
                ->where(['not', ['article'=>null]])
                ->andWhere(['not', ['category_id'=>null]])
                ->andWhere(['not', ['producer_id'=>null]])
                ->andWhere(['not', ['serie_id'=>null]])
                ->andWhere(['not', ['filter_id'=>null]])
                ->andWhere(['not', ['filter_value_id'=>null]])
                ->select('article, serie_id, filter_value_id')
                ->groupBy('serie_id, filter_value_id')
                ->asArray()
                ->all();
        
        foreach ($productFilterValue as $value) {
            $model = ProductFilterValue::find()
                    ->where(['filter_value_id'=>$value['filter_value_id']])
                    ->andWhere(['serie_id'=>$value['serie_id']])
                    ->andWhere(['not', ['category_id'=>null]])
                    ->andWhere(['not', ['producer_id'=>null]])
                    ->andWhere(['not', ['filter_id'=>null]])
                    ->all();
            
            $count = 0;
            foreach ($model as $item) {
                $count += $item->productCount;
            }
            
            if($count){
                $pfv = ProductFilterCount::createProductFilterCount(
                                        $model[0]->producer_id, 
                                        $model[0]->serie_id, 
                                        $model[0]->filter_value_id, 
                                        $count
                                    );
            }
        }
        //удаляем все поля которые  не обновились
        ProductFilterCount::deleteAll(['update'=>0]);
        ProductFilterCount::updateAll(['update'=>0]);
        return TRUE;
        
//        foreach ($distinct as $value) {
//            $count = ProductFilterValue::find()
//                    ->where([
//                        'category_id'=>$value['category_id'],
//                        'producer_id'=>$value['producer_id'],
//                        'serie_id'=>$value['serie_id'],
//                        'filter_value_id'=>$value['filter_value_id'],
//                    ])->count();
//            
//                    $productFilterCount = ProductFilterCount::find()
//                            ->where([
//                                'category_id'=>$value['category_id'],
//                                'producer_id'=>$value['producer_id'],
//                                'serie_id'=>$value['serie_id'],
//                                'filter_value_id'=>$value['filter_value_id'],
//                            ])->one();
//
//                    if(!$productFilterCount){
//                        $productFilterCount = new ProductFilterCount();
//                        
//                        $productFilterCount->category_id = $value['category_id'];
//                        $productFilterCount->producer_id = $value['producer_id'];
//                        $productFilterCount->serie_id = $value['serie_id'];
//                        $productFilterCount->filter_value_id = $value['filter_value_id'];
//                    }
//                    $productFilterCount->product_count = $count;
////                    vd($productFilterCount, false);
////                    vd($productFilterCount->save(), false);
//                    
////                                die("<br/>end");
////            echo  $value['category_id']." ".$value['producer_id'].' '.$value['serie_id']." ".$value['filter_value_id'].' = '.$count;
////            echo "<br/>";
//        }
//        die("<br/>end");
        
        
////        foreach ($catefory as $value) {
////            $producer = ProductFilterValue::find()
////                    ->where(['category_id'=>$value['category_id']])
////                    ->select('producer_id')
////                    ->asArray()
////                    ->distinct()
////                    ->all();
////            foreach ($producer as $item) {
////                $serie = ProductFilterValue::find()
////                        ->where([
////                            'category_id'=>$value['category_id'],
////                            'producer_id'=>$item['producer_id'],
////                        ])
////                        ->select('serie_id')
////                        ->asArray()
////                        ->distinct()
////                        ->all();
////                foreach ($serie as $serie_id) {
////                    $filterValue = ProductFilterValue::find()
////                            ->where([
////                                'category_id'=>$value['category_id'],
////                                'producer_id'=>$item['producer_id'],
////                                'serie_id'=>$serie_id['serie_id'],
////                            ])
////                            ->select('filter_value_id')
////                            ->asArray()
////                            ->distinct()
////                            ->all();
////                    foreach ($filterValue as $count) {
////                        $model = ProductFilterValue::find()
////                                ->where([
////                                    'category_id'=>$value['category_id'],
////                                    'producer_id'=>$item['producer_id'],
////                                    'serie_id'=>$serie_id['serie_id'],
////                                    'filter_value_id'=>$count['filter_value_id'],
////                                ])
////                                ->select('filter_value_id')
////                               ->count();
////                        $productFilterCount = ProductFilterCount::find()
////                                ->where([
////                                    'filter_value_id'=>$count['filter_value_id'],
////                                    'category_id'=>$value['category_id'],
////                                    'producer_id'=>$item['producer_id'],
////                                    'serie_id'=>$serie_id['serie_id']
////                                ])->one();
////                        
////                                if(!$productFilterCount){
////                                    $productFilterCount = new ProductFilterCount();
////                                    $productFilterCount->filter_value_id = $count['filter_value_id'];
////                                    $productFilterCount->category_id = $value['category_id'];
////                                    $productFilterCount->producer_id = $item['producer_id'];
////                                    $productFilterCount->serie_id = $serie_id['serie_id'];
////                                    $productFilterCount->product_count = $model;
////                                    vd($productFilterCount->save(), false);
//                                die("<br/>end");
////                                }
//                       echo $value['category_id']." ".$item['producer_id']." ".$serie_id['serie_id']." ".$count['filter_value_id']." = ".$model;
//                       echo "<br/>";
//                    }
//                vd($filterValue, false);
                    
//                }
                
//            echo "<br/>";
//            }
            
//        }
       
//        die("<br/>end");
        
//        foreach (FilterValue::find()->all() as $item) {
//            $product = ProductFilterValue::find()->where(["filter_value_id"=>$item->filter_value_id])->all();
//            
////            echo "<pre>";
////            print_r("filter : ".$item->filter_value_id);
//            //catArray - масив в котором будут хранится 
//            //ключ - категория; 
//            //значение - количества товаров в этой категории
//            $catArray = []; 
//            if($product){
//                foreach ($product as $value) {
//                    isset($catArray[$value->category_id]) ? $catArray[$value->category_id] += 1 : $catArray[$value->category_id] = 1;
//                }
//            }
////            echo "<br/>";
//            
//            $count   = ProductFilterCount::find()->where(['filter_value_id'=>$item->filter_value_id])->all();
//            if($count){
//                foreach ($count as $value) {
//                    if(!isset($catArray[$value->category_id])){
//                        //удаляем при условии что у товара нет данного фильтра
//                        // раньше был а сейчас нет
//                        $value->delete();
////                        echo "нет товаров в этом фильтре";
////                        echo "<br/>";
//                    }else{
//                        $value->product_count = $catArray[$value->category_id];
//                        $value->update();
//                    };
////                    print_r('c_'.$value->category_id." count :".$value->product_count);
//    //                echo "<br/>";
//                }
//            }
//            
//            if(!empty($catArray) && empty($count)){
//                foreach ($catArray as $key => $value) {
//                    $count = new ProductFilterCount();
//                    $count->filter_value_id = $item->filter_value_id;
//                    $count->category_id = $key;
//                    $count->product_count = $value;
//                    $count->save();
//                }
//
//            }
//            
////            print_r($catArray);
////            echo "<pre>";
////            echo "<br/>";
////            echo "<br/>";
////            echo "<br/>";
//        }
//
    }
    
}
