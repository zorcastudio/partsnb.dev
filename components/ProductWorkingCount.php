<?php

namespace app\components;

use app\models\Category;
use app\models\Product;

class ProductWorkingCount {

    /**
     * сохраняет количество товаров в фильтрах
     */
    public static function setCountProductCategory() {
        $category = Category::find()->where(["is_delete"=>0])->all();
        foreach ($category as $value) {
            $value->product_count = self::getCountProduct($value->id);
            $value->update();
        }
    }

    public static function getCountProduct($id) {
        $category = Category::find()->where(['id'=>$id, "is_delete"=>0])->one();
        $count = 0;
        if ($category) {
            $cat = Category::find()
                    ->where(['parent_id' => $category->id, "is_delete"=>0])
                    ->all();

            foreach ($cat as $value) {
                $count += self::getCountProduct($value->id);
            }
            return $count + Product::find()
                            ->where(['category_id' => $category->id, "is_delete"=>0, 'is_published'=>1, "status"=>1])
                            ->count();
        }
        
    }

}
