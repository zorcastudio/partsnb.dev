<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
        <?php include $block['head']; ?>
    <body>
        <?php include $block['header_admin']; ?>
            <div id="content">
                <?php echo $content; ?>
            </div>
            <div class="clear"></div>
        <script type="text/javascript" src="/js/myjs.js"></script>
    </body>
</html>