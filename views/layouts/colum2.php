<?php
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <?= $this->render('/block/head') ?>
    </head>
<body>
    <div id="body-content">
        <?php $this->beginBody() ?>
        <?= $this->render('/block/header', ['data'=>$this->context->specific_data]) ?>
        <?= $this->render('/block/navigation') ?>
        
        <?php 
//        $this->widget('bootstrap.widgets.TbAlert', array(
//            'fade' => true,
//            'closeText' => '&times;', // false equals no close link
//            'events' => array(),
//            'htmlOptions' => array(),
//            'userComponentId' => 'user',
//            'alerts' => array( // configurations per alert type
//                // success, info, warning, error or danger
//                'success' => array('closeText' => '&times;'),
//                'info', // you don't need to specify full config
//                'warning' => array('block' => false),
//                'error' => array('block' => false)
//            ),
//        ));
        
        
        ?>
        
        <section id="new-offfer">
            <div class="wrapper"></div>
        </section>
        <section id="content">
            <div class="wrapper">
                <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                <?= $this->render('/block/left_sidebar') ?>
                <div class="right-sidebar">
                    <?php echo $content; ?>
                </div>
            </div>
        </section>
        <div class="basket-wraper">
        </div>
        <?= $this->render('/block/footer') ?>
    </div>
    <?php $this->endBody() ?>
    <?= $this->render('/block/script') ?>
</body>
</html>
<?php $this->endPage() ?>
