<!DOCTYPE html>
<html>
    <?php include $block['head']; ?>
<body>
    
    <?php include $block['header']; ?>
    <section id="new-offfer">
        <div class="wrapper"></div>
    </section>
    <section id="content">
        <div class="wrapper">
            <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', ['links' => $this->params['breadcrumbs']]); ?>
            <?php echo $content; ?>
        </div>
    </section>
    <div class="basket-wraper">
        <?php include $block['basket']; ?>
    </div>
    <?php include $block['footer']; ?>
    <?= $this->render('/block/script') ?>
</body>
</html>
