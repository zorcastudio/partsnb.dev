<?php 
use yii\widgets\ActiveForm;

use yii\helpers\Url;
    
use app\models\Registration;
use app\models\Region;
use app\models\City;
use app\models\Country;
use app\models\Delivery;
use app\models\Order;
use yii\helpers\BaseHtml;

$this->title = "Оформление заказа"; 
$this->params['breadcrumbs'] = [
    ['label' => 'Корзина', 'url' => ['/basket']],
    'Оформление заказа',
];?>

<div id="basket-produkt">
    <div class="wrapper">
    <span class="basket-header">Корзина</span>
    <?= $this->render('/basket/_list_product',array('model'=>$basket));  ?>
    <table class="info" style="width: 100%;">
            <tr>
                <td style="width: 36%;"></td>
                <td>
                    <div class="basket_sum">
                        <span>Сумма заказа:</span>
                        <span class="basket_itog_sum"><?= $basket->getSummPrise()['summ'] ." руб." ?></span>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <div class="basket_sum">
                        <span>Стоимость обработки и доставки:</span>
                        <span class="put-delivery">0.00 руб.</span>
                    </div>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <div class="basket_sum">
                        <span>Всего:</span>
                        <span class="put-summ"><?= $basket->getSummPrise()['summ'] ." руб." ?></span>
                    </div>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<?php
 $form = ActiveForm::begin([
        'id' => 'user',
        "options"=>['class' => 'well'],
        'fieldConfig' => [
            'template' => "<div class=\"form-input\">{input}</div><div class='error-order'>{error}</div><div style='clear:both'></div>",
        ]
    ]);
?>
<div id="contact-info">
    <div class="cloud">
        <div class="tail"></div>
        <span>Ваши данные</span>
    </div>
    <?php // echo $form->field($model, '_name')->textInput(['placeholder'=>'Имя*']); ?>
    <?php echo $form->field($model, 'full_name')->textInput(['placeholder'=>'Ф.И.О*']); ?>
    <?php echo $form->field($model, 'email')->textInput(['placeholder'=>'Эл.почта*']); ?>
    <?= $form->field($model, 'phone')->textInput(['placeholder'=>'Телефон *'])->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7(999)999-99-99',
    ]) ?>
    
    
    
    <?php
        echo $form->field($model, 'country_id')->dropDownList(
                                                Country::getCountrys(),
                                                [   
                                                    'onchange' => '$.post( "'.Url::toRoute('ajax/country_select').'", {country_id: $(this).val()}, setRegions)',
                                                    'options' => [Registration::$_country_id =>  ['selected' => true]]
                                                ]);
                                        ?>
    <?php // echo $form->field($model, 'region_id')->textInput(['placeholder'=>'Город *']); ?>
    <?php echo $form->field($model, 'city_name')->textInput(['placeholder'=>'Город']); ?>
<?php 
//            echo  $form->field($model, 'city_id')->dropDownList(
//                            ($model->country_id || Registration::$_country_id) ? Region::getRegion($model->country_id, Registration::$_country_id) : [],
//                            $city,
//                            [   
//                                'onchange' => '$.post( "'.Url::toRoute('ajax/region_select').'", {region_id: $(this).val()}, setCities)',
//                                'options' => [Registration::$_country_id =>  ['selected' => true]],
//                                'id'=>'region', 
//                                'disabled' => false, 
//                                'prompt'=>'Выберите область',
//                            ]
//                        );
?>
    <?php 
//            echo $form->field($model,'city_id')->dropDownList(
//                                                    ($model->region_id) ? City::getCitys($model->region_id) : [],
//                                                    [
//                                                        'id'=>'city', 
//                                                        'disabled' => (!$model->region_id) ? true : false,  
//                                                        'prompt'=>'Выберите город',
//                                                        'ajax' => [ 'type'=>'POST', 'url'=>Url::toRoute('ajax/OnCitySelect'), //url to call
//                                                            'data'=>['city_id'=>"js: $(this).val()"],
//                                                            'success'=>'js: function(data) {
//                                                                setCities(data)
//                                                            }'
//                                                        ],
//                                                    ]
//                                        );?>
    <?= $form->field($model, 'zip_code')->textInput(['placeholder'=>'Индекс']); ?>
    
    <?= $form->field($model, 'address')->textInput(['placeholder'=>'Адрес']);?>
     <?php 
//        echo $form->field($model, 'payment_id')->dropDownList(
//                                app\models\Payment::getListPayment(),
//                                [
//                                    'prompt'=>'Не выбрано',
//                                ]
//                        ) 
    ?>
    
</div>

<div id="contact-info" class="right">
    <div class="cloud">
        <div class="tail"></div>
        <span>Информация о доставке</span>
    </div>
    <?= $form->field($model, 
                    'delivery_id', 
                    [
                        'template' => '<label class="signup-radio">{input}</label>{error}'
                    ]
                    )
                    ->radioList(
                            Delivery::getListDelivery($basket), 
                            [
                                'item' => function($index, $label, $name, $checked, $value) {
                                    $description = \app\models\Delivery::findOne(['id'=>$value])->description;
//                                    
                                    $line = ($value == 5) ? 'line' : NULL;
                                    $return = '<label class="modal-radio '.$line.' ">';
                                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" onchange ="setDelivery($(this).val())"  '.($checked ? "checked" : null).'>';
                                    $return .= '<span>' . ucwords($label) . '</span>';
                                    $return .= "<span class='description ' >$description</span>";
                                    $return .= '</label>';
                                    return $return;
                                },
                                
                            ]
                    )->label(false); ?>
    <script type="text/javascript">
        var working_time = '<?= date('H', time()) ?>';
        var avialable = <?= json_encode(Order::getAvialebel($basket)); ?>;
    </script>
        
</div>
<div style="clear: both"></div>

<div id="contact-info" >
    <div class="cloud">
        <div class="tail"></div>
        <span>Оплата</span>
    </div>
    <div class="op_rounded_content">
            <div id="payment_html">
                <div class="payment_inner" rel="force_show_payments">
                        <?php foreach ($payment as $key => $value) : ?>
                            <div class="payment_method_label"><?= $key ?></div>
                                <?php foreach ($value as $item) : ?>
                                    <div>
                                        <input type="radio" name="Order[payment_id]" id="payment_id_<?= $item['id'] ?>" value="<?= $item['id'] ?>" <?= ($model->payment_id == $item['id']) ? 'checked' : null ?>>
                                        <label for="payment_id_<?= $item['id'] ?>">
                                            <span class="payment">
                                                <?php if($item['logo']) : ?>
                                                    <img align="middle" src="/images/view/<?= $item['logo'] ?>">
                                                <?php endif; ?>
                                            <span class="payment_name"><?= $item['name'] ?></span>
                                            </span>
                                        </label>
                                    </div>
                                <?php endforeach; ?>
                        <?php endforeach; ?>
                </div>
            </div>
    </div>
    <?php if($model->getErrors() && ($error = $model->getErrors()) && isset($error['payment_id'])) : // ?>
        <div class="error-payment"><?= $error['payment_id'][0]; ?></div>
    <?php endif; ?>
</div>

<div id="contact-info" class="comment">
    <div class="cloud">
        <div class="tail"></div>
        <span>Комментарии к заказу</span>
    </div>
    <table>
        <tbody>
            <tr>
                <td rowspan="4" style="width: 50%;"><?= $form->field($model, 'description')->textarea(['rows'=>"5", 'cols'=>"30"]);?></td>
                <th>Сумма заказа</th>
                <td class="basket_itog_sum"><?= $basket->getSummPrise()['summ'] ." руб." ?></td>
            </tr>
            <tr>
                <th>Цена доставки</th>
                <td class="put-delivery">0,00 руб</td>
            </tr>
            <tr>
                <th>Сумма</th>
                <td class="put-summ"><?= $basket->getSummPrise()['summ'] ." руб." ?></td>
            </tr>
            <tr>
                <td colspan="2"><?= $form->field($model, 'accepted')->checkbox(['label' =>'Я согласен с Условиями обслуживания(Условия обслуживания)', 'uncheck' => null]);?></td>
            </tr>
        </tbody>
    </table>
    
    <a href="#" class="basket_but_link">
        <button class="basket_but">Подтвердить заказ</button>
    </a>
</div>
<div style="clear: both"></div>



<?php ActiveForm::end(); unset($form); ?>
<script type="text/javascript">
    var deliveryPrice =  jQuery.parseJSON( '<?= $deliveryPrice ?>' ); 
    var priceSumm = parseFloat('<?=  floatval(preg_replace('/[^\d.]/', '', $basket->getSummPrise()['summ'])); ?>');
    var allPrice;
    var delivery;
    $('#user').on('submit', function(){
        yaCounter22143797.reachGoal('CONFIRM_ORDER');
        return true;
    });
    $(document).ready(function () {
        check_cash_block();
    });
</script>