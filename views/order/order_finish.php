<h3>Спасибо за заказ!</h3>
<?php // vd($model, false); ?>
<div id="contact-info finish" >
    <table class="vmorder-done" style="font-size: 14px;">
        <tbody><tr>
                <th class="vmorder-done-payinfo">Способ оплаты</th>
                <td align="left"><span class="vmpayment_name"><?= $model->payment->name ?></span><br></td>
            </tr>
            <tr>
                <th class="vmorder-done-nr">Номер заказа</th>
                <td align="left"> <?= $model->id ?></td>
            </tr>
            <tr>
                <th class="vmorder-done-amount">Сумма</th>
                <td align="left"><?= $model->sumPrice ?> руб</td>
            </tr>
        </tbody>
    </table>

    <?php 
    if ($model->payment_id == 9 || $model->payment_id == 10) : 
    include ($_SERVER['DOCUMENT_ROOT'].'/ymconfig.php');
    ?>
    <div style="padding-top: 20px;">

        <form action="<?php echo ($configs['formAction']); ?>" method="post">
            <input name="shopId" value="<?php echo ($configs['shopId']); ?>" type="hidden"/>
            <input name="scid" value="<?php echo ($configs['scId']); ?>" type="hidden"/>
            <input name="sum" value="<?php echo ($model->sumPrice); ?>" type="hidden">
            <input name="customerNumber" value="<?php echo ($model->email); ?>" type="hidden"/>
            <?php if ($model->payment_id == 9) : ?>
            <input name="paymentType" value="AC" type="hidden"/>
            <?php elseif ($model->payment_id == 10) : ?>
            <input name="paymentType" value="PC" type="hidden"/>
            <?php endif; ?>
            <input name="orderNumber" value="<?php echo ($model->id); ?>" type="hidden"/>
            <input name="cps_email" value="<?php echo ($model->email); ?>" type="hidden">
            <input name="cps_phone" value="<?php echo ($model->phone); ?>" type="hidden">
            <input name="custName" value="<?php echo ($model->full_name); ?>" type="hidden"/> 
            <input name="custEmail" value="<?php echo ($model->email); ?>" type="hidden"/> 
            <input name="custAddr" value="<?php echo ($model->city_name); ?> <?php echo ($model->address); ?>" type="hidden"/> 
            <input type="hidden" name="shopSuccessURL" value="https://partsnb.ru/payment-thanx">
            <input type="hidden" name="shopFailURL" value="https://partsnb.ru/payment-error">
            <input type="submit" value="Оплатить" style="padding:12px 24px;" />
        </form>

    </div>
    <?php endif; ?>

</div>