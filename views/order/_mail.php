<?php 
use yii\helpers\Url;
use yii\helpers\Html;
?>
<style type="text/css">
    #message p{
        margin :4px 0;
        padding: 0;
        font-size: 14px;
    }
    #message hr{
        border-top: 1px solid #000; 
        margin: 20px 0
    }
    #message .read{
        color: #F00000;padding: 0;
        margin: 0;
    }
    #message p a{text-decoration: none}
    #message .phone span{color: #00F}
    #message .link a{color: #00F; text-decoration: underline}
    
</style>
<?php // vd($model, false); ?>  
<div id="message">
    <p class="read">№ заказа: <?= $model->id ?>, <?= $model->fullName ?>, <?= $model->sumPrice ?> руб.</p>
    <hr>
        <p>Ваш заказ принят!</p>
        <p>Как только он будет обработан, менеджер свяжется с Вами по указанному телефону для подтверждения.</p>
        <p>Спасибо, что обратились в интернет - магазин Partsnb.ru!</p>
    <hr>
        <?php if($model->description) : ?>
            <p>Ваши комментарии: <?= $model->description ?></p>
            <hr>
        <?php endif; ?>
            <p>Имя: <?= $model->fullName ?></p>
            <p>Эл.почта: <?= $model->email ?></p>
            <p class="phone">Телефон: <span><?= $model->phone ?></span></p>
            <p>Город: <?= $city ?></p>
        <?php if($model->address) : ?>
            <p>Адрес: <?= $model->address ?></p>
        <?php endif; ?>
        <?php if($model->zip_code) : ?>
            <p>Индекс: <?= $model->zip_code ?></p>
        <?php endif; ?>
    <hr>
    <p>Заказ:</p>
    <?php foreach ($model->order_p as $key => $item) : ?>
        <?php $value = $item->product; ?>
        <p><?= ++$key . ". $value->name (Артикул $value->article)"; ?></p>
        <p>Цена: <?= $value->tradePrice ?> - <?= $item->amount ?> шт.</p>
    <?php endforeach; ?>
    <br>
        <p>Итог: <?= $model->sumPrice ?> руб</p>
    <hr>
        <p>Способ оплаты: <?= $model->payment->name ?></p>
        <p>
            <?php 
                echo "Способ доставки: ". trim(preg_replace("/\d* руб\.?/iu", '', $model->delivery->name))." ";
                echo $model->delivery->price ? $model->delivery->price. ' руб.' : null 
            ?>
        </p>
        <br>
        <?php
        if($model->delivery) {
            if($model->delivery->email_info) {
                echo $model->delivery->email_info;
            }
            elseif($model->delivery->adress) {
                $avialable = app\models\Order::getAvialebel($basket);
                $text_order = 'Вы можете забрать заказ';

                if(array_key_exists($model->delivery->id, $avialable)){
                    $text_order = str_replace('заказ', $avialable[$model->delivery->id], $text_order);
                }
                echo "<p>{$text_order} по адресу:</p>";
                ?>
                <p><?= $model->delivery->adress ?></p>
                <?php if($model->page) :
                    $scheme = Yii::$app->params['scheme'];?>
                    <p>Как нас найти: </p>
                    <p class="link"><?= Html::a(Url::to(["/kontakty/{$model->page->name_lat}"], $scheme), Url::to(["/kontakty/{$model->page->name_lat}"], $scheme), ['target'=>"_blank"]) ?></p>
                <?php endif;
            }
        }
        $delivery_phones = $model->delivery->getTelefone($model->payment_id);
        if($delivery_phones) { ?>
    <hr>
    <p>Мы будем рады ответить на Ваши вопросы по телефону:</p>
    <?php foreach (explode(';', $delivery_phones) as $value) : ?>
        <p><?= trim($value) ?></p>
    <?php endforeach;
        } ?>


    <br>
    <br>
    <p>С уважением, команда интернет - магазина partsnb.ru</p>
</div>
