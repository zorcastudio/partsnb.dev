<?php 
use app\models\Category;
use yii\helpers\Html;
use yii\widgets\ListView;
use app\components\MyLinkPager;
$this->title = $category->name.' для ноутбуков и нетбуков '.$producer->name.(isset($model->name) ? ' '.$model->name : ''). ' - partsnb.ru';
$this->params['breadcrumbs'] = [
        ['label' => $category->name, 'url' => "/".$category->name_lat],
        $producer->name 
];

if(isset($_GET['page']) && $_GET['page'] > 1 ){
   $this->title .= " - Страница $_GET[page]";
}  else {
    $this->registerMetaTag(['description' =>"У нас в интернет-магазине partsnb.ru вы можете купить любые $category->name для ноутбуков $producer->name в Санкт-Петербурге. Звоните +7 (812) 920-05-75"]);
    $this->registerMetaTag(['keywords' =>"$category->name для ноутбуков $producer->name, $category->name для нетбуков $producer->name, $category->name $producer->name спб, $category->name $producer->name санкт-петербург"]);
}
?>

<h1><?= $category->name.' для ноутбука '.$producer->name.(isset($model->name) ? ' '.$model->name : '') ?></h1><?php
if(!empty($serie)) : ?>
    <h2>Cерии</h2>
    <?php foreach ($serie as $value) : ?>
        <div class="list-block">
            <?php echo Html::a($value['name'], '/'.$category->name_lat.'/'.$producer->name_lat.'/' . $value['name_lat']); ?>
        </div>
    <?php endforeach; ?>
    <div style="clear: both"></div>
<?php endif; ?>
<hr />
<div class="filter-result">	
  <?php
    \yii\widgets\Pjax::begin();
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '/category/_view',
            'layout'=>"<div id='block-summary'>{summary}</div><ul class='products-list'>{items}</ul><div class='prod-paginato-case'>{pager}</div>",
            'pager' => [
                'class'=> MyLinkPager::className(),
                'options' =>['class'=>'prod-paginator'] ,
                'firstPageLabel' => 'В начало',
                'prevPageLabel' => 'Назад',
                'nextPageLabel' => 'Вперёд',
                'lastPageLabel' => 'В конец',
            ]
        ]);
    \yii\widgets\Pjax::end(); 
?>
</div>
<div id="description">
    <?= (isset($model->description) ? $model->description : $producer->description);  ?>
</div>