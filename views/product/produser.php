<?php
use app\models\Category;
use yii\helpers\Html;
use yii\widgets\ListView;
use app\components\MyLinkPager;

$this->params['breadcrumbs'] = Category::getBreadCrumbs($category, 'category');
$this->title = $category->name.' для ноутбуков и нетбуков - partsnb.ru';//." для ноутбуков и нетбуков в Санкт-Петербурге";

if(isset($_GET['page']) && $_GET['page'] > 1 ){
   $this->title .= " - Страница $_GET[page]";
}  else {

    $this->registerMetaTag(['description' =>"У нас в интернет-магазине partsnb.ru вы можете купить любые $category->name для ноутбуков в Санкт-Петербурге. Звоните +7 (812) 920-05-75"]);
    $this->registerMetaTag(['keywords' =>"$category->name для ноутбуков, $category->name для нетбуков, $category->name спб, $category->name санкт-петербург"]);

}
?><div style="clear: both"></div>
<h1><?= $category->name_category ?></h1>
<h2>Производители</h2>
<?php foreach ($producer as $value) : ?>
    <div class="list-block">
        <?php echo Html::a($value->name, '/'.$category->name_lat.'/' . $value->name_lat); ?>
    </div>
<?php endforeach; ?>
<div class="clear"></div>
<div class="filter-result">
    <?php
     \yii\widgets\Pjax::begin();
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '/category/_view',
            'layout'=>"<div id='block-summary'>{summary}</div><ul class='products-list'>{items}</ul><div class='prod-paginato-case'>{pager}</div>",
            'pager' => [
               'class'=> MyLinkPager::className(),
                'options' =>['class'=>'prod-paginator'] ,
                'firstPageLabel' => 'В начало',
                'prevPageLabel' => 'Назад',
                'nextPageLabel' => 'Вперёд',
                'lastPageLabel' => 'В конец',
            ]
    ]);
    \yii\widgets\Pjax::end();
?>
</div>
<div id="description">
    <?= $category->description  ?>
</div>