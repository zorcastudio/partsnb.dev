<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;

$this->title = $title;

$this->registerMetaTag(['keywords' => $keywords]);
$this->registerMetaTag(['description' => $description]);
$this->params['breadcrumbs'] = $model->breadcrumbs;
$scheme = Yii::$app->params['scheme'];
?>

<script  src="/galery/jquery.min.js"></script>
<script  src="/galery/script.js"></script>
<script src="/galery/jquery.prettyPhoto.js" ></script> 
<link rel="stylesheet" type="text/css" href="/galery/prettyPhoto.css" />

<div class="page-body">

    <h1><?= $model->name ?></h1>
    <div class="image-product">
        <div class="main-image">
            <?php

            foreach ($model->image as $key => $value) {
                if ($key == 0)
                    echo "<a onclick='event.preventDefault()' class='image-full image-zoom namber-$key active' rel='prettyPhoto[gallery]' href='" . Url::toRoute('/uploadfile/images/product/' . $value['url'], $scheme) . "' data-fancybox-group='button' title='" . $value['meta'] . "'>";
                else
                    echo "<a class='image-full image-zoom namber-$key' rel='prettyPhoto[gallery]' href='" . Url::toRoute('/uploadfile/images/product/' . $value['url'], $scheme) . "' data-fancybox-group='button' title='" . $value['meta'] . "'>";
                echo "<img src='" . Url::toRoute('/uploadfile/images/product/' . $value['url'], $scheme) . "' style='width: 324px;' >";
                echo "</a>";
            }
            ?>

        </div>
        <div class="additional-images">
            <ul class="portfolio-area">        

                <?php foreach ($model->image as $key => $item) : ?>
                    <li class="portfolio-item2" data-id="id-0" data-type="cat-item-4">	
                        <div>
                            <span class="image-block">
                                <img class="image-preview namber-<?= $key ?>"   src="<?= Url::toRoute('/uploadfile/images/product/' . $item->url) ?>" alt="<?= $this->title ?>" title="<?= $this->title ?>" />                    
                            </span>
                        </div>	
                    </li>			                        		
                <?php endforeach; ?>
                <div class="clear"></div>
            </ul>
        </div>


    </div>
    <script type="text/javascript">jQuery = $ = newJQuery;</script>

    <div class="product">
        <div class="price"><?= $model->price . ' руб' ?><?php //= $model->tradePrice . ' руб' ?></div>
        <?php if ($model->available > 0) : ?>
        <div class="for-buy">
            <div class="amount">
                <div class="kol-vo">Кол-во:</div>
                <input class="amount-view" type="text" value="<?= $count ?>"/>
                <div class="add-delete">
                    <div class="add" id="<?= $model->id ?>" onclick="setAmount(this, <?= $model->available ?>)"></div>
                    <div class="remuve" id="<?= $model->id ?>" onclick="setAmount(this, <?= $model->available ?>)"></div>
                </div>
                <div style="clear: both"></div>
            </div>
            <?php
            Modal::begin([
            'toggleButton' => ['label' => 'Купить', 'class' => "buy-button", 'id' => $model->id, 'onclick' => 'getAmount(this)'],
            ]);
            echo $this->render('_modal', ['text' => $this->title]); //добавлен в корзину
            Modal::end();
            ?>
        </div>
        <?php endif; ?>
        <div class="articul">Артикул : <b><?= $model->article ?></b></div>
        <div class="check-ok <?= $model->available ? 'is' : 'none ' ?>">
            <?php 
                if($model->is_timed == 1){
                    echo "Ожидается {$model->delivery_date}";
                }elseif($model->available > 0){
                    echo 'В наличии';
                }else{
                    echo "товар отсутствует";
                }
            ?>
        </div>
        <div style="clear: both"></div>
        
        <div class="info-price spets">
            <?php
                Modal::begin([
                    'toggleButton' => [
                        'label' => 'СПЕЦИАЛЬНАЯ<br>ЦЕНА<br>(подробнее)', 
                        'class' => "wholesale-prices"
                    ],
                ]);
                echo $model->_spetc_price;
                Modal::end();
            ?>
            <div class="price-type product-price"><?= $model->trade_price1 ?> руб</div>
        </div>
        <div class="info-price max">
             <?php
                Modal::begin([
                    'toggleButton' => [
                        'label' => 'С МАКСИМАЛЬНОЙ<br>СКИДКОЙ<br>(подробнее)', 
                        'class' => "wholesale-prices"
                    ],
                ]);
                echo $this->render('_wholesale_prices');
                Modal::end();
            ?>
            <div class="price-type product-price"><?= $model->trade_price3 ?> руб</div>
        </div>
            <?php if ($model->available > 0) : ?>
            <div class="check">
                <div id="info">
                    <div class="store_info_block">
                        <div class="detail_info">
                            <span>г. Санкт-Петербург</span><br><b>Самовывоз</b>
                        </div>
                        <div class="store_item">
                            <?php $inStok = ($model->getStock(1)) > 0 ?>
                            <div class="<?= $inStok ? "store_in_stock" : "store_not_in_stock" ?> in_stock_image" title="<?= $inStok ? "В наличии" : "Товар отсутствует" ?>"></div>
                            <span class="store_separate">—</span>м. Бухарестская<br>
                            <?= $inStok ? null : "<span class='store_info'>{$delivery_rules[$delivery_stock['stock1']]}</span>" ?>
                        </div>
                        <div class="store_item">
                            <?php $inStok = ($model->getStock(3)) > 0 ?>
                            <div class="<?= $inStok ? "store_in_stock" : "store_not_in_stock" ?> in_stock_image" title="<?= $inStok ? "В наличии" : "Товар отсутствует" ?>"></div>
                            <span class="store_separate">—</span>м. Садовая<br>
                            <?= $inStok ? null : "<span class='store_info'>{$delivery_rules[$delivery_stock['stock3']]}</span>" ?>
                        </div>
                        <div class="store_item">
                            <?php $inStok = ($model->getStock(2)) > 0 ?>
                            <div class="<?= $inStok ? "store_in_stock" : "store_not_in_stock" ?> in_stock_image" title="<?= $inStok ? "В наличии" : "Товар отсутствует" ?>"></div>
                            <span class="store_separate">—</span>Удаленный склад
                        </div>
                    </div>
                    <div class="store_info_block left">
                        <div class="detail_info">
                            <span>Доставка в регионы</span> <br>
                            <b>Срок поставки - от 5 до 15 дней</b><br>
                            <div class="delivery_cost">
                                <div class="store_item">— Почта России - <span class="green">350 руб.</span></div>
                                <div class="store_item">— Почта России 1-й класс - <span class="green">400 руб.</span></div>
                            </div>
                        </div>
                        <div class="detail_link">
                            <?= Html::a('Условия доставки', '/informatsiya/dostavka-i-oplata') ?>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="store_info_block down">
                        <div class="detail_link">
                            <?= Html::a('Адреса магазинов', '/kontakty') ?>
                        </div>
                        <div class="detail_info">
                            <b>Доставка</b>
                        </div>
                        <div class="detail_info">
                            <div class="store_item">— курьером по СПб - <span class="green">350 руб.</span></div>
                        </div>
                    </div>
                </div>
                <div style="clear: both"></div>
            </div>
            <?php endif; ?>
    </div>
    <div style="clear: both"></div>
    <div class="description-product">
        <?= $model->description_product ?>
    </div>
    <div class="product-info">
        <?php if ($properties) : ?>
            <div class="properties">
                <span class="title">Технические характеристики</span>
                <table>
                    <col><col>
                    <?php foreach ($properties as $key => $value) : ?>
                        <tr>
                            <th><?= $key ?></th>
                            <td><?= $value ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        <?php endif; ?>
        <div class="clear"></div>
        <?php if (!empty($modelForSearch)) : ?>
        <h2 style="cursor: none"><?= $model->h2_compatible_model ?></h2>
            <ul>
                <?php foreach ($modelForSearch as $key => $value) : ?>
                    <?php if($key == 40 && isset($modelForSearch[41])) : ?>
                        </ul>
                        <p onclick="expandModel(this)">Показать весь список</p>
                        <ul class="none">
                    <?php endif; ?>
                    <li><?= $value ?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
<!--            
        <?php // if (!empty($otherProducer)) : ?>
            <p>А также имеет полную совместимость с ноутбуками других производителей:</p>
            <noindex>
                <ul>
                    <?php // foreach ($otherProducer as $value) : ?>
                        <li><?php //= $value['name'] ?></li>
                    <?php // endforeach; ?>
                </ul>
            </noindex>
        <?php // endif; ?>
            -->
        <?php if (!empty($pnModel)) : ?>
            <h2 onclick="expandModel(this)">Список совместимых парт-номеров p/n:</h2>
            <ul>
                <?php foreach ($pnModel as $value) : ?>
                    <li><?= $value ?></li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
    <?php // echo $model->description; ?>
</div>
