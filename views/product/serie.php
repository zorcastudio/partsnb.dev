<?php 
use app\models\Category;
use yii\helpers\Html;
use yii\widgets\ListView;
use app\components\MyLinkPager;

$this->params['breadcrumbs'] = [
        ['label' => $category->name, 'url' => "/".$category->name_lat],
        ['label' => $producer->name, 'url' => "/".$category->name_lat."/".$producer->name_lat],
        $serie ? $serie->name : null
];

$this->title = $title;

if(isset($_GET['page']) && $_GET['page'] > 1 ){
   $this->title .= " - Страница $_GET[page]";
}  else {

    $this->registerMetaTag(['description' =>$descripription]);
    $this->registerMetaTag(['keywords' =>"$category->name для ноутбуков, $category->name для нетбуков, $category->name спб, $category->name санкт-петербург"]);
    
}
?>
<h1><?= $category->name_category.' '.$producer->name.' '.$serie->name ?></h1>

<div class="filter-result">	
    <?php
    \yii\widgets\Pjax::begin();
        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '/category/_view',
            'layout'=>"<div id='block-summary'>{summary}</div><ul class='products-list'>{items}</ul><div class='prod-paginato-case'>{pager}</div>",
            'pager' => [
                'class'=> MyLinkPager::className(),
                'options' =>['class'=>'prod-paginator'] ,
                'firstPageLabel' => 'В начало',
                'prevPageLabel' => 'Назад',
                'nextPageLabel' => 'Вперёд',
                'lastPageLabel' => 'В конец',
            ]
    ]);
    \yii\widgets\Pjax::end(); 
    ?>
</div>
<div id="description">
    <?= $serie->description  ?>
</div>