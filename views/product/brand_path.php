<?php 
use app\models\Category;
use yii\helpers\Html;
use yii\widgets\ListView;
use app\components\MyLinkPager;

$this->params['breadcrumbs'] = $breadcrumbs;

$this->title = $title;
$this->registerMetaTag(['description' => $description]);
?>
<h1><?= ($title ? $title : $items_name); ?></h1>
<?php foreach ($items as $value) : ?>
<div class="list-block">
    <?php echo Html::a($value['name'], $value->urlBrandPath); ?>
</div>

<?php endforeach;?>
<div class="clear"></div>
<div id="description">
    <?= $current_item->description; ?>
</div>