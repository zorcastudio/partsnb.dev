<?php 
use yii\widgets\ListView;

$this->title = 'Проблемные товары';
$this->params['breadcrumbs'] = [$this->title]; 
?>

<h1><?= $this->title ?></h1>
<div class="filter-result">	
    <?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '/category/_view',
        'layout'=>"<ul class='products-list'>{items}</ul><div class='prod-paginato-case'>{pager}</div>",
        'pager' => [
           'options' =>['class'=>'prod-paginator'] ,
            'firstPageLabel' => 'В начало',
            'prevPageLabel' => 'Назад',
            'nextPageLabel' => 'Вперёд',
            'lastPageLabel' => 'В конец',
        ]
    ]);
    ?>
</div>