<?php 
use app\models\Category;
use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = $category->name;
$this->params['breadcrumbs'] = Category::getBreadCrumbs($category, 'category'); 
?>

<h1><?= $this->title." ".$count_all."шт. " ?></h1>
<div class="filter-value">
    <?php
    
        foreach ($active_f as $key => $value) {
            echo $this->render('_active_filter',array('value'=>$value, "key" =>$key)); 
        }
        if(!empty($active_f)){
            echo '<div class="clear-filter">Сбросить</div>';
            $text = $dataProvider->count > 1 ? "товаров" : "товар";
            echo "<div class='total-product'>Подобрано: ".$dataProvider->count." $text из ".$count_all."</div>";
        }
    ?>
    <div class="clear"></div>
</div>

<div class="filter-result">	
<?php
    \yii\widgets\Pjax::begin();
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '/category/'.$view,
        'layout'=>"<ul class='products-list'>{items}</ul><div class='prod-paginato-case'>{pager}</div>",
        'pager' => [
           'options' =>['class'=>'prod-paginator'] ,
            'firstPageLabel' => 'В начало',
            'prevPageLabel' => 'Назад',
            'nextPageLabel' => 'Вперёд',
            'lastPageLabel' => 'В конец',
        ]
    ]);
    \yii\widgets\Pjax::end(); 
?>
</div>
