<?php 
use app\models\Category;
use yii\helpers\Html;
use yii\widgets\ListView;
use app\components\MyLinkPager;

$this->params['breadcrumbs'] = [['label' => 'Производители', 'url' => '/brands'],
    $producer['name']];

if(isset($_GET['page']) && $_GET['page'] > 1 ){
   //$this->title .= " - Страница $_GET[page]";
}  else {

    //$this->registerMetaTag(['description' =>"У нас в интернет-магазине partsnb.ru вы можете купить любые $category->name для ноутбуков в Санкт-Петербурге. Звоните +7 (812) 920-05-75"]);
    //$this->registerMetaTag(['keywords' =>"$category->name для ноутбуков, $category->name для нетбуков, $category->name спб, $category->name санкт-петербург"]);
    
}
?>
<h2>Серии</h2>
<?php foreach ($series as $value) : ?>
<div class="list-block">
    <?php echo Html::a($value['name'], '/brands'.'/'.$producer['name_lat'].'/'.$value['name_lat']); ?>
</div>
<?php endforeach; ?>

