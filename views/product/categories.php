<?php 
use app\models\Category;
use yii\helpers\Html;
use yii\widgets\ListView;
use app\components\MyLinkPager;

$this->params['breadcrumbs'] = [['label' => 'Производители', 'url' => '/brands'],
    ['label' => $producer['name'], 'url' => '/brands/'.$producer['name_lat']],
    ['label' => $series['name'], 'url' => '/brands/'.$producer['name_lat'].'/'.$series['name_lat']],
    $model['name']];

if(isset($_GET['page']) && $_GET['page'] > 1 ){
   //$this->title .= " - Страница $_GET[page]";
}  else {

    //$this->registerMetaTag(['description' =>"У нас в интернет-магазине partsnb.ru вы можете купить любые $category->name для ноутбуков в Санкт-Петербурге. Звоните +7 (812) 920-05-75"]);
    //$this->registerMetaTag(['keywords' =>"$category->name для ноутбуков, $category->name для нетбуков, $category->name спб, $category->name санкт-петербург"]);
    
}
?>
<h2>Категории</h2>
<?php foreach ($categories as $value) : ?>
<div class="list-block">
    <?php echo Html::a($value['name'], '/'.$value['name_lat'].'/'.$producer['name_lat'].'/'.$series['name_lat'].'/model/'.$model['name']); ?>
</div>
<?php endforeach; ?>

