<?php

//    $this->title = $category->name.' для ноутбуков и нетбуков '.$producer->name.' в Санкт-Петербурге ';
    $this->title = "Найденные товары";
    
    if(!$serie){
        $this->params['breadcrumbs'] = array('Найденые товары');
    }else{
        $this->params['breadcrumbs'] = [
                ['label' => $category->name, 'url' => "/".$category->name_lat],
                ['label' => $producer->name, 'url' => "/".$category->name_lat."/".$producer->name_lat],
                ['label' => $serie->name, 'url' => "/".$category->name_lat."/".$producer->name_lat."/".$serie->name_lat],
                $model
        ];
    }
    

?>
<div class="browse-view">
    <h1>По вашему поисковому запросу результатов не найдено.</h1>
    <?php //= "Товар не найден в разделе '{$category}' производителя '{$producer}' и серии '{$serie}' при поиске модели '{$model}' " ?>
</div>