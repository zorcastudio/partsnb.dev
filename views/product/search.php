<?php
    use yii\widgets\ListView;
    use yii\helpers\Html;
    use app\components\MyLinkPager;
    
    $this->title = "Найденные товары";
    $this->params['breadcrumbs'] = array('Найденые товары');
?>

<div class="browse-view">
    <h1>Результаты поиска по запросу - <?= preg_replace('/c=\d+$/iu', '', $search) ?></h1>
</div>

<div class="filter-result">	
<?php if($full) : ?>
    <?php
    \yii\widgets\Pjax::begin();
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => "../category/_view",
        'layout'=>"<div id='block-summary'>{summary}</div><ul class='products-list'>{items}</ul><div class='prod-paginato-case'>{pager}</div>",
        'pager' => [
            'class'=> MyLinkPager::className(),
            'options' =>['class'=>'prod-paginator'] ,
            'firstPageLabel' => 'В начало',
            'prevPageLabel' => 'Назад',
            'nextPageLabel' => 'Вперёд',
            'lastPageLabel' => 'В конец',
        ]
    ]);
\yii\widgets\Pjax::end(); 
?>
<?php else : ?>
    <div class="itemListHolder">
        <?php foreach ($category as $value) : ?>
        <span class="categoryTitle"><?= Html::a($value->name, "/search/{$value->name} {$search} $producer_id c={$value->id}") ?> -  <span class="searchCount"><?= $value->_count ?></span> результатов </span>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
</div>