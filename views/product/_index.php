<?php $this->title = $category->name; ?>
<?php $this->params['breadcrumbs'] = Category::getBreadCrumbs($category) ?>

<table class="sort-block">
    <tbody>
        <tr>
            <td style="width: 40px; padding: 0;">
                <a href="#<?php //= '/'.$category->getUrl()."/?list=1"; ?>" id="block-view" class="sort-group <?= $category->_list == 1 ? 'active' : ''; ?>"><i class="sort-01-ico"></i></a>
            </td>
            <td style="width: 20px; padding: 0;">
                <a href="#<?php //= '/'.$category->getUrl()."/?list=2"; ?>" id="list-view" class="sort-group <?= $category->_list == 2 ? 'active' : ''; ?>"><i class="sort-02-ico"></i></a>
            </td>
            <td>
                <h2><?= $category->name ?></h2>
            </td>
            <td>
                 <div class="sort-ul">
                    <div class="sort-ul">
                        <span><?php echo CHtml::activeLabel($category, '_sort'); ?></span>
                        <?php echo CHtml::dropDownList('_sort', $category, Category::itemAlias('_sort'), array('options' => [$category->_sort => ['selected' => true]], 'onchange' => 'sort(this)', 'id' => "sort", 'class' => "sort-select")); ?>
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<div class="filter-value">
    <?php
        foreach ($active_f as $key => $value) {
            $this->renderPartial('_active_filter',array('value'=>$value, "key" =>$key)); 
        }
        if(!empty($active_f)){
            echo '<div class="clear-filter">Сбросить</div>';
            echo "<div class='total-product'>Подобрано: ".$dataprovider->getTotalItemCount()." товарa из ".$count_all."</div>";
        }
    ?>
    <div class="clear"></div>
</div>

<div class="filter-result">	
    <?php
    
   
    
    
    $this->widget('bootstrap.widgets.TbListView', [
        'dataProvider' => $dataprovider,
        'template' => "\n<ul class='products-list'>{items}</ul>\n<div class='prod-paginato-case'>{pager}</div>",
        'itemView' => ($category->_list == 1) ? '_view' : '_block',
        'ajaxUpdate'=>false,
        'ajaxUrl' => 'test', 
        'pager' => [
            'class'=>'LinkPager',
            'firstPageLabel' => '&#171;',
            'prevPageLabel' => '&#60;',
            'nextPageLabel' => '&#62;',
            'lastPageLabel' => '&#187;',
            'header' => false,
            'htmlOptions' => ['class' => 'prod-paginator'],
//            'ajaxUrl'=>$this->createUrl('site/lastPosts')
        ],
    ]);
    ?>
</div>