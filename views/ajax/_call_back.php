<?php use yii\helpers\Html;?>
<h2>Заказан обратный звонок с сайта <?= $data['host'] ?></h2>
<p>О пользователе</p>
<table>
    <tbody>
        <tr>
            <th>Время звказа</th>
            <td><?= date('d.m.Y H:i') ?></td>
        </tr>
        <tr>
            <th>Имя</th>
            <td><?= $data['name'] ?></td>
        </tr>
        <tr>
            <th>Телефон</th>
            <td><?= $data['phone'] ?></td>
        </tr>
        <? if(isset($data['city'])) { ?>
        <tr>
            <th>Страна</th>
            <td><?= $data['country']['name_ru'] ?></td>
        </tr>
        <tr>
            <th>Область</th>
            <td><?= $data['region']['name_ru'] ?></td>
        </tr>
        <tr>
            <th>Город</th>
            <td><?= $data['city']['name_ru'] ?></td>
        </tr>
        <? } ?>
        <tr>
            <th>С какой страницы</th>
            <td><?= Html::a('Страница', $data['link']); ?></td>
        </tr>
    </tbody>
</table>

