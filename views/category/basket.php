<div class="center-case">
    <h1>Корзина</h1>
    <table cellpadding="0" cellspacing="0" class="basket_btl">
        <tbody>
            <tr>
                <th class="td-pic">&nbsp;</th>
                <th class="td-name">Название</th>
                <th class="td-empty"></th>
                <th class="td-price">Цена товара</th>
                <th class="td-num">Количество</th>
                <th class="td-sum">Цена</th>
                <th class="td-del">&nbsp;</th>
            </tr>
            <tr>
                <td class="td-pic"><img src="images/device01.jpg" alt=""></td>
                <td class="td-name"><a href="#" title="Телефон DFTY6775">Телефон DFTY6775</a></td>
                <td class="td-empty">&nbsp;</td>
                <td class="td-price">2 999 руб</td>
                <td class="td-num"><input type="text" name="" value="2"></td>
                <td class="td-sum">5 998 руб</td>
                <td class="td-del"><a href="#" class="delete-link">x</a></td>
            </tr>
            <tr>
                <td class="td-pic"><img src="images/device01.jpg" alt=""></td>
                <td class="td-name"><a href="#" title="Телефон DFTY6775">Телефон DFTY6775</a></td>
                <td class="td-empty">&nbsp;</td>
                <td class="td-price">2 999 руб</td>
                <td class="td-num"><input type="text" name="" value="2"></td>
                <td class="td-sum">5 998 руб</td>
                <td class="td-del"><a href="#" class="delete-link">x</a></td>
            </tr>
        </tbody>
    </table>
    <div class="basket_nav">
        <div class="basket_nav_left"><a href="#"><i class="back-ico"></i>Вернуться в магазин</a></div>
        <div class="basket_nav_right"><a href="#"><i class="reload-ico"></i>Обновить корзину</a></div>
        <div class="clear"></div>
    </div>
    <div class="basket_sum">
        <span>ИТОГО:</span>
        <span class="basket_itog_sum">13 333 руб.</span>
    </div>
    <a href="#" class="basket_but_link">
        <button class="basket_but">Оформить заказ</button>
    </a>
</div>