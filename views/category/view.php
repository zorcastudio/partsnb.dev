<?php $this->title = $model->name; ?>
<?php $this->params['breadcrumbs'] = $model->getBreadCrumbs()?>

<script type="text/javascript" src="<?php echo Yii::$app->request->baseUrl; ?>/js/unitegallery/jquery-11.0.min.js.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::$app->request->baseUrl; ?>/js/unitegallery/unitegallery.js"></script>
<script type="text/javascript" src="<?php echo Yii::$app->request->baseUrl; ?>/js/unitegallery/ug-theme-default.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl; ?>/js/unitegallery/unite-gallery.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl; ?>/js/unitegallery/ug-theme-default.css" />

<h1><?= $model->name ?></h1>
<div class="product-pics">
    <div id="gallery" class="thumb" style="display:none;">
        <img    alt="<?php //= $item['alt']  ?>"
                   src="<?= Url::toRoute('/uploadfile/images/product/'.$model->logo) ?>"
                   data-image="<?= Url::toRoute('/uploadfile/images/product/'.$model->logo) ?>"
                   data-description="<?php //= $item['alt']  ?>"/>
        
        <?php foreach ($model->image as $item) : ?>
           <img    alt="<?php //= $item['alt']  ?>"
                   src="<?= Url::toRoute('/uploadfile/images/product/'.$item->url) ?>"
                   data-image="<?= Url::toRoute('/uploadfile/images/product/'.$item->url) ?>"
                   data-description="<?php //= $item['alt']  ?>"/>
         <?php endforeach; ?>

    </div>
               <div class="clear"></div>
</div>
<div class="product-info">	
    <?= $model->description ?>
    <div class="more-info">
        <p><b>Наличие на складе </b><?= ($model->available == 1) ? "<span class='check-ok'>✔&nbsp;Есть на складе</span>" : "<span class='check-error'>Нет на складе</span>" ?> </p>
    </div>
    <div class="clear"></div>
    <div class="product-kol">
    
    <div class="view-checkout">
        <div class="add-prod-link <?= $model->getBasket() ? 'input' : '' ?>" id="<?= "prod_" . $model->id ?>">
            <span>Купить</span>
            <div class="amount-view"><?= $model->getBasket() ? $model->getBasket()->quantity . ' шт.' : '' ?></div>
            <div class="amount <?= (($model->getBasket()) ? $model->getBasket()->quantity : false) ? '' : 'none' ?>">
                <div class="add" id="<?= $model->id ?>" onclick="setProduct(this)" ></div>
                <div class="remuve" id="<?= $model->id ?>" onclick="setProduct(this)"></div>
            </div>
            <a class="add" name='bottom' id="<?= $model->id ?>" onclick="setProduct(this)"></a>
        </div>
        <div class="price"><?= $model->price.' р.' ?></div>
        <div class="clear"></div>
    </div>

        
        
        
    </div>
    <div class="clear"></div>
</div>
<script type="text/javascript">
    JQ("#gallery").unitegallery();
</script>
