<?php use yii\bootstrap\Modal; ?>
<li>
    <a href="<?= $model->url ?>" class="link-product" data-pjax=0>
        <p><?= $model->serchName ?></p>
        <div class="prod-pic">
            <div class="logo-view" style="background: url(/uploadfile/images/product/resized/<?= $model->logo ?>) 50% 50% / contain no-repeat;"></div>
        </div>
    </a>
    <div class="basket-product">
        <div class="prod-price">
            <?= $model->price . " руб"; ?>
        </div>
            <?php
            if($model->available > 0){
                Modal::begin([
                    'toggleButton' => ['label' => 'В корзину', 'class' => "add", 'id' => $model->id, 'onclick' => 'setProduct(this)'],
                ]);
                echo $this->render('../product/_modal', ['text' => $model->name . " добавлен в корзину"]);
                Modal::end();
            }else{
                if($model->is_timed == 1){
                    echo '<span title="Ожидается">Ожидается '.$model->delivery_date.'</span>';
                }else{
                    echo '<span title="Нет в наличии">Нет в наличии</span>';
     }
            }
            ?>
        <div style='clear: both;'></div>
        <div class="opt-price-box">
            <span class="opt-price"><?= $model->trade_price3 . " р." ?></span> 
            <?php
                Modal::begin([
                    'toggleButton' => [
                        'tag'=>'a',
                        'label' => 'C максимальной скидкой', 
                        'class' => "modal-link-opt-price"
                    ],
                ]);
                echo $model->discount;
//                echo $this->render('../product/_wholesale_prices');
                Modal::end();
            ?>
        </div>
    </div>
</li>