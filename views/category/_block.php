<li class="list-block">
    <a href="<?= ($model->category) ? "/".$model->category->getUrl().'/'.$model->name_lat : '' ?>" class="link-product">
        <div class="prod-pic">
            <div class="logo-view" style="background: url(/uploadfile/images/product/<?= $model->logo ?>) 50% 50% / contain no-repeat;"></div>
        </div>
        <h3><?= $model->name ?></h3>
    </a>
    <div class="description-short"><?= $model->description_short ?></div>
    <div class="wraper-list">
        <div class="prod-price">
            <?= number_format($model->price, 2, '.', ' ') . " р." ?>
        </div>
        <div class="add-prod-link <?= $model->getBasket() ? 'input' : '' ?>" id="<?= "prod_" . $model->id ?>">
            <span>Купить</span>
            <div class="amount-view"><?= $model->getBasket() ? $model->getBasket()->quantity . ' шт.' : '' ?></div>
            <div class="amount <?= (($model->getBasket()) ? $model->getBasket()->quantity : false) ? '' : 'none' ?>">
                <div class="add" id="<?= $model->id ?>" onclick="setProduct(this)" ></div>
                <div class="remuve" id="<?= $model->id ?>" onclick="setProduct(this)"></div>
            </div>
            <a class="add" name='bottom' id="<?= $model->id ?>" onclick="setProduct(this)"></a>
        </div>
    </div>
</li>