<?php
use kartik\nav\NavX;
use yii\helpers\Html;
use yii\helpers\Url;

use yii\jui\AutoComplete;
use yii\web\JsExpression;
?>

<div id="navigation">
    <ul class="menu-goriz">
        <?= \app\models\Page::getMenuTopHtml() ?>
    </ul>
    <?php 
        $category_id =  isset($_GET['category']) ? $_GET['category'] : null;
        $producer_id = isset($_GET['producer']) ? $_GET['producer'] : null;
        $serie_id = isset($_GET['serie']) ? $_GET['serie'] : null;
    ?>
    <div id="for-serch">
        <div class="serch-block">
            <noindex>
                <?php 
                   echo Html::dropDownList(
                       'category_id', 
                       $category_id, 
                       \app\models\Category::grtCategoryArray(),
                       [
                           'id'=>'filter-category',
                           'prompt'=>'Выберите категорию',
                           'onchange'=>"overload(this)"
                       ]
                   ); 
               ?>
            </noindex>
        </div>
        <div class="serch-block">
            <noindex>
                <?= Html::dropDownList(
                        'producer_id', 
                        $producer_id, 
                        \app\models\Producer::grtProducerArray(),
                        [
                            'id'=>'filter-producer',
                            'prompt'=>'Выберите производителя',
                            'onchange'=>"overload(this)",
                            'disabled'=>  ($category_id) ? false : "disabled",
                            'style'=>($category_id) ? null : 'background-color: #EBEBE4;',
                        ]
                        ); 
                ?>
            </noindex>
        </div>
        <div class="serch-block">
            <noindex>
                <?= Html::dropDownList(
                        'serie_id', 
                        $serie_id, 
                        \app\models\Serie::getSerieArray($category_id, $producer_id),
                        [
                            'id'=>'filter-serie',
                            'prompt'=>'Выберите серию',
                            'onchange'=>"overload(this)",
                            'disabled'=>  ($category_id && $producer_id) ? false : "disabled",
                            'style'=>($category_id && $producer_id) ? null : 'background-color: #EBEBE4;',
                        ]
                        ); 
                ?>
            </noindex>
        </div>
        <div class="serch-block ajax-input">
            <script type="text/javascript">
                var url = '/ajax/search_model'+'<?= "?category_id=".$category_id."&producer_id=".$producer_id."&serie_id=".$serie_id ?>';
            </script>
            <?php
                echo AutoComplete::widget([
                    'name' => 'model_lat',
                    'value' => isset($_GET['model']) ? $_GET['model'] : null,
                    'id' => "filter-model",
                    'options' => [
                        'placeholder' => 'Поиск по моделям',
                        'onblur' => "javascript:if(this.value=='')this.value=''",
                        'onfocus' => "javascript:if(this.value=='') this.value=''",
                        'disabled'=>  ($category_id && $producer_id && $serie_id) ? false : "disabled",
                    ],
                    'clientOptions' => [
                        'class'=>'input-model',
                        'select' => new JsExpression("function( event, ui ) {
                                        $('#user_company').val(ui.item.id);
                                        if (ui.item.value) location = '/$category_id/$producer_id/$serie_id/model/'+ui.item.value; return false;
                                    }"),
                        'source' => new JsExpression("url"),
                        'autoFill' => true,
                        'minLength' => '0',
                    ],
                ]);
            ?>
            
        </div>
        <?= Html::button('Подбор', ['class' => 'selection-model', 'onclick'=>'selectionModel()']) ?>
    </div>
</div>
