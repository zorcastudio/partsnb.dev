<?php
use app\models\Page;
use yii\helpers\Html;
?>

<futer>
    <div id = "menu-futer">
        <ul>
            <?php foreach (Page::getMenuFuter() as $value) : ?>
                <li><?php echo Html::a($value['name'], $value['url']); ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div id="copyright">
        <p>PartsNB. <?= date('Y') ?>. &#169; Все права защищены.</p>
    </div>
    <div class="info-site">
        Обращаем Ваше внимание на то, что вся информация, размещенная на настоящем интернет-сайте, носит исключительно информационный характер и ни при каких условиях не являются публичной офертой, определяемой положениями Статьи 437 Гражданского кодекса Российской Федерации. Для получения точной информации о наличии товара, пожалуйста, обращайтесь к менеджерам интернет-магазина по телефону (911) 920-05-75
    </div>
    <div id="ya-market">
        <a href="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2508/*http://market.yandex.ru/shop/222450/reviews">
            <img alt="Читайте отзывы покупателей и оценивайте качество магазина на Яндекс.Маркете" border="0" height="31" src="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=2505/*https://grade.market.yandex.ru/?id=222450&amp;action=image&amp;size=0" width="88">
        </a>
    </div>

<link rel="stylesheet" href="//callbackkiller.ru/widget/cbk.css">
<script type="text/javascript" src="//callbackkiller.ru/widget/cbk.js" charset="UTF-8"></script>
<script type="text/javascript">var callbackkiller_code="4345";</script>

<!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter22143797 = new Ya.Metrika({id:22143797,
                            webvisor:true,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true});
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/22143797" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</futer>


