<?php
use yii\helpers\Url;
    
$price = $this->context->specific_data['summ_prise']['summ'];
$count = $this->context->specific_data['summ_prise']['count'];
$count_one = $this->context->specific_data['summ_prise']['count_one'];
?>

<div class="bottom-basket <?= (1*$price) ? 'block' : 'none' ?>">
    <div class="wrapper">
        <nav>
            <ul class="basket-menu">						
                <li class="bold">Количество товаров : <span class="basket-count"><?= $count ?></span> шт.</li>
                <li class="bold">Наименований : <span class="basket-count-one"><?= $count_one ?></span> шт.</li>
                <li><a href="<?=  Url::toRoute('/basket') ?>">Корзина</a></li>
                <li>
                    <a class="basket-sum" href="<?= Url::toRoute('/basket') ?>">
                        <?= $price." p." ?>
                    </a>
                </li>
                <li><a href="<?= Url::toRoute('/order') ?>">Оформить заказ</a></li>
            </ul>
        </nav>

    </div>
</div>