<?php use yii\helpers\Html;?>
<div class="news">
    <h3>Новости</h3>
    <?php foreach ($model as $value) : ?>
    <div class="new-block">
        <div class="date-new"><?= date('d.m.Y',$value['createtime']); ?></div>
        <?= Html::a($value['name'], "/novosti/".$value['name_lat']) ?>
        <div class="short-new">
            <?= $value['text_short'] ?>
        </div>
        <div class="read-more">
            <?= Html::a('Подробнее...', "/novosti/".$value['name_lat']) ?>
        </div>
    </div>
    <?php endforeach; ?>
    <?= Html::a('Архив новостей >', "/novosti/", ['class'=>'news-archive']) ?>
</div>