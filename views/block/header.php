<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use app\models\Page;
use app\models\User;
use app\models\Category;
use yii\bootstrap\Modal;


Modal::begin([
    'id' => 'callBack',
    'closeButton'=>false,
    'options'=>[
        'top'=>'20%',
    ]
]);
?>

<div id="form_landing">
    <span>Заполните все поля, пожалуйста</span>
    <input name="name" placeholder="Ваше Имя" id="form_name" class>
    <input name="phone" placeholder="Телефон" class="phone-callBack" id="form_phone" class>
    <div class="mask" id="mask-igbxtvsj" style="display: none;"></div>
    <a class="btn btn-block btn-default" id="form_send" href="#">Отправить</a>
</div>
<?php Modal::end();

$count = $this->context->specific_data['summ_prise']['count'];
?>
<header>
    <div>
        <div class="logo">
            <div class="custom">
                <p>
                    <a href="/"><img alt="" src="/images/view/logo.jpg"></a>
                </p>
            </div>
        </div>

    </div>
    <div class = "head-address">
        <div id="kartap-image">
            <div class="img"></div>
        </div>
        <div class="kartap-info">
            Санкт-Петербург<br>
            <?= Html::a('м. Садовая', '/kontakty/shema-proezda-k-magazinu-m-sadovaya'); ?>
            <br>БЦ МИР<br>
            тел. +7 (812) 922-05-70
        </div>
    </div>
    <div class = "head-address next">
        <div id="kartap-image">
            <div class="img"></div>
        </div>
        <div class="kartap-info">
            Санкт-Петербург<br>
            <?= Html::a('м. Бухарестская', '/kontakty/shema-proezda-k-magazinu-m-bukharestskaya'); ?>
            <br>ТРК "Континент"<br>
            тел. +7 (812) 920-05-75
        </div>
    </div>
    <div id="enter">
        <div class="regions_phone" data-toggle="modal" data-target="#callBack">
            <div class="region_icon">
                &nbsp;</div>
            <div>ЗАКАЗАТЬ<br>ОБРАТНЫЙ ЗВОНОК</div>
        </div>
        <?php if (Yii::$app->user->isGuest) : ?> 
            <div class="logout">
                <?= Html::a('Вход', '/login')." | " . Html::a('Регистрация', '/registration'); ?>
            </div>
        <?php else : ?>
            <div class="logout">
                <?= User::isAdmin() ? Html::a('Админ панель', '/admin')." | " : Html::a('Личный кабинет', '/user'). " | "; ?>
                <?= Html::a('Выход', '/logout'); ?>
            </div>
        <?php endif; ?>
    </div>
</header>
<div id="searh">
    <div class="searh-input">
        <?php
        echo AutoComplete::widget([
            'name' => 'text',
            'value' => isset($_GET['search']) ? trim(preg_replace('/(p=([\d,]+))?( c=\d+)?$/u', '', $_GET['search'])) : null,
            'id' => "search-in",
            'options' => [
                'placeholder' => 'Поиск по моделям, совместимым моделям, партномерам',
                'onblur' => "javascript:if(this.value=='')this.value=''",
                'onfocus' => "javascript:if(this.value=='') this.value=''",
                'class' => 'inp',
            ],
            'clientOptions' => [
                'select' => new JsExpression("function( event, ui ) {
                                    if (ui.item.value) location = ui.item.value; return false;
                                }"),
                'source' => Url::to(['/ajax/search']),
                'autoFill' => true,
                'minLength' => '0',
            ],
        ]);
        ?>
        <div onclick="Search(this)" class="vert">Найти</div>
    </div>
    <div id="basket">
        <div class="basket-img">
            <a href="<?=  Url::toRoute('/basket') ?>"><img alt="" src="/images/view/kor.png"></a>
        </div>
        <div class="basket-info">
            <?php if($count > 0) : ?>
                <div class="total-products">
                        <?php if($count == 1) : ?>
                            <div class="basket-count"><?= $count." " ?> </div>
                            <span><?= " Товар" ?></span>
                        <?php else : ?>
                            <span><?= "Товаров в корзине " ?></span>
                            <div class="basket-count"><?= $count ?></div>
                        <?php endif; ?>
                </div>
                <div class="custom">
                    <?=  Html::a('Оформить заказ', Url::toRoute('/order'));?>
                </div>
                <?php else : ?>
                <div class="total-products">Корзина пуста</div>
            <?php endif; ?>
            <div style="clear:both;"></div>
        </div>
    </div>
</div>


