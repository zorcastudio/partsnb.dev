<?php 
    use yii\helpers\VarDumper;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\jui\AutoComplete;
    use yii\web\JsExpression;
    
    use app\models\Page;
    use app\models\User;
    use app\models\Category;
 ?>
<header>
    <div id="art-main">
            <div class="art-sheet clearfix">
                <header class="art-header">
                    <div id="s61">
                        <div class="moduletable">
                            <div class="custom">
                                <p>
                                    <a href="/"><img alt="" src="/templates/xlandgroup/images/logo.png"></a></p>
                            </div>
                        </div>
                    </div>
                    <div id="s62">
                        <div class="moduletable">
                            <div class="custom">
                                <div class="kartap8">
                                    <div class="kartap">
                                        <div id="kartap11">
                                            <img alt="" src="/images/k.png" style="float: left;"></div>
                                        <div class="kartap21">
                                            Санкт-Петербург<br>
                                            <a href="/kontakty/shema-proezda-k-magazinu-m-bukharestskaya">м. Бухарестская</a><br>
                                            ТРК "Континент"<br>
                                            тел. +7 (812) 920-05-75</div>
                                    </div>
                                </div></div>
                        </div>
                    </div>
                    <div id="s64">
                        <div class="moduletable">
                            <div class="custom">
                                <div class="sign">
                                    <div class="sign1">
                                        <img alt="" src="/images/kluch.png" style="margin-left: 5px; margin-right: 5px;"></div>

                                    <div class="sign2">
                                        <a href="/component/users/?view=login">Вход</a></div>

                                    <div class="sign3">
                                        |</div>

                                    <div class="sign4">
                                        <a href="/component/users/?view=registration">Регистрация</a></div>
                                </div></div>
                        </div>
                    </div>
                </header>
                <div id="s4">
                    <div id="s7">
                        <div class="moduletables">
                            <!--BEGIN Search Box -->
                            <form action="/component/virtuemart/search" method="get">
                                <div class="searchs">
                                    <input name="keyword" id="sv" maxlength="100" alt="Найти" class="inputboxs" type="text" size="100" value="Поиск по моделям, совместимым моделям, партномерам" onblur="if (this.value == '')
            this.value = 'Поиск по моделям, совместимым моделям, партномерам';" onfocus="if (this.value == 'Поиск по моделям, совместимым моделям, партномерам')
                        this.value = '';"><input type="image" value="Найти" class="vert" src="https://partsnb.ru/components/com_virtuemart/assets/images/vmgeneral/search.png" onclick="this.form.keyword.focus();"></div>
                                <input type="hidden" name="limitstart" value="0">
                                <input type="hidden" name="option" value="com_virtuemart">
                                <input type="hidden" name="view" value="category">

                            </form>
                        </div>
                    </div>
                    <div id="s8">
                        <div id="s70">
                        </div>
                        <div id="s71">
                            <div class="moduletable">
                                <div class="custom cart-module-img">
                                    <p>
                                        <a href="/component/virtuemart/cart?Itemid=0"><img alt="" src="/images/kor.png"></a>
                                    </p>
                                </div>
                                <div class="vmCartModule " id="vmCartModule">
                                    <div class="total_products">Корзина пуста</div>
                                    <div class="custom">
                                        <p><a href="/component/virtuemart/cart?Itemid=0">Оформить заказ</a></p>
                                    </div>
                                    <div style="clear:both;"></div>
                                    <noscript>
                                    Пожалуйста, подождите</noscript>
                                </div>
                            </div>
                        </div>
                    </div></div>
                <div id="s72">
                    <div class="moduletablegg">
                        <ul class="menugoriz" id="g"><li class="item-125 current active"><a href="/">Главная</a></li><li class="item-126"><a href="/o-magazine">О магазине</a></li><li class="item-127"><a href="/vybor-tovara">Выбор товара</a></li><li class="item-128 deeper parent"><a href="/informatsiya">Информация</a><ul><li class="item-164"><a href="/informatsiya/garantiya">Гарантия</a></li><li class="item-165"><a href="/informatsiya/postoyannym-klientam">Постоянным клиентам</a></li><li class="item-171"><a href="/informatsiya/dostavka-i-oplata">Доставка и оплата</a></li></ul></li><li class="item-199"><a href="/kontakty">Контакты</a></li><li class="item-179"><a href="/otzyvy">Отзывы</a></li></ul>		</div>
                </div>
            </div>
        </div>
</header>