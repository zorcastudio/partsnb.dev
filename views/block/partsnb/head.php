<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>

<base href="https://partsnb.ru/">
<meta charset="<?= Yii::$app->charset ?>"  http-equiv="content-type" content="text/html; charset=utf-8">
<title><?= Html::encode($this->title) ?></title>
<link href="/?format=feed&amp;type=rss" rel="alternate" type="application/rss+xml" title="RSS 2.0">
<link href="/?format=feed&amp;type=atom" rel="alternate" type="application/atom+xml" title="Atom 1.0">
<link rel="stylesheet" href="/cache/widgetkit/widgetkit-92c876ad.css" type="text/css">
<link rel="stylesheet" href="/components/com_virtuemart/assets/css/jquery.fancybox-1.3.4.css" type="text/css">
<link rel="stylesheet" href="/components/com_virtuemart/assets/css/vmsite-ltr.css" type="text/css">
<script type="text/javascript" async="" src="//cs15.livetex.ru/js/client.js"></script><script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript" async="" src="http://mc.yandex.ru/metrika/watch.js"></script><script src="/components/com_onepage/assets/js/opcping.js?opcversion=2_0_274_021214" type="text/javascript"></script>
<script src="/media/system/js/mootools-core.js" type="text/javascript"></script>
<script src="/media/system/js/core.js" type="text/javascript"></script>
<script src="/media/system/js/caption.js" type="text/javascript"></script>
<script src="/media/widgetkit/js/jquery.js" type="text/javascript"></script>
<script src="/cache/widgetkit/widgetkit-e6e5e54a.js" type="text/javascript"></script>
<script src="/media/system/js/mootools-more.js" type="text/javascript"></script>
<script src="/components/com_virtuemart/assets/js/vmsite.js" type="text/javascript"></script>
<script src="/components/com_virtuemart/assets/js/fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
<script src="/components/com_virtuemart/assets/js/vmprices.js" type="text/javascript"></script>
<script type="text/javascript">
    window.addEvent('load', function () {
        new JCaption('img.caption');
    });
//<![CDATA[ 
    vmSiteurl = 'https://partsnb.ru/';
    vmLang = '&amp;lang=ru';
    Virtuemart.addtocart_popup = '1';
    usefancy = true;
//]]>

</script>
<link rel="stylesheet" href="https://partsnb.ru/modules/mod_junewsultra/tmpl/default/css/style.css" type="text/css">

<link rel="alternate" hreflang="ru-RU" href="https://partsnb.ru/">
<link rel="stylesheet" href="/templates/xlandgroup/css/custom.css">
<link rel="stylesheet" href="/templates/system/css/system.css">
<link rel="stylesheet" href="/templates/system/css/general.css">

    <!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<link rel="stylesheet" href="/templates/xlandgroup/css/template.css?v22222" media="screen">
<!--[if lte IE 7]><link rel="stylesheet" href="/templates/xlandgroup/css/template.ie7.css" media="screen" /><![endif]-->
<link rel="stylesheet" href="/templates/xlandgroup/css/template.responsive.css" media="all">

<script>if ('undefined' != typeof jQuery)
            document._artxJQueryBackup = jQuery;</script>
<script src="/templates/xlandgroup/jquery.js"></script>
<script>jQuery.noConflict();</script>

<script src="/templates/xlandgroup/script.js"></script>
<script>if (document._artxJQueryBackup)
            jQuery = document._artxJQueryBackup;</script>
<script src="/templates/xlandgroup/script.responsive.js"></script>
<script src="/templates/xlandgroup/js/jquery.leanModal.min.js"></script>

<link href="http://fonts.googleapis.com/css?family=Exo+2:100,300,600,400&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="/favicon.png" type="image/png">
<link rel="icon" href="/favicon.png" type="image/png">

<script id="SCR_UID_ID">window.webArchive=window.webArchive||{};window.webArchive.active=true;window.webArchive.UID = "143229735079218225"</script><style type="text/css"></style><link type="text/css" href="//web-client.livetex.ru/csscontroller/cssbuttons/89821.css" rel="stylesheet"><link type="text/css" href="//web-client.livetex.ru/csscontroller/cssinvite/89821.css" rel="stylesheet"><link type="text/css" href="//web-client.livetex.ru/css/predict.css" rel="stylesheet"><link type="text/css" href="//web-client.livetex.ru/css/xButton.css" rel="stylesheet"><link type="text/css" href="//web-client.livetex.ru/css/font-awesome.css" rel="stylesheet"></head>

<?= Html::csrfMetaTags() ?>

<?php $this->head() ?>
<script type="text/javascript" src="<?= Url::base(); ?>/js/myjs.js"></script>
