<?php
    use app\models\Category;
    use kartik\sidenav\SideNav;
    use app\models\ProductFilterValue;
?>
<div class="left-sidebar">
        <span class="h2-span">Каталог товаров</span>
        <div class="left-menu-kat">
            <div id="category_tree">
                    <?php 
                    echo  SideNav::widget([
                        'type' => SideNav::TYPE_WARNING,
                        'items' => Category::getList()
                    ]);
                    ?>
            </div>
        </div>
    <?php if(\app\models\Page::isLookFilter()) : ?>
        <?php 
            $filter = ProductFilterValue::getFilterOut($_GET);
            if($filter) :
        ?>
            <div class="filter">
                <p>Фильтры</p>
                <?php foreach ($filter as $key => $value) : ?>
                    <div class="filter-group">
                        <div class="filter-group-name"><?= $value['filter']['name'] ?></div>
                        <div class="filter-group-content">
                            <div class="sliding none"></div>
                            <ul class="filter-ul" id='<?= $value['filter']['name_lat'] ?>'>
                                <?php foreach ($value['value'] as $k => $val) : ?>
                                    <li>
                                        <label id='<?=  $val['name_lat']?>' class="set-filter">
                                            <input type="checkbox" <?= isset($val['checked']) ? 'checked="checked"' : null ?>>
                                            <span class="fil-name"><?= $val['name']; ?></span>
                                            <span class="fil-num">(<?= $val['count']; ?>)</span>
                                        </label>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>
        <?php if(Yii::$app->request->url === "/") : ?>
            <?= $this->render('../block/_novosti', array("model" => $this->context->specific_data['novosti'])); ?>
        <?php endif; ?>
</div>