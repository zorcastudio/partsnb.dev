<?php 
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<meta charset="<?= Yii::$app->charset ?>">
<?= Html::csrfMetaTags() ?>
<title><?= Html::encode($this->title) ?></title>
<link rel="icon" href="/favicon.png" type="image/png">
<link rel="alternate" href="<?= Url::to('',true) ?>" hreflang="ru-RU" />
<?php $this->head() ?>
<!-- Google Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-68410651-1', 'auto');
    ga('send', 'pageview');
</script>
<!-- /Google Analytics -->

