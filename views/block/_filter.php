<div class="filter-group">
    <div class="filter-group-name"><?= $model->name ?></div>
    <div class="filter-group-content">
        <div class="sliding none"></div>
        <ul class="filter-ul" id='<?= $model->id ?>'>
            <?php foreach ($model->filterValue as $value) : ?>
                <li>
                    <label id='<?= $value->filter_value_id ?>' class="set-filter">
                        <input type="checkbox" <?= $value->checked ? 'checked="checked"' : null ?>>
                        <span class="fil-name"><?= $value->name; ?></span>
                        <span class="fil-num">(<?= $value->_product_count; ?>)</span>
                    </label>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>