<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin([
    'brandLabel' => 'My Company',
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        ['label' => 'Home', 'url' => ['/page/index']],
        ['label' => 'About', 'url' => ['/page/about']],
        ['label' => 'Contact', 'url' => ['/page/contact']],
        Yii::$app->user->isGuest ?
                ['label' => 'Login', 'url' => ['/page/login']] :
                ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
            'url' => ['/page/logout'],
            'linkOptions' => ['data-method' => 'post']],
    ],
]);
NavBar::end();
