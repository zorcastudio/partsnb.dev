<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
        'fieldConfig' => [
            'template' => "<div class=\"form-label\">{label}<span>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
        ]
    ]);
?>
<?= $form->field($model, 'sety')->dropDownList([0=>'Vk', 1=>'facebook']);?>

<?= $form->field($model, 'theme')->textInput(); ?>

<?= $form->field($model, 'message')->textarea();?>
<?php 
    echo Html::submitButton('Отправить'); 
    ActiveForm::end(); unset($form); 
    
// phpinfo();
?>
