<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = isset($name) ? $name : '';
?>
<div class="site-error">
    <?= Html::img('/images/view/404_error.jpg') ?>
</div>
