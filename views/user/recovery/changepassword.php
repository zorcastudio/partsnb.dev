<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;

$this->title=Yii::$app->name . ' - Изменить пароль';

$this->params['breadcrumbs'] = [
    ['label' => "Login", 'url' => ['/user/login']],
    'Изменить пароль'
];

if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};

?>

<h1><?php echo "Изменить пароль"; ?></h1>
<div id = "changepassword">
    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
        <p class="note">Обязательные поля <span class="required">*</span></p>
        <?= $form->field($form2, 'password')->passwordInput(); ?>
        <?= $form->field($form2, 'verifyPassword')->passwordInput(); ?>
        <div class="row submit">
            <?php echo Html::submitButton("Обновить"); ?>
        </div>
    <?php echo Html::endForm(); ?>
</div>