<?php

use yii\bootstrap\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::$app->name . ' - ' . 'Восстановить';
$this->params['breadcrumbs'] = [
    ['label' => 'Вход в кабинет', 'url' => ['/user/login']],
    "Восстановить"
];
?>

<h1><?php echo 'Восстановить'; ?></h1>

<?php
if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};
$form = ActiveForm::begin(['id' => 'recovery']);
?>
<table id="recovery">
    <tr>
        <td colspan="2" style="vertical-align: top;"><?php echo $form->errorSummary($model); ?></td>
    </tr>
    <tr>
        <td><?php echo $form->field($model, 'login_or_email')->textInput(); ?></td>
        <td style="vertical-align: top;padding: 31px 0 0 0;">
        <?= Html::submitButton('Восстановить'); ?><td>
    </tr>
    <tr>
        <td colspan="2"><p><?php echo "Пожалуйста, введите Ваш адрес электронной почты."; ?></p></td>
    </tr>
</table>

<?php
ActiveForm::end();
unset($form);
?>
