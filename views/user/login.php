<?php 
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\bootstrap\Alert;

?>
<h1>Вход</h1>

<?php
if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};

$form = ActiveForm::begin([
        'id' => 'login',
        "options"=>['class' => 'well'],
        'fieldConfig' => [
            'template' => "<div class=\"form-label\">{label}<span>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
        ]
    ]);
?>

<table>
    <tbody>
        <tr>
            <td colspan="2"><?= $form->errorSummary($model); ?></td>
        </tr>
        <tr>
            <td><?= $form->field($model, 'username')->textInput() ?></td>
            <td><?= $form->field($model, 'password')->passwordInput() ?></td>
        </tr>
        <tr>
            <td><?php echo $form->field($model, 'rememberMe')->checkbox(); ?></td>
            <td><?php 
                echo Html::a('Регистрация ', '/registration'); 
                echo " | ";
                echo Html::a('Забыли пароль?', '/user/recovery');
            ?></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: right"><?= Html::submitButton('Вход', ['label' => 'Вход']); ?></td>
        </tr>
    </tbody>
</table>
<?php   ActiveForm::end(); unset($form); ?>