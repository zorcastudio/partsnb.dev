<?php 
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Url;
    
    use app\models\Registration;
    use app\models\Region;
    use app\models\City;
    use app\models\Country;
    use yii\helpers\Html;
?>
<h1>Регистрация</h1>

<div id="reistration">
    <?php
        $form = ActiveForm::begin([
            'id' => 'user',
            'layout'=>'inline',
            "options"=>['class' => 'well'],
            'fieldConfig' => [
                'template' => "<div class=\"form-label\">{label}<span>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
            ]
        ]);
    ?>

<?= $form->errorSummary($model); ?>
<label id="jform_spacer-lbl" class=""><strong class="red">*</strong> Обязательное поле</label>
<br>
<br>
<?= $form->field($model, 'name')->textInput() ?>
<?= $form->field($model, 'surname')->textInput() ?>
<?= $form->field($model, 'username')->textInput() ?>
<?= $form->field($model, 'password')->passwordInput()?>
<?= $form->field($model, 'verifyPassword')->passwordInput() ?>
<?= $form->field($model, 'email')->textInput() ?>
<?= $form->field($model, 'country_id')->dropDownList(
        Country::getCountrys(),
        [   
            'onchange' => '$.post( "'.Url::toRoute('ajax/country_select').'", {country_id: $(this).val()}, setRegions)',
            'options' => [Registration::$_country_id =>  ['selected' => true]]
        ]);
?>
<?php echo $form->field($model, 'zip_code')->textInput();?>
<?= $form->field($model, 'region_id')
                    ->dropDownList(
                        ($model->country_id || Registration::$_country_id) ? Region::getRegion($model->country_id, Registration::$_country_id) : [],
                        [   
                            'onchange' => '$.post( "'.Url::toRoute('ajax/region_select').'", {region_id: $(this).val()}, setCities)',
                            'options' => [Registration::$_country_id =>  ['selected' => true]],
                            'id'=>'region', 
                            'disabled' => false, 
                            'prompt'=>'Выберите область',
                        ]
                    );
        ?>
<?= $form->field($model,'city_id')
                    ->dropDownList(
                    ($model->region_id) ? City::getCitys($model->region_id) : [],
                    [
                        'id'=>'city', 
                        'disabled' => (!$model->region_id) ? true : false,  
                        'prompt'=>'Выберите город',
                        'ajax' => [ 'type'=>'POST', 'url'=>Url::toRoute('ajax/OnCitySelect'), //url to call
                            'data'=>['city_id'=>"js: $(this).val()"],
                            'success'=>'js: function(data) {
                                setCities(data)
                            }'
                        ],
                    ]
        );?>
<?= $form->field($model, 'address')->textarea();?>
<div style="clear: both; height: 20px;"></div>
<?= Html::submitButton('Регистрация', ['label' => 'Регистрация']); ?>

<?php
    ActiveForm::end();
    unset($form);
?>
</div>