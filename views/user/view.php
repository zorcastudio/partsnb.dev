<?php 
use yii\widgets\DetailView;
use app\models\Order;

$this->title = 'Заказ №'.$model->id; 
$this->params['breadcrumbs']  = [
        ['label' => 'Личный кабинет', 'url' => ['/user']],
        ['label' => 'Мои заказы', 'url' => ['/user/order']],
        $this->title,
    ]; 
 ?>

<?php 
echo DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            [
                'attribute'=>'createtime',
                'value'=>date(Yii::$app->params['dateFormat'], $model->createtime)
            ],
            [
                'attribute'=>'status',
                'value'=> Order::itemAlias('status', $model->status),
            ],
            [
                'attribute'=>'delivery_id',
                'value'=> $model->delivery ? $model->delivery->name." ".$model->delivery->price.' руб.' : null
            ],
            [
                'attribute'=>'payment_id',
                'value'=> \app\models\Payment::getListPayment($model->payment_id)
            ],
            [
                'attribute'=>'price',
                'value'=> $model->sumPrice
            ],
            [
                'attribute'=>'country_id',
                'value'=> $model->country ? $model->country->name : null
            ],
            [
                'attribute'=>'region_id',
                'value'=> $model->region ? $model->region->name : null
            ],
//            [
//                'attribute'=>'city_id',
//                'value'=> $model->city ? $model->city->name : null
//            ],
            [
                'attribute'=>'zip_code',
                'value'=> $model->zip_code
            ],
            [
                'attribute'=>'description',
                'value'=> $model->description ? $model->description : null
            ],
            [
                'attribute'=>'full_name',
                'value'=> $model->full_name ? $model->full_name : null
            ],
            [
                'attribute'=>'address',
                'value'=> $model->address ? $model->address : null
            ],
            'phone',
        ],
    ]);

echo $this->render('_list_product_user',array('model'=>$model->order_p));

?>