<?php 
    use yii\helpers\Url;
?>
<table cellpadding="0" cellspacing="0" class="basket_btl">
    <tbody>
        <tr>
            <th class="td-pic">&nbsp;</th>
            <th class="td-name">Название</th>
            <th class="td-price">Цена товара</th>
            <th class="td-num">Количество</th>
            <th class="td-sum">Цена</th>
        </tr>
            <?php foreach ($model as $item) : ?>
                <?php $value = $item->product; ?>
                <tr>
                    <td class="td-pic">
                        <div class="basket-logo" style="background: url(<?= Url::toRoute('/uploadfile/images/product/resized/' . $value->logo) ?>) 50% 50% / contain no-repeat;"></div>
                    </td>
                    <td class="td-name"><a href="<?= $value->url ?>"><?= $value->name ?></a></td>
                    <td class="td-price"><?= $value->tradePrice . " р." ?></td>
                    <td class="td-num">
                        <?= $item->amount ?>
                    </td>
                    <td class="td-sum"><?= $value->getPrice($item->amount) . " руб."   //. $value->getShotName()?></td> 
                </tr>
            <?php endforeach; ?>
    </tbody>
</table>