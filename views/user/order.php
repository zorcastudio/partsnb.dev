<?php 
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\grid\GridView;

    
use app\models\Order;
use app\models\Country;

$this->title = "Мои заказы"; 
$this->params['breadcrumbs'] = [
            ['label' => 'Личный кабинет', 'url' => ['/user']],
            $this->title
        ];
?>

<?php if(Yii::$app->session->hasFlash('info')): ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-info'],
            'body' => Yii::$app->session->getFlash('info'),
        ]) ?>
<?php endif; ?>

<?php 
echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'id' => 'category-form',
//        'template' => "{pager}{items}{pager}",
        'columns' => [
//            'id',
            [
                'attribute' => 'createtime',
                'value' => function ($model) {
                        return date(Yii::$app->params['dateFormat'], $model->createtime);
                    }, 
                'filter' => false,
            ],
            [
                'attribute' => 'payment_id',
                'value' => function($model){
                    return  \app\models\Payment::getListPayment($model->payment_id);
                },
                'filter' => \app\models\Payment::getListPayment(),
            ],
            [
                'attribute' => 'price',
                'value' => function($model){
                        return number_format($model->sumPrice, 2, '.', ' ');
                    },
            ],
            [ 
                'attribute' => 'country_id',
                'value' => function($model){
                        return $model->country->name;
                    },
                'filter' => Country::getCountrys(),
            ],
//            [ 
//                'attribute' => 'region_id',
//                'value' => function($model){
//                        return $model->region ? $model->region->name : null;
//                },
//                'filter' => false,
//            ],
            [ 
                'attribute' => 'city_name',
                'value' => 'city_name',
                'filter' => false,
            ],
            [ 
                'attribute' => 'status',
                'value' => function($model){
                    return Order::itemAlias('status', $model->status);
                },
                'filter' => Order::itemAlias('status'),
            ],
                        
//            [ 
//                'attribute' => 'zip_code',
//            ],
            [
                'class'=>'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:30px;'],
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(null, $model->url, [
                                    'class'=>'glyphicon glyphicon-eye-open',                                  
                        ]);
                    },
                ],
            ]
        ],
    ]);
 ?>