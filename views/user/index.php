<?php 
use yii\bootstrap\Alert;
use yii\helpers\Url;

$this->title = "Личный кабинет"; 
$this->params['breadcrumbs'] = array(
        $this->title
    );
?>
<?php if(Yii::$app->session->hasFlash('info')): ?>
        <?= Alert::widget([
            'options' => ['class' => 'alert-info'],
            'body' => Yii::$app->session->getFlash('info'),
        ]) ?>
<?php endif; ?>

<div class="center-case">
    <h1>Здравствуйте, <?= $model->surname. ' '. $model->name ?>!</h1>

    <p><a href="<?= Url::toRoute('/user/basket') ?>" class="red_link">Мои заказы</a><br>Просмотр и управление заказами</p>
    <p><a href="<?= Url::toRoute('/user/profile') ?>" class="red_link">Мой профиль</a><br>Обновление контактной информации</p>
</div>