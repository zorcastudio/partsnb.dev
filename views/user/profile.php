<?php 
use yii\helpers\Url;
use yii\helpers\Html;
    
use yii\bootstrap\ActiveForm;

use app\models\Country;
use app\models\Region;
use app\models\City;
use app\models\Registration;

$this->title = "Мой профиль"; 
$this->params['breadcrumbs'] = array(
    ['label' => 'Личный кабинет', 'url' => ['/user']],
    $this->title
);

 $form = ActiveForm::begin([
        'id' => 'user',
        "options"=>['class' => 'well'],
        'fieldConfig' => [
            'template' => "<div class=\"form-label\">{label}<span>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
        ]
    ]);
?>
<table style="width: 100%;">
    <tr>
        <td colspan="3"><?php echo $form->errorSummary($model); ?></td>
    </tr>
    <tr>
        <td><?php echo $form->field($model, 'username')->textInput(); ?></td>
        <td><?php echo $form->field($model, 'password')->passwordInput(); ?></td>
        <td><?php echo $form->field($model, 'email')->textInput(); ?></td>
    </tr>
    <tr>
        <td><?php echo $form->field($model, 'name')->textInput(); ?></td>
        <td><?php echo $form->field($model, 'surname')->textInput(); ?></td>
        <td><?php echo $form->field($model, 'middle_name')->textInput(); ?></td>
    </tr>
    <tr>
        <td><?php echo $form->field($model, 'phone')->textInput(['maxLenght'=>13]); ?></td>
        <td></td>
        <td><?php echo $form->field($model, 'zip_code')->textInput(); ?></td>
    </tr>
     <tr>
         <td><?= $form->field($model, 'country_id')->dropDownList(
                Country::getCountrys(),
                [   
                    'onchange' => '$.post( "'.Url::toRoute('ajax/country_select').'", {country_id: $(this).val()}, setRegions)',
                    'options' => [Registration::$_country_id =>  ['selected' => true]]
                ]);
        ?></td>
        <td><?= $form->field($model, 'region_id')
                    ->dropDownList(
                        ($model->country_id || Registration::$_country_id) ? Region::getRegion($model->country_id, Registration::$_country_id) : [],
                        [   
                            'onchange' => '$.post( "'.Url::toRoute('ajax/region_select').'", {region_id: $(this).val()}, setCities)',
                            'options' => [Registration::$_country_id =>  ['selected' => true]],
                            'id'=>'region', 
                            'disabled' => false, 
                            'prompt'=>'Выберите область',
                        ]
                    );
        ?></td>
        <td><?= $form->field($model,'city_id')
                    ->dropDownList(
                    ($model->region_id) ? City::getCitys($model->region_id) : [],
                    [
                        'id'=>'city', 
                        'disabled' => (!$model->region_id) ? true : false,  
                        'prompt'=>'Выберите город',
                        'ajax' => [ 'type'=>'POST', 'url'=>Url::toRoute('ajax/OnCitySelect'), //url to call
                            'data'=>['city_id'=>"js: $(this).val()"],
                            'success'=>'js: function(data) {
                                setCities(data)
                            }'
                        ],
                    ]
        );?></td>
    </tr>
    <tr>
        <td colspan="3"><?= $form->field($model, 'address')->textarea();?></td>
    </tr>
</table>

<?= Html::submitButton('Обновить'); 

ActiveForm::end();
unset($form);
?>



