<?php 
$this->title = "Интернет-магазин запчастей и расходных материалов для ноутбуков и нетбуков - Partsnb.ru г. Санкт-Петербург"; 
$this->registerMetaTag(['keywords' =>"запчасти для ноутбуков, запчасти для нетбуков"]);
$this->registerMetaTag(['description' =>"Покупайте качественные запчасти для ноутбуков и нетбуков у нас - 8 (812) 920-05-75"]);

use yii\bootstrap\Carousel;
use yii\helpers\Html;
use yii\helpers\Url;
$scheme = Yii::$app->params['scheme'];
?>
<link rel="stylesheet" type="text/css" href="/js/slider/engine/style.css" />


<div id="wowslider-container1">
<div class="ws_images">
        <ul>
            <li>
                <a href="<?= Url::toRoute('/postoyannym-klientam', $scheme) ?>">
                    <img src="/images/slider/slide.jpg" alt="" title="" id="wows1_0"/>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute('/dostavka-i-oplata', $scheme) ?>">
                    <img src="/images/slider/slide2.jpg" alt="html slideshow" title="slide2" id="wows1_1"/>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute('/novosti/otkrytie-vtorogo-magazina', $scheme) ?>">
                    <img src="/images/slider/slide3.jpg" alt="slide3" title="slide3" id="wows1_2"/>
                </a>
            </li>
	</ul></div>
	<div class="ws_bullets">
            <div>
                <a href="#" title=""><span>1</span></a>
                <a href="#" title="slide2"><span>2</span></a>
                <a href="#" title="slide3"><span>3</span></a>
            </div>
        </div>
<div class="ws_shadow"></div>
</div>	
<script type="text/javascript" src="/js/slider/engine/wowslider.js"></script>
<script type="text/javascript" src="/js/slider/engine/script.js"></script>


<div class="carusel-tovar" id="">
    <h1>Популярные модели</h1>
    <ul class="products-list">
        <?php foreach ($popular as $data) : ?>
            <?= $this->render('/category/_view',array('model'=>$data));  ?>
        <?php endforeach; ?>
        <div class="clear"></div>
    </ul>
</div>