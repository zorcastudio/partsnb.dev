<?php 
$this->title = $model->name;
$this->params['breadcrumbs'] = $model->breadCrumbs;

$this->registerMetaTag(['keywords' =>$model->keywords]);
$this->registerMetaTag(['description' =>$model->description]);
?>
<div class="page-body">
    <h1 class="page"><?= $this->title ?></h1>
<?php echo $model->text; ?>
</div>


