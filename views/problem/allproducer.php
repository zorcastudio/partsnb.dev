
<h2>Список всех производителей и их модели</h2>

<?php 
use yii\web\JsExpression;
use wbraganca\fancytree\FancytreeWidget;

echo '<div id="tree-producer">';
echo FancytreeWidget::widget([
    'options' =>[
        'source' => $producer,
        'extensions' => ['dnd'],
        'dnd' => [
            'preventVoidMoves' => true,
            'preventRecursiveMoves' => true,
            'autoExpandMS' => 400,
            'dragStart' => new JsExpression('function(node, data) {
                return true;
            }'),
            'dragEnter' => new JsExpression('function(node, data) {
                return true;
            }'),
            'dragDrop' => new JsExpression('function(node, data) {
                data.otherNode.moveTo(node, data.hitMode);
            }'),
        ],
    ]
]);
echo "</div>";
?>