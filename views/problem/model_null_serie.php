<?php
use yii\widgets\ListView;
?>
<h1>Не нашел для этих моделей серию</h1>
<?php
echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_null_serie',
        'layout'=>"<div id='block-summary'>{summary}</div><div class='prod-paginato-case'>{pager}</div><ul class='model-list'>{items}</ul><div class='prod-paginato-case'>{pager}</div>",
        'pager' => [
           'options' =>['class'=>'prod-paginator'] ,
            'firstPageLabel' => 'В начало',
            'prevPageLabel' => 'Назад',
            'nextPageLabel' => 'Вперёд',
            'lastPageLabel' => 'В конец',
        ]
    ]);
