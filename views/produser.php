<?php 
use app\models\Category;
use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = $category->name;
$this->params['breadcrumbs'] = Category::getBreadCrumbs($category, 'category'); 
?>
<h2>Производители</h2>
<?php foreach ($producer as $value) : ?>
<div class="list-block">
    <?php echo Html::a($value->name, '/'.$category->name_lat.'/' . $value->name_lat); ?>
</div>
<?php endforeach; ?>
<div style="clear: both"></div>
</br>
</br>
<h1><?= $this->title?></h1>
<div class="filter-result">	
    <?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '/category/_view',
        'layout'=>"<ul class='products-list'>{items}</ul><div class='prod-paginato-case'>{pager}</div>",
        'pager' => [
           'options' =>['class'=>'prod-paginator'] ,
            'firstPageLabel' => 'В начало',
            'prevPageLabel' => 'Назад',
            'nextPageLabel' => 'Вперёд',
            'lastPageLabel' => 'В конец',
        ]
    ]);
    ?>
</div>