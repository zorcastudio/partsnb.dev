<?php 
$this->title = $model->name;
$this->params['breadcrumbs'] = $model->breadCrumbs;

$this->registerMetaTag(['keywords' =>$model->keywords]);
$this->registerMetaTag(['description' =>$model->description]);
?>
<!--<h1 class="page"><?= $this->title ?></h1>-->
<div class="page-body">
    <?php echo $model->text; ?>
</div>


