<?php 
use yii\helpers\Url;
use yii\helpers\Html;
?>

<table cellpadding="0" cellspacing="0" class="basket_btl">
    <tbody>
        <tr>
            <th class="td-pic">&nbsp;</th>
            <th class="td-name">Название</th>
            <th class="td-name">Артикул</th>
            <th class="td-price">Цена товара</th>
            <th class="td-num">Кол-во / Обновить</th>
            <th class="td-sum"></th>
            <th class="td-del">&nbsp;</th>
        </tr>

            <?php foreach ($model->positions as $value) : ?>
                <tr>
                    <td class="td-pic">
                        <div class="basket-logo" style="background: url(<?= Url::toRoute('/uploadfile/images/product/resized/' . $value->logo) ?>) #fff 50% 50% / contain no-repeat;"></div>
                    </td>
                    <td class="td-name"><?= Html::a($value->name, Url::toRoute('/'.$value->url))?></td>
                    <td><?= $value->article ?></td>
                    <td class="td-price"><?= $value->tradePrice . " руб." ?></td>
                    <td class="td-num"><input  type="text" maxlength="3" name="<?= $value->id ?>" value="<?= $value->quantity ?>" onblur="setCount(this,'site')"></td>
                    <td class="td-sum"><?= $value->getPrice() . " руб." ?></td>
                    <td class="td-del"><a href="#" id="<?= $value->id ?>" onclick="deleteProduct(this, 'site')" class="delete-link"></a></td>
                </tr>
            <?php endforeach; ?>
        <?php // endif; ?>
    </tbody>
</table>