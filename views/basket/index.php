<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Category;

$this->title = "Корзина";
?>

<div class="center-case">
    <div class="sort-block" id="basket-produkt">
        <div class="wrapper">
            <table>
                <tr>
                    <td style="width: 40%;">
                        <h1 style="float: left;margin: 15px;"><?= $this->title ?></h1>
                    </td>
                    <td>
                        <div class="basket_nav">
                            <a href="<?= Url::toRoute('/') ?>" class="basket_but_link">
                                <div class="basket_but">Вернуться в магазин</div>
                            </a>
                        </div>
                    </td>
                    <td>
                        <div class="basket_nav">
                            <a href="<?= Url::toRoute('/') ?>" class="basket_but_link" onclick="deletAll(); return false">
                                <div class="basket_but">Очистить корзину</div>
                            </a>
                        </div>
                    </td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
    <div id="basket-produkt">
        <div class="wrapper">
            <?= $this->render('_list_product', array('model' => $model)); ?>
            <table class="info" style="width: 100%;">
                <tr>
                    <td style="width: 36%;"></td>
                    <td>
                        <div class="basket_sum">
                            <span>Сумма заказа:</span>
                            <span class="basket_itog_sum"><?= $model->getSummPrise()['summ'] . " руб." ?></span>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    
    <?php if(Yii::$app->user->isGuest) : ?>
        <a href="<?= Url::toRoute('/login') ?>" class="basket_but_link">
    <?php else : ?>
        <a href="<?= Url::toRoute('/order') ?>" class="basket_but_link">
    <?php endif; ?>
        <button class="basket_but">Оформить заказ</button>
    </a>
</div>