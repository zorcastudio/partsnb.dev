<?php
if (!defined('STDOUT'))
{
	define('STDOUT', 'null');
}
if (!defined('STDIN'))
{
	define('STDIN', 'null');
}
// Initialize Joomla framework
define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);

// Load system defines
$base = dirname(dirname(dirname(dirname(dirname(__FILE__))))); 
define('JPATH_BASE', $base);
if (file_exists($base . '/includes/defines.php'))
{
	require_once $base . '/includes/defines.php';
} else {
	$file = $base . "/logs/Sttvm2expimpcron.log";
	$fp = fopen ($file, 'a');
	fwrite ($fp, "\n" . date('Y-m-d H:i:s'));
	fwrite ($fp, ' error: ' . 'не найден файл '.$base . '/includes/defines.php');
	fclose ($fp);
}

// Get the framework.
require_once JPATH_LIBRARIES . '/import.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';
// Force library to be in JError legacy mode
JError::$legacy = true;

class Sttvm2expimpImport extends JApplicationCli
{
	private $_errormsg = '';
	private $_good = 0;
	private $_bad = 0;
	private $_heads = array();
    private $_sale_category = 7; // ID категории "Товары со скидкой"
	private $_delivary_id = 24;


	public function doExecute()
	{
        self::logInfo('Старт импорта', 'message');
		$db = JFactory::getDBO();
		// получаем список товаров для загрузки
		jimport('joomla.application.component.helper');
		$component = JComponentHelper::getComponent('com_sttvm2expimp');
		$params = $component->params;
		$filename = $params->get('importfile', 'prod_in').'.csv';
		$fp = fopen (dirname(JPATH_ROOT).'/sync/'.$filename, 'r');
		if($fp) {
			if(!$this->import($fp)) {
				self::logInfo($this->_errormsg, 'error');
			}
			fclose($fp);
			$db->setQuery('insert into #__sttvm2implog (`countgood`, `countbad`, `filename`, `created`) VALUES ('.
				$db->quote($this->_good).','.
				$db->quote($this->_bad).','.
				$db->quote($filename).',now())');
			$db->query();
		} else {
			self::logInfo('Не удалось открыть файл '.dirname(JPATH_ROOT).'/sync/'.$filename, 'error');
			$db->setQuery('insert into #__sttvm2implog (`countgood`, `countbad`, `filename`, `created`) VALUES (0,0,'.$db->quote($filename).',now())');
			$db->query();
		}

        self::logInfo('Импорт закончен', 'message');
         if($_SERVER['HTTP_REFERER']){
                      header("Location: ".$_SERVER['HTTP_REFERER']);
                      exit();
                  }
         
	}
    
	function import($fp) {
		$this->_good = 0;
		$this->_bad = 0;
		define('VMLANG', 'ru_ru');
		try {
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);
			// читаем заголовок
			$_arrfld = $this->xfgetcsv($fp);
			foreach ($_arrfld as $key => $value) {
				$this->_heads[$value] = $key;
			}
            
            
            
			while ($_arrfld = $this->xfgetcsv($fp)) 
			{
				$product_sku = $_arrfld[$this->_heads['product_sku']];
				$product_price = $_arrfld[$this->_heads['product_price']];
				$product_in_stock_1 = abs((int)$_arrfld[$this->_heads['product_in_stock']]);
				$product_in_stock_2 = abs((int)$_arrfld[$this->_heads['product_in_stock2']]);
				$product_in_res_1 = (int)$_arrfld[$this->_heads['product_in_res']];
				$product_in_res_2 = (int)$_arrfld[$this->_heads['product_in_res2']];
				$product_in_stock = max(($product_in_stock_1 + $product_in_stock_2 - $product_in_res_1 - $product_in_res_2), 0);
                $trade_price = $_arrfld[$this->_heads['trade_price']];
                $action = $_arrfld[$this->_heads['action']];
                $is_timed = $_arrfld[$this->_heads['is_timed']];
                $delivery_date = $_arrfld[$this->_heads['delivery_date']];
                $warranty = !empty($_arrfld[$this->_heads['warranty']]) ?$_arrfld[$this->_heads['warranty']]. ' мес.' : null ;

                 
                              
				if(isset($this->_heads['product_name']) && isset($_arrfld[$this->_heads['product_name']]))
					$product_name = $_arrfld[$this->_heads['product_name']];
				else $product_name = '';
				$db->setQuery('select virtuemart_product_id from #__virtuemart_products where product_sku='.$db->quote($product_sku));
				$res = $db->loadResultArray();
				if($db->getErrorMsg()) {
					self::logInfo($db->getErrorMsg(), 'error');
				}
				if(is_array($res) && count($res)) {
                    foreach ($res as $virtuemart_product_id)
                    {

                        // *******************************************************
                        // УСТАНАВЛИВАЕМ НОВУЮ ЦЕНУ
                        // *******************************************************
                        // но сначала проверим, есть ли цена вообще у этого товара
                        $q="select count(*) from #__virtuemart_product_prices where virtuemart_product_id=".$db->quote($virtuemart_product_id);

                        $db->setQuery($q);
                        $cntprice=$db->loadResult();
                        $query->clear();
                        if($cntprice) {
                            // Обновление цены
                            $query->update($db->quoteName('#__virtuemart_product_prices'));
                            $query->where($db->quoteName('virtuemart_product_id').' = '.$db->quote($virtuemart_product_id));
                            $query->set($db->quoteName('product_price').' = '.$db->quote($product_price));
                            $query->set($db->quoteName('modified_on').' = '.$db->quote(date("Y-m-d H:i:s")));

                            $db->setQuery((string)$query);
                            $db->query();
                            if($db->getErrorMsg()) {
                                self::logInfo($db->getErrorMsg(), 'error');
                            }
                            $query->clear();
                        } else {
                            // Создание цены
                            $query->insert($db->quoteName('#__virtuemart_product_prices'));
                            $query->set($db->quoteName('virtuemart_product_id').' = '.$db->quote($virtuemart_product_id));
                            $query->set($db->quoteName('product_currency').' = 131 ');
                            $query->set($db->quoteName('created_on').' = '.$db->quote(date("Y-m-d H:i:s")));
                            $query->set($db->quoteName('modified_on').' = '.$db->quote(date("Y-m-d H:i:s")));

                            $db->setQuery((string)$query);
                            $db->query();
                            if($db->getErrorMsg()) {
                                self::logInfo($db->getErrorMsg(), 'error');
                            }
                            $query->clear();
                        }
                        // -------------------------------------------------------
                        // КОНЕЦ НОВОЙ ЦЕНЫ
                        // -------------------------------------------------------

                        // *******************************************************
                        // УСТАНАВЛИВАЕМ ОПТОВУЮ ЦЕНУ
                        // *******************************************************
                        // но сначала проверим, есть ли ОПТОВАЯ ЦЕНА вообще у этого товара
                        $q="select * from `j251_virtuemart_product_customfields` where `virtuemart_custom_id` = 23 and `virtuemart_product_id` = ".$db->quote($virtuemart_product_id);

                        $db->setQuery($q);
                        $optovaya_cena=$db->loadResult();
                        $query->clear();
                        if($optovaya_cena) {
                            // обновление ОПТОВОЙ ЦЕНЫ
                            $query->update($db->quoteName('#__virtuemart_product_customfields'));
                            $query->where($db->quoteName('virtuemart_product_id').' = '.$db->quote($virtuemart_product_id));
                            $query->where($db->quoteName('virtuemart_custom_id').' = 23');
                            $query->set($db->quoteName('custom_value').' = '.$db->quote($trade_price));
                            $query->set($db->quoteName('modified_on').' = NOW()');

                            $db->setQuery((string)$query);
                            $db->query();
                            if($db->getErrorMsg()) {
                                self::logInfo($db->getErrorMsg(), 'error');
                            }
                            $query->clear();
                        } else {
                            // ДОБАВЛЕНИЕ ОПТОВОЙ ЦЕНЫ
                            $query->clear();
                            $query->insert($db->quoteName('#__virtuemart_product_customfields'));
                            $query->set($db->quoteName('virtuemart_product_id').' = '.$db->quote($virtuemart_product_id));
                            $query->set($db->quoteName('virtuemart_custom_id').' = 23');
                            $query->set($db->quoteName('custom_value').' = ' . $db->quote($trade_price));
                            $query->set($db->quoteName('published').' = 1');
                            $query->set($db->quoteName('created_on').' = NOW()');
                            $query->set($db->quoteName('created_by').' = 0');
                            $query->set($db->quoteName('modified_on').' = NOW()');
                            $query->set($db->quoteName('modified_by').' = 394');
                            // $this->dump($query);
                            $db->setQuery((string)$query);
                            $db->query();
                            if($db->getErrorMsg()) {
                                self::logInfo($db->getErrorMsg(), 'error');
                            }
                            $query->clear();
                        }
                        // -------------------------------------------------------
                        // КОНЕЦ ОПТОВОЙ ЦЕНЫ
                        // -------------------------------------------------------


                        // *******************************************************
                        // Гарантия
                        // *******************************************************
                        if($warranty !== null) {

                            $q = "select `virtuemart_customfield_id` from `j251_virtuemart_product_customfields` where `virtuemart_custom_id` = 7 and `virtuemart_product_id` = " . $db->quote($virtuemart_product_id);

                            $db->setQuery($q);
                            $warranty_db = $db->loadResult();
                            $query->clear();
                            if ($warranty_db) {
                                // обновление гарантии
                                $query->update($db->quoteName('#__virtuemart_product_customfields'));
                                $query->where($db->quoteName('virtuemart_product_id') . ' = ' . $db->quote($virtuemart_product_id));
                                $query->where($db->quoteName('virtuemart_custom_id') . ' = 7');
                                $query->set($db->quoteName('custom_value') . ' = ' . $db->quote($warranty));
                                $query->set($db->quoteName('modified_on') . ' = NOW()');

                                $db->setQuery((string)$query);
                                $db->query();
                                if ($db->getErrorMsg()) {
                                    self::logInfo($db->getErrorMsg(), 'error');
                                }
                                $query->clear();
                            } else {
                                // ДОБАВЛЕНИЕ гарантии
                                $query->clear();
                                $query->insert($db->quoteName('#__virtuemart_product_customfields'));
                                $query->set($db->quoteName('virtuemart_product_id') . ' = ' . $db->quote($virtuemart_product_id));
                                $query->set($db->quoteName('virtuemart_custom_id') . ' = 7');
                                $query->set($db->quoteName('custom_value') . ' = ' . $db->quote($warranty));
                                $query->set($db->quoteName('published') . ' = 1');
                                $query->set($db->quoteName('created_on') . ' = NOW()');
                                $query->set($db->quoteName('created_by') . ' = 0');
                                $query->set($db->quoteName('modified_on') . ' = NOW()');
                                $query->set($db->quoteName('modified_by') . ' = 394');

                                $db->setQuery((string)$query);
                                $db->query();
                                if ($db->getErrorMsg()) {
                                    self::logInfo($db->getErrorMsg(), 'error');
                                }
                                $query->clear();
                            }
                        }
                        // -------------------------------------------------------
                        // КОНЕЦ гарантии
                        // -------------------------------------------------------


                        // *******************************************************
                        // ОПРЕДЕЛЯЕМ АКЦИЮ ТОВАРА
                        // *******************************************************
                        if($action == 0)
                        {
                            $query->delete($db->quoteName('#__virtuemart_product_categories'));
                            $query->where('virtuemart_product_id = '.$db->quote($virtuemart_product_id));
                            $query->where('virtuemart_category_id = '.$db->quote($this->_sale_category));
                            $db->setQuery((string)$query);
                            $db->query();
                            if($db->getErrorMsg()) {
                                self::logInfo($db->getErrorMsg(), 'error');
                            }
                            $query->clear();
                        }
                        // Если 1 - то добавляем в категорию "Товары со скидкой"
                        else if($action == 1)
                        {
                            // Проверяем - есть ли такой товар в таблице - если ДА, то пропускаем добавление
                            $query_sale="select count(*) from #__virtuemart_product_categories where virtuemart_product_id=".$db->quote($virtuemart_product_id) . ' AND virtuemart_category_id = '. $this->_sale_category;
                            $db->setQuery($query_sale);
                            $cntprice=$db->loadResult();
                            $query->clear();
                            $count_sales = $db->loadResult();

                            if(!$count_sales)
                            {
                                $query->insert($db->quoteName('#__virtuemart_product_categories'));
                                $query->set($db->quoteName('virtuemart_product_id').' = '.$db->quote($virtuemart_product_id));
                                $query->set($db->quoteName('virtuemart_category_id').' = '.$db->quote($this->_sale_category));
                                $query->set($db->quoteName('ordering').' = 0');
                                $db->setQuery((string)$query);
                                $db->query();
                                if($db->getErrorMsg()) {
                                    self::logInfo($db->getErrorMsg(), 'error');
                                }
                                $query->clear();
                            }


                        }
                        // -------------------------------------------------------
                        // КОНЕЦ АКЦИИ
                        // -------------------------------------------------------

                        // -------------------------------------------------------
                        // НАЧАЛО ВРЕМЯ ПОСТАВКИ
                        // -------------------------------------------------------
                        // Если товара нет в наличии
                        if($product_in_stock == 0 && $product_in_res_1 == 0 && $product_in_res_2 == 0)
                        {
                            // Если известна дата поставки
                            if($is_timed == 1)
                            {
                                // Если дата поставки не равно 0 или пустой строке
                                if($delivery_date != 0 || !empty($delivery_date))
                                {
                                    // Проверяем, есть ли данные по данному товару в таблице
                                    $sql = "SELECT COUNT(virtuemart_customfield_id) as cnt FROM #__virtuemart_product_customfields WHERE virtuemart_product_id = ".$virtuemart_product_id." AND virtuemart_custom_id = ". $this->_delivary_id;
                                    $db->setQuery($sql);
                                    $cntprods = $db->loadResult();
                                    $query->clear();
                                    $count_prods = $db->loadResult();

                                    if($count_prods){

                                        // обновление даты поставки
                                        $query->update($db->quoteName('#__virtuemart_product_customfields'));
                                        $query->where($db->quoteName('virtuemart_product_id').' = '.$db->quote($virtuemart_product_id));
                                        $query->where($db->quoteName('virtuemart_custom_id').' = '. $this->_delivary_id);
                                        $query->set($db->quoteName('custom_value').' = '.$db->quote($delivery_date));
                                        $query->set($db->quoteName('modified_on').' = NOW()');

                                        $db->setQuery((string)$query);
                                        $db->query();
                                        if($db->getErrorMsg()) {
                                            self::logInfo($db->getErrorMsg(), 'error');
                                        }
                                        $query->clear();


                                    } else {
                                        // добавляем товар в таблицу
                                        $query->insert($db->quoteName('#__virtuemart_product_customfields'));
                                        $query->set($db->quoteName('virtuemart_product_id').' = '.$db->quote($virtuemart_product_id));
                                        $query->set($db->quoteName('virtuemart_custom_id').' = '. $this->_delivary_id);
                                        $query->set($db->quoteName('custom_value').' = ' . $db->quote($delivery_date));
                                        $query->set($db->quoteName('published').' = 1');
                                        $query->set($db->quoteName('created_on').' = NOW()');
                                        $query->set($db->quoteName('created_by').' = 0');
                                        $query->set($db->quoteName('modified_on').' = NOW()');
                                        $query->set($db->quoteName('modified_by').' = 394');
                                        // $this->dump($query);

                                        $db->setQuery((string)$query);
                                        $db->query();
                                        if($db->getErrorMsg()) {
                                            self::logInfo($db->getErrorMsg(), 'error');
                                        }
                                        $query->clear();

                                    }





                                } else {
                                    $query->delete($db->quoteName('#__virtuemart_product_customfields'));
                                    $query->where('virtuemart_product_id = '.$db->quote($virtuemart_product_id));
                                    $query->where('virtuemart_custom_id = '.$this->_delivary_id);
                                    $db->setQuery((string)$query);
                                    $db->query();
                                    if($db->getErrorMsg()) {
                                        self::logInfo($db->getErrorMsg(), 'error');
                                    }
                                    $query->clear();
                                }
                            } else {
                                $query->delete($db->quoteName('#__virtuemart_product_customfields'));
                                $query->where('virtuemart_product_id = '.$db->quote($virtuemart_product_id));
                                $query->where('virtuemart_custom_id = '. $this->_delivary_id);
                                $db->setQuery((string)$query);
                                $db->query();
                                if($db->getErrorMsg()) {
                                    self::logInfo($db->getErrorMsg(), 'error');
                                }
                                $query->clear();
                            }


                        }
                        // Если товар есть в наличии - то удаляем записи "В ожидании" из бд
                        else{
                            $query->delete($db->quoteName('#__virtuemart_product_customfields'));
                            $query->where('virtuemart_product_id = '.$db->quote($virtuemart_product_id));
                            $query->where('virtuemart_custom_id = '. $this->_delivary_id);
                            $db->setQuery((string)$query);
                            $db->query();
                            if($db->getErrorMsg()) {
                                self::logInfo($db->getErrorMsg(), 'error');
                            }
                            $query->clear();
                        }


                    }
                }


				// ОБНОВЛЯЕМ НАЛИЧИЕ ТОВАРА
				if(count($res)>0) {
					$db->setQuery('update #__virtuemart_products set `product_in_stock`='. $db->quote($product_in_stock) .
						', `product_in_stock_1` = '. $db->quote($product_in_stock_1) .
						', `product_in_stock_2` = '. $db->quote($product_in_stock_2) .
						', `product_in_res_1` = '. $db->quote($product_in_res_1) .
						', `product_in_res_2` = '. $db->quote($product_in_res_2) .
						' where product_sku='.$db->quote($product_sku));
					$db->query();
					if($db->getErrorMsg()) {
						self::logInfo($db->getErrorMsg(), 'error');
					} else {
						$this->_good++;
					}
                    
                    
                    
				} else {
                    self::logInfo('Нет товара с кодом '.$product_sku, 'error');
					$this->_bad += 1;
				}
			}
		} catch (Exception $exc) {
			$this->_errormsg = $exc->getTraceAsString();
			return false;
		}
		return true;
	}
	function xfgetcsv($f = '', $s = '~^~') {
		if ($str = fgets($f)) {
			$data = explode($s, trim($str));
			foreach ($data as $key=>$value) {
				$data[$key] = str_replace('~', '', $value);
			}
			return $data;
		} else {
			return FALSE;
		}
	}

	static function logInfo ($text, $type = 'message') {
		$file = JPATH_ROOT . "/logs/Sttvm2expimpcron.log";
		$date = JFactory::getDate ();
		$fp = fopen ($file, 'a');
		fwrite ($fp, "\n" . $date->toFormat ('%Y-%m-%d %H:%M:%S'));
		fwrite ($fp, ' ' . $type . ': ' . $text);
		fclose ($fp);
	}
    
    function dump($query){
        echo $query->dump().'<br />';
    }
}

JApplicationCli::getInstance('Sttvm2expimpImport')->execute();
