/**
 * запускаеть скрипт по перещитыванию товаров в фильтрах
 */
function recalculate(el) {
    el = $(el);
    el.addClass("expectation");
    el.text("Перещитывание!");
    $.post(
        "/ajax/recalculate",
        {},
        function (data) {
            if (data == 1) {
                el.text("Перещет зокончен");
                el.addClass("finished");
                el.removeClass('expectation');
            } else {
                el.text("Ошибка");
                el.addClass('error');
            }
        }
    );
} 


/**
* Начинает формировать название продукта
*/
function insertName(name, scenario) {
    var product = $("#product-name");
    name = $(name);
    switch (scenario) {
        case 'category' :
            var template = '';
            category = name.find("option[value=" + name.val() + "]").val();
            switch (category){
                case '1' : template = 'Клавиатура для ноутбука'; break;
                case '2' : template = 'Вентилятор (кулер) для ноутбука'; break;
                case '3' : template = 'Аккумулятор для ноутбука'; break;
                case '4' : template = 'Блок питания для ноутбука'; break;
                case '5' : template = 'Петли матриц для ноутбука'; break;
                case '6' : template = 'Шлейф матриц для ноутбука'; break;
                case '9' : template = 'Разъём питания для ноутбука'; break;
                case '10': template = 'Матрица для ноутбука'; break;
            }
//            $("#genereteproduct-producer_id").removeAttr('disabled');
//            productName = categoryName +" "+producerName;
            $("#genereteproduct-_nameone").val(template);
            $("#genereteproduct-_nameone").removeAttr('disabled');
            break;
        case 'producer' :
            producerName = name.find("option[value=" + name.val() + "]").text();
            productName = categoryName +" "+producerName;
            $("#genereteproduct-_nameone").val(productName);
            $("#genereteproduct-_nameone").removeAttr('disabled');
            $("#compatible").removeAttr('disabled');
            break;
        case 'model' :
            $("#product-name_lat").removeAttr('disabled');
            modelName = name.find("option[value=" + name.val() + "]").text();
            productName = categoryName +" "+producerName+" "+modelName;
            product.val(productName);
            product.removeAttr('disabled');
            $('.select2-search__field').removeAttr('disabled');
            break;
    }
}


/**
 * запускаеть скрипт по перещитыванию товаров в фильтрах
 */
function syncing(el) {
    el = $(el);
    el.addClass("expectation");
    el.text("старт!");
    $.ajax({
        url:  "/ajax/syncing",
        type: "POST",
        data: {},
        error: function(jqXHR, exception) {  
                    if (jqXHR.status === 0) {  
                        alert('.');  
                    } else if (jqXHR.status == 404) {  
                         console.log(jqXHR.responseText);
                        alert('Ошибка 404 \n Страница не найдена');  
                    } else if (jqXHR.status == 500) {  
                        alert('Внутренняя ошибка сервера [500].');  
                    } else if (exception === 'parsererror') {  
                        alert('Запрошенный JSON синтаксического анализа не удалось.');  
                    } else if (exception === 'timeout') {  
                        alert('Тайм-аут ошибка.');  
                    } else if (exception === 'abort') {  
                        alert('Ajax запрос прерван.');  
                    } else {  
                        alert('Неопределенная ошыбка.\n' + jqXHR.responseText);  
                    }  
                },
        success: function(data){
           
             if (data != -1) {
                    var message = $.parseJSON(data);
                    console.log(message);
                    var input = $('#message ul');
                    var srt = '';
                    $.each(message, function(index, value) {
                        srt += "<p><b>"+index+"</b></p>";
                        $.each(value, function(i, v) {
                            if($.isArray(v)){
                                $.each(v, function(x, error) {
                                    srt += "<p>"+error+"</p>";

                                });
                            }else{
                                srt += "<p><b>"+v+"</b></p\n>";
                            }
                        });
                    });
                    console.log(srt);
                    input.empty();
                    input.append(srt);

                    el.text("Синхронизация закончена");
                    el.addClass("finished");
                    el.removeClass('expectation');
                } else {
                    el.text("Ошибка");
                    el.addClass('error');
                }
        },
    });
} 



//$.ajax({ // отправляем данные   
//            url:"test.php"  
//            , cache: false  
//            , timeout: 10000  
//            , type:'POST'  
//            , datatype: "json"  
//            , data:'jsonData=' + $.toJSON(ArrayCheckedID),  
//            success: function(res){alert(res);},  
//            error: function(jqXHR, exception) {  
//                if (jqXHR.status === 0) {  
//                    alert('.');  
//                } else if (jqXHR.status == 404) {  
//                    alert('Requested page not found. [404]');  
//                } else if (jqXHR.status == 500) {  
//                    alert('Internal Server Error [500].');  
//                } else if (exception === 'parsererror') {  
//                    alert('Requested JSON parse failed.');  
//                } else if (exception === 'timeout') {  
//                    alert('Time out error.');  
//                } else if (exception === 'abort') {  
//                    alert('Ajax request aborted.');  
//                } else {  
//                    alert('Uncaught Error.\n' + jqXHR.responseText);  
//                }  
//            }  
//            });  




/**
 * Вставляет в страницу создания модели список серий
 */
function insertSerie(data){
    data = jQuery.parseJSON(data);
    $('#model-serie_id').empty();
    $('#model-serie_id').removeAttr("disabled");
    $('#model-serie_id').append($('<option value>Выберите серию</option>'));
    $.each(data, function (index, value) {
        $('#model-serie_id').append($('<option value="' + index + '">' + value + '</option>'));
    })
}


/**
 * Создает товар и перезапукает cron
 */
function Createproduct(first, step, article) {
  var last;
    $.post(
            "/ajax/createproduct",
            {
                first: first-step, 
                last: last = first,
                article: article
            },
            function (data){
                CronProduct(last, article, data);
            }
    );
}
/**
 * Запускает функцыю для создания товара с интервалами 
 */
function CronProduct(first, article, data){
    var step = 200;
    var time = 5000;
    if(first <= 12798){ //12798
        Createproduct(first += step, step, article);
    }else{
        console.log('конец '+data);
    }
}
/**********************************************************/

/**
 * Создает свойство товаров
 */
function CreateProperties(first, step) {
    $.post(
            "/ajax/createproperties",
            {
                first: first-step, 
                last: first,
            },
            function (data){
                CronProperties(first, data);
            }
    );
}
/**
 * Запускает функцыю для создания товара с интервалами 
 */
function CronProperties(first, data){
    var step = 200;
    if(first <= 43100){
        CreateProperties(first += step, step);
    }else{
        console.log('конец '+data);
    }
}
/**********************************************************/

/**
 * Создает свойство товаров
 */
function SetProductSerie(first, producer_id, step) {
    $.post(
            "/ajax/update_product_serie",
            {
                first: first-step, 
                last: first,
                producer: producer_id
            },
            function (data){
                CronSetProductSerie(first, data);
            }
    );
}
/**
 * Запускает функцыю для создания товара с интервалами 
 */
function CronSetProductSerie(first, producer_id, data){
    var step = 500;
    if(first <= 43100){
        SetProductSerie(first += step, producer_id, step);
    }else{
        console.log('конец '+data);
    }
}
/**********************************************************/

/**
 *  определяем каких товаров несуществует но есть связь
 */
function GetProductError(first, step) {
    $.post(
            "/ajax/product_error",
            {
                first: first-step, 
                last: first,
            },
            function (data){
                $("h1").after(data);
                CronProductError(first, data);
            }
    );
}
/**
 * Запускает функцыю для создания товара с интервалами 
 */
function CronProductError(first, data){
    var step = 20;
    if(first <= 12800){
        GetProductError(first += step, step);
    }else{
        console.log('конец '+data);
    }
}
/**********************************************************/

/**
 *  Создаем связи товаров с фильтрами
 */
function SaetProductFilter(first, step) {
    $.post(
            "/ajax/product_filter",
            {
                first: first-step, 
                last: first,
            },
            function (data){
                CronProductFilter(first, data);
            }
    );
}
/**
 * Запускает функцыю для создания товара с интервалами 
 */
function CronProductFilter(first, data){
    var step = 500;
    if(first <= 43300){
        SaetProductFilter(first += step, step);
    }else{
        console.log('конец '+data);
    }
}
/**********************************************************/

/**
 *  Создаем связи товаров с фильтрами
 */
function SaetGoFunction(first, step) {
    $.post(
            "/ajax/go_function",
            {
                first: first-step, 
                last: first,
            },
            function (data){
                CronGoFunction(first, step, data);
            }
    );
}
/**
 * Запускает функцыю для создания товара с интервалами 
 */
function CronGoFunction(first, step, data){
    if(first <= 73972){
        SaetGoFunction(first += step, step);
    }else{
        console.log('конец '+data);
    }
}


/**********************************************************/

/**
 *  Создаем связи товаров с фильтрами
 */
function getArticle(key) {
    $.post(
        "/ajax/article",
        {},
        function (data){
            CronGenereteProduct(key, $.parseJSON(data));
        }
    );
}

function GenereteProduct(key, article) {
    $.post(
            "/ajax/go_function",
            {
                article: article[key], 
            },
            function (data){
                if(data){
                    if(data){
                       CronGenereteProduct(++key, article);
                    }
                }
            }
    );
}

/**
 * Запускает функцыю для создания товара с интервалами 
 */
function CronGenereteProduct(key, article){
    if(key || key == 0 && article != ''){
        GenereteProduct(key, article)
        console.log(key+" "+article[key]);
    }else{
        getArticle(1219);
    }
}
/**********************************************************/

/**
 *  Удаляем товары и связанные таблицы ели модели являются партномерами
 */
function getPNinProduct(key) {
    $.post(
        "/ajax/get_listmodel",
        {},
        function (data){
            CronDeleteModel(key, $.parseJSON(data));
        }
    );
}

function DeleteModel(key, pn) {
    $.post(
            "/ajax/go_function",
            {
                pn: pn[key], 
            },
            function (data){
                if(data){
                    if(data){
                       CronDeleteModel(++key, pn);
                    }
                }
            }
    );
}

/**
 * Запускает функцыю для создания товара с интервалами 
 */
function CronDeleteModel(key, pn){
    if(key || key == 0 && pn != ''){
        DeleteModel(key, pn)
        console.log(key+" "+pn[key]);
    }else{
        getPNinProduct(3);
    }
}

/**********************************************************/

/**
 *  Удаляем товары и которые по артикулу не соответствуют установленой категории
 */

//список всех артикулов что находятся в таблице товаров
function getArticleProduct(key) {
    $.post(
        "/ajax/article_in_product",
        {},
        function (data){
            CronDeleteProduct(key, $.parseJSON(data));
        }
    );
}

function DeleteProduct(key, article) {
    $.post(
            "/ajax/go_function",
            {
                article: article[key], 
            },
            function (data){
                if(data){
                    if(data){
                       CronDeleteProduct(++key, article);
                    }
                }
            }
    );
}

/**
 * Запускает функцыю для создания товара с интервалами 
 */
function CronDeleteProduct(key, article){
    if(key || key == 0 && article != ''){
        DeleteProduct(key, article)
        console.log(key+" "+article[key]);
    }else{
        getArticleProduct(0);
    }
}

/**
 * Проверяем каждый товар на соответствующие изображения с главним товаром
 */
//список всех артикулов что находятся в таблице товаров
function getArticleProductImage(key) {
    $.post(
        "/ajax/article_in_product",
        {},
        function (data){
            CronControlImageInProduct(key, $.parseJSON(data));
        }
    );
}

function ControlImageInProduct(key, article) {
    $.post(
            "/ajax/go_function",
            {
                article: article[key], 
            },
            function (data){
                if(data){
                   CronControlImageInProduct(++key, article);
                }
            }
    );
}

/**
 * Запускает функцыю для создания товара с интервалами 
 */
function CronControlImageInProduct(key, article){
    if(key || key == 0 && article != ''){
        ControlImageInProduct(key, article)
        console.log(key+" "+article[key]);
    }else{
        getArticleProductImage(0);
    }
}


/**
 *  Создаем связи товаров с фильтрами
 */
function GoFunction() {
    $.post(
            "/ajax/go_function",
            {},
            function (data){
                console.log(data);
                GoFunction();
            }
    );
}



/**
 * Генерирует недостающие товары из моделей
 */
//список всех артикулов что находятся в таблице товаров
function getArticleNotProduct(key) {
    $.post(
        "/ajax/article_not_product",
        {},
        function (data){
            CronModelNotProduct(key, $.parseJSON(data));
        }
    );
}

function ControlNotProduct(key, article) {
    $.post(
            "/ajax/go_function",
            {
                article: article[key], 
            },
            function (data){
                if(data){
                   CronModelNotProduct(++key, article);
                }
            }
    );
}

/**
 * Запускает функцыю для создания товара с интервалами 
 */
function CronModelNotProduct(key, article){
    if(key || key == 0 && article != ''){
        ControlNotProduct(key, article)
        console.log(key+" "+article[key]);
    }else{
        getArticleNotProduct(0);
    }
}


/**
 * ***********************************************************
 * Скрипт по генерации изображений со старой таблицы БД
 */

//список всех артикулов что находятся в таблице товаров
function getArticleForImage(key) {
    $.post(
        "/ajax/article_product",
        {},
        function (data){
            CronCreateImage(key, $.parseJSON(data));
        }
    );
}

function createImage(key, article) {
    $.post(
            "/ajax/go_function",
            {
                article: article[key], 
            },
            function (data){
                if(data){
                   createImage(++key, article);
                }
            }
    );
}

/**
 * Запускает функцыю для создания товара с интервалами 
 */
function CronCreateImage(key, article){
    if(key || key == 0 && article != ''){
        createImage(key, article)
        console.log(key+" "+article[key]);
    }else{
        getArticleForImage(0);
    }
}


/**
 * переформатовування зображень товарів відповідно головному товару
 */
function CronCreateImage() {
    $.post(
            "/ajax/go_function",
            {},
            function (data){
                if(data){
                    console.log(data);
                    CronCreateImage();
                }
            }
    );
}

/**
 * універсальна функція циклічного виклику методів
 */
function CronGoFunction1() {
    $.post(
            "/ajax/go_function",
            {},
            function (data){
                if(data != -1){
                    console.log(data);
                    CronGoFunction1();
                }else{
                    console.log('Кінець программи');
                    return;
                }
            }
    );
}