/***********************Выбор стран городов******************/
function setRegions(data) {
    data = jQuery.parseJSON(data);

//     console.log(data);

    $('#region').empty();
    $('#region').attr('disabled', 'disabled');
//    $('#region').append($('<option value="0">Выберите регион</option>'));

    $.each(data, function (index, value) {
        $('#region').append($('<option value="' + index + '">' + value + '</option>'));
    })

    $('#region').removeAttr('disabled');

    $('#city').attr('disabled', 'disabled');
    $('#city').empty();
    $('#city').append($('<option value="0">Выберите город</option>'));
}

function setCities(data) {
    data = jQuery.parseJSON(data);

    $('#city').empty();
    $('#city').append($('<option value="0">Выберите город</option>'));

    $.each(data, function (index, value) {
        $('#city').append($('<option value="' + index + '">' + value + '</option>'));
    })
    $('#city').removeAttr('disabled');
}
/**************************** end ******************************/

$(document).ready(function () {
    
    //Відкриває праву панель зворотнього звязку
    $('.regions_phone').click(function () {
//          console.log($('.cbk-forms'));
//          $('.callbackkiller.cbk-window').removeAttr("style");
//          $('.cbk-form.cbk-callform').removeAttr("style");

    })
    
    $('.cbk-forms').click(function () {
//         javascript:void(0);
//         console.log('swew asas');
        
//         $('.callbackkiller.cbk-window').attr("class", "display: none");
//         $('.cbk-form.cbk-callform').attr("class", "display: none");
    })
    

    jQuery('.delete-image').click(function () {
        var elem = $(this).closest(".image-view");
//         if (confirm('Вы уверены сто хотите удалить это изображение')) {
        $.ajax({
            url: "/admin/product/deleteimage",
            data: {id: $(this).attr('id')},
            success: function (data) {
                if (data == 1)
                    $(elem).fadeOut(10);
            }
        });

//         };
    });

    $(".clear-filter").click(function () {
        var url = window.location;
        var url = url['href'].replace(/\?.*/g, '');
        location.href = url;
    })

    $(".active-filter").click(function () {
        var element = $(this);
        var id = element.attr("id");
        goUrlFilter(id.split('_'));
    })


    $('.set-filter').change(function () {
        var url_home = window.location['href'];
        var page = url_home.match('page=[0-9]+$');
        page = (page != null) ? "?"+ page : ''; 
        url_home = url_home.replace(/\/filter.*$/g, '');
        url_home = url_home.replace(/\?page=[0-9]+.*$/g, '');
        
        var element = $(this);
        var filter = element.closest("ul").attr('id');
        var value = element.attr("id").toString();
        var data = new Object();
        data[filter] = [value];

        var sliding = $(".sliding ");
        sliding.each(function (indx, element) {
            $(element).removeClass("none");
        });
        var getFilter = goUrlFilter();

        var url = '';
        if (typeof getFilter[filter] != "undefined") {
            if (getFilter[filter].indexOf(value) != -1) {
                index = $.inArray(value, getFilter[filter]);
                getFilter[filter].splice(index, 1);
            } else {
                getFilter[filter][getFilter[filter].length] = value;
            }

        } else {
            getFilter[filter] = [value];
        }

        url = createURL(getFilter);
        location.href = url_home + url + page;
    })

    $("#search-in").keypress(function (eventObject) {
        if (eventObject.which == 13) {
            Search();
        }
    });

    $(".sort-group").click(function () {
        var url = window.location;
        var url = url['href'].replace(/(\#)?((\/\?|&)?list=\d)?/g, '');
        var serch = url.indexOf('?') + 1;
        url += serch ? '&' : '?';
        switch ($(this).attr("id")) {
            case 'block-view':
                url += 'list=1';
                break;
            case 'list-view' :
                url += 'list=2';
                break;
        }
        location.href = url;
    });

    $('.image-preview').click(function () {
        var id = $(this).attr('class');
        id = Number(id.match(/\d+/));

        var active = $('.active');
        active.removeClass("active");
        $(".image-full.namber-" + id).addClass('active');
    });


    // преобразует объект в строку
    function createURL(object) {
        var result = '';
        $.each(object, function (index, vauel) {
            if (vauel != '') {
                result += index + "/" + vauel.join('-') + "/";
            }
        });
        if (result != '') {
            result = /filter/ + result;
        }
        result = result.replace(/\/$/g, '');
        return result;
    }

    /**
     * подключает маску к полю
     */
    $('div[data-target=#callBack]').click(function () {
        $('.phone-callBack').mask("+7 (000) 000-0000", {placeholder: "Телефон"});
    });

    $('#form_send').click(function () {
        var name = $('#form_name');
        var phone = $('#form_phone');
        name.val() == '' ? name.addClass("error") : name.removeClass("error");

        if (phone.val() == '')
            phone.addClass("error");
        else {
            if (phone.val().length != 17) {
                phone.addClass("error");
            } else
                phone.removeClass("error");
        }
        ;

        if (name.attr("class") == '' && phone.attr("class") != 'phone-callBack error') {
            $.ajax({
                url: '/ajax/call_back',
                data: "name=" + name.val() + "&phone=" + phone.val() + "&link=" + window.location.href,
                success: function (data) {
                    if (data != -1)
                        $("#form_landing").html(data);
                    else
                        $("#form_landing").html("<div class='error-message'>При отправке возникла ошибка</div>");
                }
            });
        }
    })

    $('.add-producer tbody td div[id] span').click(function () {
        console.log(this);
    });

    /**
     * Скрипт для заказа заміщається текст на сладі
     */
    if (typeof avialable !== "undefined") {
        $.each( avialable, function( key, value ) {
            var info = $('input[value=' + key + '] + span + .description p');
            var str = info.html();
            info.html(str.replace('заказ', value));
        });
    }

    prepareLinks();
    $('#content').on('pjax:end', function(){
        prepareLinks();
    });
});

function hiddenLink() {
    $('#pagination-links').on('click', 'span.hidden-link', function(){
        var el = $(this);
        var value = el.html();
        el.replaceWith(function(){return'<a id="hidden-link-'+value+'" href="'+el.data('url')+'">'+value+'</a>';});
        $('#hidden-link-'+value).click();
    });
}

function prepareLinks() {
    $('#pagination-links').find('.hidden-link').replaceWith(function(){var el = $(this); return'<a href="'+el.data('url')+'">'+el.html()+'</a>';});
}

function expandModel(el) {
    $(el).next().slideToggle(300);
}

function deletAll() {
    if (confirm('Вы уверены что хотите очистить корзину')) {
        location.href = '/basket/clear';
    }
    ;
}
;
/*******************************************************************/
function deleteProduct(el, scenario) {
    if (confirm('Вы уверены что хотите удалить этот товар с корзины')) {
        var elem = $(el).closest("tr");
        var url;
        if (scenario == 'site')
            url = "/ajax/delete_product";
        else
            url = "/ajax/delete_productadmin";

        $.ajax({
            type: "POST",
            url: url,
            data: {id: $(el).attr("id")},
            success: function (data) {
                console.log(data);
                if (data != -1) {
                    elem.fadeOut(500);
                    price = $.parseJSON(data);
                    price = parsPrice(price['summ']);
                    delivery = parsPrice($(".put-delivery").text());
                    $('.basket_itog_sum').html(number_format(price, 2, '.', ' ') + ' руб.');
                    price += delivery;
                    price = number_format(price, 2, '.', ' ');
                    elem.fadeOut(300);
                    $('.put-summ').html(price + ' руб.');
                    $('#update-block').click()
                }
            }
        });
    }
}
;
/******************** изменение количества товара ******************/
function setCount(el, scenario) {
    var elem = $(el);
    var count = elem.val();
    var id = elem.attr('name');
    var url;
    if (scenario == 'site')
        url = "/ajax/update_product";
    else
        url = "/ajax/update_productadmin";

    $.ajax({
        type: "POST",
        url: url,
        data: {id: id, count: count},
        success: function (data) {
            var price = $.parseJSON(data);
            var summ = parsPrice(price['summ']);
            delivery = parsPrice($(".put-delivery").text());
            summ += delivery;
            summ = number_format(summ, 2, '.', ' ');
            var block = $(".put-summ");
            block.each(function (indx, element) {
                $(element).empty();
                $(element).append(summ + " руб.");
            });
            var price_f = price['summ'] + ' руб.';
            elem.closest("tr").find('.td-sum').html(price['product']['price'] + " руб.")
            $('.basket_itog_sum').html(price_f);
            $('.basket-sum').html(price_f);
            $('#update-block').click()
        }
    });
}
;

function parsPrice(str) {
    if ((typeof str) == 'number') {
        str = str.toString();
    }
    if (str != undefined) {
        str = str.replace(/(\.00)?( [а-яА-Я]*\.)?$/g, '');
        str = str.replace(/ /g, '');
        return parseFloat(str);
    } else
        return '0';
}
/******************** Выбор валюты ******************/
function currency(el) {
    var currency = $(el).val();
    location.href = "/basket/currency/" + currency;
}
;
/******************** Выбор сортировки ******************/
function sort(el) {
    var url = window.location;
    url = url['href'];
    var sort = $(el).val();
    var serch = url.indexOf('?');
    if (serch == -1) {
        url = url.replace(/\?sort=\d/g, '');
        url = url + '?sort=' + sort;
    } else {
        url = url.replace(/&sort=\d/g, '');
        url = url + '&sort=' + sort;
    }
    location.href = url;
}
;
/*********************карточка товара*******************************/
//функцыя для поиска элемента на странице
jQuery.fn.exists = function () {
    return $(this).length;
}
/****************карточка товара**************************/

/**
 * Устанавливает количество товаров в карточке товаров
 */
function setAmount(el, max) {
    var scenario = $(el).attr('class');
    var amount = $('.amount-view').val();
    switch (scenario) {
        case 'add' :
            amount++;
            break;
        case 'remuve' :
            amount--;
            break;
    }
    if (amount >= 1 && amount <= max) {
        $('.amount-view').val(amount);
    }
}

/**
 * Ищем на странице количество товаров введенных покупателем
 */
function getAmount(el) {
    var amount = $('.amount-view').val();
    var id = $(el).attr('id');
    setProduct('update', id, amount);

}



function setProduct(el, id, amount) {
    var scenario;
    if (el == 'update') {
        scenario = el;
    } else {
        scenario = $(el).attr('class');
        var id = $(el).attr('id');
    }
    var list = $('.add-prod-link');

    $.ajax({
        url: "/ajax/put_basket",
        data: {id: id, scenario: scenario, amount: amount},
        success: function (data) {
            var reply = $.parseJSON(data);
            var currency_name = reply['currency_name'];
            var amount_summ = reply['amount_summ'];
            var baskets = $('.basket-sum');
            baskets.each(function (indx, element) {
                $(element).html(amount_summ + ' ' + currency_name);
            });
            if (amount_summ != 0) {
                $('.basket').fadeIn(300);
                $('.bottom-basket').fadeIn(300);
                $('.basket-count').html(reply['count']);

                if (parseInt(reply['count'], 10) == 1) {
                    $(".basket-info").html('<div class="total-products"><div class="basket-count">1  </div><span> Товар</span></div><div class="custom"><a href="/order">Оформить заказ</a></div><div style="clear:both;"></div>');
                }

                if (parseInt(reply['count'], 10) > 1) {
                    $('.total-products').html('<span>Товаров в корзине </span><div class="basket-count"> ' + reply['count'] + '</div>')

                }


                $('.basket-count-one').html(reply['count_one']);
                var basket_st = $('.basket-stat');
                basket_st.removeClass('empty');
                var url = window.location;
                var exp = url['pathname'].split('.');
                var url_b;
                if (exp[1]) {
                    url_b = '/basket' + '.' + exp[1];
                } else {
                    url_b = '/basket';
                }
                basket_st.find('a').attr("href", url_b);

            } else {
                $('.basket').fadeOut(300);
                $('.bottom-basket').fadeOut(300);
                $('.basket-stat').addClass('empty');
            }



            $(list).each(function (index, element) {
                var id = $(element).attr('id');
                var pr_id = $(element).find('.add').attr('id');
                if ($.inArray(id, reply['product_id']) != -1) {
                    $(element).addClass("input");
                    var caunt = reply['product'][pr_id]['amount'];
                    $(element).find('.amount-view').html(caunt + ' шт.');

                    $(element).find('.amount').removeClass('none');
                } else {
                    $(element).removeClass("input");
                    $(element).find('.amount').addClass('none');
                    $(element).find('.amount-view').empty();
                }
                ;
            });
        }
    });

}
;

function addFilterValue() {
    var table = $("#filter-value tbody");
    var dlete = '<div class="btn btn-default delete" onclick="deleteFilterValue(this)" type="submit" name="yt0">Удалить</div>';
    var input = '<tr><td td colspan="4" ><input class="form-control" placeholder="Название свойства" name="FilterValue[]" type="text"/></td><td>' + dlete + '</td></tr>'
    table.append(input);
}

function addModelValue() {
    var table = $("#filter-value tbody");
    var dlete = '<div class="btn btn-default delete" onclick="deleteFilterValue(this)" type="submit" name="yt0">Удалить</div>';
    var input = '<tr><td><input class="form-control" placeholder="Название свойства" name="Model[]" type="text"/></td><td>' + dlete + '</td></tr>'
    table.append(input);
}

function deleteModelValue(el) {
    var el = $(el);
    var tr = el.closest("tr");
    var id = el.attr("id");
    if (id != undefined) {
        $.ajax({
            type: "POST",
            url: "/ajax/delete_model",
            data: {id: id},
            success: function (data) {
                if (data == -2) {
                    alert("Вы не можете удалить эту модель, товар этой модели уже создан");
                }
                if (data == 1) {
                    tr.remove();
                }
            }
        })
    } else {
        tr.remove();
    }
    ;
}

function addModelProducer(el, article) {
    if (!article) {
        var article = $('#genereteproduct-article').val();
        if (!article) {
            alert('Заполните артыкль');
            return;
        }
        ;
    }
    var option = el.find("option[value=" + el.val() + "]");
    $.ajax({
        type: "POST",
        url: "/ajax/add_producer_model",
        data: {
            article: article,
            id: el.val(),
            name: option.text(),
        },
        success: function (data) {
            if (data != -1) {
                var input = $('.add-producer tbody tr:nth-child(1) td:nth-child(1)');
                input.append(data);
                option.remove();
            }
        }
    })
}


function deleteFilterValue(el) {
    var el = $(el);
    var tr = el.closest("tr");
    var id = el.attr("id");
    if (id != undefined) {
        $.ajax({
            type: "POST",
            url: "/ajax/delete_filter_value",
            data: {id: id},
            success: function (data) {
                if (data == 1) {
                    tr.remove();
                }
            }
        })
    } else {
        tr.remove();
    }
    ;
}
/**
 * Вставляет в страницу список фильтров
 */
function insertFilter(data) {
    var block = $('#filters');
    block.empty();
    block.append(data);
}

var productName = ''; //для формирования имени продукта
//var categoryName = '';
//var producerName = '';
//var modelName = '';



function insertModel(data) {
    data = jQuery.parseJSON(data);
    var elem = $('#product-model_id').empty();
    elem.append($('<option value="0">Выберите модель</option>'));
    elem.removeAttr("disabled")
    $.each(data, function (index, value) {
        elem.append($('<option value="' + index + '">' + value + '</option>'));
    })
}

function inputListModel(producer_id, id) {
    $.ajax({
        type: "POST",
        url: "/admin/product/list_model_widget",
        data: {id: id, producer_id: producer_id},
        success: function (data) {
//             console.log(data);
            if (data != -1) {
                block = $('#ajax-model');
                block.empty();
                console.log(data);
                block.append(data);
            } else {
                alert('не могу получить список моделей');
            }
        }
    })


}

/**
 *  Снимает ограничения выбора старшей категории при условии
 */
function isDisabled(item) {
    if (item != 0) {
        $("#page-parent_id").removeAttr('disabled');
    } else {
        $("#page-parent_id").attr({"disabled": true});
    }

//     console.log(item);
}

function setDelivery(val) {
    delivery = deliveryPrice[val];
    var block = $('.put-delivery');
    block.empty();
    block.append(delivery + " руб.");
    delivery = parseFloat(delivery);
    var basket_itog_sum = $(".basket_itog_sum");
    var priceS = $(basket_itog_sum[0]).text();
    priceS = parsPrice(priceS);
    if($.inArray(val, ['10','11']) == -1){
        allPrice = priceS + delivery;    
    }else{
        allPrice = priceS
    }
    allPrice = number_format(allPrice, 2, '.', ' ');
    block = $(".put-summ");
    block.empty();
    block.append(allPrice + " руб.");
    check_cash_block();
}

//костыль для методов оплаты
function check_cash_block() {
    var val = $('input[name="Order[delivery_id]"]:checked').val();
    var regions = ['5','6','10','11'];
    var local = ['1','9','4','12'];
    var cash_block = $('#payment_id_1');
    if($.inArray(val, regions) != -1) {
        cash_block.closest('div').hide();
        cash_block.removeAttr('checked');
    } else {
        cash_block.closest('div').show();
    }
    var online_block = $('#payment_id_9, #payment_id_10');
    if($.inArray(val, local) != -1) {
        online_block.closest('div').hide();
        online_block.removeAttr('checked');
    } else {
        online_block.closest('div').show();
    }
}

// вернет список фильтров в виде объукта;
function goUrlFilter() {
    var result = new Object();
    var url = window.location;
    var href = url['href'];
    var filter = url['pathname'].match(/filter\/.+/);
    if (filter != undefined) {
        filter = filter[0].replace(/filter\//g, '');
        filter = filter.replace(/\/$/g, '');

        filter = filter.split("/");
        console.log(filter);
        $.each(filter, function (indx, element) {
            if (indx % 2 == 0) {
                result[element] = filter[indx + 1].split("-");
            }
        });
    }
    ;
    return result;

    url += '/filter/' + filter + '/' + value;



    return;
    location.href = url;
    return;
    var pageObg = url['href'].match(/page=\d+/);
    var page = pageObg ? pageObg[0] : null;

    var url = url['href'].replace(/\?.*/g, '');
    var chekbox = $('[checked=checked]');

    filtObg = getFilt(chekbox, filter, value);
//     filtObg = base64_encode(filtObg);
// console.log(chekbox);
// return;
    if (filtObg != "e30=") {
        url += "/filter/" + filtObg + (page ? "&" + page : '');
    } else {
        url += page ? "/?" + page : '';
    }
    location.href = url;
}


function getFilt(chekbox, filter, value) {
    var compare;
    var filtObg = {};
    var valList = [];
    var i = 0;

    chekbox.each(function (indx, element) {
        var element = $(element);
        var v = element.closest("label").attr('id');
        var f = element.closest("ul").attr('id');

        if (compare == undefined)
            compare = f;
        if (compare == f) {
            valList[i] = v;
        } else {
            valList = [];
            i = 0;
            valList[i] = v;
        }
        compare = f;
        i = i + 1;
        filtObg[f] = valList;
    });

    if (filter in filtObg) {
        var inval = $.inArray(value, filtObg[filter]);
        if (inval == -1) {
            filtObg[filter].push(value);
        } else {
            filtObg[filter].splice(inval, 1);
            if (filtObg[filter][0] == undefined) {
                delete filtObg[filter];
            }
        }
    } else {
        filtObg[filter] = [value];
    }
    return JSON.stringify(filtObg);
}

function Search() {
    var text = $("#search-in").val();
    if (text != '') {
        location.href = "/search/" + text;
    }
}

function overload(elem) {
    var category = '';
    var producer = '';
    var serie = '';

    if ($('#filter-category').val() != undefined) {
        var category = $('#filter-category').val();
    }

    if ($('#filter-producer').val() != undefined) {
        var producer = $('#filter-producer').val();
    }

    if ($('#filter-serie').val() != undefined) {
        var serie = $('#filter-serie').val();
    }

    if ($('#filter-model').val() != undefined) {
        var model = $('#filter-model').val();
    }

    switch ($(elem).attr('name')) {
        case "serie_id" :
            model = '';
            break;
        case "producer_id" :
            serie = model = '';
            break;
    }

    var url = '';
    url += category ? "/" + category : '';
    url += producer ? "/" + producer : '';
    url += serie ? "/" + serie : '';
    url += model ? "/model/" + model : '';
    location.href = url;
}

function selectionModel() {
    var category, producer, model, serie;
    if ($('#filter-category').val()) {
        var category = $('#filter-category').val();
    }

    if ($('#filter-producer').val()) {
        var producer = $('#filter-producer').val();
    }
    if ($('#filter-model').val()) {
        var model = $('#filter-model').val();
    }

    if ($('#filter-serie').val()) {
        var serie = $('#filter-serie').val();
    }
  
    if (model != undefined && $.trim(model) != '' ) {
        location.href = '/' + category + '/' + producer + '/' + serie + '/model/' + model;
    }
}



//$(function(){
//    /*
//     * this swallows backspace keys on any non-input element.
//     * stops backspace -> back
//     */
//    var rx = /INPUT|SELECT|TEXTAREA/i;
//
//    $(document).bind("keydown keypress", function(e){
//        if( e.which == 8 ){ // 8 == backspace
//            if(!rx.test(e.target.tagName) || e.target.disabled || e.target.readOnly ){
//                var url = window.location.href;
//                var url = url.replace(/#prettyPhoto(\[gallery\]\/\d\/)?/g, '');
//                e.preventDefault();
//                var beck = history.length-10;
//                console.log(beck);
//                history.back(beck);
//                history.go(-1).
////                location.href = url;
//            }
//        }
//    });
//});

function base64_encode(data) {
    //  discuss at: http://phpjs.org/functions/base64_encode/
    // original by: Tyler Akins (http://rumkin.com)
    // improved by: Bayron Guevara
    // improved by: Thunder.m
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Rafał Kukawski (http://kukawski.pl)
    // bugfixed by: Pellentesque Malesuada
    //   example 1: base64_encode('Kevin van Zonneveld');
    //   returns 1: 'S2V2aW4gdmFuIFpvbm5ldmVsZA=='
    //   example 2: base64_encode('a');
    //   returns 2: 'YQ=='

    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
            ac = 0,
            enc = '',
            tmp_arr = [];

    if (!data) {
        return data;
    }

    do { // pack three octets into four hexets
        o1 = data.charCodeAt(i++);
        o2 = data.charCodeAt(i++);
        o3 = data.charCodeAt(i++);

        bits = o1 << 16 | o2 << 8 | o3;

        h1 = bits >> 18 & 0x3f;
        h2 = bits >> 12 & 0x3f;
        h3 = bits >> 6 & 0x3f;
        h4 = bits & 0x3f;

        // use hexets to index into b64, and append result to encoded string
        tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while (i < data.length);

    enc = tmp_arr.join('');

    var r = data.length % 3;

    return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);
}

function base64_decode(data) {
    var b64 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
            ac = 0,
            dec = '',
            tmp_arr = [];

    if (!data) {
        return data;
    }

    data += '';

    do { // unpack four hexets into three octets using index points in b64
        h1 = b64.indexOf(data.charAt(i++));
        h2 = b64.indexOf(data.charAt(i++));
        h3 = b64.indexOf(data.charAt(i++));
        h4 = b64.indexOf(data.charAt(i++));

        bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

        o1 = bits >> 16 & 0xff;
        o2 = bits >> 8 & 0xff;
        o3 = bits & 0xff;

        if (h3 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1);
        } else if (h4 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1, o2);
        } else {
            tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
        }
    } while (i < data.length);

    dec = tmp_arr.join('');

    return dec.replace(/\0+$/, '');
}

function number_format(number, decimals, dec_point, thousands_sep) {
    number = (number + '')
            .replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                return '' + (Math.round(n * k) / k)
                        .toFixed(prec);
            };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '')
            .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
                .join('0');
    }
    return s.join(dec);
}

// Костыли кассы

/*

jQuery(document).ready(function(){

    $ = jQuery;

    var baseFormAction = jQuery('form#user').attr('action');
    var kassaFormAction = jQuery('#formAction').val();
    // var kassaFormAction = '/ymyeah.php';

    $('#payment_html input').on('change',function(){

        if ($(this).val() == '9' || $(this).val() == '10') {
            
            $('form#user').attr('action',kassaFormAction);

            if ($(this).val() == '9') {
                $('#paymentType').attr('value','AC');
            }

            if ($(this).val() == '10') {
                $('#paymentType').attr('value','PC');
            }

        } else {

            $('form#user').attr('action',baseFormAction);

        }

    });

    $(document).on('submit','form#user',function(){

        if ($('#payment_html input:checked').val() == '9' || $('#payment_html input:checked').val() == '10') {

            var summ = parseFloat($('span.put-summ').text().replace(/\s/g, ''));
            $('#kassaSum').attr('value',summ);

            console.log(summ);

            var orderEmail = $('#order-email').val();
            $('#customerNumber').attr('value',orderEmail);
            $('#cps_email').attr('value',orderEmail);

            var orderPhone = $('#order-phone').val();
            $('#cps_phone').attr('value',orderPhone);

            $(this).prepend('<input type="hidden" name="orderdata" value="'+($(this).serialize())+'&isorderpayed=yeah" />');

            $(this).find('[name*=Order]').remove();

        }

    });

});

*/