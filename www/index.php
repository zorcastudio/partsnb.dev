<?php
// comment out the following two lines when deployed to production
//ввімкнем дебагер тільки для визначеного IP
if(in_array($_SERVER["REMOTE_ADDR"], ['127.0.0.1', '37.139.97.76'])){
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    defined('YII_ENV') or define('YII_ENV', 'dev');
}
else {
    defined('YII_ENV') or define('YII_ENV', 'prod');
}

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');

function debug(){
    if(in_array($_SERVER["REMOTE_ADDR"], ['127.0.0.1']))
        return true;
    else
        return false;
}

function die_my(){
    if(in_array($_SERVER["REMOTE_ADDR"], ['127.0.0.1']))
        die("<br/>end");
}

function vd($var, $exit = true)
{
    if(in_array($_SERVER["REMOTE_ADDR"], ['127.0.0.1'])){
        //header("Content-type: text/html; charset=utf-8");
        $dumper = new yii\helpers\BaseVarDumper();
        echo $dumper::dump($var, 10, true);
        if ($exit)
            exit;
    }
}

(new yii\web\Application($config))->run();
