<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\Filter;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class FilterSearch extends Filter
{
    /**
     * @inheritdoc
     */
   public function rules() {
        return [
            [['name', 'name_lat'], 'string', 'max' => 20],
            [['id', 'category_id'], 'safe'],
            ['name_lat', 'match', 'pattern' => '/^[A-Za-z0-9 \-]+$/u','message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-".'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    public function search($params){
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'name' => $this->name,
            'id' => $this->id,
            'category_id' => $this->category_id,
        ]);
//        $query->andFilterWhere(['like', 'name' => $this->name]);
        return $dataProvider;
    }
}