<?php
namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Novosti;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class NovostiSearch extends Novosti
{
    /**
     * @inheritdoc
     */
   public function rules() {
        return [
            [['name'], 'match', 'pattern' => '/^[A-Za-zА-Яа-я0-9 \-]+$/u','message' => 'В названии новости допускаются только буквы и цифры, знак "-" и пробелы.'],
            [['name_lat'], 'match', 'pattern' => '/^[A-Za-z0-9 \-]+$/u','message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-" и пробелы.'],
            [['text'], 'string'],
            [['id', 'is_published'], 'integer'],
        ];
    }

    public function search($params){
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'text' => $this->text,
            'is_published' => $this->is_published,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'name_lat', $this->name_lat]);

        return $dataProvider;
    }
}