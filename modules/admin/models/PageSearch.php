<?php
namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Page;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class PageSearch extends Page
{
    /**
     * @inheritdoc
     */
   public function rules() {
        return [
            [['name'], 'match', 'pattern' => '/^[A-Za-zА-Яа-я0-9 \-]+$/u','message' => 'В названии категории допускаются только буквы и цифры, знак "-" и пробелы.'],
            [['name_lat'], 'match', 'pattern' => '/^[A-Za-z0-9 \-]+$/u','message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-" и пробелы.'],
            [['text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    public function getUrl() {
        if($this->parent_id)
            return "/".$this->parentPage->name_lat."/".$this->name_lat;
        else 
            return "/".$this->name_lat;
    }

    public function search($params){
        $query = self::find()->where(['is_delete' => 0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'text' => $this->text,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'name_lat', $this->name_lat]);

        return $dataProvider;
    }
}