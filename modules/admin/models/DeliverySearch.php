<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\Delivery;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class DeliverySearch extends Delivery
{
    /**
     * @inheritdoc
     */
   public function rules() {
        return [
            [['id', 'status', 'is_delete'], 'integer'],
            [['description'], 'string'],
            [['id', 'name', 'description', 'status'], 'safe', 'on' => 'search'],
        ];
    }

    public function getUrl($result = null){
        $url = '/'.parent::getUrl();
        return $url;
    }
    
    
    public function search($params){
        $query = self::find()->where(['is_delete'=>0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'status' => $this->status,
        ]);
        
        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}