<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\Category;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class CategorySearch extends \app\models\Category
{
    /**
     * @inheritdoc
     */
   public function rules() {
        return [
            [['name_lat', 'name'], 'unique'],
            [['id', 'parent_id', 'is_delete'], 'integer'],
            [['name', 'name_lat'], 'string', 'max' => 255],
            [['name'], 'match', 'pattern' => '/^[A-Za-zА-Яа-я0-9\- ]+$/u','message' => 'В названии категории допускаются только буквы и цифры, знак "-".'],
            [['name_lat'], 'match', 'pattern' => '/^[A-Za-z0-9 \-]+$/u','message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-".'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    public function getUrl($result = null){
        $url = '/'.parent::getUrl();
        return $url;
    }
    
    public function search($params){
        $query = self::find()->where(['is_delete'=>0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'parent_id' => $this->parent_id,
        ]);
        
        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}