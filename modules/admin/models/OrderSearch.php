<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\Order;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends \app\models\Order
{
    /**
     * @inheritdoc
     */
   public function rules() {
        return [
            [['payment_id'], 'in', 'range' => [1,2,3]],
            [['zip_code', 'status', 'user_id', 'delivery_id', 'payment_id', 'country_id', 'city_id', 'region_id'], 'integer'],
            [['phone'], 'match', 'pattern' => '/^[+()0-9]+$/u', 'message' => 'Недопустимый формат телефона допускается "+()0-9"'],
            [['id', 'price', 'description', 'createtime', 'full_name', 'address'], 'safe'],
            [['id', 'user_id', 'delivery_id', 'payment_id', 'price', 'country_id', 'city_id', 'region_id', 'full_name', 'address'], 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    public function getUrl($result = null){
        $url = '/admin/order/view/?id='.$this->id;
        return $url;
    }
    
    public function search($params){
        $query = self::find()
                ->select("order.*, (order.price + delivery.price) AS sumPrice")
                ->join('INNER JOIN','delivery', 'order.delivery_id = delivery.id')
                ->where(['order.is_delete'=>0]);

        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
    
        $query->andFilterWhere([
            'order.id' => $this->id,
            'createtime' => $this->createtime,
            'full_name' => $this->full_name,
            'order.price' => $this->price,
            'delivery_id' => $this->delivery_id,
            'payment_id' => $this->payment_id,
            'country_id' => $this->country_id,
            'order.status' => $this->status,
            'zip_code' => $this->zip_code,
        ]);
        return $dataProvider;
    }
}