<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\modules\admin\components\TranslitFilter;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class ModelSearch extends \app\models\Model
{
    /**
     * @inheritdoc
     */
   public function rules() {
        return [[['id', 'name', 'producer_id', 'is_model', 'serie_id', '_task', 'delete', 'name_lat', 'description'], 'safe']];
    }
    
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    
    public function afterFind(){
        if(isset($_GET['ModelSearch'])){
            if(isset($_GET['ModelSearch']['_task'])){
                $this->_task = $_GET['ModelSearch']['_task'];
            }
        }
        parent::afterFind();
    }

    public function getUrl($result = null){
        $url = '/'.parent::getUrl();
        return $url;
    }
    
    public static function getTask($set=false){
        $result = [1=>'Отсутствует в товаре'];
        if($set){
            return isset($result[$set]) ? $result[$set] : false;
        }else
            return $result;
    } 

    /**
     * устанавливает для модели дополнительные условия 
     * на проверку существования товара этой модели
     * @param type $query
     */
    public static function setQueryNoneProduct($query) {
        $query->join('LEFT JOIN', 'product', 'product.model_id = model.id'
        );
        $query->andWhere('product.model_id IS NULL');
    }

    public function search($params){
        $query = self::find();
        $this->load($params);
        
//        if($this->is_model){
//            $query->where(['not', ['serie_id'=>null]]);
//        }else{
//            $query->where(['serie_id'=>null]);
//        }
        
        if (!$this->validate()) {
            return $dataProvider;
        }
        if($this->serie_id == '0'){
            $query->where('model.serie_id IS Null');
        }else
            $query->filterWhere(['model.serie_id'=>$this->serie_id]);
       
        
        $query->andFilterWhere([
                'model.producer_id' => $this->producer_id,
                'model.id' => $this->id,
                'model.delete' => $this->delete
                ])
            ->andFilterWhere(['like', 'model.name', "%".$this->name."%", false]);
        
        if(!empty($this->_task)){
            switch ($this->_task){
                case 1: self::setQueryNoneProduct($query); //добавляет в запрос условия при которых выбраны только те модели которых нет в товарах
            }
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        return $dataProvider;
    }

    public function generateAliases() {
        $models = self::find()->where(['name_lat'=>''])->all();
        foreach($models AS $model) {
            if (!$model->name_lat) {
                $model->name_lat = TranslitFilter::translitUrl($model->name);
                if($model->name_lat) {
                    $model->save();
                }
            }
        }
    }
}