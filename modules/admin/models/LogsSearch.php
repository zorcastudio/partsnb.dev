<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class LogsSearch extends \yii\db\ActiveRecord
{
    


    public static function tableName() {
        return "logs";
    }
    /**
     * @inheritdoc
     */
   public function rules() {
        return [[['id', 'type', 'createtime', 'finishtime', 'count', 'not_error'], 'safe']];
    }
    
    public function attributeLabels() {
        return [
            'id' => "ID",
            'type' => "Тип синхронизации",
            'createtime' => "Время создания",
            'finishtime' => "Время окончания",
            'not_error' => "Ошибка",
            'count' => "Количество",
            'file' => "Файл",
        ];
    }
    
    public static function itemAlias($type, $code = NULL) {
        $_items = [
            'type' => [
                0 => 'yandex-маркет',
                1 => 'google',
                2 => 'сайт-мап',
                3 => 'экспорт/импорт',
                4 => 'avito',
            ],
        ];
        if (isset($code)) {
            return isset($_items[$type][$code]) ? $_items[$type][$code] : false;
        } else
            return isset($_items[$type]) ? $_items[$type] : false;
    }
    
    public function getFile(){
        $public_path = Yii::$app->params['public_path'];
        $path = Yii::$app->basePath ."/{$public_path}";
        switch ($this->type){
            case 0 : $name = "yandexmarket_".date('Y.m.d H:i:s',$this->createtime).".yml";  break;
            case 1 : $name = "products_GM_".date('Y.m.d H:i:s', $this->createtime).".xml"; break;
            case 3 : $name = "Import.".date('Y.m.d H:i:s', $this->createtime).".xml"; $path = Yii::$app->basePath ."/sync"; break;
            case 4 : $name = "avito_".date('Y.m.d H:i:s',$this->createtime).".yml";  break;
            default : $name = false;
        }
        
       if($name && file_exists("{$path}/history/{$name}")){
            return Html::a('<span class="glyphicon glyphicon-download-alt"></span>', "/admin/logs/downloadfile/{$name}");
        }else
            return NULL;
    }


    public function search($params){
        $query = self::find();
        $this->load($params);
        
        if (!$this->validate()) {
            return $dataProvider;
        }
        
        $query->filterWhere([
                'id' => $this->id,
                'type' => $this->type,
                'count' => $this->count,
            ]);
        
        if($this->createtime){
            $unix = strtotime($this->createtime);
            $this->createtime = (strtotime(date('d', $unix).".".date('m', $unix).".".date('Y', $unix)));
            $finish = (strtotime(date('d', $unix).".".date('m', $unix).".".date('Y', $unix)." 23:59:59"));
            $query->andWhere("createtime > {$this->createtime} AND createtime < {$finish}");
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        return $dataProvider;
    }
}

//17.3 n173hge-l21