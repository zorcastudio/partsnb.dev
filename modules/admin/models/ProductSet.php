<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\Product;
use app\models\ModelCompatible;
use app\models\ProductFilterValue;
use app\models\FilterValue;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class ProductSet extends \app\models\Product
{
    public $_isDeskription;
    public $_article;
    /**
     * @inheritdoc
     */
   public function rules() {
        return [];
    }
    /**
     * Устанавливает связь фильтров с товарами
     */
    public static function setFilter($s, $f){
        $product = self::find()
                ->where([
                    'product._is_problem'=>0,
                    'product.status'=>1,
                    'product.is_delete'=>0,
                ])
                ->andWhere(['not',['product.category_id'=>null]])
                ->andWhere(['not',['product.producer_id'=>null]])
                ->andWhere(['not',['product.serie_id'=>null]])
                ->andWhere(['between', 'product.id',  $s, $f])
                ->select('product.id, product.category_id, product.serie_id, product.producer_id')
                ->joinWith('product_category_filter')
                ->asArray()
                ->all();
      
        $filterValue = FilterValue::find()
                ->asArray()
                ->all();
        $result = [];
        $output = [];
        $output[] = "$s-$f";
        foreach ($filterValue as $value) {
            $result[$value['filter_id']][]=$value['filter_value_id'];
        }
        foreach ($product as $value) {
            foreach ($value['product_category_filter'] as $item) {
                $productFilterValue = ProductFilterValue::find()
                        ->where([
                            'product_id'=>$value['id'],
                            'category_id'=>$value['category_id'],
                            'filter_id'=>$item['id'],
                        ])
                        ->one();
                if(!$productFilterValue){
                    foreach ($value['product_category_filter'] as $filter) {
                        $productFilterValue = new ProductFilterValue();
                        $productFilterValue->product_id = $value['id'];
                        $productFilterValue->category_id = $value['category_id'];
                        $productFilterValue->serie_id = $value['serie_id'];
                        $productFilterValue->producer_id = $value['producer_id'];
                        $productFilterValue->filter_id = $filter['id'];
                        $productFilterValue->filter_value_id = $result[$filter['id']][0];
                        $productFilterValue->save();
                        $output[] = $value['id'];
                    }
                }
            }
        }
        return $output;
    }

        /**
     * Помечаем товары которые имеют одинаковые значения по производителю, модели и артикулу 
     */
    public static function setProblemDuble(){
        $product = self::find()
                ->where(['_is_problem'=>0])
                ->andWhere(['like','article','09%', FALSE])
//                ->andWhere(['between', 'id', 0, 10])
                ->select('id, article, category_id, producer_id, model_id, name, _is_problem')
                ->all();
        $result= [];
        foreach ($product as $value) {
            $result[$value->id] = $value->article." ".$value->producer_id." ".$value->model_id;
        }
        $countDuble = array_count_values($result);
        foreach ($countDuble as $key=>$value) {
            if($value > 1){
                $serchData = explode(' ', $key);
                $dubleProduct = self::find()
                        ->where([
                            'article'=>$serchData[0],
                            'producer_id'=>$serchData[1],
                            'model_id'=>$serchData[2]
                        ])
                        ->andWhere(['not', ['like','name','%EMACHINES%', FALSE]])
                        ->select('id, article, category_id, producer_id, model_id, name, _is_problem')
                        ->all();
                foreach ($dubleProduct as $id=>$item) {
                    if($id > 1){
                        $item->_is_problem = 4; // такой товар уже есть
                        $item->update();
                    }
                }
            }
        }
    }
    
    /**
     * Создает товар которого нет но есть связь и модель
     */
    public static function createProductCompotible($article, $s, $f){
        $result = [];
        $compotible = \app\models\ModelCompatible::find()
                ->where(['like','model_compatible.article', $article.'%', FALSE])
                ->andWhere(['between', 'model_compatible.model_id',  $s, $f])
                ->joinWith('model')
                ->asArray()
                ->all();
        
        foreach ($compotible as $value) {
            if($value['model']){
                $product = Product::find()
                        ->where([
                            'article'=>$value['article'], 
                            'producer_id'=>$value['model']['producer_id'], 
                            'model_id'=>$value['model']['id'],
                            '_is_problem'=>0
                        ])
                        ->select('id, article, producer_id, model_id, name')
                        ->asArray()
                        ->one();
                if(!$product) {
                    $copyProduct = Product::find()
                            ->where(['article' => $value['article'], "_is_problem" => 0])
    //                        ->andWhere(['not', ['producer_id'=>null]])
    //                        ->andWhere(['not', ['_list_model'=>null]])
                            ->one();

                    $productNew = new self();
                    $productNew->in_stock1 = $copyProduct->in_stock1;
                    $productNew->in_stock2 = $copyProduct->in_stock2;
                    $productNew->in_res1 = $copyProduct->in_res1;
                    $productNew->in_res2 = $copyProduct->in_res2;
                    $productNew->trade_price = $copyProduct->trade_price;
                    $productNew->is_timed = $copyProduct->is_timed;
                    $productNew->category_id = $copyProduct->category_id;
                    $productNew->price = $copyProduct->price;
                    $productNew->producer_id = $value['model']['producer_id'];
                    $productNew->model_id = $value['model']['id'];
                    $productNew->name = self::createProductName($copyProduct->category_id, $value['model']['producer_id'], $value['model']['id']);
                    $productNew->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($productNew->name);
                    $productNew->logo = $copyProduct->logo;
                    $productNew->available = $copyProduct->available;
                    $productNew->status = $copyProduct->status;
                    $productNew->_original_id = $copyProduct->id;
                    $productNew->createtime = time();
                    $productNew->_is_new = 2; //создан с модели
                    $productNew->article = $value['article'];
                    $imageList = \app\models\Image::findAll(['product_id' => $copyProduct->id]);
                    if ($productNew->save() && $imageList) {
                        foreach ($imageList as $img) {
                            $image = new \app\models\Image();
                            $image->attributes = $img->attributes;
                            $image->product_id = $productNew->id;
                            $image->save();
                        }
                    }
                }
                $result [] = $value['model_id'];
            }else{
                $compotibleDell = \app\models\ModelCompatible::find()
                    ->where(['model_id'=>$value['model_id'], 'article'=>$value['article']])
                    ->one();
                $compotibleDell->delete();      
            }
           
            
       }
       return json_encode($result);
    }
    
    public static function updateUrlProduct(){
        $product = self::find()
                ->select('id, name_lat, name')
                ->where(['between', 'id', 45000, 50000])

//                ->where(['like','name_lat','%"%', FALSE])
                ->all();
        foreach ($product as $value) {
            $value->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($value->name);
            $value->update();
        }
    }
    
    
    /**
     * Востановление оригинальных названий 
     * с исходнного сайта
     */
    public static function updateProduct(){
        ini_set("memory_limit","200M");
        $product = self::find()
                ->where(['not', ['_original_id'=>null]])
                ->select('id, name, name_lat, old_name_lat, _original_id')
                ->andWhere(['between', 'id', 16000, 40000])
                ->all();
        
        foreach ($product as $value) {
            $connect = \app\models\settingValues::Connection();
            $result = $connect->createCommand("SELECT product_name, slug  FROM   j251_virtuemart_products_ru_ru WHERE virtuemart_product_id = ".$value->_original_id)->queryOne();
            $value->name = $result['product_name'];
            $value->old_name_lat = $result['slug'];
            $value->update();
        }
    }
    
    public static function setProductProblem(){
        $product = self::find()
                ->where([
                    '_is_problem'=>0,
                    'status'=>1,
                    'is_delete'=>0,
                ])
                ->andWhere(['like','name','%-%', FALSE])
                ->select('name, id, model_id, producer_id, category_id, article')
                ->all();
        
        foreach ($product as $value) {
//            $value->_is_problem = 5;
//            vd($value->update(), false);
//            $compatible = \app\models\ModelCompatible::find()
//                        ->where(['model_id'=>$value->model_id, 'article'=>$value->article, '_is_problem'=>0])
//                        ->one();
//            if($compatible){
//                vd($compatible, false);
//            }
            
        }
    }
    
    /**
     * Ищет свойства товаров в старой БД и присваивает их новым товарам
     */
    public static function setProperties($f, $l){
        //создаем массив с моей таблици свойств товаров с ключами старой джумлы
        $_properties = \app\models\Properties::find()->asArray()->all();
        $properties = [];
        foreach ($_properties as $value) {
            $properties[$value['v_mart_id']] =  $value['id'];
        }
        
        $product = self::find()
                ->where([
                    'status'=>1,
                    'is_delete'=>0,
                ])
                ->andWhere(['not',['_original_id'=>NULL]])
                ->andWhere(['between', 'id', $f, $l])
                ->select('id, _original_id')
                ->all();
        
        $v_properties = [];
        foreach ($product as $value) {
           $connect = \app\models\settingValues::Connection();
           $v_properties  = $connect->createCommand("SELECT virtuemart_product_id, virtuemart_custom_id, custom_value, ordering  FROM  j251_virtuemart_product_customfields WHERE virtuemart_product_id = $value->_original_id")->queryAll();
           
           foreach ($v_properties as $item) {
                $productProperties = \app\models\ProductProperties::findOne(['product_id'=>$value->id, 'properties_id' =>$properties[$item['virtuemart_custom_id']] ]);
                if(!$productProperties)
                    $productProperties = new \app\models\ProductProperties();
                
                $productProperties->product_id = $value->id;
                $productProperties->properties_id =  $properties[$item['virtuemart_custom_id']];
                $productProperties->value = $item['custom_value'];
                $productProperties->ordering = $item['ordering'];
                $productProperties->save();
           }
           return "$f-$l";
        }
    }
    
    public static function updateProductNull(){
        $product = self::find()
                ->where(['model_id'=>0]) //'serie_id'=>0 ,'model_id'=>0
                ->select('id, producer_id, model_id, serie_id')
                ->all();
        foreach ($product as $value) {
            $value->producer_id = null;
            $value->model_id = null;
            $value->serie_id = null;
            $value->update();
        }
    }
    
    public static function getError($s, $f){
        $result = [];
        foreach (\app\models\ModelCompatible::find()->where(['between', 'model_id', $s, $f])->all() as $value) {
            if (!Product::find()->where(['article'=>$value->article])->one()) {
                $result[] = $value->article;
            }
        }
        return json_encode($result);
    }
    
    public static function getCompatibleError(){
        $compotible = ModelCompatible::find()
//                ->where(['like','article','010002', FALSE])
                ->where(['article'=>'010002', 'model_id'=>4378])
                ->joinWith('products')
                ->one();
       
        $result = [];
        foreach (\app\models\ModelCompatible::find()->where(['between', 'model_id', $s, $f])->all() as $value) {
            if (!Product::find()->where(['article'=>$value->article])->one()) {
                $result[] = $value->article;
            }
        }
        return json_encode($result);
    }
    
    
    /**
     * удаляем все товары и связи с ними
     */
    public static function deleteProduct(){
        $product = self::find()
                ->where(['article'=>'013000'])
                ->select('id, name, article')
                ->all();
        foreach ($product as $value) {
            $filterValue = \app\models\ProductFilterValue::findOne(['product_id'=>$value->id]);
            if($filterValue)
                $filterValue->delete();
            $compatible = ModelCompatible::findOne(['model_id'=>$value->model_id,'article'=>$value->article]); 
            if($compatible)
                $compatible->delete();
            $value->delete();
        }
    }
}