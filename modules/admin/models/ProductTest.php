<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\Product;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class ProductTest extends \yii\db\ActiveRecord
{
    public $_isDeskription;
    public $_article;
    /**
     * @inheritdoc
     */
    
    
    public static function tableName() {
        return "_product";
    }

    
    public function rules() {
        return [];
    }
    
    
    
    public static function updateProduct(){
        ini_set("memory_limit","200M");
        $product = self::find()
                ->where(['not', ['_original_id'=>null]])
                ->andWhere(['not', ['category_id'=>4]])
                ->andWhere(['between', 'id', 40000, 100000])
                ->select('id, name')
                ->asArray()
                ->all();
        
        foreach ($product as $value) {
            $model = Product::find()
                    ->where(['id'=>$value['id']])
                    ->select('id, name')
                    ->one();
            if($model){
                $model->name = $value['name'];
                $model->update();
            }
        }
    }
}