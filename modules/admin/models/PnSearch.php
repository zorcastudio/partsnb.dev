<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;


class PnSearch extends \yii\db\ActiveRecord {
    
    public $all_article;
    public $_children;
    public $children = [];
    public $article_id;

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "product_pn_search";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [
            [['id', 'article_id'], 'integer'],
            [['article'], 'string', 'max' => 6],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return [
            'id' => "ID",
            'article' => "Артикль",
            'article_id' => "Артикль",
            'children' => "Совместимые парт-номера",
            'name' => "Название партномера",
        ];
    }
    
    public function setAll_article() {
        $model = self::find()
                ->select(['id','article'])
                ->groupBy(['article'])
                ->all();
        $result = [];
        foreach ($model as $value) {
           $result[$value['id']]= $value['article'];
        }
        $this->all_article = $result;
        return TRUE;
    }
    
    public function setNotArticle() {
        $model = \app\models\Product::find()
                ->select(['id','article'])
                ->where('article  NOT IN (SELECT article FROM  product_pn_search)')
                ->groupBy(['article'])
                ->asArray()
                ->all();
        $result = [];
        foreach ($model as $value) {
           $result[$value['id']]= $value['article'];
        }
        $this->all_article = $result;
        return TRUE;
    }
    
    public static function saveChildren($post, $pn){
        //створюємо нові парт номера
        if(is_array($post['children'])){
            foreach (array_diff($post['children'], $pn->children) as $value) {
                $model = new self();
                
                if(isset($post['article_id']))
                    $model->article = $pn->all_article[$post['article_id']];
                else 
                    $model->article = $pn->all_article[$pn->article_id];
                
                $model->name = $value;
                if(!$model->save())
                    return FALSE;
            }
            //Видалення парт номерів
            self::deleteAll(['id'=> array_keys(array_diff($pn->children, $post['children']))]);
        }else{
            $pn->delete();
        }
            return TRUE;
    }

    public function search($params){
        $this->load($params);

        Yii::$app->db->createCommand('SET group_concat_max_len = 500000')->execute();
        $query = self::find()
                ->select('id, article, GROUP_CONCAT(name SEPARATOR "; " ) AS name')
                ->groupBy('article');
        $query->andFilterWhere(['id'=>$this->id]);
        $query->andFilterWhere(['like', 'article', '%'.$this->article."%", false]);
        $query->andFilterWhere(['like', 'name', "%".$this->name."%", false]);
        
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        $this->validate();
        return $dataProvider;
    }
}
