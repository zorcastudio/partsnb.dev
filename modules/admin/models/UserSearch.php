<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\User;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
   public function rules() {
        return [
            [['zip_code', 'id', 'phone', 'country_id', 'region_id', 'city_id'], 'integer'],
            [['email'], 'email'],
            [['username'], 'string', 'max' => 20, 'min' => 3, 'message' => 'Длина имени пользователя от 3 до 20 символов.'],
            [['password'], 'string', 'max' => 128, 'min' => 4, 'message' => 'Минимальная длина пароля 4 символа.'],
            [['username'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В имени пользователя допускаются только латинские буквы и цифры.'],
            [['name'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В имени пользователя допускаются только буквы и цифры.'],
            [['middle_name'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В отчестве пользователя допускаются только буквы и цифры.'],
            [['surname'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В фамилия пользователя допускаются только буквы и цифры.'],
            [['address'], 'match', 'pattern' => '/^[A-Za-z0-9А-Яа-я\s,]+$/u', 'message' => 'В в почтовом индексе допускаются только латинские буквы и цифры.'],
            [['status'], 'in', 'range' => [self::STATUS_NOACTIVE, self::STATUS_ACTIVE, self::STATUS_BANED, self::STATUS_MODERATOR, self::STATUS_ADMIN]],
            [['name', 'surname', 'middle_name'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 12],
            [['address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    public function getUrl(){
        return "/admin/user/update/".$this->username;;
    }

    
    public function search($params){
        $query = self::find()->andWhere('status !=-1');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'name' => $this->name,
            'username' => $this->username,
            'surname' => $this->surname,
            'middle_name' => $this->middle_name,
            'phone' => $this->phone,
            'email' => $this->email,
            'country_id' => $this->country_id,
            'region_id' => $this->region_id,
            'city_id' => $this->city_id,
            'address' => $this->address,
        ]);
        
            $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}