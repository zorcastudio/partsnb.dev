<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;


class ModelTrue extends \yii\db\ActiveRecord {
    
    public $all_article;
    public $_children;
    public $children = [];
    public $article_id;

    /**
     * @return string the associated database table name
     */
    public static function tableName() {
        return "model_true";
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return [];
    }
    
    /**
     * 
     */
    public static function setNameProducer(){
        $list = Yii::$app->db->createCommand("
            SELECT *
            FROM model_true
            WHERE (producer_name IS NULL OR producer_name = '') AND prev_name IS NULL 
            ORDER BY id
        ")->queryAll();
        
        foreach ($list as $value) {
            $prev = self::find()
                    ->where(['id'=>$value['id']-1])
                    ->asArray()
                    ->one();
            $producer = $prev['producer_name'] ? $prev['producer_name'] : $prev['prev_name'];
            vd(self::updateAll(['prev_name'=>$producer, 'producer_name'=>$producer], ['id'=>$value['id']]), false);
        }
    }
    
    
    public static function parsFile(){
         $result = [];
        $step = false;
        $i=0;
        $public_path = Yii::$app->params['public_path'];
        foreach (file(Yii::$app->basePath . "/{$public_path}/_model_pars.txt") as $key => $value) {
            $res_1 = preg_match('/^"(\d{6})"/iu', $value, $search1);
            if($res_1){
                $i++;
                $step = true;
                $article = $search1[1];
                $value = trim(preg_replace("/\t/iu", '', $value));
                $result[$i][$article]['models']['name'] = preg_replace('/"'.$search1[1].'";/', '', $value);
            }
            
            $res_2 = preg_match('/^Совместим.*/iu', $value, $search2);
            if(!$res_1 && $res_2){
                $step = false;
                $result[$i][$article]['pn']['name'] = trim(preg_replace("/\t/iu", '', $value));
            }
            
            if(!$res_1 && !$res_2){
                $value = trim(preg_replace("/\t/iu", '', $value));
                if($step)
                    $result[$i][$article]['models']['id'][] = $value;
                else
                    $result[$i][$article]['pn']['id'][] = $value;
            }

//            if($key > 600 )
//                break;
        }
        
        $str = '';
        $name = '';
        foreach ($result as $value) {
            foreach ($value as $article => $item) {
                foreach ($item['models'] as $val_1) {
                    if(is_array($val_1)){
                        foreach ($val_1 as $v1) {
                            $str .= "('{$article}', '{$name}', 0, '{$v1}'),\n";
//                            vd($str, false);
//                            echo "<br/>";
                        }
                    }else{
                        $name = $val_1;
                    }
                }
                if(isset($item['pn'])){
                    foreach ($item['pn'] as $val_2) {
                        if(is_array($val_2)){
                            foreach ($val_2 as $v2) {
                                $str .= "('{$article}', '{$name}', 1, '{$v2}'),\n";
    //                            vd($str, false);
    //                            echo "<br/>";
                            }
                        }else{
                            $name = $val_2;
                        }
                    }
                }
            }
        }
        $str = preg_replace("/,$/iu", ';', $str);
        
        Yii::$app->db->createCommand("
            INSERT INTO `model_true_` (`article`, `name`, `is_pn`, `model`) 
            VALUES {$str};
        ")->execute();
    }

    

    public static function setSerie(){
//         $model = self::find()
//                ->from('model_true mt')
//                ->select('mt.*, s.id AS serie_id')
//                ->leftJoin("(SELECT id, producer_id, `name` FROM serie WHERE `name` != 'p/n' ORDER BY `name` DESC) s", "`s`.`producer_id`=mt.producer_id 
//                AND mt.model RLIKE CONCAT('^',s.`name`)")
//                ->where(['mt.is_pn'=>0])
//                ->andWhere('s.id IS NOT NULL')
//                ->andWhere('mt.serie_id_ IS NULL')
//                ->groupBy('mt.id')
//                ->orderBY('mt.id')
//                ->limit(100)
//                ->all();
        $list = Yii::$app->db->createCommand("
            SELECT mt.id, mt.article,  s.id AS serie_id, mt.is_pn, mt.producer_id, mt.model
            -- ,'#', `s`.`id` AS `serie_id`, pr.name AS `Производитель`, s1.name AS `Серия`, s.name 
            FROM `model_true` `mt`
            -- LEFT JOIN serie s1 ON mt.serie_id = s1.id 
            -- LEFT JOIN producer pr ON mt.producer_id = pr.id 
            LEFT JOIN (
                SELECT id, producer_id, `name` 
                FROM serie WHERE `name` != 'p/n' 
                ORDER BY `name` DESC) s ON `s`.`producer_id`=mt.producer_id 
                AND mt.model RLIKE CONCAT('^',s.`name`) 
            WHERE `mt`.`is_pn`=0 
            -- AND mt.serie_id != s.id
            AND mt.serie_id IS NULL
            -- AND s.id IS NOT NULL
            -- AND mt.serie_id IS NULL 
            -- AND mt.article = '080001'
            GROUP BY `mt`.`id` 
            ORDER BY `mt`.`id`
        ")->queryAll();
        
        foreach ($list as $value) {
            vd(self::updateAll(['serie_id'=>$value['serie_id']], ['id'=>$value['id']]), false);
        }
    }
    
    public static function setGroupProduct(){
        ini_set("memory_limit","2000M");
        ignore_user_abort(true);
        set_time_limit(0);
        
        $model = self::find()
                ->select(['id', 'is_pn', '_group'])
                ->orderBy('id')
                ->all();
        vd($model, false);
        die("<br/>end");
        
        $i = 0;
        $is_pn = 0;
        foreach ($model as $key => $value) {
            if($is_pn != $value['is_pn']){
                $is_pn = $value['is_pn'];
                $i++;
            }
            $value->_group = $i;
            if(!$value->save())
                return false;
            return true;
        }
    }
  
}
