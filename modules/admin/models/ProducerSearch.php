<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class ProducerSearch extends \app\models\Producer
{
    /**
     * @inheritdoc
     */
   public function rules() {
        return [[['id', 'name', 'name_lat'], 'safe']];
    }
    
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    public function getUrl($result = null){
        $url = '/'.parent::getUrl();
        return $url;
    }
    
    public function search($params){
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        $this->load($params);

        if (!$this->validate()) {
                return $dataProvider;
        }
        
        $query->andFilterWhere(['id' => $this->id]);
        $query->andFilterWhere(['like', 'name', "%".$this->name."%", false]);
        return $dataProvider;
    }
}