<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\Product;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class ProblemSearch extends \app\models\Product
{
    public $_isDeskription;
    public $_count;
    /**
     * @inheritdoc
     */
   public function rules() {
        return [
            [['category_id', 'price', 'createtime', 'is_problem', 'producer_id'], 'integer'],
            [['available', '_isDeskription'], 'boolean'],
            ['name_lat', 'match', 'pattern' => '/^[A-Za-z0-9 \-]+$/u', 'message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-" и пробелы.'],
            [['description', 'description_short', 'logo', 'id'], 'safe'],
            [['description', 'article','model_id'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['name_lat'], 'string', 'max' => 50],
            [['logo'], 'string', 'max' => 20],
            [['_filter'], 'isArrayFilter'],
            [['description_short'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    public function getUrl($result = null){
        return parent::getUrl();
    }

    public function afterFind() {
        //устанавливаем в TRUE или FALSE если есть описание
        return parent::afterFind();
    }
    
    public function getUrlUpdate(){
        $url = "/admin/product/update_problem";
        $url .= "?id=$this->id";
        if(isset($_GET['page'])){
            $url .= "&page=".$_GET['page'];
        }
        if(isset($_GET['sort'])){
            $url .= "&sort=".$_GET['sort'];
        }
        return $url;
    }
    
    public function search($params){
        $this->load($params);
        $query = self::find()->where(['status'=>1]);
        $query->select("p.*,
                (CASE 
                    WHEN p.producer_id IS NULL 
                    AND p.model_id IS NULL 
                    AND  p.serie_id IS NULL 
                    AND p.category_id IS NOT NULL
                THEN 1
                ELSE 0
                END) AS _head,
                
                (CASE 
                    WHEN p.producer_id IS NULL 
                        OR p.model_id IS NULL 
                        OR p.serie_id IS NULL 
                    THEN 1
                    ELSE 0
                END) AS _problem,
        ")
            ->from('product p')
            ->having(['_head'=>0, '_problem'=>1]);
//            ->leftJoin('product_filter_value pfv', "p.article=pfv.article");
        
        if(!empty($this->is_problem) && $this->is_problem != ''){
            switch ($this->is_problem){
                case 1 : $query->andWhere(['p.category_id'=> NULL]); break;
                case 2 : $query->andWhere(['p.producer_id'=>null]); break;
                case 3 : $query->andWhere(['p.model_id'=>null]); break;
                case 4 : $query->andWhere('p.article NOT IN (SELECT DISTINCT `article` FROM product_filter_value)')->groupBy('p.article');
                case 5 : $query->andWhere(['_is_problem'=>8]); break;
            }
        }else{
            $query->andWhere(['_is_problem'=>0]);
        }
        
        if(!empty($this->is_delete)){
            $query->andWhere(['p.is_delete'=>1]);
        }else{
            $query->andWhere(['p.is_delete'=>0]);
        }
        
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'p.id' => $this->id,
            'p.category_id' => $this->category_id,
            'p.producer_id' => $this->producer_id,
        ]);
        
        if($this->model_id){
            $query->leftJoin('model m', "p.model_id = m.id");
            $query->andFilterWhere(['like', 'm.name', $this->model_id."%", FALSE]);
        }
        
        if($this->article){
            $query->andFilterWhere(['like', 'p.article', $this->article."%", false]);
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
//            'sort'=> ['defaultOrder' => ['p.id'=>SORT_DESC]]
        ]);
        return $dataProvider;
    }
}