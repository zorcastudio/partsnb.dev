<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\Product;
use yii\helpers\ArrayHelper;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class AllProductSearch extends \app\models\Product
{
    public $_isDeskription;
    public $_count;
    /**
     * @inheritdoc
     */
   public function rules() {
        return [
            [['category_id', 'producer_id', 'ya_market', 'googl_market', 'is_published', 'status', 'createtime', 'is_delete', 'is_problem'], 'integer'],
            [['available', '_isDeskription'], 'boolean'],
            ['name_lat', 'match', 'pattern' => '/^[A-Za-z0-9 \-]+$/u', 'message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-" и пробелы.'],
            [['description', 'description_short', 'logo', 'id'], 'safe'],
            [['description', 'article', 'model_id', 'serie_id'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['name_lat'], 'string', 'max' => 50],
            [['logo'], 'string', 'max' => 20],
            [['description_short'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    public function getUrl($result = null){
        return parent::getUrl();
    }
    
    public function getUrlView($result = null){
        return "/admin/product/view?article=$this->article";
    }
    
    public function getUrlUpdate($result = null){
        $url = "/admin/product/update";
        $url .= "?id=$this->id";
        if(isset($_GET['page'])){
            $url .= "&page=".$_GET['page'];
        }
        if(isset($_GET['sort'])){
            $url .= "&sort=".$_GET['sort'];
        }
        return $url;
    }
 
//    public function getIsDeskription(){
//        return $this->_isDeskription ? 'Да' : 'Нет';
//    }
    
//    public static function find() {
//        return parent::find()->where('product.is_delete IS NOT NULL');
//    }

    public function afterFind() {
        //устанавливаем в TRUE или FALSE если есть описание
        $this->_isDeskription = $this->description ? TRUE : FALSE;
        $this->_count = self::find()->where([
            'article' => $this->article, 
            '_is_problem'=>0, 
            'is_delete'=>0, 
            'status'=>1,
            ])
            ->andWhere(['not', ['model_id'=>null]])
            ->count();
        return parent::afterFind();
    }
    
    public function search($params){
        $query = self::find();

        if(isset($_GET['sort'])){
            $sort = [$_GET['sort']=>SORT_DESC];
        }else{
            $sort = ['id'=>SORT_DESC];
        }
        
        $query->filterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'producer_id' => $this->producer_id,
            'price' => $this->price,
            'ya_market' => $this->ya_market,
            'googl_market' => $this->googl_market,
            'is_published' => $this->is_published,
        ]);
        
        
        
        if(!empty($this->is_problem) && $this->is_problem != ''){
            switch ($this->is_problem){
                case 1 : $query->andWhere(['category_id'=> NULL]); break;
                case 2 : $query->andWhere(['producer_id'=>null]); break;
                case 3 : $query->andWhere(['model_id'=>null]); break;
                case 4 : 
                    $article = Yii::$app->db->createCommand("
                        SELECT p.id
                        FROM product p
                        LEFT JOIN product_filter_value pfv ON pfv.article = p.article AND pfv.category_id = p.category_id AND pfv.producer_id = p.producer_id AND pfv.serie_id = p.serie_id
                        WHERE pfv.article IS NULL
                        AND p.article IS NOT NULL
                        AND p.category_id IS NOT NULL
                        AND p.producer_id IS NOT NULL
                        AND p.serie_id IS NOT NULL
                        -- GROUP BY p.article, p.category_id, p.producer_id, p.serie_id
                    ")->queryAll();
                    $result = [];
                    foreach($article as $value){
                        $result[] = $value['id'];
                    }
                    $query->andWhere(['id'=> $result]); break;
                    
                case 5 : $query->andWhere(['_'=>8]); break;
                case 6 : 
                    $list = Yii::$app->db->createCommand("
                        SELECT p.id, p.article 
                        FROM product p 
                        LEFT JOIN model m ON m.id = p.model_id AND m.is_pn = 1 
                        GROUP BY p.article 
                        HAVING SUM(m.is_pn) IS NULL
                    ")->queryAll();
                    $list = ArrayHelper::map($list, 'id', 'article');
                    $query->andWhere(['article'=>$list]);
            }
        }else{
            $query->andWhere(['_is_problem'=>0]);
        }
        
        
        if($this->name){
            $query->andFilterWhere(['like', 'name', "%".$this->name."%", FALSE]);
        }
        if($this->article){
            $query->andFilterWhere(['like', 'article', $this->article."%", false]);
        }
        
        if($this->model_id){
             $list = Yii::$app->db->createCommand("
                        SELECT GROUP_CONCAT(id SEPARATOR ',') AS id
                        FROM  model
                        WHERE name LIKE '%".$this->model_id."%' 
                        LIMIT 100
                    ")->queryAll();
            $query->andFilterWhere(["model_id"=>explode(',', $list[0]['id'])]);
        }
        
        if($this->serie_id){
             $list = Yii::$app->db->createCommand("
                        SELECT GROUP_CONCAT(id SEPARATOR ',') AS id
                        FROM  serie
                        WHERE name LIKE '%".$this->serie_id."%' 
                        LIMIT 100
                    ")->queryAll();
            $query->andFilterWhere(["serie_id"=>explode(',', $list[0]['id'])]);
        }
        
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => $sort],
            'pagination' => [
                'pageSize' => isset($params['list']) ? $params['list'] : 20,
            ]
        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            return $dataProvider;
        }
        return $dataProvider;
    }
}