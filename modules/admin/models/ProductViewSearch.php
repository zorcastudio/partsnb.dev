<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\Product;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class ProductViewSearch extends \app\models\Product
{
    public $_isDeskription;
    public $_article;
    /**
     * @inheritdoc
     */
   public function rules() {
        return [
            [['category_id', 'price', 'createtime', 'is_published', 'ya_market'], 'integer'],
            [['available', '_isDeskription'], 'boolean'],
            ['name_lat', 'match', 'pattern' => '/^[A-Za-z0-9 \-]+$/u', 'message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-" и пробелы.'],
            [['description', 'description_short', 'logo', 'id'], 'safe'],
            [['description', 'article'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['name_lat'], 'string', 'max' => 50],
            [['logo'], 'string', 'max' => 20],
            [['description_short'], 'string', 'max' => 500]
        ];
    }

    public function getUrl($result = null){
        return parent::getUrl();
    }
    
    public function search($params){
        $query = self::find()->where(['is_delete'=>0, 'status'=>1, '_is_problem'=>0]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'pagination' => [
                'pageSize' => isset($params['list']) ? $params['list'] : 20,
            ],
        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'article'=>$this->_article,
            'category_id' => $this->category_id,
            'id' => $this->id,
            'price' => $this->price,
            'is_published' => $this->is_published,
            'ya_market' => $this->ya_market,
        ]);
        
        $query->andWhere(['not', ['model_id' =>0]]);
        $query->andFilterWhere(['like', 'name', "%".$this->name."%", FALSE]);
        $query->andFilterWhere(['like', 'article', $this->article."%", false]);
        return $dataProvider;
    }
}