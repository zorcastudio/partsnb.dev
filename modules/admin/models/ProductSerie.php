<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\Product;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class ProductSerie extends \app\models\Product
{
    public $_isDeskription;
    public $_count;
    /**
     * @inheritdoc
     */
   public function rules() {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    
    public function search($category, $producer, $serie){
        $query = self::find()->where([
            'category_id'=>$category->id, 
            'producer_id'=>$producer->id, 
            'serie_id'=>$serie->id, 
            'is_delete'=>0, 
            'status'=>1, 
            '_is_problem'=>0, 
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            return $dataProvider;
        }
        return $dataProvider;
    }
}