<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\Product;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class ProductSearch extends \app\models\Product
{
    public $_isDeskription;
    public $_count;
    /**
     * @inheritdoc
     */
   public function rules() {
        return [
            [['category_id', 'price', 'createtime', 'producer_id'], 'integer'],
            [['available', '_isDeskription'], 'boolean'],
            ['name_lat', 'match', 'pattern' => '/^[A-Za-z0-9 \-]+$/u', 'message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-" и пробелы.'],
            [['description', 'description_short', 'logo', 'id'], 'safe'],
            [['description', 'article'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['name_lat'], 'string', 'max' => 50],
            [['logo'], 'string', 'max' => 20],
            [['description_short'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    public function getUrl($result = null){
        return parent::getUrl();
    }
    
    public function getUrlView($result = null){
        return "/admin/product/view?article=$this->article";
    }
    
    public function getUrlUpdate($result = null){
        $url = "/admin/product/update";
        $url .= "?id=$this->id";
        if(isset($_GET['page'])){
            $url .= "&page=".$_GET['page'];
        }
        if(isset($_GET['sort'])){
            $url .= "&sort=".$_GET['sort'];
        }
        return $url;
    }
 
//    public function getIsDeskription(){
//        return $this->_isDeskription ? 'Да' : 'Нет';
//    }

    public function afterFind() {
        //устанавливаем в TRUE или FALSE если есть описание
        $this->_isDeskription = $this->description ? TRUE : FALSE;
        $this->_count = self::find()->where([
            'article' => $this->article, 
            '_is_problem'=>0, 
            'is_delete'=>0, 
            'status'=>1,
            ])
            ->andWhere(['not', ['model_id'=>null]])
            ->count(); ;
//        
        return parent::afterFind();
    }
    
    public function search($params){
        $query = self::find()
                ->where(['is_delete'=>0, 'status'=>1, '_is_problem'=>0]);
//                ->andWhere('model_id IS NOT NULL');
//                ->select("*, COUNT( * ) AS _count");

        if(isset($_GET['sort'])){
            $sort = [$_GET['sort']=>SORT_DESC];
        }else{
            $sort = ['id'=>SORT_DESC, 'model_id'=>'SORT_DESC'];
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => $sort]
        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'price' => $this->price,
            'producer_id' => $this->producer_id,
        ]);
        $query->andFilterWhere(['like', 'name', $this->name."%", FALSE]);
        $query->andFilterWhere(['like', 'article', $this->article."%", false]);
//        $query->andWhere(['not', ['description' => '']]);
//        $query->andWhere(['not', ['producer_id' => 0]]);
        $query->groupBy(['article']);
        
//        if($this->_isDeskription){
//            $query->andWhere(['not', ['_list_pn' => '']]);
//        }else{
//            $query->andWhere(['description' => '']);
//        }

        return $dataProvider;
    }
}