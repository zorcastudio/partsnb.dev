<?php
namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;
use app\models\Filter;
/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class FilterSearch extends Filter
{
    /**
     * @inheritdoc
     */
   public function rules() {
        return [
            [['name', 'name_lat'], 'string', 'max' => 20],
            [['id', 'category_id','search_published'], 'safe'],
            [['sort'], 'integer'],
            ['name_lat', 'match', 'pattern' => '/^[A-Za-z0-9 \-]+$/u','message' => 'В поле для ссылки допускаются только латинские буквы и цифры знак "-".'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    
    
    
    public function search($params){
        $query = self::find();
        
        if(isset($_POST['editableKey'])){
            $value = array_shift($_POST['FilterSearch']);
//            self::updateAll(['sort'=>$value['sort']], "id={$_POST['editableKey']}");
            
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
             // read your posted model attributes
            
            $model = self::findOne($_POST['editableKey']);
            if ($model->load($_POST)) {
                // read or convert your posted information
                $model->sort = $value['sort'] ;
                $model->update();
                // return JSON encoded output in the below format
                return ['output'=>16, 'message'=>''];

                // alternatively you can return a validation error
                // return ['output'=>'', 'message'=>'Validation error'];
            }
            // else if nothing to do always return an empty JSON encoded output
            else {
                return ['output'=>'', 'message'=>''];
            }
        }
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['sort'=>SORT_ASC]]
        ]);
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'name' => $this->name,
            'id' => $this->id,
            'category_id' => $this->category_id,
            'search_published' => $this->search_published,
        ]);
//        $query->andFilterWhere(['like', 'name' => $this->name]);
        return $dataProvider;
    }
}