<?php

namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\modules\admin\models\LogsSearch;

use yii\web\NotFoundHttpException;
use Yii;

class LogsController extends Controller {

    private $_model;

    public function actionIndex() {
        $searchModel = new LogsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionDelete() {
        $model = $this->loadModel();
        $model->delete();
        Yii::$app->getSession()->setFlash('info', 'Модель удалена');
        $this->redirect('/admin/models');
    }

    public function loadModel() {
        if ($this->_model === null) {
            $this->_model = isset($_GET['id']) ? LogsSearch::findOne($_GET['id']) : new Model();
            if ($this->_model === null)
                throw new NotFoundHttpException(404, 'Cтраница не существует.');
        }
        return $this->_model;
    }
    
    public function actionDownloadfile() {
        $file = Yii::$app->request->get('file');
        $host = preg_replace("/^.*\//i", '', Yii::$app->request->hostInfo);

        if(preg_match('/Import/i', $file, $matches)){
            return Yii::$app->response->sendFile(Yii::$app->basePath."/{$host}/../sync/history/{$file}");
        };
        return Yii::$app->response->sendFile(Yii::$app->basePath."/{$host}/history/{$file}");
    }
}
