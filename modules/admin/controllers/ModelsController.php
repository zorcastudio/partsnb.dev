<?php

namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\models\Model;
use app\models\Producer;
use app\models\Serie;
use yii\web\NotFoundHttpException;
use Yii;

class ModelsController extends Controller {

    private $_model;

    public function actionIndex() {
        $searchModel = new \app\modules\admin\models\ModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionCreate() {
        $model = $this->loadModel();
        if (isset($_POST['Model']) && !isset($_POST['ajax'])) {
            $model->attributes = $_POST['Model'];
            if ($model->save()) {
                $model->createChildren();
                Yii::$app->getSession()->setFlash('info', 'Модель cоздана');
                $this->redirect(['/admin/models']);
            }
        }
        return $this->render('create', [
                    'model' => $model,
                    'producer' => Producer::getProducerArray(),
                    'serie' => Serie::getSerieAll($model->producer_id)
        ]);
    }

    public function actionUpdate() {
        $model = Model::find()
            ->select(['
                        model.id, model.producer_id, model.serie_id, model.name, model.is_pn, model.delete, model.name_lat, model.description,
                        GROUP_CONCAT(CONCAT(mc.id, ":", TRIM(LEADING model.name FROM mc.name)) SEPARATOR ";") AS _children
                    '])
            ->innerJoin('model_child mc', 'model.id = mc.model_id')
            ->where(['model.id'=>$_GET['id'], 'mc.is_dalete'=>0])
            ->andWhere('model.name != mc.name')
            ->one();
        $model->setChildren();
        $model->getChild();
        
        if (isset($_POST['Model'])) {
            $model->attributes = $_POST['Model'];
            if ($model->save()) {
                //создаем/удаляем совместимые модели
                $model->createChildren();
                
                Yii::$app->getSession()->setFlash('info', 'Модель обновлена');
                $this->redirect(['/admin/models']);
            }
        }
        return $this->render('update', [
                    'model' => $model,
                    'producer' => Producer::getProducerArray(),
                    'serie' => Serie::getSerieAll($model->producer_id)
        ]);
    }

    public function actionView() {
        $model = $this->loadModel();
        $this->render('view', ['model' => $model]);
    }

    public function actionDelete() {
        $model = $this->loadModel();
        $model->delete();
        Yii::$app->getSession()->setFlash('info', 'Модель удалена');
        $this->redirect('/admin/models');
    }

    public function loadModel() {
        if ($this->_model === null) {
            $this->_model = isset($_GET['id']) ? Model::findOne($_GET['id']) : new Model();
            if ($this->_model === null)
                throw new NotFoundHttpException(404, 'Cтраница не существует.');
        }
        return $this->_model;
    }

    public function actionGeneratealiases() {
        $searchModel = new \app\modules\admin\models\ModelSearch();
        $searchModel->generateAliases();
    }

}
