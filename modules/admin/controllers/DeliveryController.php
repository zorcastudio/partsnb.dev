<?php
namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\modules\admin\models\DeliverySearch;
use app\models\Delivery;
use Yii;


class DeliveryController extends Controller
{
    private $_model;

    public function actionIndex() {
        $searchModel = new DeliverySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }
    
    public function actionCreate() {
        $model = $this->loadModel();
        
        if(isset($_POST['Delivery']) && !isset($_POST['ajax'])){
            $model->attributes  = $_POST['Delivery'];
            if($model->save()){
                Yii::$app->getSession()->setFlash('info','Созданый новый тип доставки');
                $this->redirect(['/admin/delivery']);
            }
        }
        return $this->render('create', ['model'=>$model]);
    }
    
    public function actionUpdate() {
        $model = $this->loadModel();
        
        if(isset($_POST['Delivery'])){
            $model->attributes  = $_POST['Delivery'];
            if($model->save()){
                Yii::$app->getSession()->setFlash('info','Тип доставки обновлен');
                $this->redirect(['/admin/delivery']);
            }
        }
        return $this->render('update', ['model'=>$model]);
    }
    
    public function actionDelete() {
        $model = $this->loadModel();
        $model->is_delete = 1;
        $model->update();
        $this->redirect('/admin/delivery');
    }
    
    public function loadModel() {
        if ($this->_model === null) {
            $this->_model = (isset($_GET['id'])) ? Delivery::findOne($_GET['id']) : new Delivery();
            if ($this->_model === null)
                throw new NotFoundHttpException('Товар не определен', 404); 
        }
        return $this->_model;
    }

}