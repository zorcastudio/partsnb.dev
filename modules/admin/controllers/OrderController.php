<?php
namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\modules\admin\models\OrderSearch;
use app\models\Order;
use Yii;

class OrderController extends Controller
{
    private $_model;

    
    public function actionIndex() {
        if(Yii::$app->user->isGuest)
            $this->redirect(['/login']);

        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }
    
    public function actionUpdate() {
        $model = \app\models\OrderSearch::findOne($_GET['id']);
        
        if(isset($_POST['OrderSearch'])){
            $model->attributes  = $_POST['OrderSearch'];
            if($model->city_id == 0)
                $model->city_id = Null;
            
            if($model->save()){
                Yii::$app->getSession()->setFlash('info','Заказ обновлен');
                $this->redirect(['/admin/order']);
            }
        }
        
        return $this->render('update', ['model'=>$model]);
    }
    
    public function actionView() {
        if(!isset($_GET['id']))
            throw new NotFoundHttpException('Товар не определен', 404); 

        $model = Order::find()
                ->where(['order.id'=>$_GET['id']])
                ->select('order.*, (order.price + delivery.price) AS sumPrice')
                ->join('INNER JOIN', 'delivery', 'delivery_id = delivery.id')
                ->one();
        return $this->render('view', ['model'=>$model]);
    }
    
    public function actionDelete() {
        $model = $this->loadModel();
        $model->scenario = 'update';
        $model->is_delete = 1;
        
        if($model->save()){
            Yii::$app->getSession()->setFlash('info', 'Заказ удален');
        }else{
            Yii::$app->getSession()->setFlash('info', 'Ошибка заказ не может быть удален');
        }
        $this->redirect('/admin/order');
    }
    
    public function loadModel() {
        if ($this->_model === null) {
            $this->_model = (isset($_GET['id'])) ? Order::findOne($_GET['id']) : new Order();
            if ($this->_model === null)
                throw new NotFoundHttpException('Товар не определен', 404); 
        }
        return $this->_model;
    }

}