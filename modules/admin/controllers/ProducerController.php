<?php

namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\modules\admin\models\ProducerSearch;
use app\models\Producer;
use yii\web\NotFoundHttpException;

use Yii;

class ProducerController extends Controller {

    private $_model;

    public function actionIndex() {
        $searchModel = new ProducerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionCreate() {
        $model = $this->loadModel();
        if (isset($_POST['Producer']) && !isset($_POST['ajax'])) {
            $model->attributes = $_POST['Producer'];
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('info', 'Производитель создан');
                $this->redirect(array('/admin/producer'));
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate() {
        $model = $this->loadModel();
        if (isset($_POST['Producer'])) {
            $model->attributes = $_POST['Producer'];
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('info', 'Производитель обновлен');
                $this->redirect(array('/admin/producer'));
            }
        }
        return $this->render('update', ['model' => $model]);
    }
    
    public function actionView(){
        $model = $this->loadModel();
        $modeList = \app\models\Model::findAll(['producer_id'=>$model->id]);
        
        if(isset($_POST['Model'])){
            foreach ($_POST['Model'] as $key=>$value){
                if(is_array($value)){
                    foreach ($value as $key=>$item)
                    $view = \app\models\Model::findOne($key);
                    $view->name = $item;
                    $view->update();
                }else{
                    $item = new \app\models\Model();
                    $item->producer_id = $model->id;
                    $item->name = $value;
                    if($item->isExists()){
                        Yii::$app->getSession()->setFlash('error','В даном производителе такая модель уже существует');
                        $this->redirect('/admin/producer/view/id/'.$model->id);
                    }
                    $item->save();
                }
            }
            Yii::$app->getSession()->setFlash('info','Значения модели обновлены');
            $this->redirect('/admin/producer');
        }
        
        return $this->render('view', [
                    'model'=>$model,
                    'modeList'=>$modeList
                ]);
    }

    public function actionDelete() {
        $model = $this->loadModel();
        
        $models = \app\models\Model::findAll(['producer_id'=>$model->id]);
        $modelArray = [];
        //удаляем модели у которых есть даный производитель
        if($models){
            foreach ($models as $value) {
                $modelArray[] = $value->id;
                $value->delete();
            }
        }
        //удаляем все связи
        $modelCompatible = \app\models\ModelCompatible::findAll(['model_id'=>$modelArray]);
        if($modelCompatible){
            foreach ($modelCompatible as $value) {
                $value->delete();
            }
        }
        // удаляем товары
        $product = \app\models\Product::findAll(['producer_id'=>$model->id]);
        if($product){
            foreach ($product as $value) {
                $value->delete();
            }
        }
        \app\models\Serie::deleteAll(['producer_id'=>$model->id]);
        
        $model->delete();
        Yii::$app->getSession()->setFlash('info', 'Производитель удален');
        $this->redirect('/admin/producer');
    }

    public function loadModel() {
        if ($this->_model === null) {
            $this->_model = (isset($_GET['id'])) ? Producer::findOne($_GET['id']) : new Producer();
            if ($this->_model === null)
                throw new NotFoundHttpException(404, 'Cтраница не существует.');
        }
        return $this->_model;
    }

}
