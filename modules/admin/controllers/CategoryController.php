<?php

namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\modules\admin\models\CategorySearch;
use app\models\Category;
use app\models\CategoryFilter;
use app\models\Filter;
use Yii;

class CategoryController extends Controller {

    private $_model;

    public function actionIndex() {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionCreate() {
        $model = $this->loadModel();
        $model->filter_set = $model->filterList();

        if (isset($_POST['Category']) && !isset($_POST['ajax'])) {
            $model->attributes = $_POST['Category'];
            
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('info', 'Категория создана');
                $this->redirect(array('/admin/category'));
            }
        }
        return $this->render('create', 
            [
                'model' => $model, 
                'filters' => Filter::getListFilter()
        ]);
    }

    public function actionUpdate() {
        $model = $this->loadModel();
        $model->filter_set = $model->filterList();

        if (isset($_POST['Category'])) {
            $model->attributes = $_POST['Category'];
            if ($model->save()) {
//                CategoryFilter::deleteAll("category_id = $model->id");
//                foreach ($model->filter_set as $value) {
//                    $category_filtr = new CategoryFilter();
//                    $category_filtr->filter_id = $value;
//                    $category_filtr->category_id = $model->id;
//                    $category_filtr->save();
//                }
                Yii::$app->getSession()->setFlash('info', 'Категория обновлена');
                $this->redirect(array('/admin/category'));
            }
        }
        return $this->render('update', [
            'model' => $model, 
            'filters' => $model->listFilter
        ]);
    }

    public function actionView() {
        $model = $this->loadModel();
        $this->render('view', ['model' => $model]);
    }

    public function actionDelete() {
        $model = $this->loadModel();
        $model->is_delete = 1;
        
        if($model->update()){
            Yii::$app->getSession()->setFlash('info', 'Категория удалена');
            $this->redirect('/admin/category');
        }else{
            return $this->render('update', ['model' => $model]);
        }
    }

    public function loadModel() {
        if ($this->_model === null) {
            $this->_model = (isset($_GET['id'])) ? Category::findOne($_GET['id']) : new Category();
            if ($this->_model === null)
                throw new NotFoundHttpException('Товар не определен', 404); 
        }
        return $this->_model;
    }

}
