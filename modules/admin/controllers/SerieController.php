<?php

namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\models\Serie;
use yii\web\NotFoundHttpException;
use Yii;
use app\models\Product;
use app\models\Model;
use app\models\ProductFilterCount;
use app\models\ProductFilterValue;

class SerieController extends Controller {

    private $_model;

    public function actionIndex() {
        $searchSerie = new \app\modules\admin\models\SerieSearch();
        $dataProvider = $searchSerie->search(Yii::$app->request->queryParams);
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchSerie' => $searchSerie,
        ]);
    }

    public function actionCreate() {
        $model =  new \app\models\Serie();
        if (isset($_POST['Serie']) && !isset($_POST['ajax'])) {
            $model->attributes = $_POST['Serie'];
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('info', 'Серия cоздана');
                $this->redirect(array('/admin/serie'));
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionUpdate() {
        $model = $this->loadModel();

        if (isset($_POST['Serie'])) {
            $model->attributes = $_POST['Serie'];
            if ($model->update()) {
                Yii::$app->getSession()->setFlash('info', 'Серия обновлена');
                $this->redirect(array('/admin/serie'));
            }
        }
        return $this->render('update', [
            'model' => $model, 
        ]);
    }

    public function actionView() {
        $model = $this->loadModel();
        $this->render('view', ['model' => $model]);
    }

    public function actionDelete() {
        $model = $this->loadModel();
        //удаляем модель
        $model->delete();
        Yii::$app->getSession()->setFlash('info', 'Серия удалена');
        $this->redirect('/admin/serie');
    }

    public function loadModel() {
        if ($this->_model === null) {
            $this->_model = isset($_GET['id']) ? Serie::findOne($_GET['id']) : new Serie();
            if ($this->_model === null)
                throw new NotFoundHttpException(404, 'Cтраница не существует.');
        }
        return $this->_model;
    }

}
