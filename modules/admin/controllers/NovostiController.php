<?php

namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\modules\admin\models\NovostiSearch;
use app\models\Novosti;
use Yii;

class NovostiController extends Controller {

    private $_model;

    public function actionIndex() {
        $searchModel = new NovostiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider'=>$dataProvider,
            'searchModel'=>$searchModel,
            ]);
    }
    
    public function actionCreate() {
        $model = $this->loadModel();
        if(isset($_POST['Novosti']) && !isset($_POST['ajax'])){
            $model->attributes  = $_POST['Novosti'];
            $model->createtime  = time();
            if($model->save()){
                Yii::$app->getSession()->setFlash('info','Новость добавлена');
                $this->redirect(['/admin/novosti']);
            }
        }
        return $this->render('create', ['model'=>$model]);
    }

    
    public function actionUpdate() {
        $model = $this->loadModel();
        
        if (isset($_POST['Novosti'])) {
            $model->attributes = $_POST['Novosti'];
            $model->update  = time();
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('info','Новость обновлена');
                $this->redirect('/admin/novosti');
            }
        }
        return $this->render('update', ['model' => $model]);
    }


    public function actionDelete() {
        $model = $this->loadModel();
        $model->delete();
        $this->redirect('/admin/novosti');
    }

     
    public function loadModel() {
        if ($this->_model === null) {
            if (isset($_GET['id'])){
                    $this->_model = Novosti::findOne($_GET['id']);
                if ($this->_model === null)
                    throw new \yii\web\NotFoundHttpException('Новость не найдена', 404);
            }else
                $this->_model = new Novosti();
        }
        return $this->_model;
    }

}
