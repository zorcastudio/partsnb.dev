<?php
namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\modules\admin\models\PaymentSearch;
use app\models\Payment;
use Yii;

class PaymentController extends Controller
{
    private $_model;

    public function actionIndex() {
        $searchModel = new PaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }
    
    public function actionCreate() {
        $model = $this->loadModel();
        if(isset($_POST['Payment']) && !isset($_POST['ajax'])){
            $model->attributes  = $_POST['Payment'];
            if($model->save()){
                Yii::$app->getSession()->setFlash('info','Созданый новый тип оплаты');
                $this->redirect(['/admin/payment']);
            }
        }
        return $this->render('create', ['model'=>$model]);
    }
    
    public function actionUpdate() {
        $model = $this->loadModel();
        
        if(isset($_POST['Payment'])){
            $model->attributes  = $_POST['Payment'];
            if($model->save()){
                Yii::$app->getSession()->setFlash('info','Тип оплаты обновлен');
                $this->redirect(['/admin/payment']);
            }
        }
        
        return $this->render('update', ['model'=>$model]);
    }
    
    public function actionView() {
        $model = $this->loadModel();
        $this->render('view', ['model'=>$model]);
    }
    
    public function actionDelete() {
        $model = $this->loadModel();
        $model->is_delete = 1;
        $model->update();
        $this->redirect(['/admin/payment']);
    }
    
    
    /*
     * Загрузка изображений Логотипа
     */
    public function actionUploadlogo() {
        if(isset($_FILES['Payment'])){
            echo Payment::saveLogo($_FILES['Payment']);
        }else
            echo"{}";
    }
    
    public function loadModel() {
        if ($this->_model === null) {
            $this->_model = (isset($_GET['id'])) ? Payment::findOne($_GET['id']) : new Payment();
            if ($this->_model === null)
                throw new NotFoundHttpException('Товар не определен', 404); 
        }
        return $this->_model;
    }

}