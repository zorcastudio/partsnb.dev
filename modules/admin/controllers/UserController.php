<?php
namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\modules\admin\models\UserSearch;
use app\models\User;
use app\models\RegistrationForm;
use Yii;

class UserController extends Controller {

    private $_model;

    public function actionIndex() {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
      
        return $this->render('index', [
            'dataProvider'=>$dataProvider,
            'searchModel'=>$searchModel,
            ]);
    }
    

    public function actionCreate() {
        $model = new User();
        $model->country_id = RegistrationForm::$_country_id;
        $form = (isset($_POST['User'])) ? $_POST['User'] : null ;
        if ($form) {
            $password = $model->password;
            $model->attributes = $form;
            $model->region_id = (isset($form['region_id']) && $form['region_id']) ? $form['region_id'] : null;
            $model->city_id = (isset($form['city_id']) && $form['city_id']) ? $form['city_id'] : null;
            if ($model->validate()) {
                if($model->password != $password)
                    $model->password =  md5($model->password);
                
                $model->createtime = time();
                if ($model->save()){
                    Yii::$app->getSession()->setFlash('info','Созданый новый пользователь');
                    $this->redirect(['/admin/user']);
                }
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionView() {
        $model = $this->loadModel();
        return $this->render('view', ['model' => $model]);
    }
    
    public function actionUpdate() {
        $model = $this->loadModel();

        $form = (isset($_POST['User'])) ? $_POST['User'] : null ;
        if ($form) {
            $password = $model->password;
            $model->attributes = $form;
            $model->region_id = (isset($form['region_id']) && $form['region_id']) ? $form['region_id'] : null;
            $model->city_id = (isset($form['city_id']) && $form['city_id']) ? $form['city_id'] : null;
            if ($model->validate()) {
                if($model->password != $password)
                    $model->password =  md5($model->password);
                
                $model->createtime = time();
                if ($model->save()){
                    Yii::$app->getSession()->setFlash('info','Пользователь обновлен');
                    $this->redirect(['/admin/user']);
                }
            }
        }
        return $this->render('update', ['model' => $model]);
    }

    public function actionDelete() {
        $model= \app\models\UserCreate::findOne($_GET['id']);
        $model->status =  User::STATUS_BANED;
        $model->update();
        $this->redirect(['/admin/user']);
    }
    
    public function loadModel() {
        if ($this->_model === null) {
            if (isset($_GET['id'])){
                $this->_model = User::findOne($_GET['id']);
                if ($this->_model === null)
                    throw new NotFoundHttpException('Товар не определен', 404); 
            }
            else
                $this->_model = new User;
            
        }
        return $this->_model;
    }

}
