<?php

namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\modules\admin\models\ProductSearch;
use app\modules\admin\models\AllProductSearch;
use app\models\ProductFilterValue;
use app\models\Product;
use app\models\ProductCreate;
use app\models\Category;
use app\models\Image;
use app\models\Model;
use app\models\ModelCompatible;
use app\models\SearchModel;
use app\models\ProductPnSearch;
use app\models\Producer;
use yii\web\UploadedFile;
use app\models\GenereteProduct;
use Yii;
use yii\web\NotFoundHttpException;
use app\components\WaterMark\WaterMark;

class ProductController extends Controller {

    private $_model;

    public function actionIndex() {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }
    
     public function actionAll() {
        $searchModel = new AllProductSearch();
        if(isset($_GET['AllProductSearch'])){
            $searchModel->attributes = $_GET['AllProductSearch'];
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('all', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionView() {
        if(!isset($_GET['article']))
            throw new NotFoundHttpException('Товар не определен', 404);
            
        $model = Product::findOne(['article'=>$_GET['article']]);
        if(!isset($model))
            throw new NotFoundHttpException('Товар не определен', 404);
       
        $searchModel = new \app\modules\admin\models\ProductViewSearch();
        $searchModel->_article = $model->article;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('view', [
                'model' => $model, 
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'forSearchModel' => Product::getFoSearchModel($model->article),
                'forSearchPN' => Product::getFoSearchPN($model->article),
            ]);
    }
    
    
    
    public function actionUpdate_cut() {
        $model = GenereteProduct::find()
                ->where(['id'=>$_GET['id']])
                ->select('id, article, category_id, producer_id, model_id, name, name_lat, logo, price, is_published, ya_market')
                ->one();
        if(!$model)
            throw new NotFoundHttpException('Товар не определен', 404);
       
        $producer = Producer::getProducerArray();
        
        if(isset($_POST['GenereteProduct']['model_id'])){
            $model_p = Model::findOne($_POST['GenereteProduct']['model_id']);
            $model->producer_id = $model_p ? $model_p->producer_id : null;
            $model->serie_id = $model_p ? $model_p->serie_id : null;
            $model->model_id = $model_p->id;
        }
        
        if($model->article){
            $model->category_id = Category::getCategoryOfArticle($model->article);
        }
        
        $modelList = Model::getModelArray($model->producer_id);
 
        if (isset($_POST['GenereteProduct'])) {
            
            $model->attributes = $_POST['GenereteProduct'];
            $model->_nameOne = $_POST['GenereteProduct']['name'];
            $model->name = $_POST['GenereteProduct']['name'];
            
            if($model->name_lat == NULL){
                $model->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($model->name);
            }
            
            if(!$model->model_id && isset($_POST['GenereteProduct']['model_id'])){
                $MODEL = Model::findOne($_POST['GenereteProduct']['model_id']);
                $model->serie_id = $MODEL ? $MODEL->serie_id : NULL;
            }
            
            $modelCompatible = ModelCompatible::findOne(['article'=>$model->article, 'model_id'=>$model->model_id]);
            if(!$modelCompatible){
                $modelCompatible = new ModelCompatible();
                $modelCompatible->article = $model->article;
                $modelCompatible->model_id = $model->model_id;
                $modelCompatible->save();
            }
            
            $model->logo_temp = UploadedFile::getInstance($model, 'logo_temp');
            if ($model->logo_temp && $model->validate()) {
                $serch = preg_match("/\.\w{3}$/u", $model->logo_temp->name, $extension);
                $name = time() . $extension[0];
                $temp = $model->logo_temp->saveAs(Yii::$app->getBasePath() . "/web/uploadfile/images/product/resized/" . $name);
                $model->logo = $name;
            }

            if (isset($_FILES['images']))
                $model->saveImages($_FILES['images']);

            $model->status = 1;
//            $model->createtime = time();
//            if ($model->validate('update')) {
//                ProductFilterValue::deleteAll(['article' => $model->article]);
//                if (isset($_POST['Product']['filter'])) {
//                    $model->setProductFilterValue($_POST['Product']['filter']);
//                }
//            }

            $model->setStok();
            if ($model->update('update')) {
                //если были изменены модель и производитель то 
                //удаляем предыдущую связ и создаем новую
                if(
                    $model->producer_id != $model->oldAttributes['producer_id'] || 
                    $model->model_id != $model->oldAttributes['model_id']
                ){
                    if($model->model_id != $model->oldAttributes['model_id']){
                        $modelCompatible = ModelCompatible::findOne(['model_id'=>$model->oldAttributes['model_id'], 'article'=>$model->article]);
                        $modelCompatible->delete();
                    }
                    
                    //создаем связи
                    $modelCompatible = new ModelCompatible();
                    $modelCompatible->model_id = $model->model_id;
                    $modelCompatible->article = $model->article;
                    $modelCompatible->save();
                }
                Yii::$app->getSession()->setFlash('info', 'Товар сохранен');
                
                if(isset($_GET['get'])){
                    $url[] = '/admin/product/all';
                    $url += ['AllProductSearch'=> array_filter(json_decode($_GET['get'], true))];
                    return $this->redirect($url);
                }
                $this->redirect(['/admin/product/view', 'article'=>$model->article]);
                
            }
        }
        
        return $this->render('update_cut', [
                    'model' => $model,
                    'producer' => $producer,
                    'modelList' => $modelList,
        ]);
    }

    public function actionCreate() {
        $model = new ProductCreate();
        if ($model->save()){
            $this->redirect(array('/admin/product/update', 'id' => $model->id));
        }
    }

    public function actionUpdate() {
        $model = GenereteProduct::find()
                ->where(['id'=>$_GET['id']])
                ->one();
        if(!$model)
            $this->redirect('/admin/product');

        $model->setProducerModel();
        //$model->setStok();
        
        if ($model === null)
            throw new NotFoundHttpException('Товар не определен', 404);
        
        // Виділяємо корекційний текст в кінці
        if($model->model){
            if(preg_match("/".preg_quote($model->model->name)." (.*)$/iu", $model->name, $subject)){
                $model->_nameEnd = trim($subject[1]);
                $model->_nameOne = trim(preg_replace("/".preg_quote($model->_nameEnd)."$/iu", '', $model->name));
                $model->name = $model->_nameOne;
            }

        }
        if (isset($_POST['GenereteProduct'])) {
            ini_set("memory_limit","2000M");
            ignore_user_abort(true);
            set_time_limit(0);
            
            $model->attributes = $_POST['GenereteProduct'];
            if(!empty($_POST['GenereteProduct']['_nameEnd'])) {
                $model->name .= ' '.$_POST['GenereteProduct']['_nameEnd'];
            }
            
//            $model->_compatible = Model::getModelSeries($_POST);
            if(preg_match("/^[а-яё ()]*/iu", $model->_nameOne, $subject)){
                $model->_nameOne = trim($subject[0]);
            }
            
            if (UploadedFile::getInstance($model, 'logo_temp') && $model->validate()) {
                $model->saveLogo(true);
            }

//            if (isset($_FILES['images']))
//                $model->saveImages($_FILES['images']);
            
            if (isset($_POST['Product']['filter']))
                $model->_filter = $_POST['Product']['filter'];
           
            $model->status = 1;
            $model->warranty = 6;
            
            //проверяем существуют ли товары со списка моделей которые получили
            //ели нет то создаем, или удаляем товар соответсвенной модели
            //прикрепляем фильтры к товарам
            if ($model->save('update')) {
//                Product::createForProductModels($model);
                //создаем/удаляем модели номера
                //SearchModel::updateModel($model, $_POST);
                //создаем/удаляем парт-номера для поиска
                ProductPnSearch::updatePN($model, $_POST);
                
                //генеруємо товары з моделей
                Product::CUDmodelProduct($_POST, $model);
                
                //генеруємо товары з парт номерів
                GenereteProduct::CUDpnProduct($_POST, $model);
                
                //обновляє сортування артикула
                GenereteProduct::updateSort($model->article, 'product');
                
                //обновляємо зображення для товарів
                Image::updateImageToArticle((string)$model->article);
                
                // добавляємо або видаляємо фільтри
                ProductFilterValue::setProductFilterValue($model);
                
                \app\models\Image::imgResizeLogo($model->logo);
                
                // обновляє всі логотипи данного артикулу
                $model->updateLogoGroup();
                
//                if (!empty($model->_filter)) {
//                    $model->setProductFilterValue($model->_filter);
//                }
                
                if(
                    $model->attributes['producer_id'] != $model->oldAttributes['producer_id'] || 
                    $model->attributes['model_id'] != $model->oldAttributes['model_id']
                ){
                    $modelCompatible = ModelCompatible::findOne(['model_id'=>$model->model_id, 'article'=>$model->article]);
                    $modelCompatible ? $modelCompatible->delete() : null;
                    
                    //создаем связи
                    $modelCompatible = new ModelCompatible();
                    $modelCompatible->model_id = $model->model_id;
                    $modelCompatible->article = $model->article;
                    $modelCompatible->save();
                }
                Yii::$app->getSession()->setFlash('info', 'Товар сохранен');
                $get = ['/admin/product'];
                if(isset($_GET['page'])){
                    $get += ['page'=>$_GET['page']];
                }
                if(isset($_GET['sort'])){
                    $get += ['sort'=>$_GET['sort']];
                }
                
                if(isset($_GET['page']) || isset($_GET['sort'])){
                    $this->redirect($get);
                }else
                    $this->redirect('/admin/product');
            }
        }
        
        $category = Category::getCategoriesLast(); //getCategoryListWithoutChield()05.08.2015
        $producer = Producer::getProducerArray();
        $modelList = $model->getModelInProduct();
        $modelCheket = Model::getModelCheked($model->article);
        
        $active = ProductFilterValue::isActive($model);
//        $modelSearch = SearchModel::getModelArray($model->article);
        $pnSearch = ProductPnSearch::getPNArray($model->article);
        return $this->render('update', [
                    'model' => $model,
                    'child' => $model,
                    'series' => $model->series,
                    'category' => $category,
                    'producer' => $producer,
                    'modelList' => $modelList,
                    'pnSearch' => $pnSearch,
                    'modelCheket' => $modelCheket,
                    'for_filters' => $model->category ? $model->category->filter : false,
                    'active' => $active,
//                    'otherProducer' => $model->otherProducer,
//                    'modelSearch' => $modelSearch,
//                    'producerSearch' => \app\models\ProducerSearch::getProducerArticle($model->article),
//                    'defaultFilters' => $model->category ? $model->category->cat_filter : false,
        ]);
    }
    
    /**
     * Видалення всіх товарів данного артикула
     */
    public function actionDelete() {
        $product = \app\models\ProductUpdate::find()
                    ->where(['id'=>$_GET['id']])
                    ->select('id, article, model_id, is_delete')
                    ->one();
        //если это главный товар то удаляю и все связнные его товары
        if($product){
            $modelCompatible = ModelCompatible::deleteAll(['article'=>$product->article]);
            $product = \app\models\ProductUpdate::find()
                    ->where(['article'=>$product->article])
                    ->select('id, model_id, article')
                    ->all();
            foreach ($product as $value) {
                $value->delete();
            }
            Yii::$app->getSession()->setFlash('info', 'Товары удален');
        }
        $this->redirect('/admin/product');
    }
    
    /**
     * Видалення одного товару 
     */
    public function actionDelete_one() {
        $product = \app\models\ProductUpdate::find()
                    ->where(['id'=>$_GET['id']])
                    ->select('id, article, model_id, is_delete')
                    ->one();
        
          // Якщо знаходимо такий же товар то зберігаєм url
        $product_old = \app\models\ProductUpdate::find()
                    ->where(['!=', 'id', $_GET['id']])
                    ->andWhere(['article'=>$product->article, 'model_id'=>$product->model_id])
                    ->one();
        if($product_old){
            if($product_old->original_name_lat)
                $product_old->original_name_lat .= ", {$product->name_lat}";
            else
                $product_old->original_name_lat = $product->name_lat;
            
                if($product_old->save())
                    Yii::$app->getSession()->setFlash('success', "Ccылка переопределена на другой товар: '{$product_old->name}'");
                else
                    Yii::$app->getSession()->setFlash('error', "Не удалось сохранить ссылку");
        }
        
        if($product) {
            if($product->delete()){
                Yii::$app->getSession()->setFlash('info', 'Товар удален');
            }
        }
        $this->redirect('/admin/product');
    }
    
    /**
     * Видалення одного товару 
     */
    public function actionDel_one() {
        $product = \app\models\ProductUpdate::find()
                    ->where(['id'=>$_GET['id']])
                    ->select('id, article, model_id, name_lat')
                    ->one();
        
        // Якщо знаходимо такий же товар то зберігаєм url
        $product_old = \app\models\ProductUpdate::find()
                    ->where(['!=', 'id', $_GET['id']])
                    ->andWhere(['article'=>$product->article, 'model_id'=>$product->model_id])
                    ->one();
        if($product_old){
            if($product_old->original_name_lat)
                $product_old->original_name_lat .= ", {$product->name_lat}";
            else
                $product_old->original_name_lat = $product->name_lat;
            
                if($product_old->save())
                    Yii::$app->getSession()->setFlash('success', "Ccылка переопределена на другой товар: '{$product_old->name}'");
                else
                    Yii::$app->getSession()->setFlash('error', "Не удалось сохранить ссылку");
        }
        
        if($product) {
            if($product->delete()){
                Yii::$app->getSession()->setFlash('info', 'Товар удален');
            }
        }
        $this->redirect('/admin/product/all');
    }

    /*
     * Загрузка изображений Логотипа
     */
    public function actionUploadlogo() {
        
        
        if(isset($_GET['id']))
            $model = \app\models\ProductCreate::findOne(Yii::$app->request->get('id'));
       
        if(isset($_GET['id_cut']))
            $model = \app\models\ProductCreate::findOne(Yii::$app->request->get('id_cut'));
      
        //если на этот экшен пришел с ссылкаи редактировать загрузить модель по дефаулту
        if (!$model)
            $model = $this->loadModel();
        if(isset($_FILES['GenereteProduct'])){
            echo $model->saveLogo($_FILES['GenereteProduct'], 'GenereteProduct');
        } elseif (isset($_FILES['Product'])) {
            echo $model->saveLogo($_FILES['Product'], 'GenereteProduct');
        } 
    }
    
    /*
     * Загрузка изображений Editora
     */
    public function actionImage_editor() {
        $uploadedFile = UploadedFile::getInstanceByName('upload'); 
        $mime = \yii\helpers\FileHelper::getMimeType($uploadedFile->tempName);
        $file = $uploadedFile->name;
        
        $url = Yii::$app->urlManager->createAbsoluteUrl('/images/ckeditor/'.$file);
        $uploadPath = Yii::getAlias('@webroot').'/images/ckeditor/'.$file;
        //extensive suitability check before doing anything with the file…
        if ($uploadedFile==null){
           $message = "No file uploaded.";
        }else if ($uploadedFile->size == 0){
           $message = "The file is of zero length.";
        }else if ($mime!="image/jpeg" && $mime!="image/png"){
           $message = "The image must be in either JPG or PNG format. Please upload a JPG or PNG instead.";
        }else if ($uploadedFile->tempName==null){
           $message = "You may be attempting to hack our server. We're on to you; expect a knock on the door sometime soon.";
        }else {
          $message = "";
          $move = $uploadedFile->saveAs($uploadPath);
          if(!$move){
            $message = "Error moving uploaded file. Check the script is granted Read/Write/Modify permissions.";
          } 
        }
        $funcNum = $_GET['CKEditorFuncNum'] ;
        echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";  
    }

    /*
     * Загрузка изображений
     */
    public function actionUpload() {
        $article = isset($_GET['article']) ? $_GET['article'] : '';
        if(isset($_GET['id']))
            $model = GenereteProduct::findOne(Yii::$app->request->get('id'));
       
        if(isset($_GET['id_cut']))
            $model = \app\models\Product::findOne(Yii::$app->request->get('id_cut'));
        
        //если на этот экшен пришел с ссылкаи редактировать загрузить модель по дефаулту
        if (empty($model))
            $model = $this->loadModel();

        echo $model->saveImages($_FILES['images'], $article);
    }

    public function actionDeleteimage() {
        if (isset($_GET['id'])) {
            //если id изображения пришел с дочерного товара
            if(!preg_match('/(\d+)\+/u', $_GET['id'], $subject)){
                //находим артикуль товара
                $image = Image::find()
                        ->where(['image.id'=>$_GET['id']])
                        ->joinWith('product')
                        ->select('image.id, image.product_id, image.url')
                        ->one();
                if(isset($image['product'])){
                    $product = Product::find()
                            ->where(['article'=>$image['product']->article])
                            ->select('id')
                            ->asArray()
                            ->all();
                    $result = [];
                    foreach ($product as $value) {
                        $result[] = $value['id'];
                    }
                  
                    $images = Image::deleteAll("product_id IN (".implode(",", $result).") AND url='$image->url'"); 
                    echo $images ? TRUE : false;
                }
            }else{
                $image = Image::find()
                           ->where(['id'=>$subject[1]])
                           ->select('id, product_id')
                           ->one();
                $image->is_delete = 1;
                echo $image->update() ? TRUE : FALSE;
            }
            
        }
        echo FALSE;
    }

    public function actionAjax_filter() {
        if (isset($_POST['category_id'])) {
            $model = Category::findOne($_POST['category_id']);
            echo $this->renderAjax('_filters', array('model' => $model->filter, $active=[]), false);
        }
    }
    
    /**
     * Возвращает список моделей с производителя 
     * виджетом
     */
    public function actionList_model_widget(){
        if(isset($_POST['id'])){
            $modelList = Model::find()
                    ->where(['producer_id'=>$_POST['producer_id']])
                    ->orderBy('name ASC')
                    ->select('id, name')
                    ->asArray()
                    ->all();
            $result = [];
            foreach ($modelList as $value) {
                $result[$value['id']]= $value['name'];
            }
            echo $this->renderAjax('_ajax_model', [
                'modelList'=>$result,
                'modelCheket'=>[],
            ]);
        }else echo -1;
    }
    
    public function actionAjaxfilter() {
        if(isset($_POST['category_id'])){
            $model = Category::findOne($_POST['category_id']);
            echo  $this->renderPartial('_filters',array('model'=>$model->filter)); 
        }
    }
    
    
    public function actionProblem() {
        $searchModel = new \app\modules\admin\models\ProblemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index_problem', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }
    
    public function actionUpdate_problem() {
        if(!isset($_GET['id']))
            throw new NotFoundHttpException('Товар не определен', 404);
            
        $model = \app\models\ProductUpdate::findOne($_GET['id']);
        if ($model === null)
            throw new NotFoundHttpException('Товар не определен', 404);
      
        if (isset($_POST['ProductUpdate'])) {
            $model->attributes = $_POST['ProductUpdate'];
            $model->serie_id = $model->model_id ? $model->model->serie_id : null;
            $model->status = 1;
            $model->is_published = 1;
            $model->createtime = time();
            
            if(isset($_POST['Product']['filter'])){
                $model->_filter = $_POST['Product']['filter'];
            }
            if ($model->save('update')) {
                if (!empty($model->_filter)) {
                    ProductFilterValue::setProductFilterValue($model);
                }
                
                $modelCompatible = ModelCompatible::deleteAll(['model_id'=>$model->model_id, 'article'=>$model->article]);
                //создаем связи
                $modelCompatible = new ModelCompatible();
                $modelCompatible->model_id = $model->model_id;
                $modelCompatible->article = $model->article;
                $modelCompatible->save();
                
                Yii::$app->getSession()->setFlash('info', 'Товар сохранен');
                $get = ['/admin/product/problem'];
                if(isset($_GET['page'])){
                    $get += ['page'=>$_GET['page']];
                }
                if(isset($_GET['sort'])){
                    $get += ['sort'=>$_GET['sort']];
                }
                
                if(isset($_GET['page']) || isset($_GET['sort'])){
                    $this->redirect($get);
                }else
                    $this->redirect('/admin/product/problem');
            }
        }
        
        $category = Category::getCategoriesLast();
        $producer = Producer::getProducerArray();
        $modelList = Model::getModelArray($model->producer_id);
        $modelCheket = Model::getModelCheked($model->article);
        $active = ProductFilterValue::isActive($model);
        return $this->render('update_problem', [
                    'model' => $model,
                    'category' => $category,
                    'producer' => $producer,
                    'modelList' => $modelList,
                    'for_filters' => $model->category ? $model->category->filter : false,
                    'active' => $active,
        ]);
    }
    
    public function actionModel_list($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new \yii\db\Query;
            $query->select('id, name AS text')
                    ->from('model')
                    ->where('name LIKE "%' . $q . '%"')
                    ->limit(20);

            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => City::find($id)->name];
        }
        return $out;
    }
    
    public function actionCheck(){
        
        
        if(isset($_POST['action'])){
            $action = FALSE;
            if($_POST['action'] && $_POST['button']!=3){
                switch ($_POST['action']){
                    case 1: $action = 'ya_market';break;
                    case 2: $action = 'googl_market';break;
                    case 3: $action = 'is_published';break;
                    default : $action = FALSE;
                }
            }
            switch ($_POST['button']){
                case 1: $val = 1;break;
                case 2: $val = 0;break;
                default : $val = 0;
            }
            
        
            if($action){
                foreach ($_POST['selection'] as $value) {
                    $model = Product::findOne($value);
                    $model->$action = $val;
                    $model->_is_problem = 0;
                    $model->status = 1;
                    $model->is_delete = 0;
                    $model->save();
                }
            }
                        
            //удалить выбранные элементы
            if($_POST['button'] == 3){
                $model = \app\models\ProductCreate::findAll(['id'=>$_POST['selection']]);
                foreach ($model as $value) {
                    $value->delete();
                }
            }
            return $this->redirect('/admin/product/all');
        }
        return $this->redirect('/');
    }





    public function loadModel() {
        if ($this->_model === null) {
            if (isset($_GET['id']) || isset($_GET['article'])) {
                if(isset($_GET['id'])){
                    $this->_model = Product::findOne($_GET['id']);
                }
                if(isset($_GET['article'])){
                    $this->_model = Product::findOne(['article'=>$_GET['article']]);
                }
                if ($this->_model === null)
                    throw new NotFoundHttpException('Товар не определен', 404);

            } else
                $this->_model = new Product();
        }
        return $this->_model;
    }

}
