<?php

namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\modules\admin\models\PageSearch;
use app\models\Page;
use Yii;

class PageController extends Controller {

    private $_model;

    public function actionIndex() {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider'=>$dataProvider,
            'searchModel'=>$searchModel,
            ]);
    }
    
    public function actionCreate() {
        $model = $this->loadModel();
        
        if(isset($_POST['Page']) && !isset($_POST['ajax'])){
            $model->attributes  = $_POST['Page'];
            $model->parent_id  = isset($_POST['Page']['parent_id']) ? $_POST['Page']['parent_id'] : null;
            
            if($model->save()){
                Yii::$app->getSession()->setFlash('info','Создана новая страница');
                $this->redirect(['/admin/page']);
            }
        }
        return $this->render('create', ['model'=>$model]);
    }

    
    public function actionUpdate() {
        $model = $this->loadModel();
        
        if (isset($_POST['Page'])) {
            $model->attributes = $_POST['Page'];
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('info','Страница обновлена');
                $this->redirect('/admin/page');
            }
        }
        return $this->render('update', ['model' => $model]);
    }


    public function actionDelete() {
        $model = $this->loadModel();
        $model->is_delete = 1;
        $model->update();
        $this->redirect('/admin/page');
    }

     
    public function loadModel() {
        if ($this->_model === null) {
            if (isset($_GET['id'])){
                    $this->_model = Page::findOne($_GET['id']);
                if ($this->_model === null)
                    throw new \yii\web\NotFoundHttpException('Товар не определен', 404);
            }else
                $this->_model = new Page();
        }
        return $this->_model;
    }

}
