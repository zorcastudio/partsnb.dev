<?php

namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\modules\admin\models\PnSearch;

use yii\web\NotFoundHttpException;
use Yii;

class PnController extends Controller {

    private $_model;

    public function actionIndex() {
        $searchModel = new PnSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }

    public function actionCreate() {
        $model = $this->loadModel();
        $model->setNotArticle();
        if (isset($_POST['PnSearch'])) {
            $model->attributes = $_POST['PnSearch'];
            if (PnSearch::saveChildren($_POST['PnSearch'], $model)) {
                Yii::$app->getSession()->setFlash('info', 'Парт. номер добавлен');
                return $this->redirect(['/admin/pn']);
            }
        }
        return $this->render('create', [
                'model' => $model,
        ]);
    }

    public function actionUpdate() {
        Yii::$app->db->createCommand('SET group_concat_max_len = 500000')->execute();
        $model = PnSearch::find()
                ->select(['id, id AS article_id, article, GROUP_CONCAT(CONCAT(id, ":", name) SEPARATOR ";") AS children'])
                ->where(['article'=>$_GET['article']])
                ->groupBy('article')
                ->one();
        foreach (explode(";", $model->children) as $value) {
            $item = explode(":", $value);
            $result[$item[0]] = $item[1];
        }
        $model->children = $result;
        $model->setAll_article();
        
        if (isset($_POST['PnSearch'])) {
            $model->attributes = $_POST['PnSearch'];
            if (PnSearch::saveChildren($_POST['PnSearch'], $model)) {
                Yii::$app->getSession()->setFlash('info', 'Парт. номер обновлен');
                return $this->redirect(['/admin/pn']);
            }
        }
        return $this->render('update', ['model' => $model]);
    }

    public function actionDelete() {
        $model = PnSearch::find()
                ->select(['GROUP_CONCAT(id SEPARATOR ",") AS children'])
                ->where(['LIKE', 'article', $_GET['article']])
                ->asArray()
                ->one();
        PnSearch::deleteAll('id IN ('.$model['children'].')');
        Yii::$app->getSession()->setFlash('info', 'Парт. номера даного артикула удалены');
        $this->redirect('/admin/pn');
    }

    public function loadModel() {
        if ($this->_model === null) {
            $this->_model = isset($_GET['id']) ? PnSearch::findOne($_GET['id']) : new PnSearch();
            if ($this->_model === null)
                throw new NotFoundHttpException(404, 'Cтраница не существует.');
        }
        return $this->_model;
    }

}
