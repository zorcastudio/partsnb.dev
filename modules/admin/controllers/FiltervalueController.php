<?php

class FiltervalueController extends Controller
{
    public $layout = '//layouts/admin';
    private $_model;

    
    
    public function __construct($id, $module = null) {
        parent::__construct($id, $module);
       
        if (!Yii::$app->getModule('admin')->isAdmin()) {
            $this->redirect("/user/login");
        }
    }

    public function actionIndex() {
        $model = $this->loadModel();
      
        if(isset($_GET['ajax'])){
            if(isset($_GET['FilterValue']))
                $model->attributes = $_GET['FilterValue'];
        }
        $this->render('index', ['model'=>$model]);
    }
    
    public function actionCreate() {
        $model = $this->loadModel();
        
        if(isset($_POST['FilterValue']) && !isset($_POST['ajax'])){
            $model->attributes  = $_POST['FilterValue'];
            if($model->save()){
                $this->redirect(['/admin/filtervalue']);
            }
        }
        $this->render('create', ['model'=>$model]);
    }
    
    public function actionUpdate() {
        $model = $this->loadModel();
        
        if(isset($_POST['FilterValue'])){
            $model->attributes  = $_POST['FilterValue'];
            if($model->save())
                $this->redirect(['/admin/filtervalue']);
        }
        
        $this->render('update', ['model'=>$model]);
    }
    
    public function actionView() {
        $model = $this->loadModel();
        $this->render('view', ['model'=>$model]);
    }
    
    public function actionDelete() {
        $model = $this->loadModel();
        $model->is_delete = 1;
        $model->update();
    }
    
    public function loadModel() {
        if ($this->_model === null) {
            $this->_model = (isset($_GET['id'])) ? FilterValue::findOne($_GET['id']) : new FilterValue();
            if ($this->_model === null)
                throw new NotFoundHttpException('Товар не определен', 404); 
        }
        return $this->_model;
    }

}