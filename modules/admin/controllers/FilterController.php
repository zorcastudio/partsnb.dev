<?php
namespace app\modules\admin\controllers;

use app\modules\admin\components\Controller;
use app\modules\admin\models\FilterSearch;
use app\models\Filter;
use app\models\FilterValue;

use Yii;

class FilterController extends Controller
{
    private $_model;

    public function actionIndex() {
        $searchModel = new FilterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
                    'dataProvider' => $dataProvider,
                    'searchModel' => $searchModel,
        ]);
    }
    
    public function actionCreate() {
        $model = $this->loadModel();
        
        if(isset($_POST['Filter']) && !isset($_POST['ajax'])){
            $model->attributes  = $_POST['Filter'];
            if($model->save()){
                Yii::$app->getSession()->setFlash('info','Создан новый фильтр');
                $this->redirect(array('/admin/filter'));
            }
        }
        return $this->render('create', ['model'=>$model]);
    }
    
    public function actionUpdate() {
        $model = $this->loadModel();
        if(isset($_POST['Filter'])){
            $model->attributes  = $_POST['Filter'];
            if($model->save()){
                Yii::$app->getSession()->setFlash('info','Фильтр обновлен');
//                $this->redirect(Yii::$app->request->referrer);
                $this->redirect('/admin/filter');
            }
        }
        return $this->render('update', ['model'=>$model]);
    }
    
    public function actionView() {
        $model = $this->loadModel();
        
        if(isset($_POST['FilterValue'])){
            vd($_POST['FilterValue'], false);
            echo "<br/>";
            echo "<br/>";
            
            foreach ($_POST['FilterValue'] as $key=>$value){
                if(is_array($value)){
                    $view = FilterValue::findOne($key);
                    $view->name = $value['name'];
                    $view->name_lat = $value['name_lat'];
                    $view->update();
                }else{
                    $item = new FilterValue();
                    $item->filter_id = $model->id;
                    $item->name = $value;
                    $item->name_lat = \app\modules\admin\components\TranslitFilter::translitUrl($item->name);
                    
                    if($item->isExists()){
                        Yii::$app->getSession()->setFlash('error','В даном фильтре такое название уже существует');
                        $this->redirect("/admin/filter/view?id={$model->id}");
                    }
                    $item->save();
                }
            }
            
            Yii::$app->getSession()->setFlash('info','Значения фильтра обновлены');
            $this->redirect('/admin/filter');
        }
        return $this->render('view', array('model'=>$model));
    }
    
    public function actionDelete() {
        $model = Filter::findOne($_GET['id']);
        $model->delete();
        Yii::$app->getSession()->setFlash('info', 'Фильтр удален');
        $this->redirect(Yii::$app->request->referrer);
    }
    
    public function loadModel() {
        if ($this->_model === null) {
            $this->_model = (isset($_GET['id'])) ? Filter::findOne($_GET['id']) : new Filter();
            
            if ($this->_model === null)
                throw new NotFoundHttpException('Товар не определен', 404); 
        }
        return $this->_model;
    }

}