<?php

namespace app\modules\admin\components;

use Yii;
use app\models\User;

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends \app\components\Controller {

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = [];

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    
    public function __construct($id, $module = null) {
        parent::__construct($id, $module);
        $this->layout = 'admin';
        
        User::isAdmin() ? TRUE : $this->redirect('/');
        
    }
}
