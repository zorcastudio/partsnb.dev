<?php 
    $this->title = "Добавить тип оплаты"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Тип оплаты', 'url' => ['/admin/payment']],
        $this->title
    ];
    echo $this->render('_form', ['model'=>$model]); 
?>