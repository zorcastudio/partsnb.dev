<script type="text/javascript" src="/js/fileinput.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/fileinput.min.css">
<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Url;
use app\models\Payment;

$form = ActiveForm::begin([
        'id' => 'product',
        "options"=>['class' => 'well'],
        'fieldConfig' => [
            'template' => "<div class=\"form-label\">{label}<span class='required'>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
        ]
    ]);

echo $form->errorSummary($model);

echo $form->field($model, 'name')->textInput();
echo $form->field($model, 'type')->dropDownList(
                    Payment::itemAlias('UR/FIZ'),
                    ['prompt' => 'Выберите тип']
                );

?>
<table>
    <tr>
        <td><?= $form->field($model, 'logo_temp')->fileInput([
                        'id' => "input-logo",
                        'options' => [
                            'type' => "file", 
                            'class' => "file-loading", 
                        ]
                    ]); ?></td>
        <td style="padding: 0 10px;"><?php if ($model->logo)
                echo "<div class='logo-payment' style='background: url(/images/view/$model->logo) 50% 50% / contain no-repeat;'></div>"; 
            ?>
        </td>
    </tr>
</table>
<?php


echo $form->field($model, 'status')->dropDownList(app\models\Delivery::itemAlias('status'));

echo $form->field($model, 'description')->widget(CKEditor::className(), [
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]);
echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); 
ActiveForm::end(); unset($form);
?>
<script>
    $("#input-logo").fileinput({
        uploadUrl: "<?= Url::toRoute(['/admin/payment/uploadlogo']); ?>",
        uploadAsync: false,
        maxFileCount: false,
        allowedFileExtensions: ["jpg", "gif", "png"],
        overwriteInitial: false,
        showCaption: true,
        showRemove: true,
        showUpload: true,
        initialPreviewShowDelete: false,
        previewFileType: "Изображения",
        browseLabel: "Открыть",
        removeClass: "btn btn-danger",
        removeLabel: "Удалить",
        uploadClass: "btn btn-info",
        uploadLabel: "Загрузить",
        dropZoneEnabled: false,
    });
</script>
