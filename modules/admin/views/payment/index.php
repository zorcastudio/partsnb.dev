<?php 
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = "Тип оплаты";
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    $this->title,
];

if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'category-form',
//        'template' => "{pager}{items}{pager}",
    'columns' => [
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function ($model){
                return Html::a($model->name, ['/admin/payment/update','id'=>$model->id]);
            },
        ],
        [
            'attribute' => 'status',
            'value' => function($model) {
                return $model->getStatus();
            },
            'filter' => app\models\Payment::getListStatus(),
        ],
    ],
]);
?>