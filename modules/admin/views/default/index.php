<?php 
use yii\bootstrap\Button;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="admin-default-index">
    <h1>
        Тут будет страница администратора
    </h1>
</div>

<?php 

//echo Button::widget([
//    'label' => 'перещитать товар в фильтрах',
//    'options' => [
//        'onclick'=>'recalculate(this)',
//        'class' => 'btn-lg btn-primary',
//        'style' => 'margin:5px'
//    ]
//]);

echo Button::widget([
    'label' => 'Запустить синхронизацию',
    'options' => [
        'onclick'=>'syncing(this)',
        'class' => 'btn-lg btn-primary',
        'style' => 'margin:5px'
    ]
]);
?>
<div id="message">
    <ul></ul>
</div>