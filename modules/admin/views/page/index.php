<?php 
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\Alert;


$this->title = "Страницы"; 
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    'Страницы'
];

if(Yii::$app->session->hasFlash('info')){
    echo Alert::widget([
            'options' => ['class' => 'alert-info'],
            'body' => Yii::$app->session->getFlash('info'),
        ]);
};

echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'id' => 'category-form',
//        'template' => "{pager}{items}{pager}",
        'columns' => [
            [
                'attribute' => 'id',
                'value' => 'id', 
                'options' => ['style' =>'width: 60px'],
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($model){
                    return Html::a($model->name, ['/admin/page/update','id'=>$model->id]);
                },
            ],
            'name_lat',
           [
                'class'=>'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:80px;'],
                'template' => '{view}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(null, $model->url, [
                                    'class'=>'glyphicon glyphicon-globe',                                  
                        ]);
                    },
                ],
            ]
        ],
    ]);
 ?>