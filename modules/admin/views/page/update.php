<?php 
    $this->title = "Редактировать страницу"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Страницы', 'url' => ['/admin/page']],
        'Редактировать страницу'
    ];
    echo $this->render('_form', ['model'=>$model]); 
?>