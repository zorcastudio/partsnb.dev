<?php 
    $this->title = "Создать страницу"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Страницы', 'url' => ['/admin/page']],
        'Создать страниу'
    ];
    echo $this->render('_form', ['model'=>$model]); 
?>