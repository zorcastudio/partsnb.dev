<?php 
    use yii\bootstrap\ActiveForm;
    
//    use mihaildev\ckeditor\CKEditor;
    use yii\helpers\Html;
    use app\models\Page;
    use dosamigos\ckeditor\CKEditor;
?> 
<script type="text/javascript" src="/js/fileinput.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/fileinput.min.css">

<?php
$form = ActiveForm::begin([
        'id' => 'login',
        "options"=>[
            'class' => 'well',
            'enctype' => 'multipart/form-data'
        ],
        'fieldConfig' => [
            'template' => "<div class=\"form-label\">{label}<span class='required'>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
        ]
    ]);
echo $form->errorSummary($model);
echo $form->field($model,'is_menu')->dropDownList(
                            Page::itemAlias('NoYes'), 
                            [
                                'onchange' => 'isDisabled($(this).val())',
                            ]);

echo $form->field($model,'position_menu')->dropDownList(Page::itemAlias('POSITION'));

echo $form->field($model,'parent_id')->dropDownList(
                            Page::getPages(), 
                            [
                                'disabled'=> $model->is_menu == 1 ? false : true,
                                'prompt' => 'Выберите категорию'
                            ]);
echo $form->field($model, 'name')->textInput();


echo $form->field($model, 'name_lat')->textInput();

echo $form->field($model, 'keywords')->widget(CKEditor::className(),[
    'options' => ['rows' => 6],
    'preset' => 'basic',
    
]);

echo $form->field($model, 'description')->widget(CKEditor::className(),[
        'options' => ['rows' => 6],
        'preset' => 'basic',
        'clientOptions' => [
            'filebrowserUploadUrl' => '/admin/product/image_editor'
        ]
    ]);

echo $form->field($model, 'text')->widget(CKEditor::className(),[
    'options' => ['rows' => 6],
    'preset' => 'full',
    'clientOptions' => [
        'filebrowserUploadUrl' => '/admin/product/image_editor'
    ]
]);

//echo $form->labelEx($model,'description'); ?>
<?php
//            $this->widget('application.extensions.CKEditor.CKEditorWidget',array(
//                'model'=>$model,           
//                'attribute'=>'description',
//                'height'=>'400px',
//                'width'=>'100%',
//                'toolbarSet'=>'Default',
////                'css' => Yii::app()->baseUrl.'/css/index.css',
//                'config'=>array(
//                        "EditorAreaCSS"=>Yii::app()->baseUrl.'/css/index.css',
//                        'filebrowserUploadUrl' => '/imgUpload',
//                    ),
//            ));
echo "<br/>";

echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить'); 
ActiveForm::end(); unset($form); 

?>
