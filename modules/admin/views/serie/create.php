<?php 
    $this->title = "Создать серию"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Серии', 'url' => ['/admin/serie']],
        $this->title
    ];
    
    echo $this->render('_form', ['model'=>$model]); 
?>