<?php
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\helpers\Html;

use app\models\Producer;

$this->title = "Серии";
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    $this->title,
];


if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchSerie,
    'id' => 'category-form',
    'columns' => [
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function ($model){
                return Html::a($model->name, ['/admin/serie/update','id'=>$model->id]);
            },
        ],
        [
            'attribute' => 'producer_id',
            'value' => function($model) {
                return $model->producer->name;
            },
            'filter' => Producer::getProducerArray(),
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:60px;'],
            'template' => '{delete}',
        ]
    ],
]);
?>