<?php

use yii\bootstrap\Alert;
use kartik\grid\GridView;
use kartik\date\DatePicker;
use yii\helpers\Html;
use app\modules\admin\models\LogsSearch;
use yii\grid\ActionColumn;

$this->title = "Логи";
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    $this->title
];


if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};

echo GridView::widget([
    'layout' => "{summary}\n{pager}\n{items}\n{pager}",
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'category-form',
    'columns' => [
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
            'filter'=>false,
        ],
        [
            'attribute' => 'type',
            'value' =>function ($model){
                return LogsSearch::itemAlias('type', $model->type);
            },
            'filter' => LogsSearch::itemAlias('type')
        ],
        [
            'attribute' => 'createtime',
            'value' =>function ($model){
                return date('d.m.Y H:i:s', $model->createtime);  
            }, 
            'filter' =>
                    DatePicker::widget(
                    [
                        'model' => $searchModel,
                        'name' => 'LogsSearch[createtime]',
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
//                        'value' => "$data->createtime",//"date('d-M-Y', $this->createtime)",
                        'pluginOptions' => [
//                            'attriute' => 'createtime',
                            'autoclose' => true,
                            'format' => 'd.m.yyyy',
                            'todayHighlight' => true
                        ]
            ], TRUE)
        ],
        [
            'attribute' => 'finishtime',
            'value' =>function ($model){
              return date('Y.m.d H:i:s', $model->finishtime);  
            }, 
            'filter' =>false,
//                    DatePicker::widget(
//                                [
//                                    'name' => 'finishtime',
//                                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
////                                    'value' => '23-Feb-1982',
//                                    'pluginOptions' => [
//                                        'autoclose' => true,
//                                        'format' => 'dd-M-yyyy'
//                                    ]
//                        ])
        ],[
            'attribute' => 'count',
            'value' => 'count',
            'filter' => false,
        ],[
            'class' => ActionColumn::className(),
            'header'=>'Файл',
            'contentOptions' => ['style' => 'width:60px;'],
            'template' => '{link}',
            'buttons' => [
                'link' => function ($url,$model) {
                    return $model->file;
                },
            ],
        ],
        [
            'class'=>'kartik\grid\BooleanColumn',
            'attribute' => 'not_error',
            'vAlign'=>'middle',
            'filter' => false,
        ],
    ],
]);
?>