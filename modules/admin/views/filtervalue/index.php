<?php $this->title = "Управление значением фильтров"; ?>
<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array(
        'Панель администратора'=>'/admin',
        $this->title,
    ),
)); 

$this->widget('bootstrap.widgets.TbAlert', array(
        'fade' => true,
        'closeText' => '&times;', // false equals no close link
        'events' => array(),
        'htmlOptions' => array(),
        'userComponentId' => 'user',
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'success' => array('closeText' => '&times;'),
            'info', // you don't need to specify full config
            'warning' => array('block' => false),
            'error' => array('block' => false)
        ),
    ));

$this->widget('bootstrap.widgets.TbGridView', array(
   'dataProvider' => $model->search(),
   'id' => 'category-form',
   'filter' => $model,
   'template' => "{pager}{items}{pager}",
   'columns' => array(
        [
            'name' => 'filter_value_id',
            'header' => '#',
            'htmlOptions' => ['color' =>'width: 60px'],
        ],
        'name',
       [
            'name' => 'filter_id',
            'value' => '$data->filter_id',            
//            'filter' => $model->getListDelivery(),
        ],
       
        [
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',
            'buttons'=>[
                'view'=>['url'=>'$data->getUrl()'],
            ],
        ],
        
    ),
)); ?>