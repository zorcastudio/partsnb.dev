<?php

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', [
    'id' => 'filter',
    'htmlOptions' => ['class' => 'well'], // for inset effect
    'enableClientValidation' => true,
        ]
);?>

<?php
echo $form->errorSummary($model);
echo $form->dropDownListGroup($model,'name',['widgetOptions' => array('data' => Filter::getListFilter())]);
echo $form->textFieldGroup($model, 'filter_id');

$form->widget('bootstrap.widgets.TbButton', ['buttonType' => 'submit', 'label' => $model->isNewRecord ? 'Создать' : 'Сохранить']);
$this->endWidget();
unset($form);
?>
