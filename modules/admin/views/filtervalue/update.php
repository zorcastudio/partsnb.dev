<?php $this->title = "Редактировать значение фильтра"; ?>
<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
    'links' => array(
        'Панель администратора'=>'/admin',
        'Управление свойствами фильтра' => '/admin/filtervalue',
        'Добавить тип доставки',
    ),
)); ?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>