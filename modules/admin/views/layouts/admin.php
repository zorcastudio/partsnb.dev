<?php
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <?= $this->render('/block/head') ?>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <?= $this->render('/block/header_admin') ?>
        <div id="content" class="admin">
                <?php 
                    echo Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ]);                
                    echo  $content; 
                ?>
            </div>
            <div class="clear"></div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>