<?php
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <?= $this->render('/block/head') ?>
    </head>
<body>
    <?php $this->beginBody() ?>
    <?= $this->render('/block/header') ?>
    <section id="new-offfer">
        <div class="wrapper"></div>
    </section>
    <section id="content">
        <div class="wrapper">
            <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            <?= $content ?>
        </div>
    </section>
    <div class="basket-wraper">
        <?= $this->render('/block/basket') ?>
    </div>
    <?= $this->render('/block/footer') ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
