<?php 
    $this->title = "Создать модель"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Модели', 'url' => ['/admin/models']],
        $this->title
    ];
    
    echo $this->render('_form', [
        'model'=>$model, 
        'producer'=>$producer,
        'serie'=>$serie,
    ]); 
?>