<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use app\models\Producer;
use mihaildev\ckeditor\CKEditor;

$form = ActiveForm::begin([
            'id' => 'product',
            "options" => ['class' => 'well'],
            'fieldConfig' => [
                'template' => "<div class=\"form-label\">{label}<span class='required'>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
            ]
        ]);

echo $form->errorSummary($model);

echo $form->field($model, 'producer_id')->dropDownList(
        $producer, [
    'onchange' => '$.post( "' . Url::toRoute('/ajax/serie') . '", {serie_id: $(this).val()}, insertSerie)',
    'prompt' => 'Выберите производителя',
]);

echo $form->field($model, 'serie_id')->dropDownList(
        $serie, [
    'prompt' => 'Выберите серию',
//    'disabled' => (!$model->isNewRecord && $model->serie_id) ? true : false
]);

$model->isNewRecord ? $form->field($model, 'producer_id')->dropDownList(Producer::getProducerArray(), ['prompt' => 'Выберите производителя']) : null;

//echo $model->isNewRecord ? $form->field($model,'serie_id')->dropDownList(Serie::get getProducerArray(), ['prompt' => 'Выберите производителя']) : null;

echo $form->field($model, 'delete')->checkbox();

echo $form->field($model, 'name')->textInput();

echo $form->field($model, 'name_lat')->textInput();

echo $form->field($model, 'child')->widget(Select2::classname(), [
    'data' => $model->_children,
    'options' => [
        'placeholder' => 'Выберите совместимые модели',
        'multiple' => true
    ],
    'pluginOptions' => [
        'allowClear' => false,
        'tokenSeparators' => [',', ' '],
        'tags' => true,
    ],
]);

echo $form->field($model, 'description')->widget(CKEditor::className(), [
    'editorOptions' => [
        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
        'inline' => false, //по умолчанию false
    ],
]);

echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить');
ActiveForm::end();
unset($form);
?>
