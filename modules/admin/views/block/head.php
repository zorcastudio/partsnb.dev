<?php 
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<meta charset="<?= Yii::$app->charset ?>">
<link rel="stylesheet" type="text/css" href="<?= Url::base(); ?>/css/main.css">
<link rel="stylesheet" type="text/css" href="<?= Url::base(); ?>/css/style.css">
<link rel="stylesheet" type="text/css" href="<?= Url::base(); ?>/css/admin.css">
<?= Html::csrfMetaTags() ?>
<title><?= Html::encode($this->title) ?></title>
<link rel="icon" href="/favicon.png" type="image/png">
<?php $this->head() ?>
<script type="text/javascript" src="<?= Url::base(); ?>/js/myjs.js"></script>
<script type="text/javascript" src="<?= Url::base(); ?>/js/admin.js"></script>

