<?php

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\widgets\Menu;
?>
<header class="admin">
    <?php
    NavBar::begin([
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default'
        ],
        'brandLabel' => FALSE,
        'renderInnerContainer' => false,
    ]);

    echo Nav::widget([
        'encodeLabels' => false,
        'options' => ['class' => 'navbar-nav navbar-left', 'style' => "width: 100%;"],
        'items' => [
            ['label' => '<span class="glyphicon glyphicon-file"></span> Страницы', 'icon' => 'file', 'active' => Yii::$app->controller->id == 'page' ? true : false,
                'items' => [
                    ['label' => 'Управление страницами', 'url' => '/admin/page', 'active' => (Yii::$app->request->url == '/admin/page') ? true : false],
                    ['label' => 'Добавить страницу', 'url' => '/admin/page/create', 'active' => (Yii::$app->request->url == '/admin/page/create') ? true : false],
                ]],
            ['label' => '<span class="glyphicon glyphicon-comment"></span> Новости', 'icon' => 'file', 'active' => Yii::$app->controller->id == 'novosti' ? true : false,
                'items' => [
                    ['label' => 'Управление новостями', 'url' => '/admin/novosti', 'active' => (Yii::$app->request->url == '/admin/novosti') ? true : false],
                    ['label' => 'Добавить новости', 'url' => '/admin/novosti/create', 'active' => (Yii::$app->request->url == '/admin/novosti/create') ? true : false],
                ]],
            
            
            
            ['label' => '<span class="glyphicon glyphicon-user"></span> Пользователи', 'icon' => 'user', 'active' => Yii::$app->controller->id == 'user' ? true : false,
                'items' => [
                    ['label' => 'Управление пользователями', 'url' => '/admin/user', 'active' => Yii::$app->request->url == '/admin/user' ? true : false],
                    ['label' => 'Добавить пользователя', 'url' => '/admin/user/create', 'active' => Yii::$app->request->url == '/admin/user/create' ? true : false],
                ]],
            ['label' => '<span class="glyphicon glyphicon-folder-open"></span> Категории', 'icon' => 'folder-open', 'active' => Yii::$app->controller->id == 'category' ? true : false,
                'items' => [
                    ['label' => 'Управление категориями', 'url' => '/admin/category', 'active' => Yii::$app->request->url == '/admin/category' ? true : false],
                    ['label' => 'Добавить категорию', 'url' => '/admin/category/create', 'active' => Yii::$app->request->url == '/admin/category/create' ? true : false],
                ]],
            ['label' => '<span class="glyphicon glyphicon-eye-open"></span> Фильтры', 'icon' => 'eye-open', 'active' => Yii::$app->controller->id == 'filter' ? true : false, 
                'items' => [
                    ['label' => 'Управление Фильтрами', 'url' => '/admin/filter', 'active' => Yii::$app->request->url == '/admin/filter' ? true : false],
                    ['label' => 'Добавить Фильтр', 'url' => '/admin/filter/create', 'active' => Yii::$app->request->url == '/admin/filter/create' ? true : false],
                ]],
            ['label' => '<span class="glyphicon glyphicon-wrench"></span> Производители', 'icon' => 'folder-open', 'active' => Yii::$app->controller->id == 'producer' ? true : false,
                'items' => [
                    ['label' => 'Управление производителями', 'url' => '/admin/producer', 'active' => Yii::$app->request->url == '/admin/producer' ? true : false],
                    ['label' => 'Добавить производителя', 'url' => '/admin/producer/create', 'active' => Yii::$app->request->url == '/admin/producer/create' ? true : false],
                ]],
            ['label' => '<span class="glyphicon glyphicon-list"></span> Серии', 'icon' => 'folder-open', 'active' => Yii::$app->controller->id == 'serie' ? true : false,
                'items' => [
                    ['label' => 'Управление сериями', 'url' => '/admin/serie', 'active' => Yii::$app->request->url == '/admin/serie' ? true : false],
                    ['label' => 'Добавить серию', 'url' => '/admin/serie/create', 'active' => Yii::$app->request->url == '/admin/serie/create' ? true : false],
                ]],
//            ['label' => '<span class="glyphicon glyphicon-bookmark"></span> Парт. номера', 'icon' => 'folder-open', 'active' => Yii::$app->controller->id == 'article' ? true : false,
//                'items' => [
//                    ['label' => 'Управление парт. номерами', 'url' => '/admin/pn', 'active' => Yii::$app->request->url == '/admin/pn' ? true : false],
//                    ['label' => 'Добавить парт. номер', 'url' => '/admin/pn/create', 'active' => Yii::$app->request->url == '/admin/pn/create' ? true : false],
//                ],
//            ],
            ['label' => '<span class="glyphicon glyphicon-tags"></span> Модели', 'icon' => 'folder-open', 'active' => Yii::$app->controller->id == 'models' ? true : false,
                'items' => [
                    ['label' => 'Управление моделями', 'url' => '/admin/models', 'active' => Yii::$app->request->url == '/admin/models' ? true : false],
                    ['label' => 'Добавить модель', 'url' => '/admin/models/create', 'active' => Yii::$app->request->url == '/admin/models/create' ? true : false],
                ]],
            ['label' => '<span class="glyphicon glyphicon-shopping-cart"></span> Товары', 'icon' => 'shopping-cart', 'active' => Yii::$app->controller->id == 'product' ? true : false,
                'items' => [
                    ['label' => 'Управление товарами', 'url' => '/admin/product', 'active' => Yii::$app->request->url == '/admin/product' ? true : false],
                    ['label' => 'Все  товары', 'url' => '/admin/product/all', 'active' => Yii::$app->request->url == '/admin/product/all' ? true : false],
                    ['label' => 'Добавить товар', 'url' => '/admin/product/create', 'active' => Yii::$app->request->url == '/admin/product/update' ? true : false],
                    ['label' => 'Проблемный товар', 'url' => '/admin/product/problem', 'active' => Yii::$app->request->url == '/admin/product/problem' ? true : false],
                ]],
            ['label' => '<span class="glyphicon glyphicon-send"></span> Доставка', 'icon' => 'send', 'active' => Yii::$app->controller->id == 'delivery' ? true : false,
                'items' => [
                    ['label' => 'Управление доставкой', 'url' => '/admin/delivery', 'active' => Yii::$app->request->url == '/admin/delivery' ? true : false],
                    ['label' => 'Добавить тип доставки', 'url' => '/admin/delivery/create', 'active' => Yii::$app->request->url == '/admin/delivery/create' ? true : false],
                ]],
            ['label' => '<span class="glyphicon glyphicon-usd"></span> Оплата', 'icon' => 'usd', 'active' => Yii::$app->controller->id == 'payment' ? true : false,
                'items' => [
                    ['label' => 'Управление типами оплаты', 'url' => '/admin/payment', 'active' => Yii::$app->request->url == '/admin/payment' ? true : false],
                    ['label' => 'Добавить тип оплаты', 'url' => '/admin/payment/create', 'active' => Yii::$app->request->url == '/admin/payment/create' ? true : false],
                ]],
            ['label' => '<span class="glyphicon glyphicon-tasks"></span> Заказы', 'icon' => 'tasks', 'url' => '/admin/order', 'active' => Yii::$app->controller->id == 'order' ? true : false],
            
            ['label' => '<span class="glyphicon glyphicon-refresh"></span> Логи', 'icon' => 'tasks', 'url' => '/admin/logs', 'active' => Yii::$app->controller->id == 'logs' ? true : false],
            
            ['label' => '<span class="glyphicon glyphicon-home"></span> Сайт', 'icon' => 'home', 'url' => '/', 'options' => ['class' => 'navbar-nav navbar-right']],
            ['label' => '<span class="glyphicon glyphicon-off"></span> Выход', 'icon' => 'off', 'url' => '/user/logout', 'visible' => !Yii::$app->user->isGuest, 'options' => ['class' => 'navbar-nav navbar-right']]
        ],
            ], [
    ]);
    NavBar::end();
    ?>
    <div class="clear"></div>
</header>