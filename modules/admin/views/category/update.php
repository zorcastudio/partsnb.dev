<?php 
    $this->title = "Редактировать категорию"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Категории', 'url' => ['/admin/category']],
        'Редактировать категорию'
    ];
    echo $this->render('_form', [
                    'model'=>$model, 
                    'filters'=>$filters
            ]); 
?>