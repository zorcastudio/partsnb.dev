<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;
use mihaildev\ckeditor\CKEditor;

use app\models\Category;

$form = ActiveForm::begin([
        'id' => 'product',
        "options"=>['class' => 'well'],
        'fieldConfig' => [
            'template' => "<div class=\"form-label\">{label}<span class='required'>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
        ]
    ]);

echo $form->errorSummary($model);

//echo $form->field($model,'parent_id')->dropDownList(Category::getCategories(), ['prompt' => 'Выберите категорию', 'disabled'=> true ]);


echo $form->field($model, 'name')->textInput();
echo $form->field($model, 'name_category')->textInput();
echo $form->field($model, 'name_lat')->textInput();
echo $form->field($model, 'is_published')->checkbox();

//echo $form->field($model, 'filter_set')->widget(Select2::classname(), [
//    'name' => 'filter_set',
//    'value' => [], // initial value
//    'data' =>$filters,
//    'options' => [
//        'multiple' => true, 
//        'class'=>'countries-select',
//        'placeholder' => 'Выберите фильтр'
//    ],
//    'pluginOptions' => [
//        'allowClear' => true,
//        'tags' => true,
//        'tokenSeparators' => [',', ' '],
//        'maximumInputLength' => Category::MAX_FILTER_COUNT,
//    ],
//]);
echo Html::error($model,'filter_set'); 

echo $form->field($model, 'description')->widget(CKEditor::className(), [
    'editorOptions' => [
        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
        'inline' => false, //по умолчанию false
    ],
]);

echo $form->field($model, 'description_product')->widget(CKEditor::className(), [
    'editorOptions' => [
        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
        'inline' => false, //по умолчанию false
    ],
]);
                

echo "<br/>";



echo "<br/>";
echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); 
ActiveForm::end(); unset($form); 
?>
