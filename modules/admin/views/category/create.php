<?php 
    $this->title = "Создать категорию"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Категории', 'url' => ['/admin/category']],
        $this->title
    ];
    echo $this->render('_form', [
                    'model'=>$model, 
                    'filters'=>$filters
            ]); 
?>