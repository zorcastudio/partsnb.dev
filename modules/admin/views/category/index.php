<?php
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\helpers\Html;

use app\models\Category;

$this->title = "Категории";
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    'Категории',
];



if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'category-form',
//        'template' => "{pager}{items}{pager}",
    'columns' => [
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function ($model){
                return Html::a($model->name, ["/admin/category/update",'id'=>$model->id]);
            },
        ],
//        [
//            'attribute' => 'parent_id',
//            'value' => function($model) {
//                return $model->getNameParent();
//            },
//            'filter' => Category::getCategories(),
//        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:80px;'],
            'template' => '{update}{delete}',
            'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(null, $model->url, [
                                    'class'=>'glyphicon glyphicon-eye-open',                                  
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a(null,  $model->url, 
                        [
                            'class'=>'glyphicon glyphicon-globe',                                  
                        ]);
                    },
            ],
        ]
    ],
]);
?>