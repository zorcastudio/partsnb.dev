<?php
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = "Список парт. номеров по артикулу";
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    $this->title
];

if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};

echo GridView::widget([
    'layout'=>"{summary}\n{pager}\n{items}\n{pager}",
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'category-form',
    'columns' => [
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
        ],
        [
            'attribute' => 'article',
            'format' => 'raw',
            'value' => function ($model){
                return Html::a($model->article, ['/admin/pn/update','article'=>$model->article]);
            },
        ],
        [
            'attribute' => 'name',
//            'format' => 'raw',
            'value' => 'name'
//            function ($model){
//                return Html::a($model->name, ['/admin/pn/update','article'=>$model->article]);
//            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:60px;'],
            'template' => '{delete}',
            'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a(null, ['/admin/pn/delete','article'=>$model->article], 
                            [
                                'class'=>'glyphicon glyphicon-trash',                                  
                            ]);
                    },
            ],
        ]
                    
    ],
]);
?>