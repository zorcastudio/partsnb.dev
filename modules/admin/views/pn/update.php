<?php 
    $this->title = "Редактировать парт.номер"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Парт. номера', 'url' => ['/admin/models']],
        $this->title
    ];
    echo $this->render('_form', ['model'=>$model]); 
?>