<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\JsExpression;


$form = ActiveForm::begin([
        'id' => 'product',
        "options"=>['class' => 'well'],
        'fieldConfig' => [
            'template' => "<div class=\"form-label\">{label}<span class='required'>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
        ]
    ]);

echo $form->errorSummary($model);

echo $form->field($model, 'article_id')->widget(Select2::classname(), [
        'data' => $model->all_article,
        'options' => [
            'placeholder' => 'Выберите артикул', 
            'disabled' => $model->isNewRecord ? false : true
        ],
        'pluginOptions' => ['allowClear' => true,],
]);
echo $form->field($model, 'children')->widget(Select2::classname(), [
        'options' => ['placeholder' => 'Заполните совместимые партномера'],
        'pluginOptions' => [
            'allowClear' => false,
            'tokenSeparators' => [',', ' '],
            'tags' => true,
        ],
    ]);
    
echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); 
ActiveForm::end(); unset($form); 
?>
