<?php 
    $this->title = "Создать парт. номер"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Парт. номера', 'url' => ['/admin/article']],
        $this->title
    ];
    
    echo $this->render('_form', ['model'=>$model]); 
?>