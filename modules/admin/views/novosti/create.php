<?php 
    $this->title = "Создать новость"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Новости', 'url' => ['/admin/novosti']],
        'Создать новость'
    ];
    echo $this->render('_form', ['model'=>$model]); 
?>