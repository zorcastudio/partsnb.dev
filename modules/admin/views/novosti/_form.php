<?php 
    use yii\bootstrap\ActiveForm;
    
    use dosamigos\ckeditor\CKEditor;
    use yii\helpers\Html;
    use app\models\Page;
?> 

<script type="text/javascript" src="/js/fileinput.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/fileinput.min.css">

<?php
$form = ActiveForm::begin([
        'id' => 'login',
        "options"=>['class' => 'well'],
        'fieldConfig' => [
            'template' => "<div class=\"form-label\">{label}<span class='required'>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
        ]
    ]);
echo $form->errorSummary($model);
echo $form->field($model, 'is_published')->checkbox();
echo $form->field($model, 'name')->textInput();
echo $form->field($model, 'name_lat')->textInput();
echo $form->field($model, 'description')->widget(CKEditor::className(),[
    'preset' => 'basic',
]);
echo $form->field($model, 'keywords')->widget(CKEditor::className(),[
    'preset' => 'basic',
]);

echo $form->field($model, 'text_short')->widget(CKEditor::className(),[
    'preset' => 'basic',
]);
echo $form->field($model, 'text')->widget(CKEditor::className(),[
    'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
    'clientOptions' => [
        'filebrowserUploadUrl' => '/admin/product/image_editor'
    ],
]);
echo "<br/>";

echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить'); 
ActiveForm::end(); unset($form); 

?>