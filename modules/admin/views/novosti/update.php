<?php 
    $this->title = "Редактировать новость"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Новости', 'url' => ['/admin/novosti']],
        'Редактировать новость'
    ];
    echo $this->render('_form', ['model'=>$model]); 
?>