<?php 
    $this->title = "Создать фильтр"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Управление фильтрами', 'url' => ['/admin/filter']],
        $this->title
    ];
    echo $this->render('_form', ['model'=>$model]); 
?>