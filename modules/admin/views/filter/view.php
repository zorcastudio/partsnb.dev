<?php 
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use kartik\alert\Alert;
use yii\helpers\Html;

    $this->title = $model->name; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Управление фильтрами', 'url' => ['/admin/filter']],
        $model->name,
    ];
    
    if (Yii::$app->session->hasFlash('success')) {
            echo Alert::widget([
            'type' => Alert::TYPE_SUCCESS,
            'titleOptions' => ['icon' => 'success-sign'],
            'body' => Yii::$app->session->getFlash('success')
        ]);
    }
    if (Yii::$app->session->hasFlash('error')) {
            echo Alert::widget([
            'type' => Alert::TYPE_DANGER,
            'titleOptions' => ['icon' => 'danger-sign'],
            'body' =>  Yii::$app->session->getFlash('error')
        ]);
    };
    
    echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'name',
        'name_lat',
        [
            'attribute' => 'category_id',
            'value' => $model->category ? $model->category->name : null,
        ],
    ]
]);

$form = ActiveForm::begin([
    'id' => 'filter-value',
    'fieldConfig' => [
        'template' => "{input}{error}",
    ]
]);
 
if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};
?>


<table id="filter-value">
    <tbody>
        <tr>
            <td colspan="5" ><h4>Название фильтра </h4></td>
        </tr>
        
        <?php foreach ($model->filterValue as $value) : ?>
        <tr>
            <th style="width: 50px; vertical-align: top; padding-top: 14px;">Имя*</th>
            <td><?php echo $form->field($value, 'name')->textInput(['name'=>"FilterValue[{$value->filter_value_id}][name]"]);?></td>
            <th style="width: 100px; vertical-align: top; padding-top: 14px; padding-left: 16px;">Для ссылки</th>
            <td><?php echo $form->field($value, 'name_lat')->textInput(["name"=>"FilterValue[{$value->filter_value_id}][name_lat]"]);?></td>
            <td class="filter-btn">
                <div class="btn btn-default" id="<?= $value->filter_value_id ?>" onclick="deleteFilterValue(this)" type="submit" name="yt0">Удалить</div>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div onclick="addFilterValue();" class="btn btn-default" id="add" type="submit" name="yt0">Добавить</div>

<?php
    echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ["id"=>'add']); 
    ActiveForm::end(); unset($form); 
?>



