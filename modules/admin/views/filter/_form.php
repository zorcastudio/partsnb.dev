<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$form = ActiveForm::begin([
        'id' => 'product',
        "options"=>['class' => 'well'],
        'fieldConfig' => [
            'template' => "<div class=\"form-label\">{label}<span class='required'>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
        ]
    ]);

echo $form->errorSummary($model);

echo $form->field($model, 'search_published')->checkbox();
echo $form->field($model, 'category_id')->dropDownList(app\models\Category::getCategories(), ['prompt' => 'Выберите категорию']);

echo $form->field($model, 'name')->textInput();
echo $form->field($model, 'name_lat')->textInput();

echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); 
ActiveForm::end(); unset($form);
?>
