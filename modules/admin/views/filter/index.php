<?php 
use yii\bootstrap\Alert;
use kartik\grid\GridView;
use app\models\Filter;
use yii\helpers\Html;
use kartik\grid\EditableColumn;
use kartik\editable\Editable;
use yii\helpers\Url;

$this->title = "Управление фильтрами";
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    $this->title
];


if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'category-form',
//        'template' => "{pager}{items}{pager}",
    'columns' => [
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
        ],
        [
            'class'=>'kartik\grid\EditableColumn',
            'attribute'=>'sort',
            'header'=>'Сортировка',
            'headerOptions'=>['class'=>'kartik-sheet-style'],
            'editableOptions'=> [
                        'ajaxSettings'=>[
                            'url'=> Url::to('/ajax/editable_sort'),
                        ],
                        'inputType' =>Editable::INPUT_SPIN,
                        'options' => [
                            'pluginOptions'=>[
                                'min'=>0, 
                                'max'=>5000,
                                'verticalbuttons' => true
                            ]
                        ],
                ]
            ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function ($model){
                return Html::a($model->name, ['/admin/filter/update','id'=>$model->id]);
            },
        ],
        [
            'attribute' => 'category_id',
            'value' => function($model) {
                $array = app\models\Category::getCategories();
                return isset($array[$model->category_id]) ? $array[$model->category_id] : null;
            },
            'filter' => app\models\Category::getCategories(),
        ],
        [
            'attribute' => 'search_published',
            'value' => function($model) {
                return Filter::itemAlias('NoYes',$model->search_published);
            },
            'filter' => Filter::itemAlias('NoYes'),
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:73px;'],
            'template' => '{view}{delete}',
            'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(null, $url, [
                            'class'=>'glyphicon glyphicon-plus-sign',                                  
                        ]);
                    }
                ],
        ]
    ],
]);
?>