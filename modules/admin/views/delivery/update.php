<?php 
    $this->title = "Редактировать тип доставки"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Доставка', 'url' => ['/admin/delivery']],
        $this->title
    ];
    echo $this->render('_form', ['model'=>$model]); 
?>