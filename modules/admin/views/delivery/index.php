<?php 
use yii\bootstrap\Alert;
use yii\grid\GridView;
use app\models\Category;
use yii\helpers\Html;

$this->title = "Доставка";
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    $this->title,
];

if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'category-form',
//        'template' => "{pager}{items}{pager}",
    'columns' => [
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function ($model){
                return Html::a($model->name, ['/admin/delivery/update','id'=>$model->id]);
            },
        ],
        'price',
        [
            'attribute' => 'status',
            'value' => function($model) {
                return $model->getStatus();
            },
            'filter' => app\models\Delivery::getListStatus(),
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:60px;'],
            'template' => '{delete}',
        ]
    ],
]);
?>