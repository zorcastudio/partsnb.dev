<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use mihaildev\ckeditor\CKEditor;

use app\models\Category;
$form = ActiveForm::begin([
        'id' => 'product',
        "options"=>['class' => 'well'],
        'fieldConfig' => [
            'template' => "<div class=\"form-label\">{label}<span class='required'>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
        ]
    ]);

echo $form->errorSummary($model);

echo $form->field($model, 'name')->textInput();

echo $form->field($model, 'page_id')->dropDownList(\app\models\Page::getPages(),['prompt' => 'Выберите страницу',]);

echo $form->field($model, 'adress')->textInput();

echo "<p>Если телефонов несколько разделите иx ' ; '</p>";
echo $form->field($model, 'phones')->textInput();

echo $form->field($model, 'price')->textInput();

echo $form->field($model, 'status')->dropDownList(app\models\Delivery::itemAlias('status'));

echo $form->field($model, 'description')->widget(CKEditor::className(), [
                    'editorOptions' => [
                        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ],
                ]);

echo $form->field($model, 'email_info')->widget(CKEditor::className(), [
    'editorOptions' => [
        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
        'inline' => false, //по умолчанию false
    ],
]);


echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); 
ActiveForm::end(); unset($form); 
?>
