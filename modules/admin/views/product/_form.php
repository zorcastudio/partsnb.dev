<script type="text/javascript">
    var article = '';
</script>
<script type="text/javascript" src="/js/fileinput.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/fileinput.min.css">
<style type="text/css">td {padding: 0 8px;}</style>

<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\JsExpression;

$form = ActiveForm::begin([
            'id' => 'product',
//            'method' => 'get',
            "options" => [
                'class' => 'well',
                'enctype' => "multipart/form-data",
            ],
            'fieldConfig' => [
                'template' => "<div class=\"form-label\">{label}<span class='required'>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
            ]
        ]);
?>
<table>
    <tbody>
        <tr>
            <td colspan="3"><?= $form->errorSummary($model); ?></td>
        </tr>
        <tr>
            <td colspan="3">
                <?php 
                    echo $form->field($model, 'ya_market')->checkbox();
                    echo $form->field($model, 'is_published')->checkbox();
                ?>
            </td>
        </tr>
        <tr>
            <td style="width: 33%">
                <?=
                $form->field($model, 'category_id')->dropDownList(
                        $category, [
                    'onchange' => 'insertName(this, "category"); $.post( "' . Url::toRoute('/admin/product/ajaxfilter') . '", {category_id: $(this).val()}, insertFilter)',
                    'prompt' => 'Выберите категорию',
                ]);
            
                ?>
                <?php                
//              echo $form->field($model, 'producer_id')->dropDownList(
//                        $producer, [
//                    'onchange' => 'insertName(this, "producer");', //$.post( "' . Url::toRoute('/ajax/model') . '", {producer_id: $(this).val()}, insertModel); inputListModel($(this).val(), "'.$model->id.'");
//                    'prompt' => 'Выберите производителя',
//                    'disabled' => $model->producer_id ? false : true
//                ]);
                ?>
            </td>
            <td style="width: 33%"><?= $form->field($model, '_nameOne')->textInput(['disabled' => $model->_nameOne ? false : true]); ?></td>
            <td style="width: 33%"><?= $form->field($model, '_nameEnd')->textInput(); ?></td>
        </tr>
        <tr>
            <td><?= $form->field($model, 'in_stock1')->textInput(); ?></td>
            <td><?= $form->field($model, 'in_stock2')->textInput(); ?></td>
            <td><?= $form->field($model, 'in_stock3')->textInput(); ?></td>
        </tr>
        <?php if($series !== null) { ?>
        <tr>
            <td colspan="2"><?= $form->field($series, 'name')->textInput(['readonly' => true, 'value' => $series->name.' [id = '.$series->id.']' , 'name' => 'no_field']); ?></td>
        </tr>
        <?php } ?>
        <tr>
            <td colspan="2"><?= $form->field($model, 'price')->textInput(); ?></td>
            <td><?= $form->field($model, 'article')->textInput(['maxlength' => 6, 'readonly' => $model->article == '' ? false : true]); ?></td>
        </tr>
        <tr>
            <td colspan="3">
            <?php
                echo "<div id='filters'>";
                if ($producer && $for_filters) {
                    echo $this->render('_filters', ['model' => $for_filters, 'active' => $active]);
                }
                echo "</div>";
            ?>
            </td>
        </tr>
        <tr>
            <td style="vertical-align: bottom;">
            <?=
            $form->field($model, 'logo_temp')->fileInput([
                'id' => "input-logo",
                'options' => [
                    'type' => "file",
                    'class' => "file-loading",
                ]
            ])
            ?>
            </td>
            <td  colspan="2">
                <?php
                    if ($model->logo)
                        echo "<div class='logo-view' style='background: url(/uploadfile/images/product/resized/$model->logo) 50% 50% / contain no-repeat;'></div>";
                ?>
            </td>
        </tr>
    </tbody>
</table> 
<?php
//preg_replace('/\.\w+/', '', $value->url)
if ($model->image) {
    foreach ($model->image as $value) {
        echo '<div class="image-view" style="background: url(/uploadfile/images/product/' . $value->url . ') 50% 50% / contain no-repeat;"><div id="' . $value->id . '" class="delete-image"></div></div>';
    }
    echo "<div class='clear'></div>";
}

echo $form->field($model, 'images')->fileInput([
    'id' => "input-images",
    "multiple" => 'multiple',
    'name' => 'images[]',
    'options' => [
        'type' => "file",
        'class' => "file-loading",
]]);

//echo $form->field($model, '_pnSearch')->widget(Select2::classname(), [
//    'data' => $pnSearch,
//    'options' => [
//        'placeholder' => 'Выберите совместимые парт-номера для поиска',
//        'multiple' => true
//    ],
//    'pluginOptions' => [
//        'allowClear' => false,
//        'tokenSeparators' => [',', ' '],
//        'tags' => true,
//    ],
//]);
//echo $form->field($model, '_pnSearch')->widget(Select2::classname(), [
//        'data' => $pnSearch,
//        'options' => [
//            'placeholder' => 'Выберите совместимые парт-номера для поиска', 
//            'multiple' => true,
//        ],
//        'pluginOptions' => [
//            'allowClear' => true,
//            'minimumInputLength' => 2,
//            'ajax' => [
//                'url' => \yii\helpers\Url::to(['/ajax/add_pn']),
//                'dataType' => 'json',
//                'data' => new JsExpression("function(params) {return {q:params.term, article:'".$model->article."'}; }")
//            ],
//            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
//            'templateResult' => new JsExpression('function(model) { return model.text; }'),
//            'templateSelection' => new JsExpression('function (model) { return model.text; }'),
//        ],
//    ])

?>


<!--<div class="form-label"><label class="control-label" for="genereteproduct-category_id">Модели</label><span class="required">*</span></div>
<div id="ajax-model">
    <?php
//    echo $this->render('_ajax_model', [
//        'modelList' => $modelList,
//        'modelCheket' => $modelCheket,
//    ]);
    ?>
</div>-->
<table class="add-producer">
    <thead>
        <tr>
            <td colspan="2">Список моделей по производитям</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
               
            </td>
            <td>
                <?= $form->field($model, 'producer_search')->dropDownList(
                        $model->list_producer, [
                            'onchange' => "addModelProducer($(this), '{$model->article}');",
                            'prompt' => 'Выберите производителя',
//                            'disabled' => $model->producer_id ? false : true
                        ]); 
                ?>
            </td>
        </tr>
        <?php foreach ($model->children as $value) : ?>
        <tr>
            <td colspan="2">
                <?=  $this->render('_compatible_model', ['children' => $value, 'article'=>$model->article]); //, 'partNamber'=>$model->partNamber?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
echo Html::hiddenInput('GenereteProduct[available]', $model->available);

echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить');
ActiveForm::end();
unset($form);
?>
 <?php // vd($model, false); ?>


<script>
<?php
$categoryName = '';
if ($model->name && $model->producer) {
    $serch = preg_match('/^(.*) ' . $model->producer->name . '/i', $model->name, $sought);
    if ($serch) {
        $categoryName = $sought[1];
    }
} else {
    $categoryName = $model->category ? $model->category->name : null;
}
$producerName = $model->producer ? $model->producer->name : null;
?>
    var categoryName = '<?= $categoryName ?>';
    var producerName = '<?= $producerName ?>';
    var modelName = '';


    $("#input-logo").fileinput({
        uploadUrl: "<?= Url::toRoute(['/admin/product/uploadlogo', 'id' => $model->id]); ?>",
        uploadAsync: false,
        maxFileCount: 10,
        allowedFileExtensions: ["jpg", "gif", "png"],
        overwriteInitial: false,
        showCaption: true,
        showRemove: true,
        showUpload: true,
        initialPreviewShowDelete: false,
        previewFileType: "Изображения",
        browseLabel: "Открыть",
        removeClass: "btn btn-danger",
        removeLabel: "Удалить",
        uploadClass: "btn btn-info",
        uploadLabel: "Загрузить",
        dropZoneEnabled: false,
    });

//    var footerTemplate = '<div class="file-thumbnail-footer">\n' +
//'   <div style="margin:5px 0">\n' +
//'       <input class="kv-input kv-new form-control input-sm {TAG_CSS_NEW}" value="{caption}" placeholder="Enter caption...">\n' +
//'       <input class="kv-input kv-init form-control input-sm {TAG_CSS_INIT}" value="{TAG_VALUE}" placeholder="Enter caption...">\n' +
//'   </div>\n' +
//'   {actions}\n' +
//'</div>';
    $("#input-images").click(function(){
        article = $('#genereteproduct-article').val();
        console.log(article);
        if(article == ''){
            alert ('Вы не заполнили артикул');
            return false;
        }
    });

    $("#input-images").fileinput({
        uploadUrl: '<?= Url::toRoute(['/admin/product/upload', 'id' => $model->id]); ?>',
        uploadAsync: false,
        maxFileCount: 10,
        allowedFileExtensions: ["jpg", "gif", "png"],
        overwriteInitial: false,
        showCaption: true,
        showRemove: true,
        showUpload: true,
        initialPreviewShowDelete: false,
        previewFileType: "Изображения",
        browseLabel: "Открыть",
        removeClass: "btn btn-danger",
        removeLabel: "Удалить",
        uploadClass: "btn btn-info",
        uploadLabel: "Загрузить",
        dropZoneEnabled: false,
//    layoutTemplates: {footer: footerTemplate},
//    previewThumbTags: {
//        '{TAG_VALUE}': '',        // no value
//        '{TAG_CSS_NEW}': '',      // new thumbnail input
//        '{TAG_CSS_INIT}': 'hide'  // hide the initial input
//    }
    });
</script>
