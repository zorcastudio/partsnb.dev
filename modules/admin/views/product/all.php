<?php 
use yii\bootstrap\Alert;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\Html;
use app\models\Page;

$this->title = "Товары";
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    $this->title = "Товары"
];

if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};

if (Yii::$app->session->hasFlash('success')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-success'],
        'body' => Yii::$app->session->getFlash('success'),
    ]);
};

if (Yii::$app->session->hasFlash('error')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-error'],
        'body' => Yii::$app->session->getFlash('error'),
    ]);
};
?>
<h2>Список всех товаров</h2>
<div class="published">
    <?= Html::beginForm(['/admin/product/check'],'post');?>
    <?= Html::dropDownList('action','',[1=>'В маркете', 2=>'В googl', 3 =>'На сайте'],['class'=>'dropdown',])?>
    <?= Html::submitButton('Опубликовать', ['class' => 'btn btn-info', 'value'=>"1", 'name'=>"button"]);?>
    <?= Html::submitButton('Снять с публикации', ['class' => 'btn btn-info', 'value'=>"2", 'name'=>"button" ]);?>
    <?= Html::submitButton('Удалить', ['class' => 'btn btn-info', 'value'=>"3", 'name'=>"button" ]);?>
</div>

<div class="view-list">
    <p>Показать по </p>
    <?= Html::a('50',  Page::getUrlListALL(50), ['class'=> Page::getActiveList(50)? 'active' : null]) ?>
    <?= Html::a('100', Page::getUrlListALL(100), ['class'=> Page::getActiveList(100)? 'active' : null]) ?>
    <?= Html::a('200', Page::getUrlListALL(200), ['class'=> Page::getActiveList(200)? 'active' : null]) ?>
    <?= Html::a('500', Page::getUrlListALL(500), ['class'=> Page::getActiveList(500)? 'active' : null]) ?>
    <?= Html::a('1000', Page::getUrlListALL(1000), ['class'=> Page::getActiveList(1000)? 'active' : null]) ?>
    <p> строк</p>
    <div style="clear: both"></div>
</div>

<?php

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'product-form',
    'layout'=>"{summary}\n{pager}\n{items}\n{pager}",
    'columns' => [
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:80px;'],
            'template' => '{update}{delete}',
            'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a(null, ['/admin/product/del_one', 'id'=>$model->id], 
                            [
                                'class'=>'glyphicon glyphicon-trash',                                  
                            ]);
                    },
                    'view' => function ($url, $model) {
                        return Html::a(null, $model->urlView, 
                            [
                                'class'=>'glyphicon glyphicon-eye-open',                                  
                            ]);
                    },
                    'update' => function ($url, $model) {
                        if($model->model_id && $model->serie_id){
                            return Html::a(null,  $model->url, 
                            [
                                'class'=>'glyphicon glyphicon-globe',                                  
                            ]);
                        }else{
                            return Html::a(null,  '#', 
                            [
                                'class'=>'glyphicon glyphicon-eye-close',                                  
                            ]);
                        }
                    },
            ],
        ],
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
        ],
        [
            'attribute' => 'createtime',
            'value' =>  function ($model){
                return date('Y.m.d H:s', $model->createtime);
            },
            'options' => ['style' => 'width: 80px'],
        ],
        [
            'attribute' => 'article',
            'format' => 'raw',
            'options' => ['style' => 'width: 54px'],
            'value' => function ($model){
                $url = ['/admin/product/update_cut', 'id'=>$model->id];
                
                if(isset($_GET['AllProductSearch'])){
                    $url['get'] = json_encode($_GET['AllProductSearch']);
                }
                return Html::a($model->article, $url);
            },
        ],
//        'is_delete',
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function ($model){
                $url = ['/admin/product/update_cut', 'id'=>$model->id];
                
                if(isset($_GET['AllProductSearch'])){
                    $url['get'] = json_encode($_GET['AllProductSearch']);
                }
                return Html::a($model->name, $url);
            },
        ],
        [
            'attribute' => 'category_id',
            'value' => function($model) {
                return $model->category ? $model->category->name : null;
            },
            'filter' =>  \app\models\Category::getCategoriesLast(),
        ],
        [
            'attribute' => 'producer_id',
            'value' => function($model) {
                return $model->producer ? $model->producer->name : null;
            },
            'filter' =>  \app\models\Producer::getProducerArray(),
        ],
        [
            'attribute' => 'model_id',
            'value' => function($model) {
                return $model->model ? $model->model->name : null;
            },
        ],
        [
            'attribute' => 'serie_id',
            'value' => function($model) {
                return $model->serie ? $model->serie->name : null;
            },
        ],
        [
            'attribute' => 'is_problem',
            'value' => function($model) {
                return $model->getPropertiName($model->is_problem);
            },
            'filter' => app\models\Product::getProblem(),
        ],
        [
            'class'=>'kartik\grid\BooleanColumn',
            'attribute'=>'ya_market', 
            'vAlign'=>'middle',
        ],
        [
            'class'=>'kartik\grid\BooleanColumn',
            'attribute'=>'googl_market', 
            'vAlign'=>'middle',
        ],
        [
            'class'=>'kartik\grid\BooleanColumn',
            'attribute'=>'is_published', 
            'vAlign'=>'middle',
        ],
        [
            'class' => 'kartik\grid\CheckboxColumn',
            'checkboxOptions' => function ($model, $key, $index, $column) {
                return ['value' => $model->id];
            }   
        ]
    ],
]);
?>
<?= Html::endForm() ?>
