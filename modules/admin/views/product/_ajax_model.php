<?php 
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\models\Model;
//$cityDesc = empty($model->_compatible) ? '' : City::findOne($model->city)->description;
//vd($modelList, false);

echo Select2::widget([
    'name' => 'Compatible',
    'value' => $modelCheket, // initial value
    'data' => $modelList,
    'options' => [
        'placeholder' => 'Выберите модель', 
        'multiple' => false,
//        'disabled'=> empty($modelList) ? true : false,
        'id' => 'product-model_id'
    ],
//    'pluginOptions' => [
//        'allowClear' => false,
//        'tokenSeparators' => [',', ' '],
//        'tags' => true,
//        'maximumInputLength' => 10
//    ],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 2,
        'ajax' => [
            'url' => \yii\helpers\Url::to(['/admin/product/model_list']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) { return {q:params.term}; }')
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('function(model) { return model.text; }'),
        'templateSelection' => new JsExpression('function (model) { return model.text; }'),
    ],
]);

?>