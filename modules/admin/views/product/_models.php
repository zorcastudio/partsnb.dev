<?php 
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

foreach ($model as $value) : ?>
        <div class="form-group">
                <?php if(get_class($value) == 'app\models\ProductFilterValue') : ?>
                    <label class="control-label required" for="Product_category_id"><?= $value->filter->name ?></label>
                    <?= Html::dropDownList(
                            'Product[filter]['.$value->filter_id.']', 
                            $value->filter_value_id, 
                            ArrayHelper::map($value->value_all, 'filter_value_id', 'name'),
                            ['class'=>"form-control"]);
                    ?>
                <?php else : ?>
                    <label class="control-label required" for="Product_category_id"><?= $value->name ?></label>
                    <?= Html::dropDownList(
                            'Product[filter]['.$value->id.']', 
                            $value->name, 
                            ArrayHelper::map($value->filterValue, 'filter_value_id', 'name'),
                            ['class'=>"form-control"]);
                    ?>
                <?php endif; ?>
        </div>
<?php endforeach;?>

    
