<?php 
use kartik\select2\Select2;
use yii\web\JsExpression;
?>

<label>Модели производителя <?= $children['model']['name'] ?></label>
<?= Select2::widget([
        'name' => "Compatible[{$children['model']['p_id']}]",
        'data' => isset($children['model']['id']) ? $children['model']['id'] : [],
        'value' => isset($children['model']['id']) ? array_keys($children['model']['id']) : [],
        'options' => [
            'placeholder' => 'Выберите модель', 
            'multiple' => true,
            'id' => "producer-model-{$children['model']['p_id']}"
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 2,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/ajax/add_model']),
                'dataType' => 'json',
                'data' => new JsExpression("function(params) {return {q:params.term, producer_id:{$children['model']['p_id']}, article:'{$article}', is_pn:0};}")
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(model) { return model.text; }'),
            'templateSelection' => new JsExpression('function (model) { return model.text; }'),
        ],
    ]);
?>

<label>Парт. номер производителя <?= $children['model']['name'] ?></label>
<?= Select2::widget([
        'name' => "PartNamber[{$children['model']['p_id']}]",
        'data' => isset($children['part_namber']['id']) ? $children['part_namber']['id'] : [],
        'value' => isset($children['part_namber']['id']) ? array_keys($children['part_namber']['id']) : [],
        'options' => [
            'placeholder' => 'Выберите парт. номер', 
            'multiple' => true,
            'id' => "pn-model-{$children['model']['p_id']}"
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 2,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['/ajax/add_model']),
                'dataType' => 'json',
                'data' => new JsExpression("function(params) {return {q:params.term, producer_id:{$children['model']['p_id']}, article:'{$article}', is_pn:1};}")
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(model) { return model.text; }'),
            'templateSelection' => new JsExpression('function (model) { return model.text; }'),
        ],
    ]);
?>