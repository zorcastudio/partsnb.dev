<script type="text/javascript" src="/js/fileinput.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/fileinput.min.css">
<style type="text/css">td {padding: 0 8px;}</style>

<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\JsExpression;

$form = ActiveForm::begin([
            'id' => 'product',
            "options" => [
                'class' => 'well', 
                'enctype'=>"multipart/form-data", 
            ],
            'fieldConfig' => [
                'template' => "<div class=\"form-label\">{label}<span class='required'>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
            ]
        ]);
?>

<table>
    <tbody>
        <tr>
            <td colspan="2"><?= $form->errorSummary($model); ?></td>
        </tr>
        <tr>
            <td colspan="2">
                <?php
                echo $form->field($model, 'name');

                echo $form->field($model, 'category_id')->dropDownList(
                    $category, 
                    [
                        'onchange' => '$.post( "'.Url::toRoute('/admin/product/ajaxfilter').'", {category_id: $(this).val()}, insertFilter)',
                        'disabled'=> $model->category_id ? true : false,
                        'prompt' => 'Выберите категорию',
                    ]);
                echo   $form->field($model, 'producer_id')->dropDownList(
                    $producer, 
                    [
                        'onchange' => '$.post( "' . Url::toRoute('/ajax/model') . '", {producer_id: $(this).val()}, insertModel)',
                        'prompt' => 'Выберите производителя',
                    ]);
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <?= $form->field($model, 'model_id')->widget(Select2::classname(), [
                'data' => $modelList,//isset($children['model']['id']) ? $children['model']['id'] : [],
                'options' => [
                    'placeholder' => 'Выберите модель', 
//                    'multiple' => true,
//                    'id' => "product-model_id"
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 2,
//                    'ajax' => [
//                        'url' => \yii\helpers\Url::to(['/ajax/add_model']),
//                        'dataType' => 'json',
//                        'data' => new JsExpression("function(params) {return {q:params.term, producer_id:{$model->producer_id}, article:'{$model->article}';}")
//                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(model) { return model.text; }'),
                    'templateSelection' => new JsExpression('function (model) { return model.text; }'),
                ],
            ]);
                
//                echo $form->field($model, 'model_id')->dropDownList($modelList); 
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <?php 
                echo "<div id='filters'>";
                    if ($producer && $for_filters) {
//                        vd($for_filters, false);
//                        vd($active, false);
                        echo $this->render('_filters', ['model' => $for_filters, 'active'=>$active]);
                    }
                echo "</div>";
                ?>
            </td>
        </tr>
    </tbody>
</table>
<?php 
echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить');
ActiveForm::end();
unset($form);
?>
