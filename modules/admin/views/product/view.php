<?php 
use yii\widgets\DetailView;
use yii\helpers\Html;
use yii\grid\GridView;
use \app\models\Page;
use yii\helpers\Url;
//use yii;

$this->title = $model->article; 
$this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Товары', 'url' => ['/admin/product']],
        $this->title
    ];
 
echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'article',
        [               
            'attribute' => 'category_id',
            'value' => $model->category ? $model->category->name : null,
        ],
        [               
            'attribute' => 'category_id',
            'value' => $model->category ? $model->category->name : null,
        ],
        [               
            'attribute' => 'producer_id',
            'value' => $model->producer ? $model->producer->name : null,
        ],
        'price',
        [               
            'attribute' => '_forSearchModel',
            'value' => $forSearchModel,
        ],
        
        [               
            'attribute' => '_forSearchPN',
            'value' => $forSearchPN,
        ],
    ],
]);
?>
<div class="view-list">
    <p>Показать по </p>
    <?= Html::a('50', Page::getUrlListGV(50), ['class'=> Page::getActiveList(50)? 'active' : null]) ?>
    <?= Html::a('100', Page::getUrlListGV(100), ['class'=> Page::getActiveList(100)? 'active' : null]) ?>
    <?= Html::a('200', Page::getUrlListGV(200), ['class'=> Page::getActiveList(200)? 'active' : null]) ?>
    <?= Html::a('500', Page::getUrlListGV(500), ['class'=> Page::getActiveList(500)? 'active' : null]) ?>
    <?= Html::a('1000', Page::getUrlListGV(1000), ['class'=> Page::getActiveList(1000)? 'active' : null]) ?>
    <p> строк</p>
    <div style="clear: both"></div>
</div>
<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'product-form',
    'layout' => "{pager}\n{summary}\n{items}\n{pager}", //{sorter}\n
    'columns' => [
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
        ],
        [
            'attribute' => 'createtime',
            'value' =>  function ($model){
                return date('d.m.Y H:i', $model->createtime);
            },
            'filter'=> false,
            'options' => ['style' => 'width: 120px'],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function ($model){
                return Html::a($model->name, ["/admin/product/update_cut",'id'=>$model->id]);
            },
        ],
        [
            'attribute' => 'is_published',
            'value' => function ($model){
                return app\models\Product::itemAlias('Y/N', $model->is_published);
            },
            'filter' => app\models\Product::itemAlias('Y/N'),
        ],
        [
            'attribute' => 'ya_market',
            'format' => 'raw',
            'value' => function ($model){
                return app\models\Product::itemAlias('Y/N', $model->ya_market);
            },
            'filter' => app\models\Product::itemAlias('Y/N'),
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:60px;'],
            'template' => '{update}{delete}',
            'buttons' => [
                    'view' => function ($url, $model) {
                            return Html::a(null, $model->url, [
                                'class'=>'glyphicon glyphicon-eye-open',                                  
                            ]);
                        },
                    'update' => function ($url, $model) {
                            return Html::a(null, $model->url,[
                                'class'=>'glyphicon glyphicon-globe',                                  
                                ]);
                        },
                    'delete' => function ($url, $model) {
                            return Html::a(null,Url::toRoute(['/admin/product/delete_one', 'id' =>$model->id ]), [
                                'class'=>'glyphicon glyphicon-trash',                                  
                            ]);
                        },
            ],
        ]
    ]]);

?>