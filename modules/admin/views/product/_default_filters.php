<?php 
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
foreach ($model as $value) : ?>
        <div class="form-group">
            <label class="control-label required" for="Product_category_id"><?= $value->filter->name ?></label>
            <?= Html::dropDownList(
                    'Product[filter]['.$value->filter_id.']', 
                    null, //$value->value->filter_value_id, 
                    ArrayHelper::map($value->value, 'filter_value_id', 'name'),
                    ['class'=>"form-control"]);
            ?>
        </div>
<?php endforeach;?>

    
