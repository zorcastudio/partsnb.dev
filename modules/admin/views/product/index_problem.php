<?php 
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;
use app\models\Producer;

$this->title = "Товары";
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    $this->title = "Товары"
];

if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};
?>
<h2>Список проблемных товаров!!!</h2>
<?php



echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'product-form',
    'layout'=>"{summary}\n{pager}\n{items}\n{pager}",
    'columns' => [
//         [
//            'class' => 'yii\grid\CheckboxColumn',
//            'checkboxOptions' => function($model, $key, $index, $column) {
//                  return ['value' => $model->is_problem];
//            }
//        ],
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
        ],
        'article',
         [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function ($model){
                return Html::a($model->name, $model->urlUpdate);
            },
        ],
        [
            'attribute' => 'category_id',
            'value' => function($model) {
                return $model->category ? $model->category->name : null;
            },
            'filter' =>  \app\models\Category::getCategoriesLast(),
        ],
        [
            'attribute' => 'producer_id',
            'value' => function($model) {
                return $model->producer ? $model->producer->name : null;
            },
            'filter' => Producer::getProducerArray(),
        ],
        [
            'attribute' => 'model_id',
            'value' => function($model) {
                return $model->model ? $model->model->name : null;
            },
//            'filter' => Producer::getProducerArray(),
        ],
//        [
////            'class' => 'yii\grid\CheckboxColumn',
////            'name'=>'is_delete',
//            
//            'attribute' => 'is_delete',
//            'value' => function($model){
//                return  CheckboxX::widget([
//                                'model' => $model,
//                                'attribute' => 'is_delete',
//                                'pluginOptions' => [
//                                    'threeState' => true,
//                                    'size' => 'lg'
//                                ]
//                            ]);
//            }  
//           
//        ],
        [
            'attribute' => 'is_problem',
            'value' => function($model) {
                return $model->getPropertiName($model->is_problem);
            },
            'filter' => app\models\Product::getProblem(),
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:80px;'],
            'template' => '{view}{delete}',
            'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(null,  $model->url, [
                                    'class'=>'glyphicon glyphicon-globe',                                  
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a(null,  $model->url, [
                                    'class'=>'glyphicon glyphicon-pencil',                                  
                        ]);
                    },
            ],
        ]
    ],
]);
?>