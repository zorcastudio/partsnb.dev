<?php 
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\helpers\Html;
use app\models\Producer;

$this->title = "Товары";
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    $this->title = "Товары"
];

if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};
?>
<h2>Список уникальных товаров, которые имеют описание</h2>
<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'product-form',
    'layout'=>"{summary}\n{pager}\n{items}\n{pager}",
    'columns' => [
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
        ],
        'article',
        [
            'attribute' => 'createtime',
            'value' =>  function ($model){
                return date('Y.m.d H:s', $model->createtime);
            },
            'options' => ['style' => 'width: 80px'],
            'filter'=>false,
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function ($model){
                return Html::a($model->name, $model->urlUpdate);
            },
        ],
        [
            'attribute' => 'category_id',
            'value' => function($model) {
                return $model->category ? $model->category->name : null;
            },
            'filter' =>  \app\models\Category::getCategoriesLast(),
        ],
//        [
//            'attribute' => 'producer_id',
//            'value' => function($model) {
//                return $model->producer->name;
//            },
//            'filter' => Producer::getProducerArray(),
//        ],
        'price',
        '_count',
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:80px;'],
            'template' => '{view}{update}{delete}',
            'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(null, $model->urlView, 
                            [
                                'class'=>'glyphicon glyphicon-eye-open',                                  
                            ]);
                    },
                    'update' => function ($url, $model) {
                        if($model->model_id && $model->serie_id){
                            return Html::a(null,  $model->url, 
                            [
                                'class'=>'glyphicon glyphicon-globe',                                  
                            ]);
                        }else{
                            return Html::a(null,  '#', 
                            [
                                'class'=>'glyphicon glyphicon-eye-close',                                  
                            ]);
                        }
                    },
            ],
        ]
    ],
]);
?>