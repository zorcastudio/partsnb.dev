<?php $this->title = "Генератор товаров"; 

    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Товары', 'url' => ['/admin/product']],
        $this->title
    ];
    
    echo $this->render('_form', [
        'model' => $model,
        'series' => $series,
        "category" => $category,
        "producer" => $producer,
        "modelList" => $modelList,
//        "modelSearch" => $modelSearch,
        "pnSearch" => $pnSearch,
        "modelCheket" => $modelCheket,
        'for_filters' => $for_filters,
        'active' => $active,
//        'producerModel' => $producerModel,
//        'otherProducer' => $otherProducer,
//        'producerSearch' => $producerSearch,
//        'defaultFilters' => $defaultFilters
    ]);
?>