<script type="text/javascript" src="/js/fileinput.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/fileinput.min.css">
<style type="text/css">td {padding: 0 8px;}</style>
<?php
$this->title = "Редактировать дочерный товар"; 

    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Товары', 'url' => ['/admin/product']],
        ['label' => $model->article, 'url' => ['/admin/product/view', 'article'=>$model->article]],
        $this->title
    ];
    
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use mihaildev\ckeditor\CKEditor;

use app\models\Country;
use app\models\Region;
use app\models\Registration;
use kartik\select2\Select2;
use yii\web\JsExpression;

$form = ActiveForm::begin([
//            'layout' => 'horizontal',
            'id' => 'product',
            "options" => ['class' => 'well', 'enctype'=>"multipart/form-data"],
//            'fieldConfig' => [
//                'template' => "<div class=\"form-label\">{label}<span class='required'>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
//            ],
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-4',
                    'offset' => 'col-sm-offset-4',
                    'wrapper' => 'col-sm-8',
                    'error' => '',
                    'hint' => '',
                ],
            ],
    
    
        ]);
?>

<table>
    <tbody>
        <tr>
            <td colspan="2"><?= $form->errorSummary($model); ?></td>
        </tr>
        <tr>
            <td colspan="2">
                <?= $form->field($model, 'is_published')->checkbox();?>
                <?= $form->field($model, 'ya_market')->checkbox(); ?>
                <?= Html::activeHiddenInput($model, 'article', ['id'=>'genereteproduct-article']) ?>
            </td>
        </tr>
        <tr>
            <td style="width: 52%;">
            <?php if($model->producer_id) : ?>
                <?= $form->field($model, 'model_id')->widget(Select2::classname(), [
                    'data' => $modelList,
                    'options' => [
                        'placeholder' => 'Выберите модель',
                        'multiple' => false
                    ],
                    'pluginOptions' => [
                        'allowClear' => false,
                        'minimumInputLength' => 2,
                    ],
                ]);
                 ?>
            <?php else : ?>
               <?= $form->field($model, 'model_id')->widget(Select2::classname(), [
                'name' => 'tooth_no',
                'options' => [
                    'placeholder' => 'Пошук модели...',
                    'id' => 'user-id',
                ],
                'id'=>'user-1',
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 2,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                    ],
                    'ajax' => [
                        'url' => Url::to("/ajax/search_model_all"),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                ],
            ]);
            ?>
            <?php endif; ?>
            </td>
            <td>
                <?= $form->field($model, 'name')->textInput(); //['disabled'=>$model->name ? false : true] ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><?= $form->field($model, 'name_lat')->textInput(['disabled'=>$model->name_lat ? false : true]); ?></td>
        </tr>
        <tr>
            <td style="vertical-align: bottom;">
                <?= $form->field($model, 'logo_temp')->fileInput([
                        'id' => "input-logo",
                        'options' => [
                            'type' => "file", 
                            'class' => "file-loading", 
                        ]
                    ]) ?>
            </td>
            <td><?php if ($model->logo)
                    echo "<div class='logo-view' style='background: url(/uploadfile/images/product/resized/$model->logo) 50% 50% / contain no-repeat;'></div>";
                ?>
            </td>
        </tr>
    </tbody>
</table>

<?php
if ($model->image) {
    foreach ($model->image as $value) {
        echo '<div class="image-view" style="background: url(/uploadfile/images/product/'.$value->url.') 50% 50% / contain no-repeat;"><div id="' . $value->id . '+" class="delete-image"></div></div>';
    }
    echo "<div class='clear'></div>";
}

echo $form->field($model, 'images')->fileInput([
    'id' => "input-images",
    "multiple" => 'multiple', 
    'name' => 'images[]', 
    'options' => [
        'type' => "file", 
        'class' => "file-loading", 
    ]]);

echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить');
ActiveForm::end();
unset($form);
?>

<script>
    <?php 
    
        $serch = $model->producer ? preg_match('/^(.*) '.$model->producer->name.' /i', $model->name, $sought) : null;
        $categoryName = $serch ? $sought[1] : '';
        $producerName = $model->producer ? $model->producer->name : null;
    ?>
    var categoryName = '<?= $categoryName ?>';
    var producerName = '<?= $producerName ?>';
    var modelName = '';
    
    
    $("#input-logo").fileinput({
        uploadUrl: "<?= Url::toRoute(['/admin/product/uploadlogo', 'id_cut'=>$model->id]); ?>",
        uploadAsync: false,
        maxFileCount: 10,
        allowedFileExtensions: ["jpg", "gif", "png"],
        overwriteInitial: false,
        showCaption: true,
        showRemove: true,
        showUpload: true,
        initialPreviewShowDelete: false,
        previewFileType: "Изображения",
        browseLabel: "Открыть",
        removeClass: "btn btn-danger",
        removeLabel: "Удалить",
        uploadClass: "btn btn-info",
        uploadLabel: "Загрузить",
        dropZoneEnabled: false,
    });

    $("#input-images").fileinput({
        uploadUrl: "<?= Url::toRoute(['/admin/product/upload', 'id_cut'=>$model->id]); ?>",
        uploadAsync: false,
        maxFileCount: 10,
        allowedFileExtensions: ["jpg", "gif", "png"],
        overwriteInitial: false,
        showCaption: true,
        showRemove: true,
        showUpload: true,
        initialPreviewShowDelete: false,
        previewFileType: "Изображения",
        browseLabel: "Открыть",
        removeClass: "btn btn-danger",
        removeLabel: "Удалить",
        uploadClass: "btn btn-info",
        uploadLabel: "Загрузить",
        dropZoneEnabled: false,
//    layoutTemplates: {footer: footerTemplate},
//    previewThumbTags: {
//        '{TAG_VALUE}': '',        // no value
//        '{TAG_CSS_NEW}': '',      // new thumbnail input
//        '{TAG_CSS_INIT}': 'hide'  // hide the initial input
//    }
    });
</script>
