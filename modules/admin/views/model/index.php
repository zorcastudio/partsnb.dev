<?php
use yii\bootstrap\Alert;
use kartik\grid\GridView;
use yii\helpers\Html;

use app\models\Producer;
use app\modules\admin\models\ModelSearch;

$this->title = "Модели";
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    'Модели',
];


if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};

echo GridView::widget([
    'layout'=>"{summary}\n{pager}\n{items}\n{pager}",

    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'category-form',
    'columns' => [
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
        ],
        [
            'attribute' => 'createtime',
            'value' => function ($model){return date('d.m.Y', $model->createtime);},
            'filter' => false
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function ($model){
                return Html::a($model->name, ['/admin/model/update','id'=>$model->id]);
            },
        ],
        [
            'attribute' => 'serie_id',
            'value' => function($model) {
                return $model->serie_id ? $model->serie->name : null;
            },
            'filter' => [0=> 'Не задано'] + app\models\Serie::getSerieAll($searchModel->producer_id)
            
        ],
        [
            'attribute' => 'producer_id',
            'value' => function($model) {
                return $model->producer->name;
            },
            'filter' => Producer::getProducerArray(),
        ],
//        [
//            'attribute' => '_task',
//            'value' => function($model) {
//                return $model->_task ? ModelSearch::getTask($model->_task) : null;
//            },
//            'filter' => ModelSearch::getTask()
//            
//        ],
        [
            'class'=>'kartik\grid\BooleanColumn',
            'attribute' => 'delete',
            'value' => function($model) {
                return !$model->delete;
            },
            'vAlign'=>'middle',
            'filter' => false,
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:60px;'],
            'template' => '{delete}',
        ]
    ],
]);
?>