<?php 
    $this->title = "Редактировать модель"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Модели', 'url' => ['/admin/model']],
        $this->title
    ];
    echo $this->render('_form', [
                            'model'=>$model, 
                            'producer'=>$producer,
                            'serie'=>$serie,
                        ]); 
?>