<?php 
    use yii\helpers\Url;
    use yii\helpers\Html;
?>

<table cellpadding="0" cellspacing="0" class="basket_btl">
    <tbody>
        <tr>
            <th class="td-pic">&nbsp;</th>
            <th class="td-name">Название</th>
            <th class="td-empty"></th>
            <th class="td-price">Цена товара</th>
            <th class="td-num">Количество</th>
            <th class="td-sum">Цена</th>
            <th class="td-del">&nbsp;</th>
        </tr>
            <?php foreach ($model as $item) : ?>
        
            <?php if($value = $item->product) :
                ?>
                <tr>
                    <td class="td-pic">
                        <div class="basket-logo" style="background: url(<?= Url::toRoute('/uploadfile/images/product/resized/' . $value->logo) ?>) 50% 50% / contain no-repeat;"></div>
                    </td>
                    <td class="td-name">
                        <?=  Html::a($value->name, $value->getUrl()); ?>
                    </td>
                    <td class="td-empty">&nbsp;</td>
                    <td class="td-price"><?= $value->price . " р." ?></td>
                    <td class="td-num">
                        <input  type="text" maxlength="3" name="<?= $item->order_id . '_' . $item->product_id ?>" value="<?= $item->amount ?>" onblur="setCount(this,'admin')">
                    </td>
                    <td class="td-sum"><?= $value->getPrice($item->amount) . " руб." ?></td>
                    <td class="td-del"><a href="#" id="<?= $item->order_id . '_' . $item->product_id ?>" onclick="deleteProduct(this,'admin')" class="delete-link">x</a></td>
                </tr>
                
            <?php endif; endforeach; ?>
    </tbody>
</table>