<?php 
    $this->title = "Редактировать заказ"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Заказы', 'url' => ['/admin/order']],
        $this->title,
    ];
    echo $this->render('_form', ['model'=>$model]); 
?>