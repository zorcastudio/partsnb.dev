<?php 
use yii\widgets\DetailView;
use app\models\Order;
use app\models\Payment;
use yii\widgets\Pjax;
use yii\helpers\Html;

    $this->title = 'Заказ №'.$model->id; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Заказы', 'url' => ['/admin/order']],
        $this->title,
    ];
    
Pjax::begin();
echo Html::a("Обновить", ['/admin/order/view?id='.$model->id], ['id'=>'update-block']); //'style' => 'display:none', 
echo DetailView::widget([
    'model' => $model,
    'id'        => 'bank-details',
    'attributes' => [
        [
            'attribute' => 'createtime',
            'value' => date('d.m.Y',$model->createtime),
        ],
        [
            'attribute' => 'status',
            'value' => Order::itemAlias('status', $model->status),
        ],
        'full_name',
        [
            'attribute' => 'delivery_id',
            'value' => $model->delivery ? $model->delivery->name." ".$model->delivery->price.' руб.' : null,
        ],
        [
            'attribute' => 'payment_id',
            'value' => Payment::getListPayment($model->payment_id),
        ],
        [
            'attribute' => 'price',
            'value' => $model->sumPrice,
        ],
        [
            'attribute' => 'country_id',
            'value' => $model->country ? $model->country->name : null,
        ],
        [
            'attribute' => 'region_id',
            'value' => $model->region ? $model->region->name : null,
        ],
        [
            'attribute' => 'city_id',
            'value' => $model->city ? $model->city->name : null,
        ],
        'zip_code'
    ]
]);
Pjax::end();

echo $this->render('_list_product_admin',array('model'=>$model->order_p));

?>