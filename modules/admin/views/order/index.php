<?php 
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\helpers\Html;

use app\models\Order;
use app\models\Country;

$this->title = "Заказы";
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    $this->title,
];

if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'category-form',
//        'template' => "{pager}{items}{pager}",
    'columns' => [
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
        ],
        [
            'attribute' => 'createtime',
            'value' => function($model) {
                return date("d.m.Y", $model->createtime);
            },
            'filter' => false,
        ],
        'full_name',
        [
            'attribute' => 'delivery_id',
            'value' => function($model) {
                return $model->delivery ? $model->delivery->name : null;
            },
            'filter' => Order::getListDelivery(),
        ],        
        [
            'attribute' => 'payment_id',
            'value' => function($model) {
                return Order::itemAlias("payment", $model->payment_id);
            },
            'filter' => Order::itemAlias("payment"),
        ], 
        [
            'attribute' => 'price',
            'value' => 'sumPrice',
        ],
                    
        [
            'attribute' => 'country_id',
            'value' => function($model) {
                return $model->country ? $model->country->name : null;
            },
            'filter' => Country::getCountrys(),
        ], 
//        [
//            'attribute' => 'region_id',
//            'value' => function($model) {
//                return $model->region ? $model->region->name : null;
//            },
//            'filter' => false,
//        ], 
//        [
//            'attribute' => 'city_name',
//            'value' => function($model) {
//                return $model->city ? $model->city->name : null;
//            },
//            'filter' => false,
//        ],
        'city_name',
        'zip_code',
        [ 
            'attribute' => 'status',
            'value' => function($model){
                    return Order::itemAlias('status', $model->status);
            },
            'filter' => Order::itemAlias('status'),
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:80px;'],
            'template' => '{view}{update}{delete}',
        ]
    ],
]);
?>