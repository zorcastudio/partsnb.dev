<?php 
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

use app\models\Order;
use app\models\Country;
use app\models\Registration;
use app\models\Region;
use app\models\City;
use app\models\Delivery;

 $form = ActiveForm::begin([
        'id' => 'user',
        "options"=>['class' => 'well'],
        'fieldConfig' => [
            'template' => "<div class=\"form-label\">{label}<span>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
        ]
    ]);
?>

<table style="width: 100%;">
    <tr>
        <td colspan="2"><?php echo $form->errorSummary($model);  ?></td>
    </tr>
    <tr>
        <td colspan="2">
            <?= $form->field($model, 'status')->dropDownList(Order::itemAlias('status'));?>
        </td>
    </tr>
    <tr>
        <td><?php echo $form->field($model, 'full_name')->textInput(); ?></td>
        <td><?php echo $form->field($model, 'phone')->textInput(); ?></td>
    </tr>
    <tr>
        <td><?= $form->field($model, 'country_id')->dropDownList(
                Country::getCountrys(),
                [   
                    'onchange' => '$.post( "'.Url::toRoute('/ajax/country_select').'", {country_id: $(this).val()}, setRegions)',
                    'options' => [Registration::$_country_id =>  ['selected' => true]]
                ]);
        ?>
        </td>
        <td><?= $form->field($model, 'region_id')
                    ->dropDownList(
                        ($model->country_id || Registration::$_country_id) ? Region::getRegion($model->country_id, Registration::$_country_id) : [],
                        [   
                            'onchange' => '$.post( "'.Url::toRoute('/ajax/region_select').'", {region_id: $(this).val()}, setCities)',
                            'options' => [Registration::$_country_id =>  ['selected' => true]],
                            'id'=>'region', 
                            'disabled' => false, 
//                            'prompt'=>'Выберите область',
                        ]
                    );
        ?></td>
    </tr>
    <tr>
        <td><?= $form->field($model,'city_id')
                    ->dropDownList(
                    ($model->region_id) ? City::getCitys($model->region_id) : [],
                    [
                        'id'=>'city', 
                        'disabled' => (!$model->region_id) ? true : false,  
//                        'prompt'=>'Выберите город',
                        'ajax' => [ 'type'=>'POST', 'url'=>Url::toRoute('/ajax/OnCitySelect'), //url to call
                            'data'=>['city_id'=>"js: $(this).val()"],
                            'success'=>'js: function(data) {
                                setCities(data)
                            }'
                        ],
                    ]
        );?></td>
        <td>
            <?= $form->field($model, 'payment_id')->dropDownList(app\models\Payment::getListPayment());?>
        </td>
    </tr>
    <tr>
        <td>            
            <?= $form->field($model, 'delivery_id')->dropDownList(Delivery::getListDelivery(),[ 'prompt'=>'Выберите доставку']);?>
        </td>
        <td><?php echo $form->field($model, 'zip_code')->textInput(); ?></td>
    </tr>
    <tr>
        <td colspan="2"><?= $form->field($model, 'address')->textarea();?></td>
    </tr>
</table>
<div class="basket_sum">
    <span>ИТОГО:</span>
    <span class="basket_itog_sum"><?= $model->price." р." ?></span>
</div>
<?php

echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); 
ActiveForm::end(); unset($form); 

echo $this->render('_list_product_admin',['model'=>$model->order_p]);
?>