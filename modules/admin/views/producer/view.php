<?php 
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Html;

    $this->title = $model->name; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Управление производителями', 'url' => ['/admin/producer']],
        $this->title,
    ];
    
    echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'name',
        'name_lat',
    ]
]);

$form = ActiveForm::begin([
    'id' => 'filter-value',
    'fieldConfig' => [
        'template' => "{input}{error}",
    ]
]);
 
if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};
?>

<!--
<table id="model-value">
    <tbody>
        <tr>
            <td><h4>Название модели</h4></td>
            <td></td>
        </tr>
        
        <?php foreach ($modeList as $value) : ?>
        <tr>
            <td>
                <?php echo $form->field($value, 'name')->textInput(['name'=>'Model[]['.$value->id.']']);?>
            <td class="model-btn">
                <div class="btn btn-default" id="<?= $value->id ?>" onclick="deleteModelValue(this)" type="submit" name="yt0">Удалить</div>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<div onclick="addModelValue();" class="btn btn-default" id="add" type="submit" name="yt0">Добавить</div>-->

<?php
//    echo Html::submitButton('Сохранить', ["id"=>'add']); 
//    ActiveForm::end(); unset($form); 
?>



