<?php 
    $this->title = "Создать производителя"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Производители', 'url' => ['/admin/producer']],
        $this->title
    ];
    echo $this->render('_form', ['model'=>$model]); 
?>