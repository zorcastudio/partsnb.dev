<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use mihaildev\ckeditor\CKEditor;


use app\models\Producer;

$form = ActiveForm::begin([
        'id' => 'product',
        "options"=>['class' => 'well'],
        'fieldConfig' => [
            'template' => "<div class=\"form-label\">{label}<span class='required'>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
        ]
    ]);

echo $form->errorSummary($model);
echo $form->field($model, 'name')->textInput();
echo $form->field($model, 'name_lat')->textInput();
echo $form->field($model, 'description')->widget(CKEditor::className(), [
    'editorOptions' => [
        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
        'inline' => false, //по умолчанию false
    ],
]);

echo $form->field($model, 'description_product')->widget(CKEditor::className(), [
    'editorOptions' => [
        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
        'inline' => false, //по умолчанию false
    ],
]);
echo "<br/>";
echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); 
ActiveForm::end(); unset($form); 
?>
