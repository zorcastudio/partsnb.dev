<?php
use yii\bootstrap\Alert;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = "Производители";
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    $this->title,
];


if (Yii::$app->session->hasFlash('info')) {
    echo Alert::widget([
        'options' => ['class' => 'alert-info'],
        'body' => Yii::$app->session->getFlash('info'),
    ]);
};

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'category-form',
    'columns' => [
        [
            'attribute' => 'id',
            'value' => 'id',
            'options' => ['style' => 'width: 60px'],
        ],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function ($model){
                return Html::a($model->name, ['/admin/producer/update','id'=>$model->id]);
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:73px;'],
            'template' => '{view}{delete}',
            'buttons' => [
//                    'view' => function ($url, $model) {
//                        return Html::a(null, $url, [
//                            'class'=>'glyphicon glyphicon-plus-sign',                                  
//                        ]);
//                    }
                ],
        ]
    ],
]);

?>