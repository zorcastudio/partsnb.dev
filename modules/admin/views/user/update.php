<?php 
    $this->title = "Редактировать пользователя"; 
    $this->params['breadcrumbs'] = [
        ['label' => 'Панель администратора', 'url' => ['/admin']],
        ['label' => 'Управление пользователями', 'url' => ['/admin/user']],
        'Редактировать пользователя'
    ];
    echo $this->render('_form', ['model'=>$model]); 
?>