<?php 
    use yii\bootstrap\ActiveForm;
    use yii\helpers\Html;
    
    use app\models\User;
    use app\models\Country;
    use app\models\Registration;
    use app\models\Region;
    use app\models\City;
    use yii\helpers\Url;
?> 


<?php

$form = ActiveForm::begin([
        'id' => 'user',
        "options"=>['class' => 'well'],
        'fieldConfig' => [
            'template' => "<div class=\"form-label\">{label}<span class='required'>*</span></div>\n<div class=\"form-input\">{input}</div>\n{error}",
        ]
    ]);

    echo $form->errorSummary($model);
    
    echo $form->field($model,'status')->dropDownList(User::itemAlias('status')); 
    echo $form->field($model,'price_type')->dropDownList(User::itemAlias('priceType')); 
    
    echo $form->field($model, 'username')->textInput();
    echo $form->field($model, 'password')->passwordInput();
    echo $form->field($model, 'email')->textInput();
    echo $form->field($model, 'name')->textInput();
    echo $form->field($model, 'surname')->textInput();
    echo $form->field($model, 'middle_name')->textInput();
    echo $form->field($model, 'phone')->textInput(['maxlength'=>13]);
    
    echo $form->field($model, 'country_id')->dropDownList(
                Country::getCountrys(),
                [   
                    'onchange' => '$.post( "'.Url::toRoute('/ajax/country_select').'", {country_id: $(this).val()}, setRegions)',
                    'options' => [Registration::$_country_id =>  ['selected' => true]]
                ]);
    
    echo $form->field($model, 'region_id')
                    ->dropDownList(
                        ($model->country_id || Registration::$_country_id) ? Region::getRegion($model->country_id, Registration::$_country_id) : [],
                        [   
                            'onchange' => '$.post( "'.Url::toRoute('/ajax/region_select').'", {region_id: $(this).val()}, setCities)',
                            'options' => [Registration::$_country_id =>  ['selected' => true]],
                            'id'=>'region', 
                            'disabled' => false, 
                            'prompt'=>'Выберите область',
                        ]
                    ); 
    echo $form->field($model,'city_id')
                    ->dropDownList(
                    ($model->region_id) ? City::getCitys($model->region_id) : [],
                    [
                        'id'=>'city', 
                        'disabled' => (!$model->region_id) ? true : false,  
                        'prompt'=>'Выберите город',
                        'ajax' => [ 'type'=>'POST', 'url'=>Url::toRoute('/ajax/OnCitySelect'), //url to call
                            'data'=>['city_id'=>"js: $(this).val()"],
                            'success'=>'js: function(data) {
                                setCities(data)
                            }'
                        ],
                    ]
        );
    
    echo $form->field($model, 'address')->textarea();
    echo $form->field($model, 'zip_code')->textInput();
    echo "<br/>";
    
    echo Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); 
    ActiveForm::end(); unset($form); 
?>

