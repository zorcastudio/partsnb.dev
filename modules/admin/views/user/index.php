<?php 
use yii\grid\GridView;
use yii\bootstrap\Alert;
use app\models\Country;
use app\models\User;

$this->title = "Пользователи"; 
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    'Пользователи'
];

if(Yii::$app->session->hasFlash('info')){
    echo Alert::widget([
            'options' => ['class' => 'alert-info'],
            'body' => Yii::$app->session->getFlash('info'),
        ]);
};

echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'id' => 'category-form',
//        'template' => "{pager}{items}{pager}",
        'columns' => [
            [
                'attribute' => 'id',
                'value' => 'id', 
                'options' => ['style' =>'width: 70px'],
            ],
            'username',
            'surname',
            'name',
            'middle_name',
            'phone',
            'email',
            [ 
                'attribute' => 'country_id',
                'value' => function($model){
                    return $model->country ? $model->country->name : null;
                },
                'filter' => Country::getCountrys(),
            ],
            [ 
                'attribute' => 'region_id',
                'value' =>function($model){
                    return $model->region ? $model->region->name : null;
                },
                'filter' => false,
            ],
            [ 
                'attribute' => 'city_id',
                'value' =>function($model){
                    return $model->city ? $model->city->name : NULL;
                },
                'filter' => false,
            ],
           'address',
            [ 
                'attribute' => 'status',
                'value' => function($model){
                        return User::itemAlias("status", $model->status);
                },
                'filter' => User::itemAlias('status'),
            ],
            
           [
                'class'=>'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:80px;'],
                'template' => '{view}{update}{delete}',
            ]
        ],
    ]);
 ?>