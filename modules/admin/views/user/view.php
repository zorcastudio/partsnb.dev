<?php
use yii\widgets\DetailView;
use app\models\User;

$this->title = $model->name;
$this->params['breadcrumbs'] = [
    ['label' => 'Панель администратора', 'url' => ['/admin']],
    ['label' => 'Управление пользователями', 'url' => ['/admin/user']],
    $model->name,
];

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'username',
        'name',
        'surname',
        'middle_name',
        'phone',
        'email',
        [
            'attribute' => 'country_id',
            'value' => $model->country ? $model->country->name : null,
            
        ],
        [
            'attribute' => 'region_id',
            'value' => $model->region ? $model->region->name : null,
        ],
        [
            'attribute' => 'city_id',
            'value' => $model->city ? $model->city->name : null,
        ],
        'address',
        'zip_code',
        [
            'attribute' => 'status',
            'value' => User::itemAlias("status", $model->status),
        ],
        [
            'attribute' => 'createtime',
            'value' =>date(Yii::$app->params['dateFormat'], $model->createtime),
        ],
        [
            'attribute' => 'lastvisit',
            'value' =>$model->lastvisit ? date(Yii::$app->params['dateFormat'], $model->lastvisit) : null,
        ],
    ]
]);
?>



